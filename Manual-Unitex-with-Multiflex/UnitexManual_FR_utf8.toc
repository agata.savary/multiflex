\select@language {french}
\contentsline {chapter}{Introduction}{11}{chapter*.2}
\contentsline {section}{Quoi de neuf depuis la version 2.0 ?}{12}{section*.3}
\contentsline {section}{Content}{14}{section*.4}
\contentsline {section}{Unitex contributeurs}{15}{section*.5}
\contentsline {section}{Si vous utilisez Unitex dans des projets de recherche...}{16}{section*.6}
\contentsline {chapter}{\numberline {1}Installation d'Unitex}{17}{chapter.1}
\contentsline {section}{\numberline {1.1}Licences}{17}{section.1.1}
\contentsline {section}{\numberline {1.2}Environnement d\IeC {\textquoteright }ex\IeC {\'e}cution Java}{17}{section.1.2}
\contentsline {section}{\numberline {1.3}Installation sous Windows}{18}{section.1.3}
\contentsline {section}{\numberline {1.4}Installation sous Linux}{18}{section.1.4}
\contentsline {section}{\numberline {1.5}Installation sous MacOS X}{19}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Utiliser l'Apple Java 1.6 runtime}{19}{subsection.1.5.1}
\contentsline {subsubsection}{Option 1 : modifier le runtime par d\IeC {\'e}faut pour Java Applications}{19}{section*.7}
\contentsline {subsubsection}{Option 2 : Cr\IeC {\'e}er un alias pour lancer Java 1.6}{19}{section*.8}
\contentsline {subsection}{\numberline {1.5.2}SoyLatte}{20}{subsection.1.5.2}
\contentsline {subsubsection}{Avant de commencer}{20}{section*.9}
\contentsline {subsubsection}{Installation}{22}{section*.10}
\contentsline {subsubsection}{Installation X11.app}{22}{section*.11}
\contentsline {subsubsection}{T\IeC {\'e}l\IeC {\'e}chargez et installez Unitex, comme d'habitude}{22}{section*.12}
\contentsline {subsubsection}{T\IeC {\'e}l\IeC {\'e}chargez SoyLatte (le portage Java 1.6)}{22}{section*.13}
\contentsline {subsubsection}{Installer SoyLatte}{22}{section*.14}
\contentsline {subsubsection}{Configurez votre syst\IeC {\`e}me}{23}{section*.15}
\contentsline {subsection}{\numberline {1.5.3}Comment compiler les programmes les C++ Unitex sur un ordinateur Macintosh}{24}{subsection.1.5.3}
\contentsline {subsection}{\numberline {1.5.4}Comment rendre tous les fichiers visibles sur Mac OS}{25}{subsection.1.5.4}
\contentsline {section}{\numberline {1.6}Premi\IeC {\`e}re utilisation}{27}{section.1.6}
\contentsline {section}{\numberline {1.7}Ajout de nouvelles langues}{27}{section.1.7}
\contentsline {section}{\numberline {1.8}D\IeC {\'e}sinstallation}{28}{section.1.8}
\contentsline {section}{\numberline {1.9}Unitex pour les d\IeC {\'e}veloppeurs}{28}{section.1.9}
\contentsline {chapter}{\numberline {2}Chargement d\IeC {\textquoteright }un texte}{31}{chapter.2}
\contentsline {section}{\numberline {2.1}S\IeC {\'e}lection de la langue}{31}{section.2.1}
\contentsline {section}{\numberline {2.2}Format des textes}{32}{section.2.2}
\contentsline {section}{\numberline {2.3}Edition de textes}{34}{section.2.3}
\contentsline {section}{\numberline {2.4}Ouverture d\IeC {\textquoteright }un texte}{35}{section.2.4}
\contentsline {section}{\numberline {2.5}Pr\IeC {\'e}traitement du texte}{36}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Normalisation des s\IeC {\'e}parateurs}{37}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}D\IeC {\'e}coupage en phrases}{38}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}Normalisation de formes non ambigu\IeC {\"e}s}{39}{subsection.2.5.3}
\contentsline {subsection}{\numberline {2.5.4}D\IeC {\'e}coupage du texte en unit\IeC {\'e}s lexicales}{40}{subsection.2.5.4}
\contentsline {subsection}{\numberline {2.5.5}Application de dictionnaires}{43}{subsection.2.5.5}
\contentsline {subsection}{\numberline {2.5.6}Analyse des mots compos\IeC {\'e}s libres en n\IeC {\'e}erlandais, allemand, nor-v\IeC {\'e}gien et russe}{45}{subsection.2.5.6}
\contentsline {section}{\numberline {2.6}Ouverture d\IeC {\textquoteright }un texte taggu\IeC {\'e}}{46}{section.2.6}
\contentsline {chapter}{\numberline {3}Dictionnaires}{47}{chapter.3}
\contentsline {section}{\numberline {3.1}Les dictionnaires DELA}{47}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Format des DELAF}{47}{subsection.3.1.1}
\contentsline {subsubsection}{Syntaxe d\IeC {\textquoteright }une entr\IeC {\'e}e}{47}{section*.16}
\contentsline {subsubsection}{Mots compos\IeC {\'e}s avec espace ou tiret}{49}{section*.17}
\contentsline {subsubsection}{Factorisation d\IeC {\textquoteright }entr\IeC {\'e}es}{50}{section*.18}
\contentsline {subsection}{\numberline {3.1.2}Format des DELAS}{50}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Contenu des dictionnaires}{51}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Recherche d'un mot dans un dictionnaire}{53}{section.3.2}
\contentsline {section}{\numberline {3.3}V\IeC {\'e}rification du format d\IeC {\textquoteright }un dictionnaire}{54}{section.3.3}
\contentsline {section}{\numberline {3.4}Tri}{55}{section.3.4}
\contentsline {section}{\numberline {3.5}Flexion automatique}{57}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Flexion des mots simples}{57}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Op\IeC {\'e}rateurs de flexion avanc\IeC {\'e}s}{61}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Flexion des mots compos\IeC {\'e}s}{65}{subsection.3.5.3}
\contentsline {subsection}{\numberline {3.5.4}Flexion des langues s\IeC {\'e}mitiques}{65}{subsection.3.5.4}
\contentsline {section}{\numberline {3.6}Compression}{66}{section.3.6}
\contentsline {section}{\numberline {3.7}Application de dictionnaires}{67}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Priorit\IeC {\'e}s}{68}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}R\IeC {\`e}gles d\IeC {\textquoteright }application des dictionnaires}{68}{subsection.3.7.2}
\contentsline {subsection}{\numberline {3.7.3}Graphes dictionnaires}{69}{subsection.3.7.3}
\contentsline {subsubsection}{Exporter les entr\IeC {\'e}es produites comme dictionnaire morphologique}{69}{section*.19}
\contentsline {subsubsection}{Conventions de nommage}{69}{section*.20}
\contentsline {subsection}{\numberline {3.7.4}Graphe dictionnaire morphologique}{71}{subsection.3.7.4}
\contentsline {section}{\numberline {3.8}Bibliographie}{71}{section.3.8}
\contentsline {chapter}{\numberline {4}Recherche d\IeC {\textquoteright }expressions rationnelles}{75}{chapter.4}
\contentsline {section}{\numberline {4.1}D\IeC {\'e}finition}{75}{section.4.1}
\contentsline {section}{\numberline {4.2}Unit\IeC {\'e}s lexicales}{75}{section.4.2}
\contentsline {section}{\numberline {4.3}Motifs}{76}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Symboles sp\IeC {\'e}ciaux}{76}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Masques lexicaux}{77}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Contraintes grammaticales et s\IeC {\'e}mantiques}{77}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Contraintes flexionnelles}{78}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}N\IeC {\'e}gation d\IeC {\textquoteright }un motif}{79}{subsection.4.3.5}
\contentsline {section}{\numberline {4.4}Concat\IeC {\'e}nation}{80}{section.4.4}
\contentsline {section}{\numberline {4.5}Union}{81}{section.4.5}
\contentsline {section}{\numberline {4.6}Kleene star}{81}{section.4.6}
\contentsline {section}{\numberline {4.7}Filtres morphologiques}{82}{section.4.7}
\contentsline {section}{\numberline {4.8}Recherche}{83}{section.4.8}
\contentsline {subsection}{\numberline {4.8.1}Configuration de la recherche}{83}{subsection.4.8.1}
\contentsline {subsection}{\numberline {4.8.2}Affichage des r\IeC {\'e}sultats}{85}{subsection.4.8.2}
\contentsline {subsection}{\numberline {4.8.3}Statistiques}{89}{subsection.4.8.3}
\contentsline {chapter}{\numberline {5}Grammaires locales}{93}{chapter.5}
\contentsline {section}{\numberline {5.1}Formalisme des grammaires locales}{93}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Grammaires alg\IeC {\'e}briques}{93}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Grammaires alg\IeC {\'e}briques \IeC {\'e}tendues}{94}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}\IeC {\'E}dition de graphes}{94}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Cr\IeC {\'e}ation d'un graphe}{94}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Sous-graphes}{99}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Manipulation des bo\IeC {\^\i }tes}{103}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Sortie}{103}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Utilisation des variables}{104}{subsection.5.2.5}
\contentsline {subsection}{\numberline {5.2.6}Copie de listes}{106}{subsection.5.2.6}
\contentsline {subsection}{\numberline {5.2.7}Symboles sp\IeC {\'e}ciaux}{107}{subsection.5.2.7}
\contentsline {subsection}{\numberline {5.2.8}Commandes de la barre d\IeC {\textquoteright }ic\IeC {\^o}nes}{108}{subsection.5.2.8}
\contentsline {section}{\numberline {5.3}Options de pr\IeC {\'e}sentation}{110}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Tri des lignes d\IeC {\textquoteright }une bo\IeC {\^\i }te}{110}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Zoom}{110}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Antialiasing}{110}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Alignement des bo\IeC {\^\i }tes}{110}{subsection.5.3.4}
\contentsline {subsection}{\numberline {5.3.5}Pr\IeC {\'e}sentation, polices et couleurs}{113}{subsection.5.3.5}
\contentsline {section}{\numberline {5.4}Les graphes en dehors d\IeC {\textquoteright }Unitex}{114}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Inclusion d\IeC {\textquoteright }un graphe dans un document}{114}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Impression d\IeC {\textquoteright }un graphe}{116}{subsection.5.4.2}
\contentsline {chapter}{\numberline {6}Utilisation avanc\IeC {\'e}e des graphes}{117}{chapter.6}
\contentsline {section}{\numberline {6.1}Les types de graphes}{117}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Graphes de flexion}{117}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Graphes de pr\IeC {\'e}traitement}{118}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Graphes de normalisation de l\IeC {\textquoteright }automate du texte}{119}{subsection.6.1.3}
\contentsline {subsection}{\numberline {6.1.4}Graphes syntaxiques}{120}{subsection.6.1.4}
\contentsline {subsection}{\numberline {6.1.5}Grammaires ELAG}{121}{subsection.6.1.5}
\contentsline {subsection}{\numberline {6.1.6}Graphes param\IeC {\'e}tr\IeC {\'e}s}{121}{subsection.6.1.6}
\contentsline {section}{\numberline {6.2}Compilation d'une grammaire}{121}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Compilation d'un graphe}{121}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Approximation par un transducteur \IeC {\`a} \IeC {\'e}tats finis}{122}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Contraintes sur les grammaires}{123}{subsection.6.2.3}
\contentsline {subsection}{\numberline {6.2.4}D\IeC {\'e}tection d\IeC {\textquoteright }erreurs}{126}{subsection.6.2.4}
\contentsline {section}{\numberline {6.3}Contextes}{126}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Contextes droits}{127}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Contextes gauches}{129}{subsection.6.3.2}
\contentsline {section}{\numberline {6.4}Le mode morphologique}{133}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Pourquoi ?}{133}{subsection.6.4.1}
\contentsline {subsection}{\numberline {6.4.2}Les r\IeC {\`e}gles}{133}{subsection.6.4.2}
\contentsline {subsection}{\numberline {6.4.3}Dictionnaires morphologiques}{134}{subsection.6.4.3}
\contentsline {subsection}{\numberline {6.4.4}Variables morphologiques}{135}{subsection.6.4.4}
\contentsline {section}{\numberline {6.5}Exploration des chemins d\IeC {\textquoteright }une grammaire}{136}{section.6.5}
\contentsline {section}{\numberline {6.6}Collection de graphes}{138}{section.6.6}
\contentsline {section}{\numberline {6.7}R\IeC {\`e}gles d\IeC {\textquoteright }application des transducteurs}{139}{section.6.7}
\contentsline {subsection}{\numberline {6.7.1}Insertion \IeC {\`a} gauche du motif reconnu}{140}{subsection.6.7.1}
\contentsline {subsection}{\numberline {6.7.2}Application en avan\IeC {\c c}ant}{140}{subsection.6.7.2}
\contentsline {subsection}{\numberline {6.7.3}Priorit\IeC {\'e} \IeC {\`a} gauche}{141}{subsection.6.7.3}
\contentsline {subsection}{\numberline {6.7.4}Priorit\IeC {\'e} aux s\IeC {\'e}quences les plus longues}{141}{subsection.6.7.4}
\contentsline {subsection}{\numberline {6.7.5}Sorties \IeC {\`a} variables}{141}{subsection.6.7.5}
\contentsline {section}{\numberline {6.8}Variables de sorties}{144}{section.6.8}
\contentsline {section}{\numberline {6.9}Op\IeC {\'e}rations sur les variables}{145}{section.6.9}
\contentsline {subsection}{\numberline {6.9.1}Tests sur les variables}{145}{subsection.6.9.1}
\contentsline {subsection}{\numberline {6.9.2}Comparaison de variables}{146}{subsection.6.9.2}
\contentsline {section}{\numberline {6.10}Application des graphes aux textes}{147}{section.6.10}
\contentsline {subsection}{\numberline {6.10.1}Configuration de la recherche}{147}{subsection.6.10.1}
\contentsline {subsection}{\numberline {6.10.2}Options de recherche avanc\IeC {\'e}es}{148}{subsection.6.10.2}
\contentsline {subsection}{\numberline {6.10.3}Concordance}{150}{subsection.6.10.3}
\contentsline {subsection}{\numberline {6.10.4}Modification du texte}{152}{subsection.6.10.4}
\contentsline {subsection}{\numberline {6.10.5}Extraction des occurrences}{153}{subsection.6.10.5}
\contentsline {subsection}{\numberline {6.10.6}Comparaison de concordances}{153}{subsection.6.10.6}
\contentsline {subsection}{\numberline {6.10.7}Mode Debug}{154}{subsection.6.10.7}
\contentsline {chapter}{\numberline {7}Automate du texte}{157}{chapter.7}
\contentsline {section}{\numberline {7.1}Pr\IeC {\'e}sentation}{157}{section.7.1}
\contentsline {section}{\numberline {7.2}Construction}{159}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}R\IeC {\`e}gles de construction de l\IeC {\textquoteright }automate du texte}{159}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Normalisation de formes ambigu\IeC {\"e}s}{160}{subsection.7.2.2}
\contentsline {subsection}{\numberline {7.2.3}Normalisation des pronoms clitiques en portugais}{161}{subsection.7.2.3}
\contentsline {subsection}{\numberline {7.2.4}Conservation des meilleurs chemins}{163}{subsection.7.2.4}
\contentsline {section}{\numberline {7.3}Lev\IeC {\'e}e d\IeC {\textquoteright }ambigu\IeC {\"\i }t\IeC {\'e}s lexicales avec ELAG}{165}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}Grammaires de lev\IeC {\'e}e d\IeC {\textquoteright }ambigu\IeC {\"\i }t\IeC {\'e}s}{165}{subsection.7.3.1}
\contentsline {subsubsection}{Point de synchronisation}{166}{section*.21}
\contentsline {subsection}{\numberline {7.3.2}Compilation des grammaires ELAG}{167}{subsection.7.3.2}
\contentsline {subsection}{\numberline {7.3.3}Lev\IeC {\'e}e d\IeC {\textquoteright }ambigu\IeC {\"\i }t\IeC {\'e}s}{169}{subsection.7.3.3}
\contentsline {subsection}{\numberline {7.3.4}Ensembles de grammaires}{170}{subsection.7.3.4}
\contentsline {subsection}{\numberline {7.3.5}Fen\IeC {\^e}tre de processing d\IeC {\textquoteright }ELAG}{171}{subsection.7.3.5}
\contentsline {subsubsection}{Evaluation du taux d\IeC {\textquoteright }ambigu\IeC {\"\i }t\IeC {\'e}}{172}{section*.22}
\contentsline {subsection}{\numberline {7.3.6}Description du jeu d\IeC {\textquoteright }\IeC {\'e}tiquettes}{172}{subsection.7.3.6}
\contentsline {subsubsection}{\texttt {tagset.def} file}{172}{section*.23}
\contentsline {subsubsection}{Description des codes flexionnels}{175}{section*.24}
\contentsline {subsubsection}{Codes optionnels}{176}{section*.25}
\contentsline {subsection}{\numberline {7.3.7}Optimiser les grammaires}{178}{subsection.7.3.7}
\contentsline {subsubsection}{Limiter le nombre de branches \textit {alors}}{178}{section*.26}
\contentsline {subsubsection}{Utilisation des symboles lexicaux}{178}{section*.27}
\contentsline {section}{\numberline {7.4}Lin\IeC {\'e}arisation de l'automate du texte avec le taggeur}{179}{section.7.4}
\contentsline {subsection}{\numberline {7.4.1}Compatibilit\IeC {\'e} du jeu d'\IeC {\'e}tiquettes}{181}{subsection.7.4.1}
\contentsline {subsection}{\numberline {7.4.2}Utilisation du Tagger}{181}{subsection.7.4.2}
\contentsline {subsection}{\numberline {7.4.3}Cr\IeC {\'e}ation d'un nouveau taggeur}{181}{subsection.7.4.3}
\contentsline {section}{\numberline {7.5}Manipulation de l\IeC {\textquoteright }automate du texte}{182}{section.7.5}
\contentsline {subsection}{\numberline {7.5.1}Affichage des automates de phrases}{182}{subsection.7.5.1}
\contentsline {subsection}{\numberline {7.5.2}Modifier manuellement l\IeC {\textquoteright }automate du texte}{183}{subsection.7.5.2}
\contentsline {subsubsection}{Lev\IeC {\'e}e manuelle des ambigu\IeC {\"\i }t\IeC {\'e}s}{184}{section*.28}
\contentsline {subsection}{\numberline {7.5.3}Param\IeC {\`e}tres de pr\IeC {\'e}sentation}{184}{subsection.7.5.3}
\contentsline {section}{\numberline {7.6}Convertir l\IeC {\textquoteright }automate du texte en texte lin\IeC {\'e}aire}{186}{section.7.6}
\contentsline {section}{\numberline {7.7}Recherche de motifs dans l'automate du texte}{186}{section.7.7}
\contentsline {section}{\numberline {7.8}Affichage de la Table}{188}{section.7.8}
\contentsline {section}{\numberline {7.9}Le cas particulier du cor\IeC {\'e}en}{190}{section.7.9}
\contentsline {chapter}{\numberline {8}Automate de S\IeC {\'e}quences}{193}{chapter.8}
\contentsline {section}{\numberline {8.1}Corpus de s\IeC {\'e}quences}{193}{section.8.1}
\contentsline {section}{\numberline {8.2}Utilisation}{194}{section.8.2}
\contentsline {section}{\numberline {8.3}Recherche par approximation}{196}{section.8.3}
\contentsline {chapter}{\numberline {9}Lexique-grammaire}{199}{chapter.9}
\contentsline {section}{\numberline {9.1}Les tables de lexique-grammaire}{199}{section.9.1}
\contentsline {section}{\numberline {9.2}Conversion d\IeC {\textquoteright }une table en graphes}{200}{section.9.2}
\contentsline {subsection}{\numberline {9.2.1}Principe des graphes param\IeC {\'e}tr\IeC {\'e}s}{200}{subsection.9.2.1}
\contentsline {subsection}{\numberline {9.2.2}Format de la table}{200}{subsection.9.2.2}
\contentsline {subsection}{\numberline {9.2.3}Les graphes param\IeC {\'e}tr\IeC {\'e}s}{201}{subsection.9.2.3}
\contentsline {subsection}{\numberline {9.2.4}G\IeC {\'e}n\IeC {\'e}ration automatique de graphes}{203}{subsection.9.2.4}
\contentsline {chapter}{\numberline {10}Alignement de texte}{207}{chapter.10}
\contentsline {section}{\numberline {10.1}Chargement de textes}{207}{section.10.1}
\contentsline {section}{\numberline {10.2}Aligner des textes}{209}{section.10.2}
\contentsline {section}{\numberline {10.3}Recherche de motifs}{211}{section.10.3}
\contentsline {chapter}{\numberline {11}Flexion des mots compos\IeC {\'e}s}{215}{chapter.11}
\contentsline {section}{\numberline {11.1}Mots compos\IeC {\'e}s}{215}{section.11.1}
\contentsline {subsection}{\numberline {11.1.1}Description formelle du comportement flexionnel des mots compos\IeC {\'e}s}{216}{subsection.11.1.1}
\contentsline {subsection}{\numberline {11.1.2}Approche lexicale ou grammaticale de la description morphologique}{217}{subsection.11.1.2}
\contentsline {section}{\numberline {11.2}Formalisme de flexion des mots compos\IeC {\'e}s}{218}{section.11.2}
\contentsline {subsection}{\numberline {11.2.1}Caract\IeC {\'e}ristiques morphologiques de la langue}{218}{subsection.11.2.1}
\contentsline {subsection}{\numberline {11.2.2}D\IeC {\'e}composition d'un mot compos\IeC {\'e} en constituants}{220}{subsection.11.2.2}
\contentsline {subsection}{\numberline {11.2.3}Paradigme de flexion des mots compos\IeC {\'e}s}{221}{subsection.11.2.3}
\contentsline {subsubsection}{Variables d'unifications}{223}{section*.29}
\contentsline {subsubsection}{Variantes orthographiques et autres variantes}{225}{section*.30}
\contentsline {subsubsection}{Interface avec le syst\IeC {\`e}me de flexion des mots simples}{226}{section*.31}
\contentsline {section}{\numberline {11.3}Int\IeC {\'e}gration \IeC {\`a} Unitex}{227}{section.11.3}
\contentsline {subsection}{\numberline {11.3.1}Exemple complet en anglais}{227}{subsection.11.3.1}
\contentsline {subsection}{\numberline {11.3.2}Exemple complet en fran\IeC {\c c}ais}{231}{subsection.11.3.2}
\contentsline {subsection}{\numberline {11.3.3}Exemple en serbe}{234}{subsection.11.3.3}
\contentsline {chapter}{\numberline {12}Cascade de Transducteurs}{245}{chapter.12}
\contentsline {section}{\numberline {12.1}Appliquer une cascade de Transducteurs avec CasSys}{246}{section.12.1}
\contentsline {subsection}{\numberline {12.1.1}Cr\IeC {\'e}ation de la liste des transducteurs}{246}{subsection.12.1.1}
\contentsline {subsection}{\numberline {12.1.2}Edition de la liste des transducteurs}{247}{subsection.12.1.2}
\contentsline {subsection}{\numberline {12.1.3}Application d'une cascade}{249}{subsection.12.1.3}
\contentsline {subsection}{\numberline {12.1.4}Partage d'un fichier liste de transducteurs en cascade}{250}{subsection.12.1.4}
\contentsline {section}{\numberline {12.2}CasSys en d\IeC {\'e}tail}{250}{section.12.2}
\contentsline {subsection}{\numberline {12.2.1}Type de graphe utilis\IeC {\'e}}{250}{subsection.12.2.1}
\contentsline {subsection}{\numberline {12.2.2}Application it\IeC {\'e}rative}{250}{subsection.12.2.2}
\contentsline {subsection}{\numberline {12.2.3}R\IeC {\`e}gles utilis\IeC {\'e}es dans une cascade}{251}{subsection.12.2.3}
\contentsline {subsection}{\numberline {12.2.4}Marquage de motifs dans CasSys}{251}{subsection.12.2.4}
\contentsline {section}{\numberline {12.3}Les r\IeC {\'e}sultats d'une cascade}{253}{section.12.3}
\contentsline {subsection}{\numberline {12.3.1}Affichage des r\IeC {\'e}sultats de la cascade}{253}{subsection.12.3.1}
\contentsline {subsection}{\numberline {12.3.2}Les diff\IeC {\'e}rents fichiers r\IeC {\'e}sultats d'une cascade}{254}{subsection.12.3.2}
\contentsline {subsection}{\numberline {12.3.3}Un texte au format de type XML pour les \IeC {\'e}tiquettes lexicales}{254}{subsection.12.3.3}
\contentsline {chapter}{\numberline {13}Utilisation des programmes externes}{257}{chapter.13}
\contentsline {section}{\numberline {13.1}Cr\IeC {\'e}ation de fichiers log}{258}{section.13.1}
\contentsline {section}{\numberline {13.2}La console}{259}{section.13.2}
\contentsline {section}{\numberline {13.3}Unitex JNI}{259}{section.13.3}
\contentsline {section}{\numberline {13.4}Param\IeC {\`e}tres de codage des fichiers textes}{260}{section.13.4}
\contentsline {section}{\numberline {13.5}BuildKrMwuDic}{260}{section.13.5}
\contentsline {section}{\numberline {13.6}Cassys}{261}{section.13.6}
\contentsline {section}{\numberline {13.7}CheckDic}{262}{section.13.7}
\contentsline {section}{\numberline {13.8}Compress}{263}{section.13.8}
\contentsline {section}{\numberline {13.9}Concord}{263}{section.13.9}
\contentsline {section}{\numberline {13.10}ConcorDiff}{267}{section.13.10}
\contentsline {section}{\numberline {13.11}Convert}{267}{section.13.11}
\contentsline {section}{\numberline {13.12}Dico}{269}{section.13.12}
\contentsline {section}{\numberline {13.13}Elag}{270}{section.13.13}
\contentsline {section}{\numberline {13.14}ElagComp}{271}{section.13.14}
\contentsline {section}{\numberline {13.15}Evamb}{271}{section.13.15}
\contentsline {section}{\numberline {13.16}Extract}{272}{section.13.16}
\contentsline {section}{\numberline {13.17}Flatten}{272}{section.13.17}
\contentsline {section}{\numberline {13.18}Fst2Check}{273}{section.13.18}
\contentsline {section}{\numberline {13.19}Fst2List}{273}{section.13.19}
\contentsline {section}{\numberline {13.20}Fst2Txt}{275}{section.13.20}
\contentsline {section}{\numberline {13.21}Grf2Fst2}{275}{section.13.21}
\contentsline {section}{\numberline {13.22}GrfDiff}{276}{section.13.22}
\contentsline {section}{\numberline {13.23}GrfDiff3}{277}{section.13.23}
\contentsline {section}{\numberline {13.24}ImplodeTfst}{278}{section.13.24}
\contentsline {section}{\numberline {13.25}Locate}{278}{section.13.25}
\contentsline {section}{\numberline {13.26}LocateTfst}{280}{section.13.26}
\contentsline {section}{\numberline {13.27}MultiFlex}{282}{section.13.27}
\contentsline {section}{\numberline {13.28}Normalize}{283}{section.13.28}
\contentsline {section}{\numberline {13.29}PolyLex}{284}{section.13.29}
\contentsline {section}{\numberline {13.30}RebuildTfst}{284}{section.13.30}
\contentsline {section}{\numberline {13.31}Reconstrucao}{285}{section.13.31}
\contentsline {section}{\numberline {13.32}Reg2Grf}{285}{section.13.32}
\contentsline {section}{\numberline {13.33}Seq2Grf}{285}{section.13.33}
\contentsline {section}{\numberline {13.34}SortTxt}{286}{section.13.34}
\contentsline {section}{\numberline {13.35}Stats}{287}{section.13.35}
\contentsline {section}{\numberline {13.36}Table2Grf}{287}{section.13.36}
\contentsline {section}{\numberline {13.37}Tagger}{288}{section.13.37}
\contentsline {section}{\numberline {13.38}TagsetNormTfst}{288}{section.13.38}
\contentsline {section}{\numberline {13.39}TEI2Txt}{289}{section.13.39}
\contentsline {section}{\numberline {13.40}Tfst2Grf}{289}{section.13.40}
\contentsline {section}{\numberline {13.41}Tfst2Unambig}{290}{section.13.41}
\contentsline {section}{\numberline {13.42}Tokenize}{290}{section.13.42}
\contentsline {section}{\numberline {13.43}TrainingTagger}{291}{section.13.43}
\contentsline {section}{\numberline {13.44}Txt2Tfst}{292}{section.13.44}
\contentsline {section}{\numberline {13.45}Uncompress}{293}{section.13.45}
\contentsline {section}{\numberline {13.46}Untokenize}{293}{section.13.46}
\contentsline {section}{\numberline {13.47}UnitexTool}{294}{section.13.47}
\contentsline {section}{\numberline {13.48}UnitexToolLogger}{295}{section.13.48}
\contentsline {section}{\numberline {13.49}Unxmlize}{298}{section.13.49}
\contentsline {section}{\numberline {13.50}XMLizer}{299}{section.13.50}
\contentsline {chapter}{\numberline {14}Formats de fichiers}{301}{chapter.14}
\contentsline {section}{\numberline {14.1}Codage Unicode}{301}{section.14.1}
\contentsline {section}{\numberline {14.2}Fichiers d\IeC {\textquoteright }alphabet}{302}{section.14.2}
\contentsline {subsection}{\numberline {14.2.1}Alphabet}{302}{subsection.14.2.1}
\contentsline {subsection}{\numberline {14.2.2}Alphabet de tri}{303}{subsection.14.2.2}
\contentsline {section}{\numberline {14.3}Graphes}{304}{section.14.3}
\contentsline {subsection}{\numberline {14.3.1}Format .grf}{304}{subsection.14.3.1}
\contentsline {subsection}{\numberline {14.3.2}Format .fst2}{308}{subsection.14.3.2}
\contentsline {section}{\numberline {14.4}Textes}{309}{section.14.4}
\contentsline {subsection}{\numberline {14.4.1}Fichiers .txt}{309}{subsection.14.4.1}
\contentsline {subsection}{\numberline {14.4.2}Fichiers .snt}{309}{subsection.14.4.2}
\contentsline {subsection}{\numberline {14.4.3}Fichier text.cod}{309}{subsection.14.4.3}
\contentsline {subsection}{\numberline {14.4.4}Fichier tokens.txt}{309}{subsection.14.4.4}
\contentsline {subsection}{\numberline {14.4.5}Fichier tok\_by\_alph.txt et tok\_by\_freq.txt}{310}{subsection.14.4.5}
\contentsline {subsection}{\numberline {14.4.6}Fichier enter.pos}{310}{subsection.14.4.6}
\contentsline {section}{\numberline {14.5}Automate du texte}{310}{section.14.5}
\contentsline {subsection}{\numberline {14.5.1}Fichier text.tfst}{310}{subsection.14.5.1}
\contentsline {subsection}{\numberline {14.5.2}Fichier text.tind}{313}{subsection.14.5.2}
\contentsline {subsection}{\numberline {14.5.3}Fichier cursentence.grf}{313}{subsection.14.5.3}
\contentsline {subsection}{\numberline {14.5.4}Fichier sentenceN.grf}{314}{subsection.14.5.4}
\contentsline {subsection}{\numberline {14.5.5}Fichier cursentence.txt}{314}{subsection.14.5.5}
\contentsline {subsection}{\numberline {14.5.6}The cursentence.tok file}{314}{subsection.14.5.6}
\contentsline {subsection}{\numberline {14.5.7}Fichiers tfst\_tags\_by\_freq.txt et tfst\_tags\_by\_alph.txt}{314}{subsection.14.5.7}
\contentsline {section}{\numberline {14.6}Concordances}{314}{section.14.6}
\contentsline {subsection}{\numberline {14.6.1}Fichier concord.ind}{314}{subsection.14.6.1}
\contentsline {subsection}{\numberline {14.6.2}Fichier concord.txt}{316}{subsection.14.6.2}
\contentsline {subsection}{\numberline {14.6.3}Fichier concord.html}{316}{subsection.14.6.3}
\contentsline {subsection}{\numberline {14.6.4}Fichier diff.html}{317}{subsection.14.6.4}
\contentsline {section}{\numberline {14.7}Dictionnaires du texte}{318}{section.14.7}
\contentsline {subsection}{\numberline {14.7.1}dlf et dlc}{318}{subsection.14.7.1}
\contentsline {subsection}{\numberline {14.7.2}err}{318}{subsection.14.7.2}
\contentsline {subsection}{\numberline {14.7.3}tags\_err}{318}{subsection.14.7.3}
\contentsline {subsection}{\numberline {14.7.4}tags.ind}{318}{subsection.14.7.4}
\contentsline {section}{\numberline {14.8}Dictionnaires}{319}{section.14.8}
\contentsline {subsection}{\numberline {14.8.1}Fichier .bin}{319}{subsection.14.8.1}
\contentsline {subsection}{\numberline {14.8.2}Fichier.inf}{320}{subsection.14.8.2}
\contentsline {subsection}{\numberline {14.8.3}Fichier information sur un dictionnaire}{321}{subsection.14.8.3}
\contentsline {subsection}{\numberline {14.8.4}Fichier CHECK\_DIC.TXT}{321}{subsection.14.8.4}
\contentsline {section}{\numberline {14.9}Fichiers ELAG}{323}{section.14.9}
\contentsline {subsection}{\numberline {14.9.1}Fichier tagset.de}{323}{subsection.14.9.1}
\contentsline {subsection}{\numberline {14.9.2}Fichiers .lst}{324}{subsection.14.9.2}
\contentsline {subsection}{\numberline {14.9.3}.elg files}{324}{subsection.14.9.3}
\contentsline {subsection}{\numberline {14.9.4}Fichier .rul}{324}{subsection.14.9.4}
\contentsline {section}{\numberline {14.10}Fichier taggeur}{325}{section.14.10}
\contentsline {subsection}{\numberline {14.10.1}Fichier corpus.txt}{325}{subsection.14.10.1}
\contentsline {subsection}{\numberline {14.10.2}Le fichier de donn\IeC {\'e}es du taggueur}{326}{subsection.14.10.2}
\contentsline {section}{\numberline {14.11}Fichier de configuration}{327}{section.14.11}
\contentsline {subsection}{\numberline {14.11.1}Fichier Config}{327}{subsection.14.11.1}
\contentsline {subsection}{\numberline {14.11.2}Fichier system\_dic.def}{329}{subsection.14.11.2}
\contentsline {subsection}{\numberline {14.11.3}Fichier user\_dic.def}{330}{subsection.14.11.3}
\contentsline {subsection}{\numberline {14.11.4}Fichier user.cfg et .unitex.cfg}{330}{subsection.14.11.4}
\contentsline {section}{\numberline {14.12}Fichiers CasSys}{330}{section.14.12}
\contentsline {subsection}{\numberline {14.12.1}Fichiers de configuration CasSys csc}{330}{subsection.14.12.1}
\contentsline {section}{\numberline {14.13}Plusieurs autres fichiers}{331}{section.14.13}
\contentsline {subsection}{\numberline {14.13.1}Fichier dlf.n, dlc.n, err.n et tags\_err.n}{331}{subsection.14.13.1}
\contentsline {subsection}{\numberline {14.13.2}Fichier stat\_dic.n}{331}{subsection.14.13.2}
\contentsline {subsection}{\numberline {14.13.3}Fichier stats.n}{331}{subsection.14.13.3}
\contentsline {subsection}{\numberline {14.13.4}Fichier concord.n}{331}{subsection.14.13.4}
\contentsline {subsection}{\numberline {14.13.5}Fichier concord\_tfst.n}{332}{subsection.14.13.5}
\contentsline {subsection}{\numberline {14.13.6}Fichier r\IeC {\`e}gles de normalisation}{332}{subsection.14.13.6}
\contentsline {subsection}{\numberline {14.13.7}Fichier de mots interdits}{332}{subsection.14.13.7}
\contentsline {subsection}{\numberline {14.13.8}Fichier de log}{332}{subsection.14.13.8}
\contentsline {subsection}{\numberline {14.13.9}R\IeC {\`e}gles typographiques de l'arabe: arabic\_typo\_rules.txt}{333}{subsection.14.13.9}
\contentsline {chapter}{Annexe A - GNU Lesser General Public License}{335}{chapter*.32}
\contentsline {chapter}{Annexe B - TRE's 2-clause BSD License}{345}{chapter*.33}
\contentsline {chapter}{Annexe C - Lesser General Public License For Linguistic Resources}{347}{chapter*.34}
