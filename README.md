# MULTIFLEX

This is a repository of the MULTIFLEX system for morphosyntactic lexical encoding of continuous multi-word expressions (MWEs) such as _by and large_, _traffic jam_, _lord justice_, _crystal clear_ etc. 

Modelling is rule-based. It's formalism uses graphs decorated with unification variables.

The inflection engine allows to generate all inflected forms of a given MWE, give its lemma and its inflectional graph. For instance, given the MWE _lord justice_ and its annotated lemma:
```
lord(lord.N1 :s) justice(justice.N1 :s),NC_NN2
```
as well is its inflection graph ```NC_NN2```, the system generates the following inflected forms:
```
lord justice,lord justice.N:s
lords justice,lord justice.N:p
lord justices,lord justice.N:p
lords justices,lord justice.N:p
```

For languages with rich inflectional morphology, the number of inflected forms in MWEs can go into dozens or even hundreds but their inflection graphs usually do not exceed a few paths, thanks to unification. 

## Applications
Multiflex has been integrated into 4 natural language processing platforms:
- [Unitex](http://www-igm.univ-mlv.fr/~unitex/) (and its industry-oriented version GramLab), a multiligual corpus processor available under the GNU LGPL v3 license (see also the [Unitex/GramLab](https://groups.google.com/forum/#!forum/unitex-gramlab) forum)
- [Toposław](http://zil.ipipan.waw.pl/Toposlaw), a Polish lexicographic framework distributed under the GNU GLP v3 license,
- Verbosław, an extended version of Toposław, covering the morphological variability of verbal MWEs (under development), used in the construction of the [VERBEL](http://uwm.edu.pl/verbel/) lexicon of Polish verbal MWEs
- [LeXimir](http://korpus.matf.bg.ac.rs/soft/LeXimir.html), a Serbian tool for lexical resource management and query expansion (available from the author)

## Bibliography
- Agata Savary (2009): "Multiflex: a Multilingual Finite-State Tool for Multi-Word Units", in Proceedings of CIAA 2009, Sydney, Australie, Lecture Notes in Computer Science 5642, Springer Verlag, pp. 237-240, [[preprint](https://perso.limsi.fr/savary/papers/Savary-CIAA-2009.pdf)]
- Agata Savary, Joanna Rabiega-Wiśniewska, Marcin Woliński (2009): _Inflection of Polish Multi-Word Proper Names with Morfeusz and Multiflex_, in Małgorzata Marciniak, MYKOWIECKA, A. (eds.) _Aspects of Natural Language Processing_, Lecture Notes in Computer Science 5070, Springer Verlag, pp. 111-141 [[preprint](https://perso.limsi.fr/savary/papers/savary-et-al-LNAI-2009.pdf)].

## Authors and acknowledgment
[Agata Savary](https://perso.limsi.fr/savary)

## License
The software is distributed under the [GNU LGPL V3](https://www.gnu.org/licenses/lgpl-3.0.en.html) license.


