Problemy z wywolaniem Morfeusza z Multiflexa

22 lutego 2017

=======
1/02/2017
Zgloszenie Piotra:

- listopad 2016: poprawana kompilowacja i dzialanie MULTIFLEXA (po dodaniu nowej obslugi fleksemow)
- styczen 2017: ani najnowszej, ani poprzednich wersji nie daje sie uruchomic: zapetlanie, niepowodzenie w porozumieniu z Morfeuszem w trakcie tokenizacji; prawdopodobny powod: zmiana kompilatora

=======
16/02/2017 Agata 

Nieznane wczesniej problemy z Morfeuszem:
- etap tokenizacji - wywolanie morfeusz_analyse, ktorej jako parametr przekazywany jest nastepujacy tekst:
	"odwracanie(odwracać:ger:sg:nom:n2:imperf:aff) kota(kot:subst:sg:acc:m2) ogonem(ogon:subst:sg:inst:m3),ger(VC-O_N_Nger)"
- analizator Morfeusza nie jest w stanie dostarczyc dla niego zadnej analizy; dlatego skanowanie hasel slownikowych nie postepuje do przodu i Multiflex sie zapetla.
	Dokladniej: 
		morfeuszInstance->analyse(input, results);
	dla parametru input rownemu 
		"odwracanie(odwracać:ger:sg:nom....." 
	zwraca "results" o liczebnosci 0.

=======
22/02/2017 Agata:

1. Test dawnego Morfeusza: 
- kompilacaja test_gen jak dawniej; wywolanie powoduje blad segmentacji

2. Sciągniecie Morfeusza w wersji z 12/02/2017
- testy wersji skompilowanej interaktywnej OK: 
	* cd ~/Narzedzia/Morfeusz/2017-02-22/morfeusz2-1.9.2.sgjp.20170212-Linux-amd64/bin
	* LD_LIBRARY_PATH=../lib/; export LD_LIBRARY_PATH; ./morfeusz_analyzer 
		Morfeusz analyzer, version: 1.9.2
		Setting dictionary search path to: .
		Using dictionary: sgjp (efault)
		odwracanie
		[0,1,odwracanie,odwracać,ger:sg:nom.acc:n2:imperf:aff,_,_]
	* wziela z nowej wersji plik lib/libmorfeusz2.so i wstawilam na miejsce morfeusz/libmorfeusz2.so

- test wersji zrodlowej:
	* plik morfeusz2.h jest jak w wersji z 15/11/2015 i poprzedniej
	* plik morfeusz2_c.h - jak w wersji z 15/11/2015 i poprzedniej (uwaga, w MULTIFLEX_1.5beta ten plik ma dodatek Piotra i Agaty)
	* plik morfeusz2_c.cpp - jak z 15/11/2015 i poprzedniej (uwaga, w MULTIFLEX_1.5beta ten plik ma dodatek Piotra i Agaty, jak rowniek usuniecie dla morfeusz_about)
	* morfeusz_analyzer.cpp - jak z 15/07/2015
	* morfeusz_generator.cpp - jak z 15/07/2015

3. Parametryzacja kopilatora
- za rada Marcina dodalam do morfeusz2_c.cpp definicje oprcji kopilatora (chodzi o interakcje z std::string and std::list; zob. https://gcc.gnu.org/onlinedocs/libstdc%2B%2B/manual/using_dual_abi.html):
	#ifndef _GLIBCXX_USE_CXX11_ABI
	#define _GLIBCXX_USE_CXX11_ABI 0
	#endif
- po kompilacji test_gen wreszcie zadzialal!

4. Kompilacja multiflex-morfeusz:
- teraz morfeusz jest w podkatalogu morfeusz/; gdy kopiluje sie morfeusz/morfeusz2_c.cpp wynik morfeusz2_c.o jest zapisywany w katalogu biezacym, a nie w podkatalagu (nie rozumiem dlaczego);
- przy linkowaniu brakuje wiec morfeusz/morfeusz2_c.o (bo jest on w ./)
- rozwiazanie: dodalam linki symboliczne w ./ do morfeusz/morfeusz2_c.cpp i morfeusz/morfeusz2_c.h, a w Makefile teraz jest wymagany morfeusz2_c.o (a nie morfeusz/morfeusz2_c.o)
- kopilacja OK
- test:
	[savary@dib-0314px][~/Recherche/MULTIFLEX/MULTIFLEX_1.5beta/scripts]$ ./citest odwracanie .. Polish morfeusz 
	Unable to open equivalence file ../ling/morfeusz-ling/Polish/Inflection/Equivalences.txt
	Equivalences between dictionary and Multiflex features are considered as straightforward.
	No inflected form could be generated for: odwracać::sg:voc:n2:imperf:aff:same
	No inflected form could be generated for: odwracać::pl:voc:n2:imperf:aff:same
	No inflected form could be generated for: odwracać::sg:voc:n2:imperf:neg:same
	No inflected form could be generated for: odwracać::pl:voc:n2:imperf:neg:same
	Done.
- slownik odmieniony jest poprawnie - sukces!


