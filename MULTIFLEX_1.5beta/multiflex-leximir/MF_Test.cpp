/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

/********************************************************************************/
/* MAIN MODULE FOR RUNNING THE DELAC-INTO-DELACF INFLECTION                     */
/********************************************************************************/


#include <stdio.h>
#include <string.h>
#include "IOBuffer.h"  //Required by Unitex "golden rules"
#include "Alphabet.h"
#include "Error.h"
#include "File.h"
#include "MF_Copyright.h"
#include "MF_DLC_inflect.h"
#include "MF_LangMorpho.h"
#include "MF_MorphoEquiv.h"
#include "MF_MU_graph.h"
#include "MF_LetterCase.h"
#include "MF_MU_embedded.h"

#include "MF_FeatStruct.h"
#include "MF_FeatStructStr.h"
#include "MF_Tokenize.h"
#include "MF_SU_morpho.h"
#include "MF_CurrentForms.h"

//Current language's alphabet
Alphabet* alph;

// Directory containing the inflection tranducers and the 'Morphology' file
char inflection_directory[FILENAME_MAX];

//Error code indicating loading status of configuration files 'Morphology' and 'Equivalences' if any.
//extern int config_files_status=CONFIG_FILES_OK;

void usage();
int init_morphology(char* dir);
int init_alphabet(char* alph_file);
int init_equivalences(char* dir);
int init_transducers(char* dir);


/////////////////////////////////// 
// Inflection of a DELAC to a DELACF
// argv[1] = path and name of the example file
// argv[2] = path and name of the alphabet file
// If any problem occurs, returns 1. Otherwise returns 0.
int main(int argc, char* argv[]) {
  
  //  int err;  //0 if a function completes with no error 
  int arg_shift; //argument index shift caused by the appearence or no of the '-d' option

  setBufferMode();  //Required by Unitex "golden rules"
  
  if (argc < 5) {
    usage();
    return 0;
  }

  //Check if the -d option is present
  if (!strcmp(argv[1], "-d")) {
    arg_shift = 1;
    debug_status = GRAPH_DEBUG;
  }
  else {
    arg_shift = 0;
    debug_status = GRAPH_NO_DEBUG;    
  }

  //Load the morphology description, the alphabet, the equivalence files, 
  //the structure for inflection transducers, a
  //and the list of DELAC files containing embedded compounds
  if (init_morphology(argv[4+arg_shift]))
      return 1;
  //  print_language_morpho();

  if (init_alphabet(argv[3+arg_shift]))
      return 1;
  if (init_equivalences(argv[4+arg_shift]))
      return 1;
  if (init_transducers(argv[4+arg_shift]))
    return 1;
  if (init_embedded_dlc(argv+5+arg_shift, argc-arg_shift-5))
    return 1;
  init_current_forms();

  initEmbeddingLevel();

  /**************************************************/
  /* 
 // Testing Morfeusz' segmentation

  unichar s_u[1000];
  char s_c[1000];
  FILE* f;
  InterpMorf* im;  //Morfeusz interpretation of a string

  f = u_fopen(argv[1],U_READ);
  if (!f) {
    error("Unable to open file: '%s' !\n", argv[1]);
    return 1;
  }
  
  while (!feof(f)) { 
    fscanf(f,"%s\n",s_c);
    im = morfeusz_analyse(s_c);
    int t; //Number of the current token
    for (t=0; im[t].p!=-1; t++) {
      u_printf("%s ",im[t].forma);
    }
    u_printf("\n");
  }
  
  //End of testing Testing Morfeusz' segmentation
  */
  /**************************************************/

  /**************************************************
  // Testing Letter Case
  //Test of letter case functions on a file
  unichar s[1000];
  FILE* f;
  f = u_fopen(argv[1],U_READ);
  if (!f) {
    error("Unable to open file: '%s' !\n", argv[1]);
    return 1;
  }
  
  while (!feof(f)) { 
    u_fgets(s,f);

    //Output the letter case of the current string
    u_printf("Letter case of %S is ",s);
    switch (letter_case(s)) {
    case all_lower:  u_printf("all_lower"); break;
    case all_upper:  u_printf("all_upper"); break;
    case first_upper:  u_printf("first_upper"); break;
    case all_lower_each_word: u_printf("all_lower_each_word"); break;
    case all_upper_each_word: u_printf("all_upper_each_word"); break;
    case first_upper_each_word: u_printf("first_upper_each_word"); break;
    case no_letter_case: u_printf("no_letter_case"); break;
    case other: u_printf("other"); break;
    }
    u_printf(".\n");

    //Output the result of tranforming the current string into another letter case
    unichar s_copy[1000];
    u_strcpy(s_copy,s);
    u_printf("%S transformed into all_lower gives %S\n",s,letter_case_apply(s_copy,all_lower));
    u_strcpy(s_copy,s);
    u_printf("%S transformed into all_upper gives %S\n",s,letter_case_apply(s_copy,all_upper));
    u_strcpy(s_copy,s);
    u_printf("%S transformed into first_upper gives %S\n",s,letter_case_apply(s_copy,first_upper));
    u_strcpy(s_copy,s);
    u_printf("%S transformed into all_lower_each_word gives %S\n",s,letter_case_apply(s_copy,all_lower_each_word));
    u_strcpy(s_copy,s);
    u_printf("%S transformed into all_upper_each_word gives %S\n",s,letter_case_apply(s_copy,all_upper_each_word));
    u_strcpy(s_copy,s);
    u_printf("%S transformed into first_upper_each_word gives %S\n",s,letter_case_apply(s_copy,first_upper_each_word));
    u_strcpy(s_copy,s);
    u_printf("%S transformed into no_letter_case gives %S\n",s,letter_case_apply(s_copy,no_letter_case));
    u_strcpy(s_copy,s);
    u_printf("%S transformed into other gives %S\n",s,letter_case_apply(s_copy,other));
    u_printf("\n");
  }
  //End of testing Letter Case
  ************************************/

  /**************************************************
  // Testing feature structure vs. string conversion
  int fs_i;
  unichar feat_str[2000];
  //  u_strcpy(feat_str, ":adms1g");
   u_strcpy(feat_str, ":adms1g:aems4q:aems5g:aemp1g:aemp5g");

  //Testing get_feat_struct_set_str
  f_feat_struct_set_T* fss = get_feat_struct_set_str(feat_str);
  u_printf("%S = \n", feat_str);
  for (fs_i=0; fs_i<fss->no_feat_structs; fs_i++)
    f_print_feat_struct(&(fss->feat_structs[fs_i]));
  f_delete_feat_struct_set(&fss);

  //Testing get_feat_struct_str
  u_printf("\n");
  f_feat_struct_T* fs = get_feat_struct_str(feat_str);
  if (fs)
    f_print_feat_struct(fs);

  //Testing get_str_feat_struct
  u_printf("\n");
  unichar* feat_str2;
  feat_str2 = get_str_feat_struct(fs);
  u_printf("%S\n", feat_str2);
  free(feat_str2);

  //Testing get_class_str and get_class_str_direct
  unichar cl1[100], cl2[100];
  u_strcpy(cl1, "VC");
  u_strcpy(cl2, "verb");
  u_printf("%S == %S\n", cl1, get_class_str(cl1)->name);
  u_printf("%S == %S\n", cl2, get_class_str_direct(cl2)->name);

  f_delete_feat_struct(&fs);
  //End of testing feature structure vs. string
  ************************************/

  /**************************************************
  // Testing single word inflection
  unichar* word = (unichar*)malloc(1000*sizeof(unichar));
  //  u_strcpy(word, "vojni");
  u_strcpy(word, "rok");
  char* code = (char*)malloc(100*sizeof(char));
  // strcpy(code,"A2");
 strcpy(code,"N1298");
  unichar cl[100];
  //  u_strcpy(cl,"adj");
  u_strcpy(cl,"noun");

  U_lemma_T* lemma = (U_lemma_T*)malloc(sizeof(U_lemma_T));
  lemma->type = simple;
  lemma->simple_lemma = word;
  lemma->cl = is_valid_class(cl);
  lemma->paradigm = code;
  U_print_lemma(lemma);

é  U_forms_T forms;  //inflected forms of the MWU
  U_init_forms(&forms);
  SU_inflect(lemma,NULL,&forms);
  U_print_forms(&forms);
  U_delete_forms(&forms);

  U_delete_lemma(lemma,1);
  //End of testing single word inflection
  ************************************/

  // /**************************************************
  // Testing DELAC inflection
  //unichar DLC_entry[2000];
  //  u_strcpy(DLC_entry, "vojni(vojni.A2:ms1q) rok(rok.N1298:ms1q),N(NC_AXN)");
  //u_strcpy(DLC_entry, "civilni(civilni.A2:adms1g) {vojni rok}(vojni rok.N(NC_AXN):ms1q),N(NC_AXN)");
  //DLC_entry_T* entry=(DLC_entry_T*)malloc(sizeof(DLC_entry_T));
  //  DLC_line2entry(DLC_entry, entry, 1);
  // DLC_print_entry(entry);
  
  //DELAC inflection
  int err = DLC_inflect(argv[1+arg_shift],argv[2+arg_shift]);
  if (err)
    u_printf("Problems occured while inflecting a DELAC.\n");
  else 
    u_printf("Done.\n");

  //End of testing DELAC inflection
  //************************************/

  /**************************************************
  // Testing tokenization
  unichar str[2000];
  //  u_strcpy(str, "vojni(vojni.A2:ms1q) rok(rok.N1298:ms1q),N(NC_AXN)");
  //  u_strcpy(str, "voj\\ni(vojni.A2:ms1q) rok(rok.N1298:ms1q),N(NC_AXN)");
  //  u_strcpy(str, ",vojni(vojni.A2:ms1q) rok(rok.N1298:ms1q),N(NC_AXN)");
  //    u_strcpy(str, "\,vojni(vojni.A2:ms1q) rok(rok.N1298:ms1q),N(NC_AXN)");
  u_strcpy(str, "vojni(vojni.A2:ms1q)\\\\\\, rok(rok.N1298:ms1q),N(NC_AXN)");
  unichar unit[2000]; 

  int l; //Length of the scanned sequence
  l = get_unit(unit, str, 1000, 1);
  u_printf("%S\n", unit);

  //  tokenization* t = tokenize(str, 0);
  tokenization* t = tokenize(str, 1);
  int tok; //Current token
  for (tok=0; tok<t->nb_tokens; tok++)
    u_printf("%S\n", t->tokens[tok]);


  //End of testing tokenization
  ************************************/

  /**************************************************/

  //Liberate the structures needed for the DELAC inflection
  MU_graph_free_graphs();
  free_alphabet(alph);
  free_language_morpho();
  //  free_convert();
  free_embedded_dlc();
  return 0;
}

//////////////////////////////////
// Usage note shown if the number of command line arguments is incorrect (<5) 
void usage() {
  u_printf("%S",COPYRIGHT);
  u_printf("Usage: DlcInflect <delac> <delacf> <alpha> <dir>\n");
  u_printf("     <delac> : the unicode DELAC file to be inflected\n");
  u_printf("     <delacf> : the unicode resulting DELACF dictionary \n");
  u_printf("     <alpha> : the alphabet file \n");
  u_printf("     <dir> : the directory containing 'Morphology' file and \n");
  u_printf("             inflection graphs for single and compound words.\n");
  u_printf("\nInflects a DELAC into a DELACF.\n");
}

/////////////////////////////////// 
// Loads the Morphology.txt file
// containing the morphological model of the language
// (i.e. the list of inflectional classes, categories and values).
// dir = directory containing this file
// If any problem occurs, returns 1. Otherwise returns 0.
int init_morphology(char* dir) {
  char morphology[FILENAME_MAX];
  int err; //Error code
  new_file(dir,"Morphology.txt",morphology);
  err = read_language_morpho(morphology);
  if (err) {
    free_language_morpho();
    config_files_status=CONFIG_FILES_ERROR;
    return 1;
  }
  //  print_language_morpho();
  return 0;
}

/////////////////////////////////// 
// Loads the alphabet file 'alph_file'
// If any problem occurs, returns 1. Otherwise returns 0.
int init_alphabet(char* alph_file) {
  alph=load_alphabet(alph_file);  //To be done once at the beginning of the inflection
  if (alph==NULL) {
    error("Cannot open alphabet file %s\n",alph_file);
    free_language_morpho();
    free_alphabet(alph);    
    return 1;
  }
  return 0;
}

/////////////////////////////////// 
// Loads the Equivalences.txt file if any
// containing the equivalences between the morphological
// values for simple words and those for compounds.
// dir = directory containing this file
// If any problem occurs, returns 1. Otherwise returns 0.
int init_equivalences(char* dir) {
  char equivalences[FILENAME_MAX];
  int err; //Error code
  new_file(dir,"Equivalences.txt",equivalences);
  err = init_morpho_equiv(equivalences);
  if (err) {
    free_language_morpho();
    free_alphabet(alph);    
    config_files_status=CONFIG_FILES_ERROR;
    return 1;
  }
  //  print_morpho_equiv();
  
  init_class_equiv();
  return 0;
}

/////////////////////////////////// 
// Initializes the structure of inflection transducers
// dir = directory containing this file
// If any problem occurs, returns 1. Otherwise returns 0.
int init_transducers(char* dir) {
  int err; //Error code
  strcpy(inflection_directory,dir);
  err = MU_graph_init_graphs();
  if (err) {
    MU_graph_free_graphs();
    return 1;
  }
  return 0;
}
