/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */
/* This file contains parts of the Unitex software distributed under the terms of LGPL.   */
/* It is a modified and extended version of Inflect.cpp file of the the Unitex 1.2 beta   */
/* version downloaded from http://www-igm.univ-mlv.fr/~unitex/ on June 6, 2005.           */ 
/* The modifications has been done by Agata Savary (agata.savary@univ-tours.fr).          */
/* Latest modification on December 6, 2005.                                               */


/********************************************************************************/
/* MORPHOLOGY AND INFLECTION OF A SIMPLE WORD                                   */
/********************************************************************************/

#include <string.h>
#include "MF_LangMorpho.h"
#include "MF_UnitMorpho.h"
#include "MF_SU_morpho.h"
#include "MF_FeatStructStr.h"
#include "MF_CurrentForms.h"
#include "Transitions.h"
#include "MF_Util.h"


#define MAX_CHARS_IN_STACK 100

//////////////////////////////
struct inflect_infos {
   unichar inflected[MAX_CHARS_IN_STACK];
   unichar output[MAX_CHARS_IN_STACK];
   struct inflect_infos* next;
};

//////////////////////////////
int SU_unitex_get_all_forms(MultiFlexParameters* params,U_lemma_T* lemma, U_forms_T* all_forms);
int SU_explore_state(MultiFlexParameters* params,unichar* inflected,U_lemma_T* lemma,unichar* output, Fst2* a,int etat_courant, U_forms_T* forms);
int SU_explore_tag(MultiFlexParameters* params,Transition* T,unichar* inflected,U_lemma_T* lemma,unichar* output, Fst2* a, U_forms_T* forms);
int SU_explore_state_recursion(MultiFlexParameters* params,unichar* inflected,U_lemma_T* lemma,unichar* output, Fst2* a,int etat_courant,struct inflect_infos** L, U_forms_T* forms);
void SU_explore_tag_recursion(MultiFlexParameters* params,Transition* T,unichar* inflected,U_lemma_T* lemma,unichar* output, Fst2* a,struct inflect_infos** LISTE,U_forms_T* forms);
void shift_stack_right(unichar* stack,int pos);
void shift_stack_left(unichar* stack,int pos);


////////////////////////////////////////////
// For a given single unit, generates all the inflected forms corresponding to the given inflection features.
// params = "global" parameters
// CURRENT_FORMS = inflected forms of the current MWU generated up till now
/// lemma:  single unit's identifier
// desired_features: feature structure of the desired forms, e.g. {Gen=fem, Case=Inst}, or {} (if separator)
// forms: return parameter; set of the inflected forms corresponding to the given inflection features
//        e.g. (3,{[reka,{Gen=fem,Nb=sing,Case=Instr}],[rekami,{Gen=fem,Nb=pl,Case=Instr}],[rekoma,{Gen=fem,Nb=pl,Case=Instr}]})
//        or   (1,{["-",{}]})
// If feat is NULL, all inflected forms are to be generated
// If feat is [0,NULL] no inflected form is to be generated
// Returns 0 on success, 1 otherwise.   
int SU_inflect(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_lemma_T* lemma,f_feat_struct_T* desired_features, U_forms_T* forms) {
  int err;
  int unfound; //Boolean saying if the lemma's forms failed to be found in the current forms

  //If no form required return
  if (desired_features && desired_features->no_catvals == 0)
    return 0;

  U_forms_T all_forms;  //All inflected forms of the current lemma
  U_init_forms(&all_forms);

  //Check if the forms have already generated
  unfound = get_current_form_list(CURRENT_FORMS,lemma, &all_forms);

  //If not yet generated, generate them all
  if (unfound) {

    //Generate all inflected forms of the simple word via Morfeusz
    err = SU_unitex_get_all_forms(params,lemma, &all_forms);
    if (err) 
      return 1;
    
    //Add the new forms to the global set of all previously generated forms
    err = add_current_forms(CURRENT_FORMS,lemma,&all_forms);
    if (err) {
      U_delete_forms(&all_forms);
      return 1;
    }
  }

  //If all forms required return them all
  if (!desired_features) {
    err = U_copy_forms(forms,&all_forms);
    U_delete_forms(&all_forms);
    return err;
  }
  
 //Choose only the desired forms
  U_forms_T desired_forms;
  U_init_forms(&desired_forms);  
  err = U_duplicate_desired_forms(&all_forms, desired_features, &desired_forms);
  if (err) {
    U_delete_forms(&all_forms);
    U_delete_forms(&desired_forms);
    return 1;
  }

  //Apply the post-inflection graphical forms, e.g. Init, LetterCase
  err = U_apply_graphical_feat(params,&desired_forms,desired_features,forms);
  U_delete_forms(&all_forms);
  U_delete_forms(&desired_forms);

  return err;
}


////////////////////////////////////////////
// For a given single unit, generates all the inflected forms corresponding to the given inflection features.
// params: "global" parameters
// lemma:  single unit's identifier
// forms: return parameter; set of all inflected forms corresponding to the given inflection features
// Returns 0 on success, 1 otherwise.   
int SU_unitex_get_all_forms(MultiFlexParameters* params,U_lemma_T* lemma, U_forms_T* all_forms) {
  int err; //Error code
  unichar inflected[MAX_CHARS_IN_STACK];
  unichar output[MAX_CHARS_IN_STACK];
  output[0]='\0';
  int T=get_transducer(params,lemma->paradigm);
  if (params->fst2[T]==NULL) {
    // if the automaton has not been loaded
    return 1;
  }
  u_strcpy(inflected,lemma->simple_lemma);
  err = SU_explore_state(params,inflected,lemma,output,params->fst2[T],0,all_forms);
  return err;
}


////////////////////////////////////////////
// Explore the transducer a starting from state 'etat_courant'.
// params: "global" parameters
// forms: return parameter; set of all inflected forms corresponding to the given inflection features
// Returns 0 on success, 1 otherwise.   
int SU_explore_state(MultiFlexParameters* params,unichar* inflected,U_lemma_T* lemma,unichar* output, Fst2* a,int current_state, U_forms_T* forms) {
  int err;

  Fst2State e = a->states[current_state];
  if (e->control & 1) {  //If final state
    f_feat_struct_set_T* feat;  //Set of feature structures; necessary in case of factorisation of entries, e.g. :ms:fs
    feat = get_feat_struct_set_str(params,output);
    if (!feat) 
      return (1);

    int fs;  //Index of the current feature structure in the current node
    for (fs=0; fs<feat->no_feat_structs; fs++) {
      //Create a new inflected form description
      U_form_id_T new_form;  //Structure for the new form
      new_form.form = inflected;
      new_form.lemma = lemma;
      new_form.feat = &(feat->feat_structs[fs]);
      new_form.extras = NULL;

      //Add the new form to the previous ones
      err = U_add_form(&new_form, forms, 1);
    }
    f_delete_feat_struct_set(&feat);
  }
  //Explore all outgoing transitions
  Transition* t=e->transitions;
  while (t!=NULL) {
    err = SU_explore_tag(params,t,inflected,lemma,output,a,forms);
    if (err)
      return err;
    t=t->next;
  }
  return 0;
}

////////////////////////////////////////////
// Explore the tag of the transition T
// params: "global" parameters
// forms: return parameter; set of all inflected forms 
// Returns 0 on success, 1 otherwise.   
int SU_explore_tag(MultiFlexParameters* params,Transition* T, unichar* inflected, U_lemma_T* lemma, unichar* output, Fst2* a, U_forms_T* forms) {
  /*
  unichar tmp[100];
  u_strcpy_char(tmp,"\nTransition: ");  //debug
  u_prints(tmp);
  u_prints(a->etiquette[T->etiquette]->contenu);  //debug
  u_strcpy_char(tmp,"/");  //debug
  u_prints(tmp);
  u_prints(a->etiquette[T->etiquette]->transduction);  //debug
  u_strcpy_char(tmp,"\n");  //debug
  u_prints(tmp);
  */

  if (T->tag_number < 0) {
    // if we are in the case of a call to a sub-graph
    struct inflect_infos* L=NULL;
    struct inflect_infos* temp;
    SU_explore_state_recursion(params,inflected, lemma, output, a, a->initial_states[-(T->tag_number)], &L, forms);
    while (L!=NULL) {
      SU_explore_state(params,L->inflected,lemma,L->output,a,T->state_number,forms);
      temp=L;
      L=L->next;
      free(temp);
    }
    return 0;
  }
  Fst2Tag t = a->tags[T->tag_number];
  //  Etiquette e=a->etiquette[T->etiquette];
  int pos=u_strlen(inflected);
  unichar out[MAX_CHARS_IN_STACK];
  unichar pile[MAX_CHARS_IN_STACK];
  unichar etiq[MAX_CHARS_IN_STACK];
  int pos_etiq;
  u_strcpy(out,output);
  if (u_strcmp(t->output,"<E>")) {
    u_strcat(out,t->output);
  }
  u_strcpy(pile,inflected);
  u_strcpy(etiq,t->input);
  if (u_strcmp(etiq,"<E>")) {
    // if the tag is not <E>, we process it
    for (pos_etiq=0;etiq[pos_etiq]!='\0';) {
      switch (etiq[pos_etiq]) {
      case (unichar) 'L':   {
	if (pos!=0) {
	  // if the stack is not empty, we decrease the
	  // stack pointer
	  pos--;
	}
	pos_etiq++;
      };break;
      case (unichar) 'R':   {
	pos++;
	pos_etiq++;
      };break;
      case (unichar) 'C':   {
	shift_stack_right(pile,pos);
	pos++;
	pos_etiq++;
      };break;
      case 'D':   {
	shift_stack_left(pile,pos);
	pos--;
	pos_etiq++;
      };break;
      default:    {
	pile[pos++]=etiq[pos_etiq++];
      };break;
      }
    }
  }
  // then, we go the next state
  pile[pos]=(unichar)'\0';  //This line was put inside a comment on Dec 6, 2005 in the new version 
                            //of Inflect.cpp by S�bastien, sent by Cvetana.
                            //However, without this line the generated forms are incorrect
  SU_explore_state(params,pile,lemma,out,a,T->state_number,forms);
  return 0;
}

////////////////////////////////////////////
// Explore the sub-transducer
// params: "global" parameters
// forms: return parameter; set of all inflected forms 
// Returns 0 on success, 1 otherwise.   
int SU_explore_state_recursion(MultiFlexParameters* params,unichar* inflected,U_lemma_T* lemma,unichar* output, Fst2* a,int current_state,struct inflect_infos** L, U_forms_T* forms) {
  Fst2State e = a->states[current_state];
  if (e->control & 1) {
    // if we are in a final state, we save the computed things
    struct inflect_infos* res=(struct inflect_infos*)malloc(sizeof(struct inflect_infos));
    u_strcpy(res->inflected,inflected);
    u_strcpy(res->output,output);
    res->next=(*L);
    (*L)=res;
  }
  Transition* t=e->transitions;
  while (t!=NULL) {
    SU_explore_tag_recursion(params,t,inflected,lemma,output,a,L,forms);
    t=t->next;
  }
  return 0;
}

////////////////////////////////////////////
// explore the tag of the transition T
// params: "global" parameters
// forms: return parameter; set of all inflected forms
// Returns 0 on success, 1 otherwise.   
void SU_explore_tag_recursion(MultiFlexParameters* params,Transition* T,unichar* inflected,U_lemma_T* lemma,unichar* output, Fst2* a,struct inflect_infos** LISTE,U_forms_T* forms) {
  /*  unichar tmp[100];
  u_strcpy_char(tmp,"\nTransition: ");  //debug
  u_prints(tmp);
  u_prints(a->etiquette[T->etiquette]->contenu);  //debug
  u_strcpy_char(tmp,"/");  //debug
  u_prints(tmp);
  u_prints(a->etiquette[T->etiquette]->transduction);  //debug
  u_strcpy_char(tmp,"\n");  //debug
  u_prints(tmp);
  */

  if (T->tag_number < 0) {
    // if we are in the case of a call to a sub-graph
    struct inflect_infos* L=NULL;
    struct inflect_infos* temp;
    SU_explore_state_recursion(params,inflected,lemma,output,a,a->initial_states[-(T->tag_number)],&L,forms);
    while (L!=NULL) {
      SU_explore_state_recursion(params,L->inflected,lemma,L->output,a,T->state_number,LISTE,forms);
      temp=L;
      L=L->next;
      free(temp);
    }
    return;
  }
  Fst2Tag t = a->tags[T->tag_number];
  int pos=u_strlen(inflected);
  unichar out[MAX_CHARS_IN_STACK];
  unichar pile[MAX_CHARS_IN_STACK];
  unichar etiq[MAX_CHARS_IN_STACK];
  int pos_etiq;
  u_strcpy(out,output);
  if (u_strcmp(t->output,"<E>")) {
    u_strcat(out,t->output);
  }
  u_strcpy(pile,inflected);
  u_strcpy(etiq,t->input);
  if (u_strcmp(etiq,"<E>")) {
    // if the tag is not <E>, we process it
    for (pos_etiq=0;etiq[pos_etiq]!='\0';) {
      switch (etiq[pos_etiq]) {
      case 'L':   {
	if (pos!=0) {
	  // if the stack is not empty, we decrease the
	  // stack pointer
	  pos--;
	}
	pos_etiq++;
      };break;
      case 'R':   {
	pos++;
	pos_etiq++;
      };break;
      case 'C':   {
	shift_stack_right(pile,pos);
	pos++;
	pos_etiq++;
      };break;
      case 'D':   {
	shift_stack_left(pile,pos);
	pos--;
	pos_etiq++;
      };break;
      default:    {
	pile[pos++]=etiq[pos_etiq++];
      };break;
      }
    }
  }
  // then, we go the next state
  pile[pos]='\0'; //This line was put inside a comment on Dec 6, 2005 in the new version 
                  //of Inflect.cpp by S�bastien, sent by Cvetana.
                  //However, without this line the generated forms are incorrect
  SU_explore_state_recursion(params,pile,lemma,out,a,T->state_number,LISTE,forms);
}

////////////////////////////////////////////
// Shifts right all the stack from the position pos
void shift_stack_right(unichar* stack,int pos) {
  if (pos==0) {
    // this case should never happen
    return;
  }
  for (int i=(MAX_CHARS_IN_STACK-1);i>=pos;i--) {
    stack[i]=stack[i-1];
  }
}

////////////////////////////////////////////
// Shifts left all the stack from the position pos
void shift_stack_left(unichar* stack,int pos) {
  if (pos==0) {
    // this case should never happen
    return;
  }
  for (int i=pos-1;i<MAX_CHARS_IN_STACK;i++) {
    stack[i]=stack[i+1];
  }
}

////////////////////////////////////////////
// *COMMENT CONCERNING THE UNIQUE IDENTIFICATION OF SINGLE WORD FORMS
//
// In order to uniquely identify a word form four elements are necessary:
//	- the form  (e.g. "rekami")
//	- its lemma (e.g. "reka")
//	- its inflection paradigm (e.g. N56)
//	- its inflection features (e.g. {Gen=fem,Nb=pl,Case=Inst}
// Note that omitting one of these elements may introduce ambiguities from the morphological point of view.
// Examples :
//	1) If the form itself is missing, an ambiguity may exist between variants corresponding to the same inflection features
//	   e.g. given only the lemma "reka", the paradigm N56, and the features {Gen=fem,Nb=pl,Case=Inst}), we may not distinguish
// 	   between "rekami" and "rekoma"
//	2) If the lemma is missing we may not lemmatize the form (unless the paradigm allows to do that on the basis of the 3 elements)
//	3) If the inflection paradigm is missing we may not produce other inflected forms.
//	4) If the inflection deatures are missing the may be an ambiguity in case of homographs e.g. "rece" may be both
//	   {Gen=fem,Nb=pl,Case=Nom} and {Gen=fem,Nb=pl,Case=Acc}
