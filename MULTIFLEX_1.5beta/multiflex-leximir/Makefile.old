#
# This is the multi-system makefile of Unitex (http://igm.univ-mlv.fr/~unitex/)
# Compilation under Windows is supposed to be done with Dev-C++. To compile under
# Windows, type:
#
#    make SYSTEM=windows
#
# If you don't want to use the TRE library, use the 'USE_TRE_LIBRARY=no' parameter:
#
#    make USE_TRE_LIBRARY=no
#
# By default, 'make' will compile Unitex for Linux, using the TRE library
#
SYSTEM = linux-like

#Modification by A. Savary
#USE_TRE_LIBRARY = yes
USE_TRE_LIBRARY = no
DATE = `date +%d%m%Y`
#End of modification by A. Savary


ifeq ($(SYSTEM),linux-like)
#
#
# Here are the makefile instructions used to compile Unitex under a Linux-like system.
#
#
#
#Modified by A. Savary (-g option added)
CC = g++ -g
COREDIR = ../multiflex-core #Directory of multiflex core modules (indendent of the underlying module for simple words)
#End of modification by A. Savary
RM      = rm -f
INSTALL = cp

NAME    = unitex-src
VERSION = $(shell date +"%Y-%m-%d")
BUILD   = $(NAME)-$(VERSION)


#
# Compiling instructions for the TRE library
#
# (by default, the TRE library is used)
#
INSTALL_COMMAND = make USE_TRE_LIBRARY=yes install

ifeq  ($(USE_TRE_LIBRARY),yes)
TRE_SRC = $(PWD)/../tre-0.7.2
TRE_TGZ = $(PWD)/../tre-0.7.2.tar.gz
TRE_TEMP = $(TRE_SRC)/TEMP
INSTALL_COMMAND = make install
endif


# APPDIR = where to install bins

APPDIR = ../../App

ifeq ($(USE_TRE_LIBRARY),yes)
DEFFLAGS = -D_NOT_UNDER_WINDOWS
CFLAGS   = -Wall -W $(DEFFLAGS) -Wno-write-strings -Ilibtre/include
LIBTRE   = libtre
CLEAN-LIBTRE   = clean-libtre
#Modified by A.Savary for Multiflex-Morfeusz interface
LIBS   = -Llibtre/lib -ltre
#LIBS   = -Llibtre/lib -ltre -L./ -lmorfeusz-gen 
#LIBS   = -Llibtre/lib -ltre -L./ -lmorfeusz-gen -lmorfeusz
#End of modification by A.Savary for Multiflex-Morfeusz interface


else
DEFFLAGS = -D_NOT_UNDER_WINDOWS -DDO_NOT_USE_TRE_LIBRARY
CFLAGS   = -Wall -W $(DEFFLAGS) -Wno-write-strings
LIBTRE   = 
CLEAN-LIBTRE   = 
#Modified by A.Savary for Multiflex-Morfeusz interface
LIBS   =
#LIBS   = -L./ -lmorfeusz-gen
#LIBS   = -L./ -lmorfeusz-gen -lmorfeusz
#End of modification by A.Savary for Multiflex-Morfeusz interface
endif


OPTIONS = $(CFLAGS) $(LDFLAGS) $(LIBS)
EXTENSION = 

#
#
# End of makefile instructions for Linux-like systems
#
#

else
#
#
# Here are makefile instructions for compiling with Dev-C++ under Windows
#
#
CC   = g++.exe -g

ifeq ($(USE_TRE_LIBRARY),yes)

LIBS =  -L"C:/Dev-Cpp/lib" -L"C:/C++/Libtre" Libtre/tre.lib 
INCS =  -I"C:/Dev-Cpp/include" -I"C:/C++/Libtre" 
CXXINCS =  -I"C:/Dev-Cpp/include/c++"  -I"C:/Dev-Cpp/include/c++/mingw32"  -I"C:/Dev-Cpp/include/c++/backward"  -I"C:/Dev-Cpp/include"  -I"C:/C++/Libtre" 
CXXFLAGS = $(CXXINCS)  -Wall -Wno-write-strings
CFLAGS = $(INCS)  -Wall -Wno-write-strings

else
#Modification by A. Savary
#LIBS =  -L"C:/Dev-Cpp/lib"
#INCS =  -I"C:/Dev-Cpp/include"
#CXXINCS =  -I"C:/Dev-Cpp/include/c++"  -I"C:/Dev-Cpp/include/c++/mingw32"  -I"C:/Dev-Cpp/include/c++/backward"  #-I"C:/Dev-Cpp/include"
#LIBS =  -L"C:/MinGW/lib" -liconv -ldxerr9 -L./ -lmorfeusz -lmorfeusz-gen
#INCS =  -I"C:/MinGW/include"
#CXXINCS =  -I"C:/Dev-Cpp/include/c++"  -I"C:/Dev-Cpp/include/c++/mingw32"  -I"C:/Dev-Cpp/include/c++/backward"  -I"C:/Dev-Cpp/include"
#End of modification by A. Savary

#CXXFLAGS = $(CXXINCS)  -Wall -Wno-write-strings
CFLAGS = $(INCS)  -Wall -Wno-write-strings -D_NOT_UNDER_WINDOWS -DDO_NOT_USE_TRE_LIBRARY

endif



OPTIONS = $(LIBS) $(CFLAGS) $(LDFLAGS)
EXTENSION = .exe


#
#
# End of makefile instructions for Windows systems
#
#
endif


MULTIFLEX	= multiflex-leximir
MULTIFLEX_OBJS= Alphabet.o Error.o File.o Fst2.o MF_Global2.o IOBuffer.o List_int.o List_ustring.o \
   MF_Copyright.o MF_CurrentForms.o MF_DLC_inflect.o MF_FeatStruct.o MF_FeatStructStr.o \
   MF_FormExtras.o MF_GraphDebug.o  MF_Inflect.o MF_Initialism.o \
   MF_LangMorpho.o MF_LetterCase.o MF_MorphoEquiv.o MF_MU_embedded.o MF_MU_graph.o \
   MF_MU_graph_explore.o MF_MU_graph_label.o MF_SU_morpho.o MF_Tokenize.o MF_Unif.o \
   MF_UnitMorpho.o MF_Util.o Pattern.o String_hash.o StringParsing.o Transitions.o Unicode.o

#For testing purposes: MF_Test.o replaces MF_Inflect.o
#MULTIFLEX = test
#MULTIFLEX_OBJS= Alphabet.o Error.o File.o Fst2.o IOBuffer.o List_int.o List_ustring.o MF_Copyright.o MF_CurrentForms.o MF_DLC_inflect.o MF_FeatStruct.o MF_FeatStructStr.o MF_FormExtras.o MF_GraphDebug.o  MF_Inflect.o MF_InflectTransd.o MF_Initialism.o MF_LangMorpho.o MF_LetterCase.o MF_MorphoEquiv.o MF_MU_embedded.o MF_MU_graph.o MF_MU_graph_explore.o MF_MU_graph_label.o MF_SU_morpho.o MF_Test.o MF_Tokenize.o MF_Unif.o MF_UnitMorpho.o MF_Util.o Pattern.o String_hash.o StringParsing.o Transitions.o Unicode.o 


#For testing purposes: 
#MULTIFLEX_OBJS= Error.o  Unicode.o MF_Test.o


PROGS =   $(MULTIFLEX)

OBJS  = $(MULTIFLEX_OBJS)

DIR = ../../MULTIFLEX_1.4beta
DATE = `date +"%d%m%Y"`


ifeq ($(SYSTEM),linux-like)
#
#
# Here are the makefile targets used to compile Unitex under a Linux-like system.
#
#

all: bin
	@echo
	@echo "all is done." #Type '$(INSTALL_COMMAND)' to install programs in '$(APPDIR)'."
	@echo

bin: $(LIBTRE) $(PROGS)

%.o: %.cpp
	$(CC) -c $(CFLAGS) $<

.libtre-done:
	touch .libtre-done


$(TRE_SRC): $(TRE_TGZ)
	@echo "unpacking TRE library ..."
	cd .. && (cat $(TRE_TGZ) | gunzip | tar xf -)

libtre: .libtre-done $(TRE_SRC)
	@echo "Making TRE library ..."
	chmod +x $(TRE_SRC)/configure
	cd $(TRE_SRC) && ./configure --disable-shared --enable-static && make
	mkdir -p libtre/lib libtre/include
	cd $(TRE_SRC)/lib && ar rcs libtre.a *.o
	cp $(TRE_SRC)/lib/libtre.a  libtre/lib/libtre.a

clean-libtre:
	rm -rf libtre
	rm -rf $(TRE_SRC)
	rm -f .libtre-done

#install: bin
#	$(INSTALL) $(PROGS) $(APPDIR)


clean: $(CLEAN-LIBTRE)
	rm -f $(OBJS) $(PROGS) *~

dist: clean
	if test -d $(BUILD); then rm -rf $(BUILD); fi
	mkdir $(BUILD)
	cp -a $(shell find . -mindepth 1 -maxdepth 1 -name $(BUILD) -prune -o -print) $(BUILD)
	tar cf - $(BUILD)  | gzip > ../$(BUILD).tar.gz
	rm -rf $(BUILD)

zip: clean
	if test -d $(BUILD); then rm -rf $(BUILD); fi
	mkdir $(BUILD)
	cp -a $(shell find . -mindepth 1 -maxdepth 1 -name $(BUILD) -prune -o -print) $(BUILD)
	zip -r -q ../$(BUILD).zip $(BUILD)
	rm -rf $(BUILD)

remake: clean all

archive:
	tar -cvzf ../../Archives/Linux/MULTIFLEX_1.4beta.$(DATE).tar.gz  $(DIR)/multiflex-leximir/*.h $(DIR)/multiflex-leximir/*.cc $(DIR)/multiflex-leximir/*.cpp $(DIR)/multiflex-leximir/Makefile $(DIR)/multiflex-core/*.h $(DIR)/multiflex-core/*.cpp $(DIR)/ling/ $(DIR)/scripts/
	cp ../../Archives/Linux/MULTIFLEX_1.4beta.$(DATE).tar.gz /media/disk/recherche/MULTIFLEX/Archives/Linux/

webarchive:
	tar -cvzf ../../Archives/Linux/MULTIFLEX_1.4beta.$(DATE).web.tar.gz  $(DIR)/multiflex-morfeusz/*.h $(DIR)/multiflex-morfeusz/*.cc $(DIR)/multiflex-morfeusz/*.cpp $(DIR)/multiflex-morfeusz/*morfeusz* $(DIR)/multiflex-morfeusz/Makefile $(DIR)/multiflex-core/*.h $(DIR)/multiflex-core/*.cpp $(DIR)/scripts/
	cp ../../Archives/Linux/MULTIFLEX_1.4beta.$(DATE).web.tar.gz /media/disk/recherche/MULTIFLEX/Archives/Linux/


#	@echo ../archives/MULTIFLEX_1.3beta.$(DATE).tar.gz
#	tar -cvzf ../archives/MULTIFLEX_1.3beta.$(DATE).tar.gz ../ling ../multiflex-core/ ./ ../scripts
#	cp  ../archives/MULTIFLEX_1.3beta.$(DATE).tar.gz /media/disk/recherche/MULTIFLEX/Archives/Linux/

#
#
# End of makefile instructions for Linux-like systems
#
#
else
#
#
# Here are the makefile targets used to compile Unitex under a Linux-like system.
#
#

all: bin

bin: $(PROGS)

%.o: %.cpp
	$(CC) -c $(CFLAGS) $<

clean: 
	rm -f $(OBJS) *.exe *~


#
#
# End of makefile instructions for Windows systems
#
#
endif

$(MULTIFLEX): $(MULTIFLEX_OBJS)
	$(CC) -o $(MULTIFLEX)$(EXTENSION) $(MULTIFLEX_OBJS) $(OPTIONS)

