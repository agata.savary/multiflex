/*
  * Multiflex for Morfeusz - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

/********************************************************************************/
/* INFLECTION OF A DELAC FILE INTO A DELACF                                     */
/********************************************************************************/

#ifndef DlcInflectH
#define DlcInflectH

#include "Unicode.h"
#include "MF_LangMorpho.h"
#include "MF_UnitMorpho.h"

/////////////////////////////////////////////////
//Maximum number of semantic or syntactic codes
#define MAX_CODES 20

/////////////////////////////////////////////////
//Maximum length of a DELAC line
#define MAX_DLC_LINE 500
//Maximum length of a DELACF line
#define MAX_DLCF_LINE 500

//////////////////////////////////////////////////////
// Separators in a DELAC entry e.g. civilni(civilni.A2:adms1g) {vojni rok}(vojni rok.NC_AXN:ms1q),NC_AXN
//Separator marking the beginning of a class
//#define CL_BEG (unichar) ','
//Separator marking the end of a class
//#define CL_END (unichar) '('
//Separator marking the beginning of a paradigm
#define PARA_BEG (unichar) ','
//Separator marking the end of a paradigm
//#define PARA_END (unichar) ')'
//Separator marking the beginning of semantic or syntactic codes
#define CODE_BEG (unichar) '+'
//Separator marking the beginning of a comment
#define COMMENT_BEG (unichar) '/'

//Separators marking the beginning and the end of a compound unit in an entry
#define CMPND_BEG (unichar) '{'
#define CMPND_END (unichar) '}'

//Separator marking the beginning of a constituent's annotation, e.g. (civilni.A2:adms1g)
#define CONSTIT_ANNOT_BEG (unichar) '('
//Separator marking the end of a constituent's annotation
#define CONSTIT_ANNOT_END (unichar) ')'
//Separator marking the beginning of inflection class in a constituent's annotation
//#define CONSTIT_CAT_BEG (unichar) '.'
//Separator marking the beginning of paradigm (homonym) number in a constituent's annotation
#define CONSTIT_PARA_BEG (unichar) '.'
//Separator marking the end of paradigm (homonym) number in a constituent's annotation
#define CONSTIT_PARA_END (unichar) ':'
//Separator marking the beginning of class in a constituent's annotation
#define CONSTIT_CL_BEG (unichar) ':'
//Separator marking the end of class in a constituent's annotation
#define CONSTIT_CL_END (unichar) ':'
//Separator marking the beginning of inflection features in a constituent's annotation
#define CONSTIT_FEAT_BEG (unichar) ':'
//Separator marking the end of inflection features in a constituent's annotation
#define CONSTIT_FEAT_END (unichar) ')'

//////////////////////////////////////////////////////
// Separators in a DELACF inflected form
//Separator marking the begining of a lemma
#define DLCF_LEMMA_BEG (unichar) ','
//Separator marking the begining of category
#define DLCF_CL_BEG (unichar) '.'
//Separator marking the begining of a semantic or syntactic code
#define DLCF_CODE_BEG (unichar) '+'
//Separator marking the begining of a comment
#define DLCF_COMMENT_BEG (unichar) '/'
//Separator marking the begining of extras
#define DLCF_EXTRAS_BEG (unichar) '\n'

////////////////////////////////////////////
// A DELAC entry.
typedef struct {
  U_lemma_T* lemma;           //lemma, with its class (e.g. noun)  and paradigm (e.g. "NC-NXX41")
  unichar* codes[MAX_CODES];   //semantic or syntactic codes, e.g. ("Hum","z1"), possibly void
  unichar* comment;            //e.g. "electricity", possibly void
} DLC_entry_T;

/////////////////////////////////////////////////////////////////////////////////
// Initializes a DELAC entry
void init_DLC_entry(DLC_entry_T* entry);

/////////////////////////////////////////////////////////////////////////////////
// Converts a DELAC line ('line') into a structured DELAC entry ('entry').
// 'line' is non terminated by a newline.
// 'embedded' = Boolean saying if constituents' lemmas should be searched for in embedded DLC files
// Morfeusz DELAC line example:
//  pranie(pranie.0:subst:sg:nom:n) mozgu(mozg.0:subst:sg:gen:m3).NC_CXC1
// If the code is 0 (see above then it may be omitted)
// Initially, 'entry' has its space allocated but is empty.
// Returns 1 if 'line' is empty, -1 if its format is incorrect, 0 otherwise. 
int DLC_line2entry(unichar* line, DLC_entry_T* entry, int embedded);

/////////////////////////////////////////////////////////////////////////////////
// Inflects a DELAC into a DELACF.
// On error returns 1, 0 otherwise.
int DLC_inflect(char* DLC, char* DLCF);

/////////////////////////////////////////////////////////////////////////////////
// Prints a DELAC entry.
int DLC_print_entry(DLC_entry_T* entry);

/////////////////////////////////////////////////////////////////////////////////
// Liberates the memory allocated for a DELAC entry.
// The main structure is not liberated
void DLC_delete_entry(DLC_entry_T* entry);

////////////////////////////////////////////
// Scans a constituent's lemma string, starting from position 'pos' 
// in the input line 'line' of a DELAC entry, 
// and puts it to 'lemma_str'.
// '*lemma_str' is allocated but empty
// Returns the length of the scanned sequence.
// Returns -1 on format problems
int DLC_get_constit_lemma_str(unichar* line, int pos, unichar** lemma_str);

////////////////////////////////////////////
// Scans the inflection paradigm code, starting from position 'pos' 
// in the input line 'line' of a DELAC entry, 
// and puts it to 'para'.
// '*paradigm' is allocated but empty
// If 'unit_or_entry' is 0 then the paradigm appears in a unit's annotation
// (i.e. it is possibly empty, or preceded by a CONSTIT_PARA_BEG and followed by a CONSTIT_PARA_END)
// If 'unit_or_entry' is 1 then the paradigm appears in a DELAC entry's annotation
// (i.e. preceded by a PARA_BEG and followed by a PARA_END).
// Returns the length of the scanned sequence.
// Returns -1 on format problems
int DLC_get_paradigm(unichar* line, int pos, char** paradigm, int unit_or_entry);

////////////////////////////////////////////
// Scans the inflection class, starting from position 'pos' 
// in the input line 'line' of a DELAC entry, 
// and puts it to 'cl'.
// 'cl' is not allocated before the function call
// On return '*cl' points to a global structure that 
// should not be liberated until the end of programme
// If 'unit_or_entry' is 0 then the class appears in a unit's annotation
// (i.e. preceded by CONSTIT_CL_BEG and followed by a CONSTIT_CL_END)
// If 'unit_or_entry' is 1 then the class appears in a DELAC entry's annotation
// (i.e. preceded by a CL_BEG and followed by a CL_END or by end of line).
// Returns the length of the scanned sequence.
// Returns -1 on format problems
int DLC_get_class(unichar* line, int pos, l_class_T** cl, int unit_or_entry);

#endif
