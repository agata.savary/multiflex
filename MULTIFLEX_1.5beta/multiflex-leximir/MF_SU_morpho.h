/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */
/* This file contains parts of the Unitex software distributed under the terms of LGPL.   */
/* It is a modified and extended version of Inflect.cpp file of the the Unitex 1.2 beta   */
/* version downloaded from http://www-igm.univ-mlv.fr/~unitex/ on June 6, 2005.           */ 
/* The modifications has been done by Agata Savary (agata.savary@univ-tours.fr).          */
/* Latest modification on January, 2009.                                                  */


/********************************************************************************/
/* MORPHOLOGY AND INFLECTION OF A SIMPLE WORD                                   */
/********************************************************************************/

#ifndef SU_morphoH
#define SU_morphoH

#include "Unicode.h"
#include "Alphabet.h"
#include "MF_FeatStruct.h"
#include "MF_Global2.h"

////////////////////////////////////////////
// For a given single unit, generates all the inflected forms corresponding to the given inflection features 'feat'.
// The forms generated are put into 'forms' (which has its space allocated and initialized)
// params = "global" parameters
// CURRENT_FORMS = inflected forms of the current MWU generated up till now
// lemma:  the unit's lemma
// desired_features: feature structure of the desired forms, e.g. {Gen=fem, Case=Inst}, or {} (if separator)
// forms: return parameter; set of the inflected forms corresponding to the given inflection features
//        e.g. (3,{[reka,{Gen=fem,Nb=sing,Case=Instr}],[rekami,{Gen=fem,Nb=pl,Case=Instr}],[rekoma,{Gen=fem,Nb=pl,Case=Instr}]})
//        or   (1,{["-",{}]})
// If feat is NULL, all inflected forms are to be generated
// If feat is [0,NULL] no inflected form is to be generated
// Returns 0 on success, 1 otherwise.   
int SU_inflect(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_lemma_T* lemma,f_feat_struct_T* desired_features, U_forms_T* forms);

////////////////////////////////////////////
// COMMENT CONCERNING THE UNIQUE IDENTIFICATION OF SINGLE WORD FORMS
//
// In order to uniquely identify a word form four elements are necessary:
//	- the form  (e.g. "rekami")
//	- its lemma (e.g. "reka")
//	- its inflection paradigm (e.g. N56)
//	- its inflection features (e.g. {Gen=fem,Nb=pl,Case=Inst}
// Note that omitting one of these elements may introduce ambiguities from the morphological point of view.
// Examples :
//	1) If the form itself is missing, an ambiguity may exist between variants corresponding to the same inflection features
//	   e.g. given only the lemma "reka", the paradigm N56, and the features {Gen=fem,Nb=pl,Case=Inst}), we may not distinguish
// 	   between "rekami" and "rekoma"
//	2) If the lemma is missing we may not lemmatize the form (unless the paradigm allows to do that on the basis of the 3 elements).
//         A certain form may be identical for two different lemmas.  
//	3) If the inflection paradigm is missing we may not produce other inflected forms.
//	4) If the inflection features are missing there may be an ambiguity in case of homographs e.g. "rece" may be both
//	   {Gen=fem,Nb=pl,Case=Nom} and {Gen=fem,Nb=pl,Case=Acc}
//
// A UNIQUE identification of a form in the set of all inflected forms
#endif
