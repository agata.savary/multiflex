/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2009 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */
/* This file contains parts of the Unitex software distributed under the terms of LGPL.   */
/* It is a modified and extended version of Inflect.cpp file of the the Unitex 1.2 beta   */
/* version downloaded from http://www-igm.univ-mlv.fr/~unitex/ on June 6, 2005.           */ 
/* The modifications has been done by Agata Savary (agata.savary@univ-tours.fr).          */
/* Latest modification on January 2009.                                                  */


/********************************************************************************/
/* TOKENIZATION OF A UNICODE STRING                                             */
/********************************************************************************/

#ifndef TokenizeH
#define TokenizeH

#include "Unicode.h"
#include "MF_Global2.h"

//Despecialisation character
#define DESP (unichar) '\\'

////////////////////////////////////////////
// A tokenization of a string is a (unique) sequence of strings
typedef struct {
  unichar** tokens;  //List of tokens
  int nb_tokens;     //Number of tokens
} _tokenization;

typedef _tokenization tokenization;

////////////////////////////////////////////
// Divides a string into tokens
// 'params' = "global" parameters
// 's' = string to be tokenized
// If "eliminate_bcksl" is set to 1 each protecting backslash is omitted in the
// copied sequence.                                                            
// Returns the tokenization os 's' (NULL in case of errors)
// The resulting structure should be deleted by the calling function by 'delete_tokenization'
tokenization* tokenize(MultiFlexParameters* params,unichar* s, int eliminate_bcksl);

////////////////////////////////////////////
// Gets next unit from the input line 'line' and puts it to 'unit'.
// 'params' = "global" parameters
// 'unit' must have its space allocated
// 'max' is the maximum length of the copied sequence
// This is the essential function defining the segmentation
// of a text into units.
// If "eliminate_bcksl" is set to 1 each protecting backslash is omitted in the
// copied sequence.                                                            
// Returns the length of the scanned sequence.
int get_unit(MultiFlexParameters* params,unichar* unit,unichar* line, int max, int eliminate_bcksl);

////////////////////////////////////////////
// Deletes a tokenization
// The main structure is liberated
void delete_tokenization(tokenization* t);

#endif
