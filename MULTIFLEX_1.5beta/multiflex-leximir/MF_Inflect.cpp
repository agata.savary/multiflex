/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

/********************************************************************************/
/* MAIN MODULE FOR RUNNING THE DELAC-INTO-DELACF INFLECTION                     */
/********************************************************************************/


#include <stdio.h>
#include <string.h>
#include "IOBuffer.h"  //Required by Unitex "golden rules"
#include "Alphabet.h"
#include "Error.h"
#include "File.h"
#include "MF_Copyright.h"
#include "MF_DLC_inflect.h"
#include "MF_LangMorpho.h"
#include "MF_MorphoEquiv.h"
#include "MF_MU_graph.h"
#include "MF_LetterCase.h"
#include "MF_MU_embedded.h"
//#include "MF_Convert.h"
#include "MF_SU_morpho.h"
#include "MF_UnitMorpho.h"
#include "MF_Global2.h"
#include "MF_LangMorpho.h"


//Error code indicating loading status of configuration files 'Morphology' and 'Equivalences' if any.
//extern int config_files_status=CONFIG_FILES_OK;

void usage();
int init_morphology(MultiFlexParameters* params, char* dir);
int init_alphabet(MultiFlexParameters* params, char* alph_file);
int init_equivalences(MultiFlexParameters* params, char* dir);
int init_transducers(MultiFlexParameters* params, char* dir);


/////////////////////////////////// 
// Inflection of a DELAC to a DELACF
// argv[1] = optional -d option
// argv[1] or argv[2] = path and name of the DELAC file
// argv[2] or argv[3] = path and name of the DELACF file
// argv[3] or argv[4] = path and name of the alphabet file
// argv[4] or argv[5] = path and name of the inflection directory 
//           containing the 'Morphology' file and the inflection transducers
//           for simple and compound words
// argv[5] or argv[6], 
// argv[6] or argv[7], 
// etc.    = 0 or more DELAC files containing compounds that can be constituents
//           of those to be inflected (in argv[1]), these files are not
//           inflected themselves
// If any problem occurs, returns 1. Otherwise returns 0.
int main(int argc, char* argv[]) {
  int err;  //0 if a function completes with no error 
  int arg_shift; //argument index shift caused by the appearence or no of the '-d' option

  setBufferMode();  //Required by Unitex "golden rules"
  
  if (argc < 5) {
    usage();
    return 0;
  }

  MultiFlexParameters* params=new_MultiFlexParameters();

  //Check if the -d option is present
  if (!strcmp(argv[1], "-d")) {
    arg_shift = 1;
    params->debug_status = GRAPH_DEBUG;
  }
  else {
    arg_shift = 0;
    params->debug_status = GRAPH_NO_DEBUG;
  }

  //Load the morphology description, the alphabet, the equivalence files, 
  //the structure for inflection transducers, a
  //and the list of DELAC files containing embedded compounds
  if (init_morphology(params, argv[4+arg_shift]))
      return 1;
  //  print_language_morpho();

  if (init_alphabet(params, argv[3+arg_shift]))
      return 1;
  if (init_equivalences(params, argv[4+arg_shift]))
      return 1;
  if (init_transducers(params, argv[4+arg_shift]))
    return 1;
  if (init_embedded_dlc(params, argv+5+arg_shift, argc-arg_shift-5))
    return 1;
  //  if (init_convert()) //Initialize the conversion descriptors
  //    return 1;
  
  initEmbeddingLevel(params);


  //DELAC inflection
  err = DLC_inflect(params, argv[1+arg_shift],argv[2+arg_shift]);
  if (err)
    u_printf("Problems occured while inflecting a DELAC.\n");
  else 
    u_printf("Done.\n");
    
  //Liberate the structures needed for the DELAC inflection
  free_MultiFlexParameters(params);
  return 0;
}

//////////////////////////////////
// Usage note shown if the number of command line arguments is incorrect (<5) 
void usage() {
  u_printf("%S",COPYRIGHT);
  u_printf("Usage: DlcInflect <delac> <delacf> <alpha> <dir>\n");
  u_printf("     <delac> : the unicode DELAC file to be inflected\n");
  u_printf("     <delacf> : the unicode resulting DELACF dictionary \n");
  u_printf("     <alpha> : the alphabet file \n");
  u_printf("     <dir> : the directory containing 'Morphology' file and \n");
  u_printf("             inflection graphs for single and compound words.\n");
  u_printf("\nInflects a DELAC into a DELACF.\n");
}

/////////////////////////////////// 
// Loads the Morphology.txt file
// containing the morphological model of the language
// (i.e. the list of inflectional classes, categories and values).
// 'params' = "global" parameters
// dir = directory containing this file
// Checks if the letter case category and values have correct labels
// If any problem occurs, returns 1. Otherwise returns 0.
int init_morphology(MultiFlexParameters* params, char* dir) {
  char morphology[FILENAME_MAX];
  new_file(dir,"Morphology.txt",morphology);
  if(read_language_morpho(params,morphology)) {
    free_language_morpho(params);
    params->config_files_status=CONFIG_FILES_ERROR;
    return 1;
  }
  //  print_language_morpho();
  return 0;
}

/////////////////////////////////// 
// Loads the alphabet file 'alph_file'
// 'params' = "global" parameters
// If any problem occurs, returns 1. Otherwise returns 0.
int init_alphabet(MultiFlexParameters* params, char* alph_file) {
  params->alph=load_alphabet(alph_file);  //To be done once at the beginning of the inflection
  if (params->alph==NULL) {
    error("Cannot open alphabet file %s\n",alph_file);
    free_language_morpho(params);
    return 1;
  }
  return 0;
}

/////////////////////////////////// 
// Loads the Equivalences.txt file if any
// containing the equivalences between the morphological
// values for simple words and those for compounds.
// 'params' = "global" parameters
// dir = directory containing this file
// If any problem occurs, returns 1. Otherwise returns 0.
int init_equivalences(MultiFlexParameters* params, char* dir) {
  char equivalences[FILENAME_MAX];
  int err; //Error code
  new_file(dir,"Equivalences.txt",equivalences);
  err = init_morpho_equiv(params,equivalences);
  if (err) {
    free_language_morpho(params);
    params->config_files_status=CONFIG_FILES_ERROR;
    return 1;
  }
  //  print_morpho_equiv();
  
  init_class_equiv(params);
  return 0;
}

/////////////////////////////////// 
// Initializes the structure of inflection transducers
// 'params' = "global" parameters
// dir = directory containing this file
// If any problem occurs, returns 1. Otherwise returns 0.
int init_transducers(MultiFlexParameters* params, char* dir) {
  strcpy(params->inflection_directory,dir);
  return 0;
}

