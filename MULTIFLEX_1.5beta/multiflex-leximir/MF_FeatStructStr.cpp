/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

/**********************************************************************************/
/* CONVERTING FEATURE STRUCTURES INTO SURFACE FEATURE STRINGS AND VICE VERSA      */
/* (ACCORDING TO MORPHOLOGICAL EQUIVALENCES BETWEEN SIMPLES WORDS AND MULTIFLEX), */
/* e.g {<Gen=fem;Case=Inst;Nb=sing>,<Gen=fem;Case=Loc;Nb=sing>} <=> fIs:fLs       */
/**********************************************************************************/

#include <stdio.h>
#include "MF_LangMorpho.h"
#include "MF_FeatStruct.h"
#include "MF_FeatStructStr.h"
#include "MF_Util.h"
#include "Error.h"
#include "MF_MorphoEquiv.h"
#include "Unicode.h"
#include "MF_Global2.h"

//Separator of substrings of morphological features (e.g. a colon in ":mIp:mLp")
static unichar FEAT_SUBSTR_SEP = (unichar) ':';

f_feat_struct_set_T* get_feat_struct_set_substr(MultiFlexParameters* params,unichar* feat_str);
f_catval_T* get_one_catval_str(MultiFlexParameters* params,unichar* feat);
unichar* get_one_str_catval(MultiFlexParameters* params,f_catval_T* feat);
l_class_T* get_class_str(MultiFlexParameters* params,unichar* cl_str);
void delete_catvals(f_catval_T** catvals);

/****************************************************************************************/
/* "get a feature structure set (i.e. a set of category-value sets) from a string"      */
/* Produces a set of feature structures,                                                */
/* {<Gen=masc;Case=Inst;Nb=pl>,<Gen=masc;Case=Loc;Nb=pl>}                               */
/* from a feature string (e.g. ":mIp:mLp").                                             */
/* A feature string is a sequence of feature substrings separated by FEAT_SUBST_SEP.    */
/* See get_feat_struct_set_substr for the format of a feature substring.                */
/* If the feature string has a bad format or if a string component is not equivalent    */
/* to a morphological feature or in case of any other problem the function returns NULL */
/* The return structure is allocated in the function. The liberation has to take place  */
/* in the calling function (by f_delete_feat_struct_set).                               */
/* 'params' = "global" parameters                                                       */
f_feat_struct_set_T* get_feat_struct_set_str(MultiFlexParameters* params,unichar* feat_str) {
  //If the feature string is void so is the feature structure
  if (!feat_str)
    return NULL;

  int err; //Error code

  f_feat_struct_set_T* feat_struct_set;  //Set of all feature structures to be returned
  f_create_feat_struct_set(&feat_struct_set);  //Allocate the memory for the set of features
  f_init_feat_struct_set(feat_struct_set);
  f_feat_struct_set_T* new_feat_struct_set;  //Temporary set of extended feature structures

  //Convert each alternative feature string into a feature structure set
  int l;   //Length of a scanned sequence
  unichar feat_substr[MAX_MORPHO_TAG];  //buffer for an individual feature substring scanned from feat_str
  int done = 0; //Boolean indicating if the feature string is totally scanned

  while (!done)
    //The feature string must start with a ':'
    if (feat_str[0] != FEAT_SUBSTR_SEP)  //If a new alternative present
      done = 1;
    else {
      feat_str++; //Omit the substring separator (e.g. ':')

      //Extract a feature substring 
      unichar SEP_TAB[2] = {FEAT_SUBSTR_SEP, (unichar) '\0'};
      l = u_scan_until(feat_substr,feat_str,MAX_MORPHO_TAG-1,SEP_TAB,1); //Read the value substring e.g. "mIp"
      if (l==0) {
	error("Bad morphological tag :%S.\n",feat_substr);
	return NULL;
      }
      
      //Get a feature structure set from the tag
      new_feat_struct_set = get_feat_struct_set_substr(params,feat_substr);
      if (!new_feat_struct_set) {
	f_delete_feat_struct_set(&feat_struct_set);
	return NULL;
      }
      
      //Copy all structures obtained from the current substring to the previous structures
      err = f_merge_feat_struct_sets(feat_struct_set,new_feat_struct_set);
      if (err) {
	f_delete_feat_struct_set(&feat_struct_set);
	f_delete_feat_struct_set(&new_feat_struct_set);
	return NULL;
      }
      
      //Delete the new structures (which have been dublicated and added to feat_struct_set)
      f_delete_feat_struct_set(&new_feat_struct_set);    
      
      //Go to the next subsequence
      feat_str = feat_str + l; 
    }
  
  return feat_struct_set;
  }

/****************************************************************************************/
/* "get a feature structure set (i.e. a set of category-value sets) from a substring"   */
/* Produces a one-element set of feature structures, e.g. {<Gen=masc;Case=Inst;Nb=pl>}  */
/* from a feature string (e.g. "mIp").                                                 */
/* A features substring is a sequence of one-character features.                        */
/* If a character in the substring is not equivalent to a morphological feature         */
/* or in case of any other problem the function returns NULL.                           */
/* The return structure is allocated in the function. The liberation has to take place  */
/* in the calling function (by f_delete_feat_struct_set).                               */
/* 'params' = "global" parameters                                                       */
f_feat_struct_set_T* get_feat_struct_set_substr(MultiFlexParameters* params,unichar* feat_str) {

  //If the feature substring is void so is the feature structure
  if (!feat_str)
    return NULL;

  int err; //Error code

  int no_catvals=0; //Number of category-value pairs
  f_catval_T* catvals=NULL;  //Set of category-value pairs obtained from feat_str
  unichar f[2];  //buffer for an individual feature character in feat_str
  int f_i; //Index of the current feature character
  f[1] = (unichar) '\0';
  f_catval_T* cv; //Current category-value pair  

  //Omit void characters at the beginning of the feature string
  unichar tmp[MAX_MORPHO_TAG];  //dummy buffer for trailing separators
  feat_str = feat_str + u_scan_while_char(tmp, feat_str, MAX_MORPHO_TAG-1," \t");

  //Get the set of category-value pairs from the substring
  for (f_i=0; f_i<u_strlen(feat_str); f_i++) {
    f[0] = feat_str[f_i];
    cv = get_one_catval_str(params,f); //Get the category-value pair equivalent to the current dictionary feature
    if (!cv) {  //If invalid feature
      delete_catvals(&catvals);
      return NULL;
    }
    catvals = (f_catval_T*)realloc(catvals,(no_catvals+1)*sizeof(f_catval_T));
    if (!catvals) {
      delete_catvals(&catvals);
      fprintf(stderr,"Memory allocation problem in function 'get_feat_struct_set_substr'!\n");
      return NULL;
    }
    catvals[no_catvals] = *cv;
    no_catvals++;
  }

  //Add the each category-value pair to the result while checking the coherence of the resulting set (two different values values of the same category, e.g. "mIpf", are not allowed)
  f_feat_struct_set_T* feat_struct_set;  //Set of (unique) feature structure to be returned
  f_create_feat_struct_set(&feat_struct_set);  //Allocate the memory for the set of features
  f_init_feat_struct_set_one_empty(feat_struct_set);
  int c; //Index of the current category-value pair
  for (c=0; c<no_catvals; c++) {
    //Add the current category-value pair to the (unique) structure in feat_struct_set. The resulting structure should be sorted.
    err = f_enlarge_feat_struct_set(params,feat_struct_set, catvals[c].cat, catvals[c].val, 1);
    if (err) {
      f_delete_feat_struct_set(&feat_struct_set);
      delete_catvals(&catvals);
      return NULL;
    }
  }
  delete_catvals(&catvals);
  return feat_struct_set;
}

/****************************************************************************************/
/* "get a feature structure (i.e. a set of category-value pairs) from a string"         */
/* Produces a feature structures e.g. {<Gen=masc;Case=Inst;Nb=pl>                       */
/* from a feature string (e.g. "mIp").                                                  */
/* A valid feature string here is a sequence of unique one-letter values.               */
/* The feature string must correspond to one feature structure only,                    */
/* e.g."mIp:mLp" is not a correct string as it correponds to 2 feature                  */
/* structures.                                                                          */
/* If the feature string has a bad format, or if it is ambiguous (corresponds to several*/
/* feature structures), or if a string component is not equivalent to a morphological   */
/* feature, or in case of any other problem the function returns NULL                   */
/* The return structure is allocated in the function. The liberation has to take place  */
/* in the calling function (by f_delete_feat_struct).                                   */
/* 'params' = "global" parameters                                                       */
f_feat_struct_T* get_feat_struct_str(MultiFlexParameters* params,unichar* feat_str) {
  f_feat_struct_set_T* fss; //Set of feature structures obtained from the string
                           //It should contain one feature structure only
  fss = get_feat_struct_set_substr(params,feat_str);

  if (!fss)
    return NULL;

  if (fss->no_feat_structs != 1) {
    f_delete_feat_struct_set(&fss);
    return NULL;
  }
  
  f_feat_struct_T* fs; //The feature structure to be returned
  f_create_feat_struct(&fs);
  f_init_feat_struct(fs);
  //Copy the (first and only) feature structure from fss to fs
  int cv; //Index of the current category-value pair
  int no_catvals = fss->feat_structs[0].no_catvals; //Number of category-value pairs to be copied
  //Allocate space for the category-value pairs
  fs->catvals = (f_catval_T*) malloc(no_catvals*sizeof(f_catval_T));
  if (!fs->catvals)
    fatal_error("Not enough memory in function 'get_feat_struct_str'!\n");
  for (cv=0; cv<no_catvals; cv++)
    fs->catvals[cv] = fss->feat_structs[0].catvals[cv];
  fs->no_catvals = no_catvals;
  f_delete_feat_struct_set(&fss);

  return fs;
}

/**************************************************************************************/
/* Produces a feature string (e.g. "fIs")) from a feature structure                   */
/* (e.g.   <Gen=fem;Case=Inst;Nb=sing>).                                              */
/* The return string is allocated in the function. The liberation has to take place   */
/* in the calling function.                                                           */
/* If 'feat' is empty or a morphological feature has no corresponding string value,   */
/* returns NULL.                                                                      */
/* 'params' = "global" parameters                                                     */
unichar* get_str_feat_struct(MultiFlexParameters* params,f_feat_struct_T* feat) {
  int f;  //index of the current category-value pair in 'feat'
  unichar* f_str; //Pointer to the current feature string

  if (!feat || (feat->no_catvals==0))
    return NULL;
  
  //Allocate and initialise the feature string
  unichar* tmp;
  tmp = (unichar*) malloc( (feat->no_catvals * (MAX_MORPHO_TAG+1) + 1 ) * sizeof(unichar));
  if (!tmp)
    fatal_error("Not enough memory in function 'get_str_feat_struct'!\n");
  tmp[0] = (unichar) '\0';
  
  for (f=0; f<feat->no_catvals; f++) {
    //Concatenante the current value if it is not empty and if it is a morphological value
    //if (!is_empty_val(feat->catvals[f].cat,feat->catvals[f].val) && (feat->catvals[f].cat->cat_type==basic)) {
    //Concatenante the current value if it is not empty
    if (!is_empty_val(feat->catvals[f].cat,feat->catvals[f].val)) {
	  f_str = get_one_str_catval(params,&(feat->catvals[f]));
	  if (!f_str) {
	    free(tmp);
	    return NULL;
	  }
	  u_strcat(tmp,f_str);
	}
   }
 return tmp;
}


/****************************************************************************************************/
/* "get one category-value pair from a string"                                                      */
/* Points at the category-value pair (e.g. <Gen=fem>) equivalent to a feature string (e.g. "f").    */
/* No structure is allocated in the function. The result points to a global structure that should   */
/* not be liberated except at the end of the programme.                                             */
/* If the string value 'feat' is empty or it has no corresponding category-value pair returns NULL. */
/* 'params' = "global" parameters                                                                   */
f_catval_T* get_one_catval_str(MultiFlexParameters* params,unichar* feat_str) {
  int e;  //index of the current equivalence
  
  if (!feat_str || !u_strlen(feat_str))
    return NULL;

  for (e=0; e<params->L_MORPHO_EQUIV.no_equiv; e++)
    if (!u_strcmp(params->L_MORPHO_EQUIV.equiv[e].SU_feat,feat_str)) {
      return &(params->L_MORPHO_EQUIV.equiv[e].MU_catval);
    }

  return NULL;  //If no equivalent feature structure found
}

/***************************************************************************************************/
/* "get one string from a category-value pair"                                                     */
/* Points at the feature string (e.g. "f") equivalent to a category-value pair (e.g. <Gen=fem>).   */
/* No string is allocated in the function. The result points to a global structure that should not */
/* be liberated except at the end of the programme.                                                */
/* If 'feat' empty or the category-value pair has no corresponding character value returns NULL.   */
/* 'params' = "global" parameters                                                                  */
unichar* get_one_str_catval(MultiFlexParameters* params,f_catval_T* feat) {
  int e; //index of the current equivalence
 
  if (!feat)
    return NULL;

  for (e=0; e<params->L_MORPHO_EQUIV.no_equiv; e++)
    if ((feat->cat == params->L_MORPHO_EQUIV.equiv[e].MU_catval.cat) && (feat->val == params->L_MORPHO_EQUIV.equiv[e].MU_catval.val) )
      return params->L_MORPHO_EQUIV.equiv[e].SU_feat;

  return NULL;
}

/**************************************************************************************/
/* Deletes the set of category-value pairs.                                           */
/* The structures pointed by each pair are global and should not be liberated         */
/* 'catvals' is transferred as a double pointer so that                               */
/* it can remain NULL after the function return                                       */
void delete_catvals(f_catval_T** catvals){
  if (catvals) {
    if (*catvals) {
      free (*catvals);
      *catvals = NULL;
    }
  }
}

/**************************************************************************************/
/* Returns the class (e.g. noun) corresponding to a class string as it appears in a   */
/* dictionary (e.g. "N"). If no class corresponds to the string, returns NULL.        */
/* The return structure is NOT allocated in the function.                             */
/* 'params' = "global" parameters                                                     */
l_class_T* get_class_str(MultiFlexParameters* params,unichar* cl_str) {
  int c;  //index of the current class in the equivalence set
  for (c=0; c<params->L_CLASS_EQUIV.no_equiv; c++)
    if (!u_strcmp(cl_str,params->L_CLASS_EQUIV.equiv[c].SU_class))
      return params->L_CLASS_EQUIV.equiv[c].MU_class;
  return NULL;
	
}

/*****************************************************************************/
/* Returns the pointer to the class (e.g. <noun,...>) directly corresponding */
/* to the class string cl_str (e.g. "noun").                                 */
/* If no class corresponds to the string returns NULL.                       */
/* The return structure is NOT allocated in the function.                    */
/* 'params' = "global" parameters                                                     */
l_class_T* get_class_str_direct(MultiFlexParameters* params,unichar* cl_str) {
  int c;  //index of the current class in the equivalence set
  for (c=0; c<params->L_CLASSES.no_classes; c++)
    if (!u_strcmp(cl_str,params->L_CLASSES.classes[c].name))
      return &(params->L_CLASSES.classes[c]);
  return NULL;
}
