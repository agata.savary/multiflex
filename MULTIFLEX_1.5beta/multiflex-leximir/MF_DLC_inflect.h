
/*
  * Multiflex for Morfeusz - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

/********************************************************************************/
/* INFLECTION OF A DELAC FILE INTO A DELACF                                     */
/********************************************************************************/

#ifndef DlcInflectH
#define DlcInflectH

#include "Unicode.h"
#include "MF_LangMorpho.h"
#include "MF_UnitMorpho.h"
#include "MF_Global2.h"

/////////////////////////////////////////////////
//Maximum number of semantic or syntactic codes
#define MAX_CODES 20

/////////////////////////////////////////////////
//Maximum length of a DELAC line
#define MAX_DLC_LINE 500
//Maximum length of a DELACF line
#define MAX_DLCF_LINE 500

//////////////////////////////////////////////////////
// Separators in a DELAC entry
//Separator marking the beginning of a class-and-paradigm, e.g. NC_NXN, N(NC_NXN)
#define CL_PARA_BEG (unichar) ','
//Separator marking the end of a class
//#define CL_END (unichar) '('
//Separator marking the beginning of a paradigm
#define PARA_BEG (unichar) '('
//Separator marking the end of a class-and-paradigm
#define CL_PARA_END (unichar) ')'
//Separator marking the beginning of semantic or syntactic codes
#define CODE_BEG (unichar) '+'
//Separator marking the beginning of a comment
#define COMMENT_BEG (unichar) '/'

//Separators marking the beginning and the end of a compound unit in an entry
#define CMPND_BEG (unichar) '{'
#define CMPND_END (unichar) '}'

//Separator marking the beginning of a constituent's annotation
#define CONSTIT_ANNOT_BEG (unichar) '('
//Separator marking the end of a constituent's annotation
#define CONSTIT_ANNOT_END (unichar) ')'
//Separator marking the beginning of inflection paradigm in a constituent's annotation (if this paradigm is separate from the class identifier, as in N(NC_NXN)
#define CONSTIT_PARA_BEG (unichar) '('
//Separator marking the end of inflection paradigm in a constituent's annotation (if this paradigm is separate from the class identifier, as in N(NC_NXN)
#define CONSTIT_PARA_END (unichar) ')'
//Separator marking the beginning of class in a constituent's annotation
#define CONSTIT_CL_PARA_BEG (unichar) '.'
//Separator marking the end of class in a constituent's annotation
#define CONSTIT_CL_PARA_END (unichar) ':'
//Separator marking the beginning of inflection features in a constituent's annotation
#define CONSTIT_FEAT_BEG (unichar) ':'
//Separator marking the end of inflection features in a constituent's annotation
#define CONSTIT_FEAT_END (unichar) ')'

//////////////////////////////////////////////////////
// Separators in a DELACF inflected form
//Separator marking the begining of a lemma
#define DLCF_LEMMA_BEG (unichar) ','
//Separator marking the begining of category
#define DLCF_CL_BEG (unichar) '.'
//Separator marking the begining of morphological features
#define DLCF_FEAT_BEG (unichar) ':'
//Separator marking the begining of a semantic or syntactic code
#define DLCF_CODE_BEG (unichar) '+'
//Separator marking the begining of a comment
#define DLCF_COMMENT_BEG (unichar) '/'
//Separator marking the begining of extras
#define DLCF_EXTRAS_BEG (unichar) '\n'

////////////////////////////////////////////
// A DELAC entry.
typedef struct {
  U_lemma_T* lemma;           //lemma, with its class (e.g. noun)  and paradigm (e.g. "NC-NXX41")
  unichar* codes[MAX_CODES];   //semantic or syntactic codes, e.g. ("Hum","z1"), possibly void
  unichar* comment;            //e.g. "electricity", possibly void
} DLC_entry_T;

/////////////////////////////////////////////////////////////////////////////////
// Initializes a DELAC entry
void init_DLC_entry(DLC_entry_T* entry);

/////////////////////////////////////////////////////////////////////////////////
// Converts a DELAC line ('line') into a structured DELAC entry ('entry').
// 'params' = "global" parameters
// 'line' is non terminated by a newline.
// 'embedded' = Boolean saying if constituents' lemmas should be searched for in embedded DLC files
// Morfeusz DELAC line example:
// vojni(vojni.A2:ms1q) rok(rok.N1298:ms1q),N(NC_AXN)
// Initially, 'entry' has its space allocated but is empty.
// Returns 1 if 'line' is empty, -1 if its format is incorrect, 0 otherwise. 
int DLC_line2entry(MultiFlexParameters* params,unichar* line, DLC_entry_T* entry, int embedded);

/////////////////////////////////////////////////////////////////////////////////
// Inflects a DELAC into a DELACF.
// 'params' = "global" parameters
// On error returns 1, 0 otherwise.
int DLC_inflect(MultiFlexParameters* params,char* DLC, char* DLCF);

/////////////////////////////////////////////////////////////////////////////////
// Prints a DELAC entry to stdin.
// If entry void or entry's lemma void returns 1, 0 otherwise.
int DLC_print_entry(DLC_entry_T* entry);

/////////////////////////////////////////////////////////////////////////////////
// Liberates the memory allocated for a DELAC entry.
// The main structure is not liberated
void DLC_delete_entry(DLC_entry_T* entry);

////////////////////////////////////////////
// Scans a constituent's lemma string, starting from position 'pos' 
// in the input line 'line' of a DELAC entry, 
// and puts it to 'lemma_str'.
// 'params' = "global" parameters
// '*lemma_str' is allocated but empty
// Returns the length of the scanned sequence.
// Returns -1 on format problems
int DLC_get_constit_lemma_str(MultiFlexParameters* params,unichar* line, int pos, unichar** lemma_str);

////////////////////////////////////////////
// Scans the inflection class and paradigm (e.g. A2, NC_NXN, N(NC_NXN)), 
// starting from position 'pos' 
// in the input line 'line' of a DELAC entry, 
// and puts it to 'cl' and 'para', respectively.
// 'cl' is not allocated before the function call
// '*paradigm' is allocated but empty
// On return '*cl' points to a global structure that 
// should not be liberated until the end of programme
// If 'unit_or_entry' is 0 then the class appears in a unit's annotation
// (i.e. preceded by CONSTIT_CL_BEG and followed by a CONSTIT_CL_END)
// If 'unit_or_entry' is 1 then the class appears in a DELAC entry's annotation
// (i.e. preceded by a CL_BEG and followed by a CL_END or by end of line).
// 'params' = "global" parameters
// Returns the length of the scanned sequence.
// Returns -1 on format problems
int DLC_get_class_and_paradigm(MultiFlexParameters* params,unichar* line, int pos, l_class_T** cl, char** paradigm, int unit_or_entry);

#endif
