/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2009 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */
/* This file contains parts of the Unitex software distributed under the terms of LGPL.   */
/* It is a modified and extended version of Inflect.cpp file of the the Unitex 1.2 beta   */
/* version downloaded from http://www-igm.univ-mlv.fr/~unitex/ on June 6, 2005.           */ 
/* The modifications has been done by Agata Savary (agata.savary@univ-tours.fr).          */
/* Latest modification on January 2009.                                                  */


/********************************************************************************/
/* TOKENIZATION OF A UNICODE STRING                                             */
/********************************************************************************/

#include "MF_Tokenize.h"
#include "Unicode.h"
#include "Alphabet.h"
#include "MF_DLC_inflect.h"
#include "Error.h"

int get_separator(MultiFlexParameters* params,unichar* unit,unichar* line, int max, int eliminate_bcksl);
int get_word(MultiFlexParameters* params,unichar* unit,unichar* line, int max, int eliminate_bcksl);
tokenization* init_tokenization();
int add_token(tokenization* t, unichar* ct);

////////////////////////////////////////////
// Divides a string into tokens
// 'params' = "global" parameters
// 's' = string to be tokenized
// If "eliminate_bcksl" is set to 1 each protecting backslash is omitted in the
// copied sequence.                                                            
// Returns the tokenization in 's' (NULL in case of errors)
// The resulting structure should be deleted by the calling function by 'delete_tokenization'
tokenization* tokenize(MultiFlexParameters* params,unichar* s, int eliminate_bcksl) {
  tokenization* t; //Tokenization to be produced
  t = init_tokenization();
  unichar* token; //Current token
  unichar tmp[MAX_DLC_LINE+1];  //Current token
  int l; //Length of the scanned sequence
  int done=0; //Boolean for controlling the loop's end

  while (!done) {
    l = get_unit(params,tmp, s, MAX_DLC_LINE, eliminate_bcksl);
    if (l > 0) {
      token = (unichar*)malloc((u_strlen(tmp)+1) * sizeof(unichar));
      if (!token)
	fatal_error("Memory allocation problem in function 'tokenize'!\n");
      u_strcpy(token, tmp);
      add_token(t, token);
      s = s+l;
    }
    else
      done = 1;
  }
  return t;   
}

////////////////////////////////////////////
// Gets next unit from the input line 'line' and puts it to 'unit'.
// 'params' = "global" parameters
// 'unit' must have its space allocated
// 'max' is the maximum length of the copied sequence
// This is the essential function defining the segmentation
// of a text into units.
// If "eliminate_bcksl" is set to 1 each protecting backslash is omitted in the
// copied sequence.
// Returns the length of the scanned sequence.
int get_unit(MultiFlexParameters* params,unichar* unit,unichar* line, int max, int eliminate_bcksl) {

  if (!line || !u_strlen(line) || (max < 1))
    return 0;

  int l=0;  //length of the scanned sequence

  //Scan a separator if it appears in the beginning of the line
  unichar sep[3]; //A separator may take two characters at most (if it is despecialised)
  l = get_separator(params,sep, line, 2, eliminate_bcksl);
  if (l > 0)
    if (l > max)
      return 0;
    else {
      u_strcpy(unit, sep);
      return l;
    }
  
  //Scan a word
  else
    l = get_word(params, unit, line, max, eliminate_bcksl);

  return l;
}

////////////////////////////////////////////
// Gets next unit from the input line 'line' and puts it to 'unit'
// provided that it is a separator.
// 'params' = "global" parameters
// 'unit' must have its space allocated
// 'max' is the maximum length of the copied sequence
// If "eliminate_bcksl" is set to 1 each protecting backslash is omitted in the
// copied sequence.
// Returns the length of the scanned sequence.
int get_separator(MultiFlexParameters* params,unichar* unit,unichar* line, int max, int eliminate_bcksl) {

  if (!line || !u_strlen(line) || (max < 1))
    return 0;

  //Scanning a despecialised separator
  if (line[0]==(unichar)'\\' && line[1] && !is_letter(line[1],params->alph))
    if (eliminate_bcksl) {
      unit[0] = line[1];
      unit[1] = '\0';
      return 2;
    }
    else {
      if (max > 1) {
	unit[0] = line[0];
	unit[1] = line[1]; 
	unit[2] = '\0'; 
	return 2;
      }
      else 
	return 0;
    }
  else
    //Scanning a non-despecialised separator
    if(line[0]!=(unichar)'\\' && line[0] && !is_letter(line[0],params->alph)) {
      unit[0] = line[0];
      unit[1] = '\0';
      return 1;      
    }
    else 
      return 0;
}
  
////////////////////////////////////////////
// Gets next unit from the input line 'line' and puts it to 'unit'
// provided that it is a word (not a separator).
// 'params' = "global" parameters
// 'unit' must have its space allocated
// 'max' is the maximum length of the copied sequence
// If "eliminate_bcksl" is set to 1 each protecting backslash is omitted in the
// copied sequence.
// Returns the length of the scanned sequence.
int get_word(MultiFlexParameters* params,unichar* unit,unichar* line, int max, int eliminate_bcksl) {

  if (!line || !u_strlen(line) || (max < 1))
    return 0;

  int l=0;  //length of the scanned sequence
  int u=0;  //index of the next element in 'unit'
  int end=0; //Boolean saying if the loop is finished
  int no_elim_bcksl=0;  //number of eliminated backslashes
  int bcksl_precedes=0;  //1 if the preceding character was a '\', 0 otherwise

  

  //////////////
  while (!end && u<max && line[l]!=(unichar)'\0') {
    //Current character is a despecialising '\'
    if ((line[l]==(unichar)'\\') && (!bcksl_precedes)) {
      bcksl_precedes = 1;
      l++;
    }
    
    else {
      //If the current character is a separator the current word is terminated
      if (!is_letter(line[l],params->alph))
	end = 1;

      //The current character is a letter is a letter
      else {                        
	if (bcksl_precedes) {
	  if (!eliminate_bcksl) {
	    unit[u++] = (unichar) '\\';
	  } else {
	    no_elim_bcksl++;
	  }
	}
	unit[u++] = line[l++];
	bcksl_precedes = 0;
      }
    }
  }

  unit[u] = (unichar) '\0';
  return u+no_elim_bcksl;
}

////////////////////////////////////////////
// Creates and initializes a tokenization
// 'nb_tokens' = number of tokens to be created
// Returns the created tokenization or NULL on errors
tokenization* init_tokenization() {
  tokenization* t = (tokenization*) malloc(sizeof(tokenization));
  if (!t)
    fatal_error("Memory allocation problem in function init_tokenization.\n");
  t->tokens = NULL;
  t->nb_tokens = 0;
  return t;
}

////////////////////////////////////////////
// Adds token 'ct' to the tokenization 't'
// The token 'ct' is not duplicated
// Returns 1 in case of errors, 0 otherwise
int add_token(tokenization* t, unichar* ct) {
  if (!t || !ct)
    return 1;
  t->tokens = (unichar**) realloc(t->tokens, (t->nb_tokens+1) * sizeof(unichar*));
  if (!t->tokens)
    fatal_error("Memory allocation problem in function 'add_token'.\n");
  t->tokens[t->nb_tokens] = ct;
  t->nb_tokens = t->nb_tokens + 1;
  return 0;
}

////////////////////////////////////////////
// Deletes a tokenization
// The main structure is liberated
void delete_tokenization(tokenization* tt) {
  if (!tt || !tt->tokens)
    return;
  int t; //Index of the current token
  for (t=0; t<tt->nb_tokens; t++)
    if (tt->tokens[t])
	free(tt->tokens[t]);
  free(tt->tokens);
  free(tt);
}
