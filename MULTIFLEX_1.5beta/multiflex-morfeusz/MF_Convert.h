/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2009 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */
/* This file contains parts of the Unitex software distributed under the terms of LGPL.   */
/* It is a modified and extended version of Inflect.cpp file of the the Unitex 1.2 beta   */
/* version downloaded from http://www-igm.univ-mlv.fr/~unitex/ on June 6, 2005.           */ 
/* The modifications has been done by Agata Savary (agata.savary@univ-tours.fr).          */
/* Latest modification on January 15, 2009.                                               */


/********************************************************************************/
/* UTF-16 TO/FROM UTF-8 CONVERSION                                              */
/********************************************************************************/

#ifndef ConvertH
#define ConvertH

#include "Unicode.h"

////////////////////////////////////////////
// Initializes the conversion descriptors (needed only for Linux)
// Returns 1 in case of errors, 0 otherwise
int init_convert();

////////////////////////////////////////////
// Deallocates the conversion descriptors (needed only for Linux)
// Returns 1 in case of errors, 0 otherwise
int free_convert();

////////////////////////////////////////////
// Converts a UTF-16 string into a UTF-8 string.
// 'utf16_s' = UTF-16 string to be converted
// The resulting UTF-8 string is created in the function
// It must be liberated by the calling function
// Returns the converted string on succes, NULL otherwise.
char* convert_utf16_to_utf8(unichar* utf16_s);

////////////////////////////////////////////
// Converts a UTF-8 string into a UTF-16 string.
// 'utf8_s' = UTF-8 string to be converted
// The resulting UTF-16 string is created in the function
// It must be liberated by the calling function
// Returns the converted string on succes, NULL otherwise.
unichar* convert_utf8_to_utf16(char* utf8_s);

#endif
