/*
  * Multiflex for Morfeusz - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

/********************************************************************************/
/* INFLECTION OF A DELAC FILE INTO A DELACF                                     */
/********************************************************************************/


#include "Unicode.h"
#include "MF_DLC_inflect.h"
#include "MF_SU_morpho.h"
#include "MF_UnitMorpho.h"
#include "MF_MU_embedded.h"
#include "MF_FeatStructStr.h"
#include "MF_Util.h"
#include "MF_LetterCase.h"
#include "MF_Tokenize.h"
#include "MF_CurrentForms.h"
#include "Error.h"

void init_DLC_entry(DLC_entry_T* entry);
int DLC_inflect(MultiFlexParameters* params, char* DLC, char* DLCF);
int DLC_line2entry(MultiFlexParameters* params, unichar* line, DLC_entry_T* entry, int embedded);
int DLC_get_lemma(MultiFlexParameters* params, unichar* line, int pos, U_lemma_T** lemma, int embedded);
int DLC_get_constit(MultiFlexParameters* params, unichar* line, int pos, U_form_id_T* c, int embedded);
int DLC_get_constit_annot(MultiFlexParameters* params, unichar* line, int pos, U_lemma_T** lemma, f_feat_struct_T** feat, int embedded);
int DLC_get_constit_feat(MultiFlexParameters* params, unichar* line, int pos, f_feat_struct_T** feat);
int DLC_get_constit_form(MultiFlexParameters* params, unichar* line, int pos, unichar* form, int max);
int DLC_get_constit_lemma_str(MultiFlexParameters* params, unichar* line, int pos, unichar** lemma_str);
int DLC_get_constit_list(MultiFlexParameters* params, unichar* line, int pos, U_forms_T* constit_list, int embedded);
int DLC_get_paradigm(unichar* line, int pos, char** paradigm, int unit_or_entry);
int DLC_get_class(MultiFlexParameters* params, unichar* line, int pos, l_class_T** cl, int unit_or_entry);
int DLC_get_codes(unichar* line, int pos, unichar** codes);
int DLC_get_comment(unichar* line, int pos, unichar** comment);
int DLC_print_entry(MultiFlexParameters* params, DLC_entry_T* entry);
void DLC_print_unit(MultiFlexParameters* params, U_form_id_T* unit);
int DLC_format_form(MultiFlexParameters* params, unichar* entry, int max, U_form_id_T f, DLC_entry_T* dlc_entry);
int DLC_protect_dlcf_string(unichar* protected_entry, int max, unichar* entry);
void DLC_print_inflected_forms(MultiFlexParameters* params, U_forms_T* MU_forms, FILE* dlcf, DLC_entry_T* dlc_entry);
void DLC_delete_entry(DLC_entry_T* entry);

/////////////////////////////////////////////////////////////////////////////////
// Initializes a DELAC entry
void init_DLC_entry(DLC_entry_T* entry) {
  if (!entry)
    return;
  entry->lemma = NULL;
  int c; //Index of the current code
  for (c=0; c<MAX_CODES; c++)
    entry->codes[c] = NULL;
  entry->comment = NULL;
}

/////////////////////////////////////////////////////////////////////////////////
// Inflect a DELAC into a DELACF.
// 'params' = "global" parameters
// On error returns 1, 0 otherwise.
int DLC_inflect(MultiFlexParameters* params, char* DLC, char* DLCF) {
  FILE *dlc, *dlcf;  //DELAC and DELACF files
  unichar dlc_line[MAX_DLC_LINE];  //current DELAC line 
  int l;  //length of the line scanned
  DLC_entry_T dlc_entry;
  U_forms_T MU_forms;  //inflected forms of the MWU
  U_init_forms(&MU_forms);
  int err1, err2; //Error codes
  
  //Open DELAC
  dlc = u_fopen(DLC, (char*) U_READ);
  if (!dlc) {
    error((char*) "Unable to open file: '%s' !\n", DLC);
    return 1;
  }
  
  //Open DELACF
  dlcf = u_fopen(DLCF, (char*) U_WRITE);
  if (!dlcf) {
    error((char*) "Unable to open file: '%s' !\n", DLCF);
    return 1;
  }
  
  //Inflect compounds only if the configuration files Morphology.txt and Equivalences.txt (if any) 
  //have been successfully loaded
  if (params->config_files_status!=CONFIG_FILES_ERROR) {
    //Inflect one entry at a time
    l = u_fgets(dlc_line,MAX_DLC_LINE-1,dlc);
    //Omit the final newline
    u_delete_newline(dlc_line);
    
    //If a line is empty the file is not necessarily finished. 
    //If the last entry has no newline, we should not skip this entry
    while (l!=EOF || !feof(dlc)) { 
      init_DLC_entry(&dlc_entry);
      err1 = DLC_line2entry(params,dlc_line,&dlc_entry,1);/* Convert a DELAC entry into the internal multi-word format */
      if (!err1) {
	//	DLC_print_entry(&dlc_entry);
	U_form_list_set_T* CURRENT_FORMS = init_current_forms(); //Initialize the register of current forms
	//Inflect the entry
	err2 = U_inflect(params,CURRENT_FORMS,dlc_entry.lemma,NULL,&MU_forms);
	free_current_forms(CURRENT_FORMS); //Delete  the register of current forms
	if (!err2) {
	  //Inform the user if no form generated 
	  if (MU_forms.no_forms == 0) 
	    error((char*) "No inflected form could be generated for:\n%S\n",dlc_line);
	  DLC_print_inflected_forms(params,&MU_forms, dlcf, &dlc_entry);
	}
	else 
	  error((char*) "The following delac entry could not be inflected:\n%S\n",dlc_line);
      }
      DLC_delete_entry(&dlc_entry);
      U_delete_forms(&MU_forms);      
      
      //Get next entry
      l = u_fgets(dlc_line,MAX_DLC_LINE-1,dlc);
      //Omit the final newline
      u_delete_newline(dlc_line);
    }
  } 
  else {  //The configuration files have not been loaded 
    error((char*) "WARNING: Compound words won't be inflected because configuration files\n");
    error((char*) "         have not been correctly loaded.\n");
  }

  u_fclose(dlc);
  u_fclose(dlcf);
  return 0;
}

/////////////////////////////////////////////////////////////////////////////////
// Converts a DELAC line ('line') into a structured DELAC entry ('entry').
// 'params' = "global" parameters
// 'line' is non terminated by a newline.
// 'embedded' = Boolean saying if constituents' lemmas should be searched for in embedded DLC files
// Morfeusz DELAC line example:
//  czysty(czysty:adj:sg:nom:m2:pos) pływak(pływak.s2:subst:sg:nom:m2),subst(NC-O_O-1)
//  OLD: pranie(pranie.0:subst:sg:nom:n) mozgu(mozg.0:subst:sg:gen:m3).NC_CXC1
// If the code is 0 (see above then it may be omitted)
// Initially, 'entry' has its space allocated and initialized (and is empty).
// Returns 1 if 'line' is empty, -1 if its format is incorrect, 0 otherwise. 
int DLC_line2entry(MultiFlexParameters* params,unichar* line, DLC_entry_T* entry, int embedded) {
  if (!line || !entry)
    return -1;

  if (!u_strlen(line))
    return 1;
  
  int pos = 0; //Current position in 'line'
  int l; //Length of the scanned sequence

  //Get the annotated lemma
  l= DLC_get_lemma(params,line, pos, &(entry->lemma),embedded);
  if (l<0) {
    DLC_delete_entry(entry);
    return -1;
  }
  pos = pos + l;
    
  //Get the semantic and syntactic codes
  l= DLC_get_codes(line, pos, entry->codes);
  if (l<0) {
    DLC_delete_entry(entry);
    return -1;
  }
  pos = pos + l;

  //Get the comment
  l = DLC_get_comment(line, pos, &(entry->comment));
  if (l<0) {
    DLC_delete_entry(entry);
    return -1;
  }
  pos = pos + l;

  //If line not finished, format error
  if (line[pos]) {
    error((char*) "Bad format in DELAC line:\n%S\n",line);
    DLC_delete_entry(entry);  //delete entry
    return -1;
  }
  return 0; 
}

/////////////////////////////////////////////////////////////////////////////////
// Gets a lemma annotation (lemma+class+paradigm) from a DELAC line into a structured lemma.
// Sample lemma in a DELAC:  czysty(czysty:adj:sg:nom:m2:pos) pływak(pływak.s2:subst:sg:nom:m2),subst(NC-O_O-1)
// 'params' = "global" parameters
// 'line' = input line, not terminated by a newline
// 'pos' = current position in the line
// 'lemma' = return structure (*lemma is allocated but empty)
// 'embedded' = Boolean saying if constituents' lemmas should be searched for in embedded DLC files
// Returns the length of the scanned sequence, -1 if line format incorrect 
int DLC_get_lemma(MultiFlexParameters* params,unichar* line, int pos, U_lemma_T** lemma, int embedded){
  if (!line || !lemma )
    return -1;

  int l; //Length of the scanned sequence
  int tot_l; //Total length of the scanned sequences
  tot_l = 0;
  
  //Allocate and initialize the lemma 
  *lemma = (U_lemma_T*)malloc(sizeof(U_lemma_T));
  if (! *lemma)
    fatal_error((char*)"Memory allocation problem in function 'DLC_get_lemma'!\n");
  U_init_lemma(*lemma,compound);

  //Get the the list of components of the lemma e.g. "czysty(czysty:adj:sg:nom:m2:pos) pływak(pływak.s2:subst:sg:nom:m2)"
  l= DLC_get_constit_list(params,line, pos, &((*lemma)->compound_lemma), embedded);
  if (l<0) {
    U_delete_lemma(*lemma,1);
    *lemma = NULL;
    return -1;
  }
  pos = pos + l;
  tot_l = tot_l + l;
  
    
  //Get the inflection class e.g. "subst"
  l= DLC_get_class(params,line, pos, &((*lemma)->cl), 1);
  if (l<0) {
    U_delete_lemma(*lemma,1);
    *lemma = NULL;
    return -1;
  }
  pos = pos + l;
  tot_l = tot_l + l;
  
  //Get the inflection paradigm e.g. NC-O_O-1
  l= DLC_get_paradigm(line, pos, &((*lemma)->paradigm), 1);
  if (l<0) {
    U_delete_lemma(*lemma,1);
    *lemma = NULL;
    return -1;
  }
  pos = pos + l;
  tot_l = tot_l + l;
  return tot_l;
}

/////////////////////////////////////////////////////////////////////////////////
// Gets the list of constituents of a compound) lemma from a DELAC line.
// e.g. czysty(czysty:adj:sg:nom:m2:pos) pływak(pływak.s2:subst:sg:nom:m2)
// 'params' = "global" parameters
// 'line' = input line, not terminated by a newline
// 'pos' = current position in the line
// 'constit_list' = return structure with scanned constituents (allocated before function call but empty)
// 'embedded' = Boolean saying if the constituents' lemmas should be searched for in embedded DLC files
// Returns the length of the scanned sequence, -1 if line format incorrect 
int DLC_get_constit_list(MultiFlexParameters* params, unichar* line, int pos, U_forms_T* constit_list, int embedded){
  if (!line || !constit_list)
    return -1;

  //int no_const; //Number of the current lemma constituent
  int l; //Length of the scanned sequence
  int tot_l; //Total length of the scanned sequences
  U_form_id_T constit; //
  int err; //Error code

  tot_l = 0;
  //no_const = 0;

  //Scan all constituents one by one e.g. "pływak(pływak.s2:subst:sg:nom:m2)"
  while (line[pos] && line[pos]!=CL_BEG) {
    U_init_form_id(&constit);
    l = DLC_get_constit(params,line,pos,&constit,embedded);
    if (l<0) {
      U_delete_f(&constit);
      U_delete_forms(constit_list);
      return -1;
    }
  
    //Add the scanned constituent into the list of lemma constituents
    err =U_add_form(&(constit),constit_list, 0);
    U_delete_f(&constit);
    if (err) {
      U_delete_forms(constit_list);
      return -1;
    }
    pos = pos + l;
    tot_l = tot_l + l;
  }
  return tot_l;
}

/////////////////////////////////////////////////////////////////////////////////
// Scans a (single or compound) constituent from a DELAC entry. 'line' is non terminated by a newline.
// Examples of a unit in a Morfeusz DELAC :
//         pływak(pływak.s2:subst:sg:nom:m2)
//         OLD: pranie(pranie.0:subst:sg:nom:n2)
//         {Marii Sklodowskiej-Curie}(Maria Sklodowska-Curie:subst:sg:gen:f)
// 'params' = "global" parameters
// 'line' = line to be scanned
// 'pos' = current position in 'line' from which the scan is to be started
// 'c' = return structure containing the scanned constituent and its annotation
// 'embedded' = Boolean saying if the constituent's lemma should be searched for in embedded DLC files
// Initially, 'c' has its space allocated but is empty.
// Returns the length of the scanned sequence, -1 if a format error occured. 
int DLC_get_constit(MultiFlexParameters* params, unichar* line, int pos, U_form_id_T* c, int embedded) {
  if (!line || pos <0 || pos >= u_strlen(line) || !c)
    return -1;
  int l; //Length of the scanned sequence
  int tot_l; //Total length of scanned sequences
  unichar form[MAX_DLC_LINE+1];  //Constituent's inflected form
  U_lemma_T* lemma;  //Constituent's lemma
  f_feat_struct_T* feat; //Constituent's features

  tot_l = 0;
  
  //Get the constituent's inflected form e.g. "pływak"
  l = DLC_get_constit_form(params, line, pos, form, MAX_DLC_LINE);
  if (l<0)
    return -1;
  tot_l = tot_l + l;
  pos = pos + l;
  
  //Get the constituent's annotation e.g. "pływak.s2:subst:sg:nom:m2"
  l = DLC_get_constit_annot(params, line,pos, &lemma, &feat, embedded);
  if (l<0)
    return -1;
  tot_l = tot_l + l;
  pos = pos + l;

  //Copy the elements to the constituent's structure
  int err; //Error code
  U_form_id_T constit; //Scanned constituent
  U_init_form_id(&constit);
  constit.form = form;
  constit.lemma = lemma;
  constit.feat = feat;
  err = U_duplicate_form_id(c, &constit);
  U_delete_lemma(lemma,1);
  f_delete_feat_struct(&feat);
  if (err)
    return -1;
  return tot_l;
}

/////////////////////////////////////////////////////////////////////////////////
// Scans a (single or compound) constituent form from a DELAC entry. 'line' is non terminated by a newline.
// Examples of a form in a Morfeusz DELAC : 
//         pływak
//         OLD: pranie
//         {Marii Sklodowskiej-Curie}
// 'params' = "global" parameters
// 'line' = line to be scanned
// 'pos' = current position in 'line' from which the scan is to be started
// 'form' = return parameter to which the scanned constituent is copied, 
//          'form' has its space allocated before the function call
// 'max' = maximum length of the scanned sequence
// 'c' = return string containing the scanned form
// Initially, 'c' has its space allocated but is empty.
// Returns the length of the scanned sequence, -1 if a format error occured. 
int DLC_get_constit_form(MultiFlexParameters* params, unichar* line, int pos, unichar* form, int max) {
  if (!line || pos <0 || pos >= u_strlen(line) || !form || max <=0 )
    return -1;

  int l; //Length of the scanned sequence
  int tot_l; //Total length of scanned sequences

  form[0] = (unichar)'\0';
  tot_l = 0;

  //If the line does not start with a CMPND_BEG, a single unit must be scanned
  if (line[pos] != CMPND_BEG) {
    l = get_unit(params, form,&(line[pos]),max,1);  //Get a unit (single or compound), eliminate backslashes if any
    tot_l = l;
  }
  //If the line starts with a CMPND_BEG all units until the corresponding (unprotected) CMPND_END must be scanned
  else {
    int done; //Boolean indicating if the closing CMPND_END has been reached
    unichar tmp[MAX_DLC_LINE+1]; //Buffer for the scanned fragments of the form
    pos++; //Omit the CMPND_BEG
    tot_l++;
    done = 0;
    while (!done && line[pos] && u_strlen(form)<max) {
      l = get_unit(params, tmp, &(line[pos]), max, 1);
      tot_l = tot_l + l;
      pos = pos + l;
      //The end of compound reached if an unprotected closing separator was reached
      if (tmp[0]==CMPND_END && l==1)
	done = 1;
      else {
	//Total sequence too long for the buffer
	if (u_strlen(form)+u_strlen(tmp) > max)
	  return -1;
	u_strcat(form,tmp);
      }
    }
  }

  return tot_l;
}

/////////////////////////////////////////////////////////////////////////////////
// Gets an annotation (lemma+class+paradigm+features) for constituent of a compound (if any)
// from a DELAC line into a structured lemma.
// e.g. pływak.s2:subst:sg:nom:m2
// 'params' = "global" parameters
// 'line' = input line, not terminated by a newline
// 'pos' = current position in the line
// 'lemma' = return structure for the lemma+class+paradigm (*lemma is allocated but empty)
// 'feat' = return structure for the constituent's features (*feat is allocated but empty)
// 'embedded' = Boolean saying if the constituent's lemma should be searched for in embedded DLC files
// Returns the length of the scanned sequence, -1 if line format incorrect 
int DLC_get_constit_annot(MultiFlexParameters* params, unichar* line, int pos, U_lemma_T** lemma, f_feat_struct_T** feat, int embedded){
 if (!line || pos <0 || pos >= u_strlen(line) || !lemma  || !feat )
    return -1;

  int l; //Length of the scanned sequence
  int tot_l; //Total length of the scanned sequences
  U_lemma_T tmp_lemma;
  int err; //Error code
  
  U_init_lemma(&tmp_lemma,simple);
  
  //If no annotation for the constituent
  if (line[pos]!=CONSTIT_ANNOT_BEG) {
    *lemma = NULL;
    *feat = NULL;
    return 0;
  }
  
  //Omit the separator preceding the lemma (usually '(')
  pos = pos + 1;
  tot_l = 1; 

  //If empty parentheses
  if (line[pos]==CONSTIT_ANNOT_END) {
    *lemma = NULL;
    *feat = NULL;
    return 2;
  }
  
  //Get the lemma string e.g. "pływak"
  unichar* lemma_str;
  lemma_str = NULL;
  l= DLC_get_constit_lemma_str(params, line, pos, &lemma_str);
  if (l<0) 
    return -1;
  pos = pos + l;
  tot_l = tot_l + l;
  tmp_lemma.simple_lemma = lemma_str;
  
  //Get the inflection paradigm or homonym e.f. "s2"
  char* para;
  para = NULL;
  l= DLC_get_paradigm(line, pos, &para, 0);
  if (l<0) {
    U_delete_lemma(&tmp_lemma,0);
    return -1;
  }
  pos = pos + l;
  tot_l = tot_l + l;
  tmp_lemma.paradigm = para; 

  //Get the inflection class e.g. "subst"
  l= DLC_get_class(params, line, pos, &(tmp_lemma.cl), 0);
  if (l<0) {
    U_delete_lemma(&tmp_lemma,0);
    return -1;
  }
  pos = pos + l;
  tot_l = tot_l + l;

  //If it is a compound lemma, its annotation must be found in DELAC files of embedded compounds
  if (U_is_compound(params, tmp_lemma.simple_lemma) && embedded) {
    err = get_MU_annotation(params, tmp_lemma.simple_lemma, tmp_lemma.cl, atoi(tmp_lemma.paradigm), lemma);
    if (err) {
      error((char*) "No annotation could be found for: %S\nin entry: %S.\n",tmp_lemma.simple_lemma , line);
      U_delete_lemma(&tmp_lemma,0);
      return -1;
    }
    U_delete_lemma(&tmp_lemma,0);
  }
  
  //If it is a simple lemma, duplicate it
  else {
    *lemma = (U_lemma_T*)malloc(sizeof(U_lemma_T));
    if (!*lemma)
      fatal_error((char*) "Memory allocation problem in function 'DLC_get_constit_annot'!\n");			
    err = U_duplicate_lemma(*lemma,&tmp_lemma);
    U_delete_lemma(&tmp_lemma,0);
    if (err)
      return -1;
  }

  //Get the constituent's features e.g. ":sg:nom:m2"
  l = DLC_get_constit_feat(params, line, pos, feat);
  if (l<0) {
    U_delete_lemma(&tmp_lemma,0);
    U_delete_lemma(*lemma,0); 
    *lemma = NULL;
    return -1;
  }
  tot_l = tot_l + l;
  pos = pos + l;

  //Get the closing separator for annotation (usually ')')
  if (line[pos]!=CONSTIT_ANNOT_END) {
    error((char*) "Unit's annotation should be followed by a %C:\n%S\n",CONSTIT_ANNOT_END, line);
    U_delete_lemma(&tmp_lemma,0);
    U_delete_lemma(*lemma,0);
    *lemma = NULL;
    f_delete_feat_struct(feat);
    return -1;
  }
  //Omit the closing separator for annotation
  tot_l = tot_l + 1;

  return tot_l;
}

////////////////////////////////////////////
// Scans a constituent's lemma string, starting from position 'pos' 
// in the input line 'line' of a DELAC entry, 
// and puts it to 'lemma_str'.
// 'params' = "global" parameters
// '*lemma_str' is allocated but empty
// Returns the length of the scanned sequence.
// Returns -1 on format problems
int DLC_get_constit_lemma_str(MultiFlexParameters* params, unichar* line, int pos, unichar** lemma_str) {
   if (!line || pos <0 || pos >= u_strlen(line) || !lemma_str)
    return -1;

   int done; //Boolean indicating if end of lemma string has been reached
   unichar tmp[MAX_DLC_LINE+1]; //Buffer for the scanned fragments of the form
   int l; //Length of the scanned sequence
   int tot_l; //Total length of the scanned sequences
   *lemma_str = (unichar*) malloc(sizeof(unichar));
   if (!*lemma_str)
	 fatal_error((char*) "Not enough memory in function DLC_get_constit_lemma_str\n");   
   (*lemma_str)[0] = '\0';
   done = 0;
   tot_l = 0;
   while (!done && line[pos]) {
     l = get_unit(params, tmp, &(line[pos]), MAX_DLC_LINE, 1);
     //The end of compound reached if an unprotected closing separator was reached
     if ((tmp[0]==CONSTIT_CAT_BEG || tmp[0]==CONSTIT_PARA_BEG) && l==1)
       done = 1;
     else {
       *lemma_str = (unichar*)realloc(*lemma_str, (u_strlen(*lemma_str)+u_strlen(tmp)+1) * sizeof(unichar));
       if (! *lemma_str)
	 fatal_error((char*) "Not enough memory in function DLC_get_constit_lemma_str\n");
       u_strcat(*lemma_str,tmp);
       tot_l = tot_l + l;
       pos = pos + l;
     }
   }
   if (!done) {
     error((char*) "Unit's lemma should be followed by a %C or a %C:\n%S\n",CONSTIT_PARA_BEG, CONSTIT_CAT_BEG, line);
     free(lemma_str);
     return -1;
   }
   
   return tot_l;
}

////////////////////////////////////////////
// Scans the inflection paradigm (or homonym) code, e.g. "s2", starting from position 'pos' 
// in the input line 'line' of a DELAC entry, 
// and puts it to 'para'.
// '*paradigm' is allocated but empty
// If 'unit_or_entry' is 0 then the paradigm appears in a unit's annotation
// (i.e. it is possibly empty, or preceded by a CONSTIT_PARA_BEG and followed by a CONSTIT_PARA_END)
// If 'unit_or_entry' is 1 then the paradigm appears in a DELAC entry's annotation
// (i.e. preceded by a PARA_BEG and followed by a PARA_END).
// Returns the length of the scanned sequence.
// Returns -1 on format problems
int DLC_get_paradigm(unichar* line, int pos, char** paradigm, int unit_or_entry) {
  if (!paradigm || !line || pos <0 || pos >= u_strlen(line))
    return -1;

  int l; //Length of the scanned sequence
  unichar u_para[MAX_DLC_LINE];  //Paradigm unicode string 
 
  //Paradigm of a unit
  if (unit_or_entry == 0) {
    //Either a CONSTIT_PARA_BEG or a CONSTIT_PARA_END must follow the lemma string in an annotated lemma
    if ((line[pos]!=CONSTIT_PARA_BEG) && (line[pos]!=CONSTIT_PARA_END)) {
      error((char*) "%C or %C missing after a unit's lemma:\n%S\n",CONSTIT_PARA_BEG, CONSTIT_PARA_END, line);
      return -1;
    }
    
    //If the lemma's inflection paradigm inexistent then it is 0 by default
    if (line[pos]==CONSTIT_PARA_END) {
      u_strcpy(u_para,"0");
      l = 0;
    }
    else  {
      pos ++;  //Omit the dot
      unichar sep[] = {CONSTIT_PARA_END,(unichar)'\0'};
      l = u_scan_until(u_para,&(line[pos]),MAX_DLC_LINE-1,sep,1);
      if (!l) {    
	error((char*) "Unit's inflection paradigm non existent in DELAC line:\n%S\n",line);
	return -1;     
      }
      l++; //The CONSTIT_PARA_BEG counts in the scanned sequence
    }
  }

  //Paradigm of an entry
  else {
    if (line[pos]!=PARA_BEG) {
      error((char*) "%C missing before an entry's paradigm:\n%S\n",PARA_BEG,line);
      return -1;
    }
    pos ++;  //Omit the PARA_BEG
    unichar sep[] = {PARA_END, (unichar) '\0'};
    l = u_scan_until(u_para,&(line[pos]),MAX_DLC_LINE-1,sep,1);
    if (!l) {    
      error((char*) "Unit's inflection paradigm non existent for DELAC entry:\n%S\n",line);
      return -1;     
    }
    pos = pos + l;
    if (line[pos]!=PARA_END) {
      error((char*) "%C missing after an entry's paradigm:\n%S\n",PARA_END,line);
      return -1;
    }
    l = l+2;  //PARA_BEG and PARA_END count in the scanned sequence
  }

  //Allocate the paradigm char code
  *paradigm = (char*) malloc((u_strlen(u_para)+1) * sizeof(char));
  if (! *paradigm)
    fatal_error((char*) "Not enough memory in function DLC_get_paradigm\n");
  //Copy unichar into char
  for (int c=0; c<=u_strlen(u_para); c++)
    (*paradigm)[c] = (char)u_para[c];

  return l;  //Length of the scanned sequence
}

////////////////////////////////////////////
// Scans the inflection class, starting from position 'pos' 
// in the input line 'line' of a DELAC entry, 
// and puts it to 'cl'.
// 'cl' is not allocated before the function call
// On return '*cl' points to a global structure that 
// should not be liberated until the end of programme
// If 'unit_or_entry' is 0 then the class appears in a unit's annotation
// (i.e. preceded by CONSTIT_CL_BEG and followed by a CONSTIT_CL_END)
// If 'unit_or_entry' is 1 then the class appears in a DELAC entry's annotation
// (i.e. preceded by a CL_BEG and followed by a CL_END or by end of line).
// 'params' = "global" parameters
// Returns the length of the scanned sequence.
// Returns -1 on format problems
int DLC_get_class(MultiFlexParameters* params, unichar* line, int pos, l_class_T** cl, int unit_or_entry) {
  if (!cl || !line || pos <0 || pos >= u_strlen(line))
    return -1;
  
  int l; //Length of the scanned sequence
  unichar cl_str[MAX_CLASS_NAME];  //Scanned class sequence

  //Class of a unit
  if (unit_or_entry == 0) {
    if (line[pos]!=CONSTIT_CL_BEG) {
      error((char*) "%C missing after a unit's inflection code or lemma: \n%S\n",CONSTIT_CL_BEG,line);
      return -1;
    }
    pos++; //Omit the CONSTIT_CL_BEG
    unichar sep[] = {CONSTIT_CL_END, CONSTIT_ANNOT_END, (unichar)'\0'};
    l = u_scan_until(cl_str,&(line[pos]),MAX_DLC_LINE-1,sep,1);
    if (!l) {    
      error((char*) "Unit's inflection class non existent in DELAC line:\n%S\n",line);
      return -1;     
    }
    l++; //The preceding CONSTIT_CL_BEG counts in the length of the scanned sequences
  }

  //Class of a DELAC entry
  else {
    if (line[pos]!=CL_BEG) {
      error((char*) "%C missing a DELAC entry's inflection class: \n%S\n",CL_BEG,line);
      return -1;
    }
    pos++; //Omit the CL_BEG
    unichar sep[] = {CL_END,(unichar)'\0'};
    l = u_scan_until(cl_str,&(line[pos]),MAX_DLC_LINE-1,sep,1);
    if (!l) {    
      error((char*) "Entry's inflection class non existent in DELAC line:\n%S\n",line);
      return -1;     
    }
    l++; //The preceding CL_BEG counts in the length of the scanned sequences
  }
  
  //Get the class structure
  *cl = get_class_str_direct(params, cl_str);
  if (!*cl) {    
    error((char*) "Impossible to deduce the unit's or enty's inflection class (noun, adj, etc.) at position %d:\n%S\n",pos, line);
    return -1;
  }
  return l;
}

////////////////////////////////////////////
// Scans the inflection class, starting from position 'pos' 
// in the input line 'line' of a DELAC entry, 
// and puts it to 'feat'.
// '*feat' is allocated before the function call but empty
// 'params' = "global" parameters
// Returns the length of the scanned sequence.
// Returns -1 on format problems
int DLC_get_constit_feat(MultiFlexParameters* params, unichar* line, int pos, f_feat_struct_T** feat) {
  if (!feat || !line || pos <0 || pos >= u_strlen(line))
    return -1;

  int l; //Length of the scanned sequence
  unichar tmp[MAX_DLC_LINE+1]; //Buffer for the scanned fragments of the form

  
  if (line[pos] && line[pos]!=CONSTIT_FEAT_BEG) {
    //error((char*) "%C missing before inflection features in a DELAC entry: \n%S\n",CONSTIT_FEAT_BEG, line);
    //return -1;
    *feat = NULL;
    return 0;
  }
  unichar sep[] = {CONSTIT_FEAT_END,(unichar)'\0'};
  l = u_scan_until(tmp,&(line[pos]),MAX_DLC_LINE,sep,1);
  if (l<=0) {
    error((char*) "Inflection features missing after %C for a unit:\n%S\n",CONSTIT_FEAT_BEG,line);
    return -1;
  }
  pos += l;
  if (line[pos] != CONSTIT_FEAT_END) {
	error((char*) "%C missing after a unit's inflection features:\n%S\n",CONSTIT_FEAT_END, line);
    return -1;
  }
  *feat = get_feat_struct_str(params,tmp);
  if (! *feat) {
    error((char*) "Incorrect inflection features in a unit:\n%S\n",line);
    return -1;
  }
  return l; 
}


/////////////////////////////////////////////////////////////////////////////////
// Scans semantic codes (e.g. "+Hum+z1") from a DELAC entry. 'line' is non terminated by a newline.
// 'line' = line to be scanned
// 'pos' = current position in 'line' from which the scan starts
// 'codes' = result table of scanned codes, *codes is allocated before function call
// The function allocates space for codes scanned. It must be liberated by the calling function.
// Returns the length of the scanned sequence, -1 if a format error occured. 
int DLC_get_codes(unichar* line, int pos, unichar** codes) {
  int l; //length of the scanned sequence
  int tot_l; //Total length of scanned sequences
  int c;  //number of codes
  unichar tmp[MAX_DLC_LINE];

  c = 0;
  tot_l = 0;
  while (line[pos+tot_l] == CODE_BEG) {
    tot_l ++;  //Omit the CODE_BEG
    unichar sep[] = {COMMENT_BEG, CODE_BEG,(unichar)'\0'};
    l = u_scan_until(tmp,&(line[pos+tot_l]),MAX_DLC_LINE-1,sep,1);
    if (l) {
      codes[c] = (unichar*) malloc((u_strlen(tmp)+1) * sizeof(unichar));
      if (!codes[c]) 
	fatal_error((char*) "Memory allocation problem in function 'DLC_get_codes'!\n");
      u_strcpy(codes[c],tmp);
      tot_l = tot_l + l;
      c++;
    }
  }
  codes[c] = NULL; 
  return tot_l;
}

/////////////////////////////////////////////////////////////////////////////////
// Scans comment (e.g. "/electricity") from a DELAC entry. 'line' is non terminated by a newline.
// 'line' = line to be scanned
// 'pos' = current position in 'line' from which the scan starts
// 'comment' = result structure for the scanned comment
// The function allocates space for comment scanned. It must be liberated by the calling function.
// Returns the length of the scanned sequence, -1 if a format error occured. 
int DLC_get_comment(unichar* line, int pos, unichar** comment) {
  int l = 0;  //length of the scanned sequence
  unichar tmp[MAX_DLC_LINE];
  
  if (line[pos] == COMMENT_BEG) {
    pos++;  //Omit the COMMENT_BEG
    l = u_scan_until_char(tmp,&(line[pos]),MAX_DLC_LINE-1,(char*)"",1);
    if (l>=0) {
      *comment =(unichar*) malloc((u_strlen(tmp)+1) * sizeof(unichar));
      if (!(*comment))
	fatal_error((char*) "Memory allocation problem in function 'DLC_get_comment'!\n");
      u_strcpy(*comment,tmp);
      return l+1;  //Length of the comment plus one for '/'
    }
    else
      return -1;
  }
  else {
    *comment = NULL;
    return 0;
  }
}

/////////////////////////////////////////////////////////////////////////////////
// Prints a DELAC entry to stdin.
// 'params' = "global" parameters
// If entry void or entry's lemma void returns 1, 0 otherwise.
int DLC_print_entry(MultiFlexParameters* params, DLC_entry_T* entry) {
  int err; //Error code
  unichar line[MAX_DLC_LINE];
  u_strcpy(line,"");
  
  if (!entry || !entry->lemma) 
    return 1;  

  //Print units
  for (int u=0; u<entry->lemma->compound_lemma.no_forms; u++) 
    DLC_print_unit(params, &(entry->lemma->compound_lemma.forms[u]));

  //Print class
  unichar cl[MAX_CLASS_NAME];
  err = copy_class_str(cl, entry->lemma->cl);
  if (!err && u_strlen(cl))
    u_printf((char*)"%C%S",CL_BEG,cl); 

  //Print paradigm if it is not empty
  if (strcmp(entry->lemma->paradigm,"0"))
      u_printf((char*)"%C%s%C",PARA_BEG,entry->lemma->paradigm,PARA_END);
  
  //Print codes
  for (int c=0; entry->codes[c]; c++) {
    u_printf((char*)"%C%S",CODE_BEG,entry->codes[c]);
  }

  //Print comment
  if (entry->comment)
    u_printf((char*)"%C%S",COMMENT_BEG,entry->comment);

  u_printf((char*)"\n");
  return 0;
}

/////////////////////////////////////////////////////////////////////////////////
// Prints single unit of a DELAC entry to stdout.
// 'params' = "global" parameters
void DLC_print_unit(MultiFlexParameters* params, U_form_id_T* unit) {
  if (!unit)
    return;

  int comp; //Boolean saying if the unit is compound
  if ((unit->lemma) && (unit->lemma->type == compound))
    comp = 1;
  else 
    comp = 0;

  //Print the unit's form
  if (comp)
    u_printf((char*)"%C",CMPND_BEG);
  u_printf((char*)"%S",unit->form);
  if (comp)
    u_printf((char*)"%C",CMPND_END);
  
  //Print the unit's annotation if any
  int err; //Error code
  unichar lemma_str[MAX_DLC_LINE+1];
  if (unit->lemma) {
    err = U_get_lemma_str(lemma_str,MAX_DLC_LINE,unit->lemma);
    if (err)
      return;
    u_printf((char*)"(%S%C%s",lemma_str,CONSTIT_PARA_BEG,unit->lemma->paradigm);
    if (unit->feat) {
      unichar* tmp;
      tmp = get_str_feat_struct(params,unit->feat);
      if (!tmp)
	return;
      u_printf((char*)"%S",tmp);
      free(tmp);
    }
   u_printf((char*)")");      
  }
}

/////////////////////////////////////////////////////////////////////////////////
// Puts an inflected form 'f' corresponding to the DELAC entry 'dlc_entry' into the DELACF format ('entry').
// The resulting entry may take up to 'max' characters.
// 'entry' already has its space allocated.
// 'params' = "global" parameters
// Returns 1 on error, 0 otherwise.
int DLC_format_form(MultiFlexParameters* params, unichar* entry, int max, U_form_id_T f, DLC_entry_T* dlc_entry) {
  if (!entry || max<0 || !dlc_entry)
    return 1;

  int l;  //length of the entry
  int err; //Error code
  unichar sep[] = {(unichar)'\0',(unichar)'\0'}; //Unichar table for separator concatenation
  unichar* tmp; //Container for a possibly protected dlcf entry's substring
  tmp = (unichar*) malloc(max * sizeof(unichar));
  if (!tmp)
    fatal_error((char*) "Memory allocation problem in function 'DLC_format_form'!\n");

  //Inflected form
  if (DLC_protect_dlcf_string(tmp, max, f.form)) {
    free(tmp);
    return 1;
  }
  l = u_strlen(tmp);
  u_strcpy(entry, tmp);

  //Lemma separator
  l++; 
  if (l >= max) {
    free(tmp);
    return 1;
  }
  sep[0] = DLCF_LEMMA_BEG;
  u_strcat(entry,sep);

  //Lemma
  unichar lemma_str[MAX_DLCF_LINE+1]; //String containing the lemma
  err = U_get_lemma_str(lemma_str, MAX_DLCF_LINE, dlc_entry->lemma);
  if (err || DLC_protect_dlcf_string(tmp, max-l, lemma_str)) {
    free(tmp);
    return 1;
  }
  l = l + u_strlen(tmp);
  u_strcat(entry, tmp);
  
  //Class separator
  l++; 
  if (l >= max) {
    free(tmp);
    return 1;
  }
  sep[0] = DLCF_CL_BEG;
  u_strcat(entry,sep);

  //Class
  unichar cl[MAX_DLCF_LINE+1] ;
  err = copy_class_str(cl, dlc_entry->lemma->cl);
  if (err || DLC_protect_dlcf_string(tmp, max-l, cl)) {
    free(tmp);
    return 1;
  }
  l = l + u_strlen(tmp);
  u_strcat(entry,tmp);

  //Inflection features
  unichar* feat;  //sequence of single-letter inflection features, e.g. 'sIf'
  if (f.feat && f.feat->no_catvals > 0) {
    feat = get_str_feat_struct(params, f.feat);
    if (!feat) {
      free(tmp);
      return 1;
    }
    l = l + u_strlen(feat);
    if (l>=max) {
      free(tmp);
      free(feat);
      return 1;
    }
    u_strcat(entry,feat);
    free(feat);
  }

   //Semantic codes
  int c;  //index of the current semantic code
  sep[0] = DLCF_CODE_BEG;
  for (c=0; dlc_entry->codes[c]; c++) {
    if (DLC_protect_dlcf_string(tmp, max-l-1, dlc_entry->codes[c])) {
      free(tmp);
      return 1;
    }
    u_strcat(entry,sep);
    u_strcat(entry,tmp);
    l = l + u_strlen(tmp)+1;
  }

 //Comment
  if (dlc_entry->comment && u_strlen(dlc_entry->comment)) {
    if (DLC_protect_dlcf_string(tmp, max-l-1, dlc_entry->comment)) {
      free(tmp);
      return 1;
    }
    sep[0] = DLCF_COMMENT_BEG;
    u_strcat(entry, sep);
    u_strcat(entry,tmp);
    l = l + u_strlen(tmp) + 1;
  }

  //Extras
  if (params->debug_status==GRAPH_DEBUG && f.extras) {
    unichar* extras; //Extras string
    extras = fe_get_str_extras(f.extras);
    //Check if the total length needed after adding the extras is not too big (+1 is there for the newline)
    l = l + u_strlen(extras) + 1;  
    if (l >= max)
      return 1;
    sep[0] = DLCF_EXTRAS_BEG;
    u_strcat(entry,sep);
    u_strcat(entry,extras);
    free(extras);
  }

  free(tmp);
  return 0;
}

/////////////////////////////////////////////////////////////////////////////////
// Transforms a part of a delacf entry ('entry') by protecting all special characters
// and puts the result to 'protected_entry'.
// The resulting entry may take up to 'max' characters.
// 'entry' already has its space allocated.
// Returns 1 on error, 0 otherwise.
int DLC_protect_dlcf_string(unichar* protected_entry, int max, unichar* entry) {
  if (!protected_entry || !entry)
    return 1;

  unichar specials[] = {DLCF_LEMMA_BEG, DLCF_CL_BEG, DLCF_CODE_BEG, DLCF_COMMENT_BEG, DESP, (unichar)'\0'};
  int i,j; //Indices of the current characters in entry and protected_entry
  j=0;
  for (i=0; i<u_strlen(entry) && j<max; i++) {
    if (u_is_in(entry[i],specials))
      protected_entry[j++] = DESP;
    protected_entry[j++] = entry[i];
  }
  if (j>=max)
    return 1;
  else
    protected_entry[j] = (unichar) '\0';
  return 0;
}

/////////////////////////////////////////////////////////////////////////////////
// Formats and prints the inflected forms of a MWU into the delacf file
// Each resulting entry may take up to 'max' characters.
// 'params' = "global" parameters
// MU_forms = the inflected forms to be printed
// dlcf = the output delacf file
// dlc_entry = the MWU lemma
// Returns 1 on error, 0 otherwise.
void DLC_print_inflected_forms(MultiFlexParameters* params, U_forms_T* MU_forms, FILE* dlcf, DLC_entry_T* dlc_entry) {
  unichar dlcf_line[MAX_DLC_LINE];  //string representing one inflected form 
  int f; //Index of the current inflected form
  int err; //Error code
  for (f=0; f<MU_forms->no_forms; f++) {
    //Format the inflected form to the DELACF format
    err = DLC_format_form(params,dlcf_line,MAX_DLCF_LINE-1,MU_forms->forms[f],dlc_entry);
    if (!err) {
      //Print one inflected form at a time to the DELACF file
      u_fprintf(dlcf,(char*)"%S\n",dlcf_line);
      //u_fprintf(stdout,"%S\n",dlcf_line); 
    }
    else {
      error((char*) "An inflected form could not be printed for ");
      DLC_print_entry(params, dlc_entry);
    }
  }
}

/////////////////////////////////////////////////////////////////////////////////
// Liberates the memory allocated for a DELAC entry.
// The main structure is not liberated
void DLC_delete_entry(DLC_entry_T* entry) {
  if (entry) {
    if (entry->lemma) {
      U_delete_lemma(entry->lemma,1);
      entry->lemma = NULL;
    }
    for (int c=0; entry->codes[c]; c++)  //delete codes
      if (entry->codes[c]) {
	free(entry->codes[c]);
	entry->codes[c] = NULL;
      }  
    if (entry->comment) {
      free(entry->comment);  //delete comment
      entry->comment = NULL;
    }
  }
}
