/* 
 * File:   test_morfeusz_generator.cpp
 * Author: Piotr Sikora & Agata Savary
 *
 * Created on 21 Jul 2015, 19:00
 *
 * Testing the C version of Morfeusz 2 generator
 *
 * Compiling: 
 *   g++ -g -I. -L./ -lmorfeusz2 -c morfeusz2_c.cpp -o morfeusz2_c
 *   g++ -g -I. -L./ -lmorfeusz2 -c test_morfeusz_generator.cpp -o test_morfeusz_generator
 *   g++ -g -I. -L./ -lmorfeusz2 morfeusz2_c test_morfeusz_generator -o test_gen
 * Running:
 *   LD_LIBRARY_PATH=.; export LD_LIBRARY_PATH; ./test_gen piec:v
 */

#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include "morfeusz2_c.h"

using namespace std;

#define BUFLEN 10000

///////////////////////////////////////////////
//Print analyses received from Morfeusz
// forms = the forms (the end of forms is signalled by a sentinel: -1 in the "starting state" forms[i].p)
int printMorfeuszResults(InterpMorf* forms) {
   int i;
   for (i=0; forms[i].p != -1; i++) {
     //      cout << forms[i].forma << ',' << forms[i].haslo << ':' << forms[i].interp << endl;
     cout << "Forma: " << forms[i].forma << endl ;
     cout << "Haslo: " << forms[i].haslo <<endl;
     cout << "Interp: " << forms[i].interp << endl << endl;
    }
   return 0;
}

///////////////////////////////////////////////
// Testing Morfeusz generator
// Command line parameter:
//     lemma for which forms are to be generated
int main(int argc, char* argv[]) {

    //Test command line parameter
    if (argc != 2) {
        cerr << "test_gen needs one parameter: a lemma" << endl;
        cerr << "Sample run:" << endl;
        cerr << "      LD_LIBRARY_PATH=.; export LD_LIBRARY_PATH; ./test_gen piec:v" << endl;
        exit(-1);
    }

    InterpMorf *forms;

    //  morfeusz_set_option(MORFOPT_ENCODING, MORFEUSZ_ISO8859_2);
#ifdef __WIN32
    morfeusz_set_option(MORFOPT_ENCODING, MORFEUSZ_CP852);
#else
    morfeusz_set_option(MORFOPT_ENCODING, MORFEUSZ_UTF_8);
#endif

    forms = morfeusz_generate(argv[1]);
    printMorfeuszResults(forms);

    return 0;
}
