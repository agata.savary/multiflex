#include "morfeusz2.h"
#include <iostream>

using namespace std;
using namespace morfeusz;


int main() {
  Morfeusz *morfeusz=Morfeusz::createInstance();
  //  Morfeusz *morfeusz=Morfeusz::createInstance("sgjp");

  cerr << morfeusz->getVersion() << endl;
  cerr << morfeusz->getDefaultDictName() << endl;
  cerr << morfeusz->getDictID() << endl;

  // Morfeusz::dictionarySearchPaths.pop_front();

  cerr << "search paths (" << Morfeusz::dictionarySearchPaths.size() << "):";
  for (list<string>::iterator it=Morfeusz::dictionarySearchPaths.begin(); 
       it != Morfeusz::dictionarySearchPaths.end(); ++it)
    cerr << ' ' << *it;
  cerr <<endl << endl;
  //cerr << morfeusz->getDictCopyright() << endl;

  // Morfeusz::dictionarySearchPaths.push_front(".");
  // cerr << "search paths (" << Morfeusz::dictionarySearchPaths.size() << "):";
  // for (list<string>::iterator it=Morfeusz::dictionarySearchPaths.begin(); 
  //      it != Morfeusz::dictionarySearchPaths.end(); ++it)
  //   cerr << ' ' << *it;
  // cerr <<endl << endl;


  string line;

  while (getline(cin, line)) {
    ResultsIterator *r=morfeusz->analyse(line);

    while(r->hasNext()) {
      MorphInterpretation i=r->next();
      cout << i.startNode <<" " 
	   << i.endNode <<" " 
	   << i.orth  <<" " 
	   << i.lemma  <<" " 
	   << i.getTag(*morfeusz) <<" "
	   << i.getName(*morfeusz) <<" "
	   << i.getLabelsAsString(*morfeusz) <<" "
	   << endl;
    }
  }

  return 0;
}
