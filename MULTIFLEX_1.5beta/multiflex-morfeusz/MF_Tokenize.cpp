/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2009 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */
/* This file contains parts of the Unitex software distributed under the terms of LGPL.   */
/* It is a modified and extended version of Inflect.cpp file of the the Unitex 1.2 beta   */
/* version downloaded from http://www-igm.univ-mlv.fr/~unitex/ on June 6, 2005.           */ 
/* The modifications has been done by Agata Savary (agata.savary@univ-tours.fr).          */
/* Latest modification on January 2009.                                                  */


/********************************************************************************/
/* TOKENIZATION OF A UNICODE STRING                                             */
/********************************************************************************/

#include "MF_Tokenize.h"
#include "Unicode.h"
#include "MF_Tokenize.h"
#include "MF_Convert.h"
#include "morfeusz/morfeusz2_c.h"
#include "Error.h"

tokenization* tokenize(MultiFlexParameters* params, unichar* s, int eliminate_bcksl);
int get_unit(unichar* unit,unichar* line, int max, int eliminate_bcksl);
char* morfeusz_get_first_longest_token(InterpMorf* im, int p, int* k, int* nb_slashes, int eliminate_bcksl, int preceding_bcksl);
tokenization* morfeusz_get_first_longest_tokenization(InterpMorf* im, int eliminate_bcksl);
tokenization* init_tokenization();
int add_token(tokenization* t, unichar* ct);
void delete_tokenization(tokenization* t);

////////////////////////////////////////////
// Divides a string into tokens
// 'params' = "global" parameters
// 's' = string to be tokenized
// If "eliminate_bcksl" is set to 1 each protecting backslash is omitted in the
// copied sequence.                                                            
// Returns the tokenization os 's' (NULL in case of errors)
// The resulting structure should be deleted by the calling function by 'delete_tokenization'
tokenization* tokenize(MultiFlexParameters* params, unichar* s, int eliminate_bcksl) {
  
  //Convert the UTF-16 string into UTF-8
  char* s_utf8 = convert_utf16_to_utf8(s);
  if (!s_utf8) 
    return NULL;
  
  //Call the Morfeusz analyser to tokenize the line
  InterpMorf* im;  //Morfeusz interpretation of a string
  if (!morfeusz_set_option(MORFOPT_WHITESPACE, MORFEUSZ_KEEP_WHITESPACE) || 
      ! morfeusz_set_option(MORFOPT_ENCODING, MORFEUSZ_UTF_8)) {
    error("Incorrect option of value for Mofeusz while tokenizing '%S'!", s);
    return NULL;
  }
  im = morfeusz_analyse(s_utf8);
  if (!im){
    error("No analysis could be done by Morfeusz for: %S.\n", s);
    return NULL;
  }
  
  //For debugging
//      int token; //Number of the current token
//    u_printf("Tokens:\n");
//    for (token=0; im[token].p!=-1; token++) {
//      u_printf("%s - ",im[token].forma);
//      u_printf("poczatek: %i, koniec:%i\n", im[token].p, im[token].k);
//    }
//    u_printf("\n");
//    u_printf("Number of tokens: %i\n",token);
//End of debugging
  
  free(s_utf8);
  
  //Get the Morfeusz' tokenization in which the first longest token is chosen each time
  tokenization* t;
  t = morfeusz_get_first_longest_tokenization(im,eliminate_bcksl);
  if (! t) {
    error("No tokenization could be done for: %S.\n", s);
    return NULL;
  }
  return(t);
}


////////////////////////////////////////////
// Gets next unit from the input line 'line' and puts it to 'unit'.
// 'params' = "global" parameters
// 'unit' must have its space allocated
// 'max' is the maximum length of the copied sequence
// This is the essential function defining the segmentation
// of a text into units.
// If "eliminate_bcksl" is set to 1 each protecting backslash is omitted in the
// copied sequence.
// Returns the length of the scanned sequence.
int get_unit(MultiFlexParameters* params, unichar* unit,unichar* line, int max, int eliminate_bcksl) {
  if (!line || !u_strlen(line))
    return 0;
  int nb_slashes; //Number of backslashes eliminated while scanning a unit

  //Convert the UTF-16 line into UTF-8
  char* line_utf8 =  convert_utf16_to_utf8(line);
  if (! line_utf8)
    return 0;
  
  //Call the Morfeusz analyser to tokenize the line
  InterpMorf* im;  //Morfeusz interpretation of a string
  if (!morfeusz_set_option(MORFOPT_WHITESPACE, MORFEUSZ_KEEP_WHITESPACE))
    return 0;
  im = morfeusz_analyse(line_utf8);
  free(line_utf8);
  
  //Take the first token and return its length
  char* first_token; //The first longest token found by Morfeusz
  int k; //Arrival state of the first token
  first_token = morfeusz_get_first_longest_token(im,0,&k,&nb_slashes,eliminate_bcksl,0);
  if (! first_token) {
    error("No token found by Morfeusz in: %S.\n", line);
    return 0;
  }
  
  unichar* first_token_utf16 = convert_utf8_to_utf16(first_token);
  free(first_token);
  if (!first_token_utf16) 
    return 0;
  if (u_strlen(first_token_utf16) > max) {
    error("Token too long: %S.\n", first_token_utf16);
    free(first_token_utf16);
    return 0;
  }
  u_strcpy(unit,first_token_utf16);
  free(first_token_utf16);

  return u_strlen(unit)+nb_slashes;
}

/*
////////////////////////////////////////////
// Gets next unit from the input line 'line' and puts it to 'unit'.
// 'max' is the maximum length of the copied sequence
// 'alph' is the current alphabet
// This is the essential function defining the segmentation
// of a text into units.
// If "eliminate_bcksl" is set to 1 each protecting backslash is omitted in the
// copied sequence.                                                            
// Returns the length of the scanned sequence.
int SU_get_unit(unichar* unit,unichar* line, int max, Alphabet* alph, int eliminate_bcksl) {
  int l=0;  //length of the scanned sequence
  int u=0;  //index of the next element in 'unit'
  int end=0;
  int no_elim_bcksl=0;  //number of eliminated backslashes
  int bcksl_precedes=0;  //1 if the preceding character was a '\', 0 otherwise
  if (!line)
    return 0;

  //Separator
  if (line[0]==(unichar)'\\' && line[1] && !is_letter(line[1],alph))
    if (eliminate_bcksl) {
      unit[0] = line[1];
      unit[1] = '\0';
      return 2;
    }
    else {
      unit[0] = line[0];
      unit[1] = line[1]; 
      unit[2] = '\0'; 
      return 2;
    }
  else
    if(line[0]!=(unichar)'\\' && line[0] && !is_letter(line[0],alph)) {
      unit[0] = line[0];
      unit[1] = '\0';
      return 1;      
    }
  
  //Word
    else {
      l = 0;
      u = 0;
      end = 0; 
      no_elim_bcksl =0;
      bcksl_precedes = 0;
      while (!end && u<max && line[l]!=(unichar)'\0')
	if (line[l] == (unichar) '\\')   //current character is a '\'
	  if (!bcksl_precedes) {         //not preceded by '\'
	    bcksl_precedes = 1;
	    l++;
	  }
	  else                          //preceded by '\'
	    if (!is_letter(line[l],alph))  //'\' is no letter
	      end = 1;
	    else {                        //\ '\' is a letter
	      if (!eliminate_bcksl)
		unit[u++] = (unichar) '\\';
	      else
		no_elim_bcksl++;
	      unit[u++] = line[l++];
	      bcksl_precedes = 0;
	    }
	else //Current character is not a '\'
	  if (!is_letter(line[l],alph))  //'\' is no letter
	    end = 1;
	  else {                        //\ '\' is a letter
	    if (bcksl_precedes)
	      if (!eliminate_bcksl)
		unit[u++] = (unichar) '\\';
	      else
		no_elim_bcksl++;
	    unit[u++] = line[l++];
	    bcksl_precedes = 0;
	  }
    }
  unit[u] = (unichar) '\0';
  return u+no_elim_bcksl;
p }
*/

////////////////////////////////////////////
// Gets the first longest token found by Morfeusz in a string starting from state 'p'.
// 'im' = morphological interpretation of the string produced by Morfeusz
// 'p' = desired starting state of the token
// 'k' = output parameter: arrival state of the token found
// 'nb_slashes' = outpur parameter: number of backslahes eliminated while scanning the token  
// If "eliminate_bcksl" is set to 1 each protecting backslash is omitted in the
// returned sequence.  
// 'preceding_bcksl' = boolean saying if the token to be retrieved is preceded by a backslash
// The return string is allocated in the function, it has to be liberated by the calling module.                                                          
// Returns a pointer to the first longest token found, or NULL on error.
char* morfeusz_get_first_longest_token(InterpMorf* im, int p, int* k, int* nb_slashes, int eliminate_bcksl, int preceding_bcksl) {
  int t; //Number of the current token
  int first_longest; //Number of the currently longest first token
  int first_longest_len; //Length of the currently longest first token
  char* first_longest_str; //Final longest first token

  *nb_slashes = 0; //No protecting backslash eliminated initially
  
  if (!im) 
    return NULL;

  first_longest = -1;
  first_longest_len = 0;
  for (t=0; im[t].p!=-1; t++) {
    //If it is the first token, and it's longer than the previously found first token
    if (im[t].p == p && (int)strlen(im[t].forma)>first_longest_len) {
      //It becomes the current longest first token
      first_longest = t;
      first_longest_len = strlen(im[t].forma);      
    }
  }
  if (first_longest == -1) {
    *k = -1;
    return NULL;
  }
  else {
    *k = im[first_longest].k; 
    first_longest_str = (char*) malloc((strlen(im[first_longest].forma)+1) * sizeof(char));
    if (!first_longest_str)
      fatal_error("Memory allocation problem in function 'morfeusz_get_first_longest_token'.\n");
    strcpy(first_longest_str, im[first_longest].forma);
  }

  //If the token founnd is a despecialisation append the token that follows
  char desp_str[] = {DESP,'\0'};
  if (!preceding_bcksl && !strcmp(im[first_longest].forma, desp_str)) {
    char* next; //Token following the despecialisation
    next = morfeusz_get_first_longest_token(im, im[first_longest].k, k, nb_slashes, eliminate_bcksl, 1);
    if (next) { //If no token follows the slash do nothing more
      *nb_slashes = *nb_slashes + 1; //The initial backslash counts in the length 
      first_longest_str = (char*) realloc(first_longest_str, (strlen(first_longest_str)+strlen(next)+1) * sizeof(char));
      if (!first_longest_str)
	fatal_error("Memory allocation problem in function 'morfeusz_get_first_longest_token'.\n");
      if (eliminate_bcksl) 
	strcpy(first_longest_str, next);
      else
	//Concat the despecialisation character and the form
	strcat(first_longest_str, next);
    }
    free(next);
  }
  
  return first_longest_str;
}

////////////////////////////////////////////
// Get the Morfeusz' tokenization in which the first longest token is chosen each time
// 'im' = morphological interpretation of the string produced by Morfeusz
// If "eliminate_bcksl" is set to 1 each protecting backslash is omitted in the
// returned sequence.  
// Returns a pointer to the tokenization, or NULL on error.
tokenization* morfeusz_get_first_longest_tokenization(InterpMorf* im, int eliminate_bcksl) {
  tokenization* t; //Tokenization to be produced
  int p; //Current starting state
  int k; //Current arrival state
  char* ct; //Current token
  int ct_len; //Length of the sequence scanned for the current token  
  unichar* ct_utf16; //Current token in UTF-16
  int err; //Error code if any
  t = init_tokenization();
  
  p = 0; //Go to the initial state
  while (p != -1) {
    ct = morfeusz_get_first_longest_token(im, p, &k, &ct_len, eliminate_bcksl,0); //Get the next longest token
    //Convert the token into UTF-16 and add to the list of tokens
    if (ct && strlen(ct)) {
      ct_utf16 = convert_utf8_to_utf16(ct);
      free(ct);
      err = add_token(t,ct_utf16);
      if (!ct_utf16 || err) {
	free(ct_utf16);
	delete_tokenization(t);
	return NULL;
      }
    }     

    //Go to the next token
    p = k;
  }
  return t;
}

////////////////////////////////////////////
// Creates and initializes a tokenization
// 'nb_tokens' = number of tokens to be created
// Returns the created tokenization or NULL on errors
tokenization* init_tokenization() {
  tokenization* t = (tokenization*) malloc(sizeof(tokenization));
  if (!t)
    fatal_error("Memory allocation problem in function init_tokenization.\n");
  t->tokens = NULL;
  t->nb_tokens = 0;
  return t;
}

////////////////////////////////////////////
// Adds token 'tc' to the tokenization 't'
// The token 'tc' is not duplicated
// Returns 1 in case of errors, 0 otherwise
int add_token(tokenization* t, unichar* ct) {
  if (!t || !ct)
    return 1;
  t->tokens = (unichar**) realloc(t->tokens, (t->nb_tokens+1) * sizeof(unichar*));
  if (!t->tokens)
    fatal_error("Memory allocation problem in function 'add_token'.\n");
  t->tokens[t->nb_tokens] = ct;
  t->nb_tokens = t->nb_tokens + 1;
  return 0;
}

////////////////////////////////////////////
// Deletes a tokenization
// The main structure is liberated
void delete_tokenization(tokenization* tt) {
  if (!tt || !tt->tokens)
    return;
  int t; //Index of the current token
  for (t=0; t<tt->nb_tokens; t++)
    if (tt->tokens[t])
	free(tt->tokens[t]);
  free(tt->tokens);
  free(tt);
}
