 /*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

/**********************************************************************************/
/* CONVERTING FEATURE STRUCTURES INTO SURFACE FEATURE STRINGS AND VICE VERSA      */
/* (ACCORDING TO MORPHOLOGICAL EQUIVALENCES BETWEEN SIMPLE WORDS AND MULTIFLEX), */
/* e.g <Gen=masc_hum;Nb=plural;Case=mian> <=> :m1:pl:nomin                        */
/**********************************************************************************/

#include <stdio.h>
#include "MF_LangMorpho.h"
#include "MF_FeatStruct.h"
#include "MF_FeatStructStr.h"
#include "MF_Util.h"
#include "Error.h"
#include "MF_MorphoEquiv.h"
#include "Unicode.h"

//Separator of morphological features
unichar FEAT_SEP = (unichar) ':';

//unichar* omit_class(MultiFlexParameters* params, unichar* tag);
unichar* get_class_from_tag(MultiFlexParameters* params, unichar* tag, l_class_T** cl);
f_feat_struct_set_T* get_feat_struct_set_str(MultiFlexParameters* params, unichar* feat_str);
f_feat_struct_set_T* get_feat_struct_set_substr(MultiFlexParameters* params, unichar* feat_str);
f_catval_T* get_catval_set_str(MultiFlexParameters* params, unichar *feat_str, int* no_catvals);
unichar* get_str_feat_struct(MultiFlexParameters* params, f_feat_struct_T* feat);
f_catval_T* get_one_catval_str(MultiFlexParameters* params, unichar* feat);
unichar* get_one_str_catval(MultiFlexParameters* params, f_catval_T* feat);
l_class_T* get_class_str(MultiFlexParameters* params, unichar* cl_str);
l_class_T* get_class_str_direct(MultiFlexParameters* params, unichar* cl_str);
void delete_catvals(f_catval_T** catvals);


/****************************************************************************************/
/* "get a feature structure set (i.e. a set of category-value sets) from a string"      */
/* Produces a set of feature structures,                                                */
/* e.g. {<Gen=m1;Case=inst;Nb=pl;Deg=pos>,<Gen=m2;Case=inst;Nb=pl;De=pos>,              */
/* <Gen=m3;Case=inst;Nb=pl;Deg=pos>,<Gen=f;Case=inst;Nb=pl;Deg=pos>}                    */
/* from a feature string (e.g. "subst:pl:inst:m1.m2.m3.f|subst:pl:nom.acc:m1")          */
/* A features string is a sequence of feature substrings separated by a colon bar ('|').*/
/* See get_feat_struct_set_substr for the format of a feature substring.                */
/* If the feature string has a bad format or if a string component is not equivalent    */
/* to a morphological feature or in case of any other problem the function returns NULL */
/* The return structure is allocated in the function. The liberation has to take place  */
/* in the calling function (by f_delete_feat_struct_set).                               */
/* 'params' = "global" parameters                                                       */
f_feat_struct_set_T* get_feat_struct_set_str(MultiFlexParameters* params, unichar* feat_str) {
  l_class_T* cl;
  return get_feat_struct_set_class_str(params,feat_str,&cl);
}

/************************************************************************************************/
/* "get a feature structure set and a class (i.e. a set of category-value sets) from a string"  */
/* Produces a set of feature structures,                                                        */
/* e.g. {<Gen=m1;Case=inst;Nb=pl;Deg=pos>,<Gen=m2;Case=inst;Nb=pl;De=pos>,                      */
/* <Gen=m3;Case=inst;Nb=pl;Deg=pos>,<Gen=f;Case=inst;Nb=pl;Deg=pos>}                            */
/* as well an inflection class, e.g. subst                                                      */
/* from a feature string (e.g. "subst:pl:inst:m1.m2.m3.f|subst:pl:nom.acc:m1")                  */
/* A features string is a sequence of feature substrings separated by a colon bar ('|').        */
/* See get_feat_struct_set_substr for the format of a feature substring.                        */
/* If the feature string has a bad format or if a string component is not equivalent            */
/* to a morphological feature or in case of any other problem the function returns NULL         */
/* The return structure is allocated in the function. The liberation has to take place          */
/* in the calling function (by f_delete_feat_struct_set).                                       */
/* 'params' = "global" parameters                                                               */
f_feat_struct_set_T* get_feat_struct_set_class_str(MultiFlexParameters* params,unichar* feat_str,l_class_T** cl){
  //If the feature string is void so is the feature structure
  if (!feat_str)
    return NULL;

  int err; //Error code

  f_feat_struct_set_T* feat_struct_set;  //Set of all feature structures to be returned
  f_create_feat_struct_set(&feat_struct_set);  //Allocate the memory for the set of features
  f_init_feat_struct_set(feat_struct_set);
  f_feat_struct_set_T* new_feat_struct_set;  //Temporary set of extended feature structures

  //Convert each alternative feature string into a feature structure set
  int l;   //Length of a scanned sequence
  unichar feat_substr[MAX_MORPHO_TAG];  //buffer for an individual feature substring scanned from feat_str
  unichar* fss; //Pointer to the feature substring after the class name has been omitted 
  int done = 0; //Boolean indicating if the feature string is totally scanned

  do {
    //Extract a feature substring 
    l = u_scan_until_char(feat_substr,feat_str,MAX_MORPHO_TAG-1,(char*)"|",1); //Read the value substring e.g. "subst:pl:inst:m1.m2.m3.f"
    //fss = omit_class(params,feat_substr); //Omit the class tag, return the remaining substring, e.g. ":pl:inst:m1.m2.m3.f"
    fss = get_class_from_tag(params,feat_substr,cl);
    
    if (!fss) {
      error((char*)"Bad Morfeusz tag :%S.\n",feat_substr);
      return NULL;
    }

    //Get a feature structure set from the tag
    new_feat_struct_set = get_feat_struct_set_substr(params,fss);
    if (!new_feat_struct_set) {
      f_delete_feat_struct_set(&feat_struct_set);
      return NULL;
    }
    
    //Copy all structures obtained from the current substring to the previous structures
    err = f_merge_feat_struct_sets(feat_struct_set,new_feat_struct_set);
    if (err) {
      f_delete_feat_struct_set(&feat_struct_set);
      f_delete_feat_struct_set(&new_feat_struct_set);
      return NULL;
    }
    
    //Delete the new structures (which have been dublicated and added to feat_struct_set)
    f_delete_feat_struct_set(&new_feat_struct_set);    
    
    //Go to the next subsequence
    feat_str = feat_str + l; 
    if (feat_str[0] == (unichar) '|')  //If a new alternative present
      feat_str++; //Omit the '|'
    else  
      done = 1;
  } while (!done);
  
  return feat_struct_set;
}

////////////////////////////////////////////
// Input:
//   params: "global" parameters
//   tag: a Morfeusz tag, e.g. subst:pl:acc:m3
// Input/output:
//   cl: holder for a pointer to the class, to be filled by the function
// Output :
//   pointer to the beginning of the morphological values right after the class (here pointer to :pl:acc:m3)
//   NULL if the tag format incorrect (class tag not found at the beginning)
unichar* get_class_from_tag(MultiFlexParameters* params, unichar* tag, l_class_T** cl) {
  unichar tmp[MAX_CLASS_NAME];  //buffer for an individual element scanned from tag

  tag = tag + u_scan_while_char(tmp, tag, MAX_CLASS_NAME-1,(char*)" \t"); //Omit void characters at the beginning of the tag
  tag = tag + u_scan_until_char(tmp,tag,MAX_CLASS_NAME-1,(char*)" :",1); //Read the class tag
  //cl* = is_valid_class(params,tmp);
  *cl = is_valid_class(params,tmp);
  if (!cl)  //If the class tag invalid or empty
    return NULL;
  tag = tag + u_scan_while_char(tmp, tag, MAX_CLASS_NAME-1,(char*)" \t"); //Omit void characters after the class tag
  return tag;
}

/****************************************************************************************/
/* "get a feature structure set (i.e. a set of category-value sets) from a substring"   */
/* Produces a set of feature structures,                                                */
/* e.g. {<Gen=m1;Case=inst;Nb=pl;Deg=pos>,<Gen=m2;Case=inst;Nb=pl;Deg=pos>,             */
/* <Gen=m3;Case=inst;Nb=pl;Deg=pos>,<Gen=f;Case=inst;Nb=pl;Deg=pos>}                    */
/* from a feature string (e.g. ":pl:inst:m1.m2.m3.f:pos").                              */
/* A features substring is a sequence of positions separated by a FEAT_SEP.             */
/* Each position contains one or more morphological values from the same category       */
/* separated by a dot.                                                                  */
/* If the feature substring has a bad format or if a string component is not equivalent */
/* to a morphological feature or in case of any other problem the function returns NULL */
/* The return structure is allocated in the function. The liberation has to take place  */
/* in the calling function (by f_delete_feat_struct_set).                               */
/* 'params' = "global" parameters                                                       */
f_feat_struct_set_T* get_feat_struct_set_substr(MultiFlexParameters* params, unichar* feat_str) {

  //If the feature substring is void so is the feature structure
  if (!feat_str)
    return NULL;

  int err; //Error code

  f_feat_struct_set_T* feat_struct_set;  //Set of all feature structures to be returned
  f_create_feat_struct_set(&feat_struct_set);  //Allocate the memory for the set of features
  f_init_feat_struct_set_one_empty(feat_struct_set);
  f_feat_struct_set_T* new_feat_struct_set;  //Temporary set of extended feature structures

  int l;   //Length of a scanned sequence
  unichar tmp[MAX_MORPHO_TAG];  //buffer for an individual element scanned from feat_str
  //Omit void characters at the beginning of the feature string
  feat_str = feat_str + u_scan_while_char(tmp, feat_str, MAX_MORPHO_TAG-1,(char*)" \t");

  //Scan the feature string value by value
  f_catval_T* catvals;  //Set of category-value pairs obtained from a Morfeusz subsequence (e.g. m1.m2.m3.f.n1)
  int no_catvals; //Number of category-value pairs obtained
  while (feat_str[0] == FEAT_SEP) {
    feat_str++;  //Skip the semicolon
    l = u_scan_until_char(tmp,feat_str,MAX_MORPHO_TAG-1,(char*)":",1); //Read the value subsequence
    feat_str = feat_str + l; //Go to the next subsequence
    catvals = get_catval_set_str(params,tmp,&no_catvals); //Get the equivalent set of features (e.g. get {Gen=m1, Gen=m2, Gen=f} from "m1.m2.f")
    if (!catvals) {
      f_delete_feat_struct_set(&feat_struct_set);
      return NULL;
    }

    //Create and initialise a new empty set of feature structures
    f_create_feat_struct_set(&new_feat_struct_set); //Allocate the memory for the set of features
    f_init_feat_struct_set(new_feat_struct_set);

    int c; //Index of the current category-value pair
    for (c=0; c<no_catvals; c++) {
      f_feat_struct_set_T *feat_struct_set_cpy = f_duplicate_feat_struct_set(feat_struct_set);  //Create a copy of the previous structure set
      if (!feat_struct_set_cpy) {
	f_delete_feat_struct_set(&feat_struct_set);
	f_delete_feat_struct_set(&new_feat_struct_set);
	delete_catvals(&catvals);
	return NULL;
      }
      //Add the current category-value pair to each structure in feat_set_cpy. The resulting structure should be sorted.
      err = f_enlarge_feat_struct_set(params,feat_struct_set_cpy, catvals[c].cat, catvals[c].val, 1);
      if (err) {
	f_delete_feat_struct_set(&feat_struct_set);
	f_delete_feat_struct_set(&new_feat_struct_set);
	f_delete_feat_struct_set(&feat_struct_set_cpy);
	delete_catvals(&catvals);
	return NULL;
      }
      //Copy all structures from feat_set_cpy to new_feat_set
      err = f_merge_feat_struct_sets(new_feat_struct_set,feat_struct_set_cpy);
      if (err) {
	f_delete_feat_struct_set(&feat_struct_set);
	f_delete_feat_struct_set(&new_feat_struct_set);
	f_delete_feat_struct_set(&feat_struct_set_cpy);
	delete_catvals(&catvals);
	return NULL;
      }
      //Delete the temporary copy
      f_delete_feat_struct_set(&feat_struct_set_cpy);
    }
    //Replace the previous set of feature structures by the new one
    f_delete_feat_struct_set(&feat_struct_set);
    feat_struct_set = new_feat_struct_set; 
    new_feat_struct_set = NULL;
    //Delete the current set of category-value pairs
    delete_catvals(&catvals);
  }
  return feat_struct_set;
}

/****************************************************************************************/
/* "get a feature structure (i.e. a set of category-value pairs) from a string"         */
/* Produces a feature structures, e.g. {<Gen=m1;Case=inst;Nb=pl;Deg=pos>                */
/* from a feature string (e.g. ":pl:inst:m1:pos").                                      */
/* A valid features string here is a sequence of unique values separated by a FEAT_SEP. */
/* The feature string must correspond to one feature structure only,                    */
/* e.g.":pl:inst:m1.m2.f:pos" is not a correct string as it correponds to 3 feature     */
/* structures.                                                                          */
/* If the feature string has a bad format,or if it is ambiguous (contains multiple      */
/* for one category), or if a string component is not equivalent to a morphological     */
/* feature, or in case of any other problem the function returns NULL                   */
/* The return structure is allocated in the function. The liberation has to take place  */
/* in the calling function (by f_delete_feat_struct).                                   */
/* 'params' = "global" parameters                                                       */
f_feat_struct_T* get_feat_struct_str(MultiFlexParameters* params, unichar* feat_str) {
  f_feat_struct_set_T* fss; //Set of feature structures obtained from the sting
                           //It should contain one feature structure only
  fss = get_feat_struct_set_substr(params,feat_str);

  if (!fss)
    return NULL;

  if (fss->no_feat_structs != 1) {
    f_delete_feat_struct_set(&fss);
    return NULL;
  }
  
  f_feat_struct_T* fs; //The feature structure to be returned
  f_create_feat_struct(&fs);
  f_init_feat_struct(fs);
  //Copy the (first and only) feature structure from fss to fs
  int cv; //Index of the current category-value pair
  int no_catvals = fss->feat_structs[0].no_catvals; //Number of cateegory-value pairs to be copied
  //Allocate space for the category-value pairs
  fs->catvals = (f_catval_T*) malloc(no_catvals*sizeof(f_catval_T));
  if (!fs->catvals)
    fatal_error((char*)"Not enough memory in function 'get_feat_struct_str'!\n");
  for (cv=0; cv<no_catvals; cv++)
    fs->catvals[cv] = fss->feat_structs[0].catvals[cv];
  fs->no_catvals = no_catvals;
  f_delete_feat_struct_set(&fss);

  return fs;
}

/////////////////////////////////////////////////////////////////////////////////////////////
//"get a set of category-value pairs from a string"
// Produces a set of category-value pairs, all concerning the same category 
// (e.g. {Gen=m1,Gen=m2,Gen=fem} from a feature string (e.g. "m1.m2.f").
// Input: feat_str - the unicode string representing a sequence of morphological values separated by dots
//        params - "global" parameters
// Output: 
//    - result: pointer to the set of feature-value pairs equivalent to feat_str                              
//    - no_catvals - output parameter; the number of produced feature-value pairs
// If 'feat_str' is empty or is an invalid value sequence, the output value is NULL.
// The resulting structure is allocated in the function. It should be liberated by the calling function by delete_catvals
// (the structures pointed by the elements of the result are parts of a global structure and should not be liberated
// until the end of the program).
f_catval_T* get_catval_set_str(MultiFlexParameters* params, unichar *feat_str, int* no_catvals) {

  f_catval_T* catvals=NULL;  //Set of category-value pairs obtained from a Morfeusz subsequence (e.g. m1.m2.m3.f.n1)
  int l;   //Length of a scanned sequence
  unichar tmp[MAX_MORPHO_TAG];  //buffer for an individual element scanned from feat_str
  f_catval_T* cv; //Current category-value pair  

  *no_catvals=0; //Number of category-value pairs obtained
  while (feat_str[0] != '\0') {
 
    //Omit void characters at the beginning of the feature string
    feat_str = feat_str + u_scan_while_char(tmp, feat_str, MAX_MORPHO_TAG-1,(char*)" \t");
    //Read one value
    l = u_scan_until_char(tmp,feat_str,MAX_MORPHO_TAG-1,(char*)" .",1);
    feat_str = feat_str + l;
    if (feat_str[0] == (unichar) '.') //Omit the dot 
      feat_str = feat_str + 1;  

    //Check the value's category
    if (l) {
      cv = get_one_catval_str(params,tmp); //Get the category-value pair equivalent to the current dictionary feature string
      if (!cv) {  //If invalid feature
	delete_catvals(&catvals);
	return NULL;
      }
      //If the previous features found in the same string are of the same category, add the new feature
      if (*no_catvals==0 || catvals[(*no_catvals)-1].cat == cv->cat) {
	catvals = (f_catval_T*)realloc(catvals,((*no_catvals)+1)*sizeof(f_catval_T));
	if (!catvals) {
	  delete_catvals(&catvals);
	  fprintf(stderr,"Memory allocation problem in function 'get_feat_set_str'!\n");
	  return NULL;
	}
	catvals[*no_catvals] = *cv;
	(*no_catvals)++;
      }
    }
  }
  return catvals;
}

/**************************************************************************************/
/* Produces a feature string (e.g. ":zen:narz:poj")) from a feature structure         */
/* (e.g.  <Gen=fem;Case=Inst;Nb=sing>).                                               */
/* 'params' = "global" parameters                                                     */
/* The return string is allocated in the function. The liberation has to take place   */
/* in the calling function.                                                           */
/* If 'feat' is empty or a morphological feature has no corresponding string value,   */
/* returns NULL.                                                                      */
unichar* get_str_feat_struct(MultiFlexParameters* params, f_feat_struct_T* feat) {
  int f;  //index of the current category-value pair in 'feat'
  unichar* f_str; //Pointer to the current feature string

  if (!feat || (feat->no_catvals==0))
    return NULL;
  
  //Allocate and initialise the feature string
  unichar* tmp;
  tmp = (unichar*) malloc( (feat->no_catvals * (MAX_MORPHO_TAG+1) + 1 ) * sizeof(unichar));
  if (!tmp)
    fatal_error((char*)"Not enough memory in function 'get_str_feat_struct'!\n");
  tmp[0] = (unichar) '\0';
  
  for (f=0; f<feat->no_catvals; f++) {
    //Concatenante the current value if it is not empty
    if (!is_empty_val(feat->catvals[f].cat,feat->catvals[f].val)) {
      f_str = get_one_str_catval(params, &(feat->catvals[f]));
	  if (!f_str) {
	    free(tmp);
	    return NULL;
	  }
	  unichar sep[] = {FEAT_SEP,(unichar)'\0'};
	  u_strcat(tmp,sep);
	  u_strcat(tmp,f_str);
	}
   }
 return tmp;
}

/****************************************************************************************************/
/* "get one category-value pair from a string"                                                      */
/* Points at the category-value pair (e.g. <Gen=fem>) equivalent to a feature string (e.g. "zen").  */
/* No structure is allocated in the function. The result poits to a global structure that should    */
/* not be liberated except at the end of the programme.                                             */
/* If the string value 'feat' is empty or it has no corresponding category-value pair returns NULL. */
/* 'params' = "global" parameters                                                                   */
f_catval_T* get_one_catval_str(MultiFlexParameters* params, unichar* feat_str) {
  int e;  //index of the current equivalence
  
  if (!feat_str || !u_strlen(feat_str))
    return NULL;

  for (e=0; e<params->L_MORPHO_EQUIV.no_equiv; e++)
    if (!u_strcmp(params->L_MORPHO_EQUIV.equiv[e].SU_feat,feat_str)) {
      return &(params->L_MORPHO_EQUIV.equiv[e].MU_catval);
    }

  return NULL;  //If no equivalent feature structure found
}

/***************************************************************************************************/
/* "get one string from a category-value pair"                                                     */
/* Points at the feature string (e.g. "zen") equivalent to a category-value pair (e.g. <Gen=fem>). */
/* No string is allocated in the function. The result points to a global structure that should not */
/* be liberated except at the end of the programme.                                                */
/* If 'feat' empty or the category-value pair has no corresponding character value returns NULL.   */
/* 'params' = "global" parameters                                                                  */
unichar* get_one_str_catval(MultiFlexParameters* params, f_catval_T* feat) {
  int e; //index of the current equivalence
 
  if (!feat)
    return NULL;

  for (e=0; e<params->L_MORPHO_EQUIV.no_equiv; e++)
    if ((feat->cat == params->L_MORPHO_EQUIV.equiv[e].MU_catval.cat) && (feat->val == params->L_MORPHO_EQUIV.equiv[e].MU_catval.val) )
      return params->L_MORPHO_EQUIV.equiv[e].SU_feat;

  return NULL;
}

///////////////////////////////////////////////////////////////////////
// Deletes the set of category-value pairs.
// The structures pointed by each pair are global and should not be liberated
// 'catvals' is transferred as a double pointer so that
// it can remain NULL after the function return
void delete_catvals(f_catval_T** catvals){
  if (catvals) {
    if (*catvals) {
      free (*catvals);
      *catvals = NULL;
    }
  }
}

/**************************************************************************************/
/* Returns the class (e.g. noun) corresponding to a class string as it appears in a   */
/* dictionary (e.g. "N"). If no class corresponds to the string, returns NULL.        */
/* The return structure is NOT allocated in the function.                             */
/* 'params' = "global" parameters                                                     */
l_class_T* get_class_str(MultiFlexParameters* params, unichar* cl_str) {
  int c;  //index of the current class in the equivalence set
  for (c=0; c<params->L_CLASS_EQUIV.no_equiv; c++)
    if (!u_strcmp(cl_str,params->L_CLASS_EQUIV.equiv[c].SU_class))
      return params->L_CLASS_EQUIV.equiv[c].MU_class;
  return NULL;
	
}

/*****************************************************************************/
/* Returns the pointer to the class (e.g. <noun,...>) directly corresponding */
/* to the class string cl_str (e.g. "noun").                                 */
/* If no class corresponds to the string returns NULL.                       */
/* The return structure is NOT allocated in the function.                    */
/* 'params' = "global" parameters                                            */
l_class_T* get_class_str_direct(MultiFlexParameters* params, unichar* cl_str) {
  int c;  //index of the current class in the equivalence set
  for (c=0; c<params->L_CLASSES.no_classes; c++)
    if (!u_strcmp(cl_str,params->L_CLASSES.classes[c].name))
      return &(params->L_CLASSES.classes[c]);
  return NULL;
}
