/*
  * Multiflex for Morfeusz - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */
/* This file contains parts of the Unitex software distributed under the terms of LGPL.   */
/* It is a modified and extended version of Inflect.cpp file of the the Unitex 1.2 beta   */
/* version downloaded from http://www-igm.univ-mlv.fr/~unitex/ on June 6, 2005.           */ 
/* The modifications has been done by Agata Savary (agata.savary@univ-tours.fr).          */
/* Latest modification on December 6, 2005.                                               */


/********************************************************************************/
/* MORPHOLOGY AND INFLECTION OF A SIMPLE WORD                                   */
/********************************************************************************/

#include "MF_Convert.h"
#include "Error.h"
#include "Unicode.h"
#include <string.h>
#include <stdlib.h>

#ifndef __WIN32
// LINUX CONVERSION
#include <iconv.h>

//Conversion descriptors
iconv_t cd_utf8_to_utf16;
iconv_t cd_utf16_to_utf8;

////////////////////////////////////////////
// Initializes the conversion descriptors
// Returns 1 in case of errors, 0 otherwise
int init_convert() {

  //Opening the conversion descriptor for iconv from UTF-16 to UTF-8
  cd_utf16_to_utf8 = iconv_open ("UTF-8", "UTF-16LE");
  if (cd_utf16_to_utf8 == (iconv_t)(-1)) {
    error("The conversion descriptor from UTF-16 to UTF-8 could not be initialized.\n");
    return 1;
  }

  //Opening the conversion descriptor for iconv from UTF-8 to UTF-16
  cd_utf8_to_utf16 = iconv_open ("UTF-16LE", "UTF-8");
  if (cd_utf8_to_utf16 == (iconv_t)(-1)) {
    error("The conversion descriptor from UTF-8 to UTF-16 could be not initialized.\n");
    return 1;
  }

  return 0;
}

////////////////////////////////////////////
// Deallocates the conversion descriptors 
// Returns 1 in case of errors, 0 otherwise
int free_convert() {

  //Deallocating the conversion descriptor for iconv from UTF-16 to UTF-8
  if (iconv_close(cd_utf16_to_utf8)) {
    error("The conversion descriptor from UTF-16 to UTF-8 could not be liberated.\n");
    return 1;
  }

  //Deallocating the conversion descriptor for iconv from UTF-8 to UTF-16
  if (iconv_close(cd_utf8_to_utf16)) {
    error("The conversion descriptor from UTF-8 to UTF-16 could not be liberated.\n");
    return 1;
  }

  return 0;
}


////////////////////////////////////////////
// Converts a UTF-16 string into a UTF-8 string.
// 'utf16_s' = UTF-16 string to be converted
// The resulting UTF-8 string is created in the function
// It must be liberated by the calling function
// Returns the converted string on succes, NULL otherwise.
char* convert_utf16_to_utf8(unichar* utf16_s) {

  //Creating an empty UTF-8 string
  size_t utf8_s_size = (u_strlen(utf16_s)+1) * 4 * sizeof(char); //A UTF-8 character may take up to 4 bytes
  char* utf8_s = (char*) malloc(utf8_s_size);
  char* utf8_s_cp = utf8_s;
  if (!utf8_s)
    fatal_error("Memory allocation problem in function 'convert_utf16_to_utf8'!\n");

  //Converting a UTF-16 string into UTF-8
  size_t utf16_s_size = (u_strlen(utf16_s)+1) * sizeof(unichar);  //Size in bytes of the converted string
  size_t res = (size_t)0; //Result of convertion
  res = iconv(cd_utf16_to_utf8, (char**)(&utf16_s), &utf16_s_size, &utf8_s_cp, &utf8_s_size);
  if (res == (size_t)-1) {
    error("No conversion to UTF-8 could be done for: %S.\n",  utf16_s);
    free(utf8_s);
    return NULL;
  }

  return utf8_s;
}

////////////////////////////////////////////
// Converts a UTF-8 string into a UTF-16 string.
// 'utf8_s' = UTF-8 string to be converted
// The resulting UTF-16 string is created in the function
// It must be liberated by the calling function
// Returns the converted string on succes, NULL otherwise.
unichar* convert_utf8_to_utf16(char* utf8_s) {

  //Creating an empty UTF-16 string
  //A UTF-16 character takes at most twice as many bytes as a UTF-16 character
  size_t utf16_s_size = (strlen(utf8_s)+1) * sizeof(unichar); 
  unichar* utf16_s = (unichar*) malloc(utf16_s_size);
  unichar* utf16_s_cp = utf16_s;
  if (!utf16_s)
    fatal_error("Memory allocation problem in function 'convert_utf16_to_utf8'!\n");

  //Converting a UTF-8 string into UTF-16 
  size_t utf8_s_size = (strlen(utf8_s)+1) * sizeof(char);  //Size of the converted string
  size_t res = (size_t)0; //Result of convertion
  res = iconv(cd_utf8_to_utf16, (char**)&utf8_s, &utf8_s_size, (char**)&utf16_s_cp, &utf16_s_size);
  if (res == (size_t)-1) {
    error("No conversion to UTF-16 could be done for: %s.\n", utf8_s);
    free(utf16_s);
    return NULL;
  }

  return utf16_s;
}


#else
// WINDOWS CONVERSION

#include <windows.h>

////////////////////////////////////////////
// Initializes the conversion descriptors (needed only for Linux)
// Returns 1 in case of errors, 0 otherwise
int init_convert() {
  return 0;
}

////////////////////////////////////////////
// Deallocates the conversion descriptors (needed only for Linux)
// Returns 1 in case of errors, 0 otherwise
int free_convert() {
  return 0;
}

////////////////////////////////////////////
// Converts a UTF-16 string into a UTF-8 string.
// 'utf16_s' = UTF-16 string to be converted
// The resulting UTF-8 string is created in the function
// It must be liberated by the calling function
// Returns the converted string on succes, NULL otherwise.
char* convert_utf16_to_utf8(unichar* utf16_s) {
  //Creating an empty UTF-8 string
  size_t utf8_s_size = (u_strlen(utf16_s)+1) * 4 * sizeof(char); //A UTF-8 character may take up to 4 bytes
  char* utf8_s = (char*) malloc(utf8_s_size);
  if (!utf8_s)
    fatal_error("Memory allocation problem in function 'convert_utf16_to_utf8'!\n");

  WideCharToMultiByte(CP_UTF8, 0, (WCHAR*) utf16_s, -1, utf8_s, utf8_s_size, NULL, NULL);
  return utf8_s;
}

////////////////////////////////////////////
// Converts a UTF-8 string into a UTF-16 string.
// 'utf8_s' = UTF-8 string to be converted
// The resulting UTF-16 string is created in the function
// It must be liberated by the calling function
// Returns the converted string on succes, NULL otherwise.
unichar* convert_utf8_to_utf16(char* utf8_s) {
  //Creating an empty UTF-16 string
  //A UTF-16 character takes at most twice as many bytes as a UTF-16 character
  size_t utf16_s_size = (strlen(utf8_s)+1) * sizeof(unichar); 
  unichar* utf16_s = (unichar*) malloc(utf16_s_size);
  if (!utf16_s)
    fatal_error("Memory allocation problem in function 'convert_utf16_to_utf8'!\n");

  MultiByteToWideChar(CP_UTF8, 0, utf8_s, -1, (WCHAR*) utf16_s, utf16_s_size);
  return utf16_s;
}
#endif

