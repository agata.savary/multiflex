/*
  * Unitex 
  *
  * Copyright (C) 2001-2007 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on July 11 2005
 */
//---------------------------------------------------------------------------

/*********************************************************************************************/
/* Scanning and printing the graph node labels in an inflection graphs for multi-word units  */
/*********************************************************************************************/

#include "MF_MU_graph_label.h"
#include "MF_Util.h"
#include "MF_FeatStruct.h"
#include "MF_FeatStructStr.h"
#include "MF_DLC_inflect.h"
#include "MF_MU_embedded.h"
#include "Error.h"

int MU_graph_scan_label(unichar* label_in, unichar* label_out, MU_graph_label_T* MU_label,U_lemma_T* MU_lemma);
int MU_graph_scan_label_in(MultiFlexParameters* params,unichar* label, MU_graph_in_T* MU_label_in,U_lemma_T* MU_lemma);
int MU_graph_scan_label_in_var(unichar* label, MU_graph_in_T* MU_label_in, U_lemma_T* MU_lemma);
int MU_graph_scan_label_in_ext(MultiFlexParameters* params,unichar* label, MU_graph_in_T* MU_label_in,U_lemma_T* MU_lemma);
int MU_graph_scan_label_out(MultiFlexParameters* params, unichar* label, MU_graph_out_T* MU_label_out,U_lemma_T* MU_lemma);
int MU_graph_scan_graph_feat_struct(MultiFlexParameters* params, unichar* label, MU_graph_feat_struct_T* MU_graph_feat_struct,U_lemma_T* MU_lemma);
int MU_graph_scan_get_values_str(unichar* buff, unichar* label,int max);
int MU_graph_scan_graph_variable_value(MultiFlexParameters* params, unichar* label, MU_graph_catval_T* MU_graph_catval,U_lemma_T* MU_lemma);
int MU_graph_scan_graph_variable_value_inherit_const(MultiFlexParameters* params, unichar* label, MU_graph_catval_T* MU_graph_catval,U_lemma_T* MU_lemma);
int MU_graph_scan_graph_variable_value_inherit_var(unichar* label, MU_graph_catval_T* MU_graph_catval,U_lemma_T* MU_lemma);
int MU_graph_scan_graph_variable_value_unif_var(unichar* label, MU_graph_catval_T* MU_graph_catval,U_lemma_T* MU_lemma);
int MU_graph_scan_graph_variable_value_unif_var_values(unichar* label, MU_graph_catval_T* MU_graph_catval,U_lemma_T* MU_lemma);
//int MU_graph_scan_graph_variable_value_unif_var_all_values(MU_graph_catval_T* MU_graph_catval);
int MU_graph_scan_graph_constant_value(unichar* label, MU_graph_catval_T* MU_graph_catval,U_lemma_T* MU_lemma);
int* MU_graph_scan_graph_restricted_values(unichar* label, l_category_T* cat, U_lemma_T* MU_lemma);
void MU_graph_print_label(MU_graph_label_T* MU_label);
void MU_graph_print_feat_struct(MU_graph_feat_struct_T* MU_feat_struct);
void MU_graph_print_values(l_category_T* cat, int* values);
void MU_graph_free_label(MU_graph_label_T* MU_label);
void MU_graph_free_feat_struct(MU_graph_feat_struct_T* MU_feat_struct);

/////////////////////////////////////////////////
// Creates a MU_graph label from two strings. 
// params = "global" parameters
// label_in = inout label of the node
// label_out = output label of a node
// 'MU_lemma' = multi-word unit lemma being inflected
// MU_label = output parameter: the resulting graph node
// We suppose that MU_label already has its memory allocated.
// Returns 0 on success, 1 otherwise.
int MU_graph_scan_label(MultiFlexParameters* params, unichar* label_in, unichar* label_out, MU_graph_label_T* MU_label,U_lemma_T* MU_lemma) {
  int err1, err2;
  err1 = err2 =0;

  if (!u_strcmp(label_in,"<E>"))  //Epsilon case
    MU_label->in = NULL;
  else {
    MU_label->in = (MU_graph_in_T*)malloc(sizeof(MU_graph_in_T));
    err1 = MU_graph_scan_label_in(params, label_in, MU_label->in, MU_lemma);
  }

  if (label_out==NULL || (!u_strcmp(label_out,"<E>")) || (!u_strlen(label_out)) ) //Case of epsilon or void output
    MU_label->out = NULL;
  else {
    MU_label->out = (MU_graph_out_T*)malloc(sizeof(MU_graph_out_T));
    err2 = MU_graph_scan_label_out(params, label_out, MU_label->out, MU_lemma);
  }
  return (err1 or err2);
}


/////////////////////////////////////////////////
// Creates a MU_graph label's input from a string (e.g. "of" or "<$1:Gen==$g;Nb=$n;Case=Inst>"  
// or "<trzeci.0:adj:Case=$c;Gen=$1.Gen;Nb=$1.Nb>"
// We suppose that MU_label_in already has its memory allocated.
// 'params' = "global" parameters
// 'MU_lemma' = multi-word unit lemma being inflected
// Returns 0 on success, 1 otherwise.
int MU_graph_scan_label_in(MultiFlexParameters* params, unichar* label, MU_graph_in_T* MU_label_in,U_lemma_T* MU_lemma) {
  int l; //length of a scanned sequence
  unichar* pos; //current position in label
  unichar tmp[MAX_GRAPH_NODE];
  int err=0;
  
  pos = label;

  //Omit void characters 
  //pos = pos + u_scan_while_char(tmp, pos, MAX_GRAPH_NODE-1," \t");
  //Omitting void characters in the beginning is incorrect: we do not allow a space
  //to appear in a node as a constituent to be generated unless it appears in quotes

  //Constant unit, e.g. "of"
  if (*pos != (unichar)'<') {
    l = u_scan_until_char(tmp,pos,MAX_GRAPH_NODE-1,"",1);
    MU_label_in->unit.type = cst;
    MU_label_in->unit.u.seq = (unichar*) malloc((u_strlen(tmp)+1) * sizeof(unichar));
    u_strcpy(MU_label_in->unit.u.seq,tmp);    
    MU_label_in->feat_struct = NULL;
    pos = pos + l;
  }
  else { 
    pos++;  //Omit the '<'
    if (*pos == (unichar)'$') { //Variable unit, e.g. <$2:Gen==$g> 
      MU_label_in->unit.type = var;
      l = MU_graph_scan_label_in_var(pos, MU_label_in, MU_lemma);
      if (l<0)
	return 1;
    }
    else { //External lemma, e.g. <trzeci.0:adj:Case=$c;Gen=$1.Gen;Nb=$1.Nb>
      MU_label_in->unit.type = ext; 
      l = MU_graph_scan_label_in_ext(params, pos, MU_label_in, MU_lemma);
      if (l<0)
	return 1;
    }

    pos = pos + l;  //Variable or external lemma consumed

    //Omit void characters
    pos = pos + u_scan_while_char(tmp, pos, MAX_GRAPH_NODE-1," \t");
    
    //A ':' or a '>' must follow
    if ((*pos != (unichar) ':') && (*pos != (unichar) '>')) {
      error("In graph %s label format incorrect in ",MU_lemma->paradigm);
      error("Graph label format incorrect in %S",label);
      error(" (at position %d): ",(int)(pos-label));
      error("':' or '>' missing.\n");
      return 1;
    }

    //Check if morphology follows
    if (*pos == (unichar) ':') {
      pos++;
      MU_label_in->feat_struct = (MU_graph_feat_struct_T*) malloc(sizeof(MU_graph_feat_struct_T));
      l = MU_graph_scan_graph_feat_struct(params, pos, MU_label_in->feat_struct,MU_lemma);
      if (l < 0)
	return 1;
      pos = pos + l;
    }
    else {
      MU_label_in->feat_struct = NULL;
      if (MU_label_in->unit.type == ext) { //An external lemma must be followed by features (otherwise no form is specified)
	error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
	error(" (at position %d): character ':' missing.\n",(int)(pos-label));
	return 1;	
      }
    }
    //Closing '>'
    if (*pos !=  (unichar)'>') {
      error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
      error(" (at position %d): ",(int)(pos-label));
      error("'>' missing.\n");
      return 1;
    }
    pos++;
  }

  //Omit void characters
  pos = pos + u_scan_while_char(tmp, pos, MAX_GRAPH_NODE-1," \t");

  if (*pos != 0) {
    error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
    error(" (at position %d): ",(int)(pos-label));
    error(" end of label expected.\n");
    return 1;      
  }
  return err;
}

/////////////////////////////////////////////////
// Scans a variable idetifier from input string, e.g. $2   
// We suppose that MU_label_in already has its memory allocated.
// 'MU_lemma' = multi-word unit lemma being inflected
// Returns the length of the scanned sequence on success, -1 otherwise.
int MU_graph_scan_label_in_var(unichar* label, MU_graph_in_T* MU_label_in, U_lemma_T* MU_lemma) {
  if (!MU_label_in || !MU_lemma || !label)
    return -1;

  int l; //length of a scanned sequence
  unichar* pos; //current position in label
  unichar tmp[MAX_GRAPH_NODE];
  pos = label;
  pos++;  //Omit the '$'
  l = u_scan_while_char(tmp, pos, MAX_GRAPH_NODE-1,"0123456789");
  if (!l) {
    error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
    error(" (at position 1): unit number missing after \'$\'.\n");
    return -1;
  }
  char tmp_char[MAX_GRAPH_NODE];
  u_to_char(tmp_char,tmp);
  MU_label_in->unit.u.num = atoi(tmp_char);
  pos = pos + l;
  
  return l+1;
}

/////////////////////////////////////////////////
// Scans an external lemma from input string, e.g. trzeci.0:adj   
// We suppose that MU_label_in already has its memory allocated.
// 'params' = "global" parameters
// 'MU_lemma' = multi-word unit lemma being inflected
// Returns the length of the scanned sequence on success, -1 otherwise.
int MU_graph_scan_label_in_ext(MultiFlexParameters* params, unichar* label, MU_graph_in_T* MU_label_in,U_lemma_T* MU_lemma) {
  if (!label || !MU_label_in || !MU_lemma)
    return -1;

  U_lemma_T tmp_lemma; //Temporary lemma
  U_init_lemma(&tmp_lemma,simple);

  int l; //length of a scanned sequence
  unichar* pos; //current position in label
  pos = label;

  //Get the lemma e.g. "trzeci"
  unichar* lemma_str;
  lemma_str = NULL;
  l= DLC_get_constit_lemma_str(params, label, 0, &lemma_str);
  if (l<0) 
    return -1;
  pos = pos + l;
  tmp_lemma.simple_lemma = lemma_str;

  //Get the inflection paradigm
  char* para;
  para = NULL;
  l= DLC_get_paradigm(label, (int)(pos-label), &para, 0);
  if (l<0) {
    U_delete_lemma(&tmp_lemma,0);
    return -1;
  }
  pos = pos + l;
  tmp_lemma.paradigm = para;

  //Get the inflection class
  l= DLC_get_class(params, label, (int)(pos-label), &(tmp_lemma.cl), 0);
  if (l<0) {
    U_delete_lemma(&tmp_lemma,0);
    return -1;
  }
  pos = pos + l;

  //If it is a compound lemma, its annotation must be found in DELAC files of embedded compounds
  int err; //Error code if any
  if (U_is_compound(params, tmp_lemma.simple_lemma)) {
    err = get_MU_annotation(params, tmp_lemma.simple_lemma, tmp_lemma.cl, atoi(tmp_lemma.paradigm), &(MU_label_in->unit.u.ext));
    if (err) {
      error("No annotation could be found for: %S\nin entry: %S.\n",tmp_lemma.simple_lemma , label);
      U_delete_lemma(&tmp_lemma,0);
      return -1;
    }
    U_delete_lemma(&tmp_lemma,0);
  }
  
  //If it is a simple lemma, duplicate it
  else {
    MU_label_in->unit.u.ext = (U_lemma_T*)malloc(sizeof(U_lemma_T));
    if (!MU_label_in->unit.u.ext)
      fatal_error("Memory allocation problem in function 'MU_graph_scan_label_in_ext'!\n");			
    err = U_duplicate_lemma(MU_label_in->unit.u.ext,&tmp_lemma);
    U_delete_lemma(&tmp_lemma,0);
    if (err)
      return -1;
  }
  return (int) (pos-label);
}

/////////////////////////////////////////////////
// Creates a MU_graph label's output structure from a string.    
// We suppose that MU_label_out already has its memory allocated.
// 'params' = "global" parameters
// 'MU_lemma' = muti-word unit lemma being inflected
// Returns 0 on success, 1 otherwise.
int MU_graph_scan_label_out(MultiFlexParameters* params, unichar* label, MU_graph_out_T* MU_label_out,U_lemma_T* MU_lemma) {
  int cv;  //number of the current category-value pair
  int l; //length of a scanned sequence
  unichar* pos; //current position in label

  pos = label;

  //Opening '<'
  if (*pos != (unichar) '<') {
    error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
    error(" (at position %d): ",(int)(pos-label));
    error(" '<'  expected.\n");
    return 1;      
  }
  pos++;
  l = MU_graph_scan_graph_feat_struct(params, pos, MU_label_out,MU_lemma);
  if (l < 0)
    return 1;
  pos = pos + l;
  for (cv=0; cv<MU_label_out->no_catvals; cv++)
    if (MU_label_out->catvals[cv].type == inherit_var) {
      error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
      error(": an output label may not contain a double assignment \'==\'.\n");
      return 1;
    }
  //Closing '>'
  if (*pos != (unichar) '>') {
    error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
    error(" (at position %d): ",(int)(pos-label));
    error(" '>'  expected.\n");
    return 1;      
  }
  return 0;
}

/////////////////////////////////////////////////
// Scans a MU_graph morpho structure from an input string.    
// We suppose that MU_graph_feat_struct already has its memory allocated.
// 'params' = "global" parameters
// 'MU_lemma' = multi-word unit lemma being inflected
// Returns the length of the scanned sequence on success, -1 otherwise.
int MU_graph_scan_graph_feat_struct(MultiFlexParameters* params, unichar* label, MU_graph_feat_struct_T* MU_graph_feat_struct,U_lemma_T* MU_lemma) {
  int l; //length of a scanned sequence
  unichar* pos; //current position in label
  unichar tmp[MAX_GRAPH_NODE];
  int cv;  //number of the current category-value pair
  l_category_T* cat;
  int dbl_eq; //Checks if a double equal sign has appeared
  int err; //Error code
  
  pos = label;

  //Category-value pairs
  int done = 0;
  cv = 0;
  while (!done) {
    //Omit void characters
    pos = pos + u_scan_while_char(tmp, pos, MAX_GRAPH_NODE-1," \t");
    
    //Category, e.g. Nb
    l = u_scan_until_char(tmp,pos,MAX_GRAPH_NODE-1," \t=",1);
    cat = is_valid_cat(params, tmp);
    if (!cat) { 
      error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
      error(" (at position %d): %S",(int)(pos-label),tmp);
      error(" is not a valid category\n");
      return -1;
    }    
    MU_graph_feat_struct->catvals[cv].cat = cat;
    pos = pos+l;
    
    //Omit void characters
    pos = pos + u_scan_while_char(tmp, pos, MAX_GRAPH_NODE-1," \t");
    
    //The '=' character
    if (*pos != (unichar) '=') {
      error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
      error(" (at position %d): ",(int)(pos-label));
      error("\'=\' missing.\n");
      return -1;
    }
    pos++;
    
    //The double '==' if any
    if (*pos == (unichar) '=') {
      dbl_eq = 1;
      pos++;
    }
    else 
      dbl_eq = 0;

    //Omit void characters
    pos = pos + u_scan_while_char(tmp, pos, MAX_GRAPH_NODE-1," \t");

    //Scan values
    l = MU_graph_scan_get_values_str(tmp,pos,MAX_GRAPH_NODE-1);    //Get the current value string   
    if (l==-1)
      return -1;
    if (*tmp == (unichar) '$') {
      if (!dbl_eq)
	err = MU_graph_scan_graph_variable_value(params,tmp,&(MU_graph_feat_struct->catvals[cv]),MU_lemma);
      else //Inheritence variable
	err = MU_graph_scan_graph_variable_value_inherit_var(tmp,&(MU_graph_feat_struct->catvals[cv]),MU_lemma);
    }
    else {
      if (dbl_eq) {  //No constant value after a double equation
	error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
	error(" (at position %d): ",(int)(pos-label));
	error("a variable missing after \'==\'.\n");
	return -1;      
      }
      err = MU_graph_scan_graph_constant_value(tmp,&(MU_graph_feat_struct->catvals[cv]),MU_lemma);
    }
    if (err)
	return -1;      
    pos = pos + l;  //go to the end of the current category-vale pair
    
    //New category-value pair read in
    cv++;

    //Omit void characters
    pos = pos + u_scan_while_char(tmp, pos, MAX_GRAPH_NODE-1," \t"); 
    
    //See if end of category-value pairs
    if ((*pos == (unichar)'>') || (*pos == (unichar)'\0'))
      done = 1;
    else { 
      if (*pos != (unichar)';') {
	error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
	error(" (at position %d): %S",(int)(pos-label),tmp);
	error(" ';' missing\n");
	return -1;      
      }
      pos++; //Omit the ';'
    }
  }
  MU_graph_feat_struct->no_catvals = cv;
  
  //Omit void characters
  pos = pos + u_scan_while_char(tmp, pos, MAX_GRAPH_NODE-1," \t"); 
  
  return pos-label;
}

/////////////////////////////////////////////////
// Scans a string of values from the label.    
// label = current label stating with a value string e.g. $c=inst|loc|<E>;Gen=f>
// max = maximum length of the scanned sequence
// buff = output parameter, to which the value string is copied (e.g. $c=inst|loc|<E>)
// We suppose that buff already has its memory allocated.
// Returns the length of the scanned sequence on succes, -1 otherwise.
int MU_graph_scan_get_values_str(unichar* buff, unichar* label,int max) {
  int c=0; //Index of the current character
  int nb_l_angles=0; //Number of unlatched left angles found
  int done=0; //Boolean saying if the value string is terminated
  
  while (label[c] && c<max && !done) {
    buff[c] = label[c];
    switch (label[c]) {
    case (unichar)'<':   //New left angle met
      nb_l_angles++;
      break;
    case (unichar) '>':
      nb_l_angles--;  //A new left angle matched
      if (nb_l_angles<0) {
	buff[c] = (unichar)'\0';
	c--; //The '>' character should not be consumed
	done = 1;
      }
      break;
    case (unichar) ';':
      if (!nb_l_angles) { //A ';' outside '<...>' marks the end of the value string
	buff[c] = (unichar)'\0';
	c--; //The ';' character should not be consumed
	done = 1;
      }
      break;
    }
    c++; //Go to the next character
  }
  if (c==max && !done) {
    buff[c-1] = (unichar)'\0';
    error("Graph label too long: %S\n", label);
    return -1;
  }
  return c;
}

/////////////////////////////////////////////////
// Fills out the value in a category-value pair appearing in a graph node.    
// label = value string whose prfix is to be analysed, beginning with a '$', e.g. $c, $1.Nb, $c=nom|acc
// 'params' = "global" parameters
// MU_graph_catval = category-value pair whose value needs to be filled out
// 'MU_lemma' = muti-word unit lemma being inflected
// We suppose that MU_graph_catval already has its memory allocated.
// Returns 0 on success, 1 otherwise.
int MU_graph_scan_graph_variable_value(MultiFlexParameters* params, unichar* label, MU_graph_catval_T* MU_graph_catval,U_lemma_T* MU_lemma) {
  unichar* pos; //Current position in the label
  int l; //Length of the scanned sequence
  unichar tmp[MAX_GRAPH_NODE];  //Temporary buffer

  pos = label;
  pos++;  //omit the '$'

  //Check if the value is inherited from a constituent, e.g. $1.Nb
  l = u_scan_while_char(tmp, pos, MAX_GRAPH_NODE-1,"0123456789"); //Try to scan a constituent's number
  if (l) {
    if (MU_graph_scan_graph_variable_value_inherit_const(params, pos, MU_graph_catval, MU_lemma))
      return 1;
  }
  
  //If the value is not inherited from a constituent, it is a unification value, e.g. $c, $c in {Nom, Acc}, $c in ^{Inst,Loc}
  else 
    if (MU_graph_scan_graph_variable_value_unif_var(pos, MU_graph_catval, MU_lemma))
      return 1;
  
  return 0;
}

/////////////////////////////////////////////////
// Fills out the value in a category-value appearing in a graph node.
// 'params' = "global" parameters
// 'label' = value string to be analysed representing value inheritence, e.g. "1.Nb"
// MU_graph_catval = category-value pair whose value needs to be filled out
// 'MU_lemma' = muti-word unit lemma being inflected
// We suppose that MU_graph_catval already has its memory allocated.
// Returns 0 on success, 1 otherwise.
int MU_graph_scan_graph_variable_value_inherit_const(MultiFlexParameters* params, unichar* label, MU_graph_catval_T* MU_graph_catval,U_lemma_T* MU_lemma) {
  unichar* pos; //Current position in the label
  int l; //Length of the scanned sequence
  unichar tmp[MAX_GRAPH_NODE];  //Temporary buffer
  int const_nb; //Number of the constituent from which a value is to be inherited

  pos = label;

  //Check if the value is inherited from a constituent
  l = u_scan_while_char(tmp, pos, MAX_GRAPH_NODE-1,"0123456789"); //Try to scan a constituent's number
  //If the string does not begin with an integer, it is not an inherited constant
  if (!l)
    return 0;

  char tmp_char[MAX_GRAPH_NODE];
  u_to_char(tmp_char,tmp);
  const_nb = atoi(tmp_char);
  if (const_nb <=0 || MU_lemma->compound_lemma.no_forms < const_nb) { //If no constituent of the given number exists in the compound
    error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
    error(" the constituent number %d does not exist in the following compound ",const_nb);
    U_print_lemma(MU_lemma);
    return 1;            
  }
  pos = pos + l;  //Omit the constituent number
  if (*pos != (unichar)'.') {
    error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
    error(" (at position %d): %S",(int)(pos-label),tmp);
    error(" '.' missing\n");
    return 1;      
  }
  pos++; //Omit the '.'
  //Scan the category
  l = u_scan_until_char(tmp,pos,MAX_GRAPH_NODE-1,"",1);
  if (!l) {
    error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
    error(" (at position %d): %S",(int)(pos-label),tmp);
    error("a category missing after the dot (\'.\').'\n");
    return 1;      
  }
  l_category_T* cat;  //Pointer to the category corresponding to the string
  cat = is_valid_cat(params, tmp);
  if (!cat) { 
    error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
    error(" (at position %d): %S",(int)(pos-label),tmp);
    error(" is not a valid category\n");
    return 1;
  }
  if (cat != MU_graph_catval->cat) {
    error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
    error(" (at position %d): %S",(int)(pos-label),tmp);
    error(" is not the same category as the left side of the equation\n");
    return 1;
  }
  //Fill in the constituent number, from which the value is to be inherited
  MU_graph_catval->type = inherit_const;
  MU_graph_catval->val.inherit_const = const_nb;
  return 0; 
}

/////////////////////////////////////////////////
// Fills out the value in a category-value appearing in a graph node.
// label = value string to be analysed representing an inheritence variable, e.g. "$g"
// MU_graph_catval = category-value pair whose value needs to be filled out
// 'MU_lemma' = muti-word unit lemma being inflected
// We suppose that MU_graph_catval already has its memory allocated.
// Returns 0 on success, 1 otherwise.
int MU_graph_scan_graph_variable_value_inherit_var(unichar* label, MU_graph_catval_T* MU_graph_catval,U_lemma_T* MU_lemma) {
  unichar* pos; //Current position in the label
  int l; //Length of the scanned sequence
  unichar tmp[MAX_GRAPH_NODE];  //Temporary buffer

  pos = label;
  pos++;  //omit the '$'
  //Scan the variable's identifier  
  l = u_scan_until_char(tmp,pos,MAX_GRAPH_NODE-1," \t=",1);
  if (!l) {
    error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
    error(" (at position %d): ",(int)(pos-label));
    error("a variable missing after \'$\'.\n");
    return 1;      
  }

  //Allocate and copy the variable identifier
  MU_graph_catval->type = inherit_var;
  MU_graph_catval->val.inherit_var = (unichar*) malloc((u_strlen(tmp)+1) * sizeof(unichar));
  if (!MU_graph_catval->val.unif_var->var)
    fatal_error("Not enough memory in function 'MU_graph_scan_graph_variable_value_inherit_var'\n");
  u_strcpy(MU_graph_catval->val.inherit_var,tmp);

  pos = pos + l;
  if (*pos) {
    error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
    error(" (at position %d): ",(int)(pos-label));
    error("end of variable expected \'$\'.\n");
    return 1;      
  }

  return 0;
}

/////////////////////////////////////////////////
// Fills out the value in a category-value appearing in a graph node.
// label = value string to be analysed representing a unification variable, e.g. "c", "c=nom|acc"
// MU_graph_catval = category-value pair whose value needs to be filled out
// 'MU_lemma' = muti-word unit lemma being inflected
// We suppose that MU_graph_catval already has its memory allocated.
// Returns 0 on success, 1 otherwise.
int MU_graph_scan_graph_variable_value_unif_var(unichar* label, MU_graph_catval_T* MU_graph_catval,U_lemma_T* MU_lemma) {
  unichar* pos; //Current position in the label
  int l; //Length of the scanned sequence
  unichar tmp[MAX_GRAPH_NODE];  //Temporary buffer

  pos = label;
  //Scan the variable's identifier  
  l = u_scan_until_char(tmp,pos,MAX_GRAPH_NODE-1," \t=",1);
  if (!l) {
    error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
    error(" (at position %d): ",(int)(pos-label));
    error("a variable missing after \'$\'.\n");
    return 1;      
  }
  pos = pos + l;
  
  //Allocate and copy the variable identifier
  MU_graph_catval->type = unif_var;
  MU_graph_catval->val.unif_var = (MU_graph_unif_var_T*) malloc(sizeof(MU_graph_unif_var_T));
  if (!MU_graph_catval->val.unif_var)
    fatal_error("Not enough memory in function 'MU_graph_scan_graph_variable_value_unif_var'\n");
  MU_graph_catval->val.unif_var->var = (unichar*) malloc((u_strlen(tmp)+1) * sizeof(unichar));
  if (!MU_graph_catval->val.unif_var->var)
    fatal_error("Not enough memory in function 'MU_graph_scan_graph_variable_value_unif_var'\n");
  u_strcpy(MU_graph_catval->val.unif_var->var,tmp);

  //Scan the values admitted for the unification variable
  if (MU_graph_scan_graph_variable_value_unif_var_values(pos, MU_graph_catval, MU_lemma)) {
    free(MU_graph_catval->val.unif_var->var);
    free(MU_graph_catval->val.unif_var);
    return 1;
  }
  
  return 0; 
}
  
/////////////////////////////////////////////////
// Fills out the list of values in a category-value appearing in a graph node.
// label = value string to be analysed represents the value admitted for a  unification variable, 
// If 'label' is empty then all values are admitted.
// Otherwise 'label' must be of the form "=nom|acc",
// MU_graph_catval = category-value pair whose value needs to be filled out
// 'MU_lemma' = muti-word unit lemma being inflected
// We suppose that MU_graph_catval already has its memory allocated.
// Returns 0 on success, 1 otherwise.
int MU_graph_scan_graph_variable_value_unif_var_values(unichar* label, MU_graph_catval_T* MU_graph_catval,U_lemma_T* MU_lemma) {
  unichar* pos; //Current position in the label
  unichar tmp[MAX_GRAPH_NODE];  //Temporary buffer
  int err=0; //Error code if any
  
  if (!MU_graph_catval || !MU_graph_catval->val.unif_var)
    return 1;
  pos = label;

  //Omit void characters
  pos = pos + u_scan_while_char(tmp, pos, MAX_GRAPH_NODE-1," \t");

  //If no info it is a non restricted unification variable
  if (*pos == (unichar)'=') {
      pos = label+1; //Omit the '=' character
      MU_graph_catval->val.unif_var->values = MU_graph_scan_graph_restricted_values(pos, MU_graph_catval->cat,MU_lemma);
      if (!MU_graph_catval->val.unif_var->values)
	return 1;
  }
  else
    if (!*pos)
      MU_graph_catval->val.unif_var->values = NULL;  //If the list of values for a unification variable is NULL
                                                     //then all values from the corresponding category are allowed
  //Sinon it is a restricted variable
    else {
      error("In graph %s label format incorrect in %S",MU_lemma->paradigm,label);
      error(" (at position %d): %S",(int)(pos-label),tmp);
      error(" End of variable identifier or '=' expected.\n");
      return 1;   
    }   

  if (err) 
    return 1;
  return 0;
}

/////////////////////////////////////////////////
// Fills out the list of values in a category-value appearing in a graph node.
// ALl values edmitted by the corresponding category are to be admitted
// MU_graph_catval = category-value pair whose value needs to be filled out
// We suppose that MU_graph_catval already has its memory allocated.
// Returns 0 on success, 1 otherwise.
//int MU_graph_scan_graph_variable_value_unif_var_all_values(MU_graph_catval_T* MU_graph_catval) {
//  if (!MU_graph_catval || !MU_graph_catval->val.unif_var)
//    return 1;
//  
//  int nb_val; //Number of values in the current category
//  int i; //Index of the current value
//
//  //Get the number of values in the current category
//  nb_val = get_cat_nb_values(MU_graph_catval->cat);
//  MU_graph_catval->val.unif_var->values = (int*) malloc((nb_val+1) * sizeof(int));
//  if (!MU_graph_catval->val.unif_var->values)
//    fatal_error("Not enough memory in function 'MU_graph_scan_graph_variable_value_unif_var_values'\n");
//  for (i=0; i<nb_val; i++)
//    MU_graph_catval->val.unif_var->values[i] = i;
//  MU_graph_catval->val.unif_var->values[nb_val] = -1;
//  return 0;
//}

/////////////////////////////////////////////////
// Fills out the list of values admitted for a category
// 'label' = value string to be analysed represents the values admitted
// 'label' must be of the form "nom|acc", or "nom"
// cat = category
// 'MU_lemma' = muti-word unit lemma being inflected
// Returns a list of value indices on success, NULL otherwise.
int* MU_graph_scan_graph_restricted_values(unichar* label, l_category_T* cat, U_lemma_T* MU_lemma) {

  int l; //Length of the scanned sequence
  unichar* pos; //Current position in the label
  int* admitted_values=NULL;  //List of admitted values
  int nb_admitted=0; //Number of admitted values
  unichar tmp[MAX_GRAPH_NODE];  //Temporary buffer
  int v; //Index of the current value

  pos = label;

  while(u_strlen(pos)) {

    //Omit void characters
    pos = pos + u_scan_while_char(tmp, pos, MAX_GRAPH_NODE-1," \t");

    //Scan a constant value 
    l = u_scan_until_char(tmp,pos,MAX_GRAPH_NODE-1," \t|",1);
    if (l) {
      v = is_valid_val(cat,tmp);
      if (v == -1) {  
	error("In graph %s label format incorrect in %S: ",MU_lemma->paradigm,label);
	error("%v is not a valid value in category %S.\n",tmp, cat->name);
	free(admitted_values);
	return NULL;
      }
      else {
	nb_admitted++; //A new admitted value found
	admitted_values = (int*) realloc(admitted_values,(nb_admitted+1) * sizeof(int));
	if (!admitted_values) 
	  fatal_error("Not enough memory in function 'MU_graph_scan_graph_restricted_values'.\n");
	admitted_values[nb_admitted-1] = v;
	admitted_values[nb_admitted] = -1;
      }
      pos = pos + l;
      if (*pos == (unichar)'|') //Omit the '|' if any
	pos++; 
    }
    else {
	error("In graph %s label format incorrect in %S: ",MU_lemma->paradigm,label);
	error("a morphological value expected before %C.\n", *pos);
	free(admitted_values);
	return NULL;
    }
  }
  return admitted_values;
}

/////////////////////////////////////////////////
// Fills out the value in a category-value pair appearing in a graph node.    
// lab = value string to be analysed, not beginning with a '$', e.g. fem (is is not a variable value)
// MU_graph_catval = category-value pair whose value needs to be filled put
// 'MU_lemma' = muti-word unit lemma being inflected
// We suppose that MU_graph_catval already has its memory allocated.
// Returns 0 on success, 1 otherwise.
int MU_graph_scan_graph_constant_value(unichar* label, MU_graph_catval_T* MU_graph_catval,U_lemma_T* MU_lemma) {
  MU_graph_catval->type = cnst;
  MU_graph_catval->val.values = MU_graph_scan_graph_restricted_values(label, MU_graph_catval->cat, MU_lemma);
  if (!MU_graph_catval->val.values)
    return 1;
  return 0;
}

/////////////////////////////////////////////////
// Prints a MU_graph label.    
void MU_graph_print_label(MU_graph_label_T* MU_label) {
//Label's input
if (!MU_label->in) {  //Epsilon case
   u_printf("<E>");
} else {
   if (MU_label->in->unit.type == cst) 
     u_printf("%S",MU_label->in->unit.u.seq);
   else {
      u_printf("<");
      if (MU_label->in->unit.type == var)
	u_printf("$%d",MU_label->in->unit.u.num);
      else {
	unichar lemma_str[MAX_DLC_LINE+1];
	if (U_get_lemma_str(lemma_str,MAX_DLC_LINE,MU_label->in->unit.u.ext))
	  return;
	u_printf("%S%C%s%C%S",lemma_str,CONSTIT_PARA_BEG,MU_label->in->unit.u.ext->paradigm,CONSTIT_CAT_BEG,MU_label->in->unit.u.ext->cl->name);
      }
      if (MU_label->in->feat_struct) {
         //Opening ':'
         u_printf(":");
         MU_graph_print_feat_struct(MU_label->in->feat_struct);
      }
      u_printf(">");
   }
}
//Label's output
if (MU_label->out) {  //Non epsilon case
  u_printf("/<");
  MU_graph_print_feat_struct(MU_label->out);
  u_printf(">");
 }
//Newline 
 u_printf("\n");
}

/////////////////////////////////////////////////
// Prints a MU_graph morpho.    
void MU_graph_print_feat_struct(MU_graph_feat_struct_T* MU_feat_struct) {
  int cv;  //number of the current category-value pair
  //Category-value features
  for (cv=0;cv<MU_feat_struct->no_catvals;cv++) {
    //Category
    u_printf("%S",MU_feat_struct->catvals[cv].cat->name);
    if (MU_feat_struct->catvals[cv].type==inherit_var)
      u_printf("=");   //Double '=='
    //Value
    switch (MU_feat_struct->catvals[cv].type) {
    case cnst: //e.g. Case=acc|loc
      MU_graph_print_values(MU_feat_struct->catvals[cv].cat, MU_feat_struct->catvals[cv].val.values);
      break;
    case inherit_const: //e.g. Case=$1.Case
      int no_u; //Number of the unit from which the value is inherited
      no_u = MU_feat_struct->catvals[cv].val.inherit_const;
      u_printf("=$%d.%S",no_u,MU_feat_struct->catvals[cv].cat->name);
      break;
    case unif_var:   //e.g. Case=$c or Case=$c=acc|loc
      u_printf("=$%S",MU_feat_struct->catvals[cv].val.unif_var->var);
      MU_graph_print_values(MU_feat_struct->catvals[cv].cat, MU_feat_struct->catvals[cv].val.unif_var->values);
      break;
    case inherit_var:   //e.g. Case==$c
      u_printf("=$%S",MU_feat_struct->catvals[cv].val.inherit_var);
      break;
    }
    //Semi-colon
    if (cv<MU_feat_struct->no_catvals-1) {
      u_printf(";");
    }
  }
}
 
/////////////////////////////////////////////////
// Prints a list of values in a category-value pair: acc|inst|loc
// cat = the category
// values = indices of the values to be printed
void MU_graph_print_values(l_category_T* cat, int* values) {
  int v; //Index of the current value
  if (values){
    u_printf("=");
    v=0;
    while (values[v] != -1) {
      u_printf("%S",cat->values[values[v]]);
      v++;
      if (values[v] != -1)
	u_printf("|");
    }
  }
}

/////////////////////////////////////////////////
// Frees the memory allocated for a MU_graph label.    
// 'liberate_main_structure' = Boolean saying if the main structure should be liberated (1) or not (0)
void MU_graph_free_label(MU_graph_label_T* MU_label, int liberate_main_structure) {
  if (!MU_label)
    return;
  //Free the label's input
  if (MU_label->in) {
    if (MU_label->in->unit.type == cst)
      free(MU_label->in->unit.u.seq);
    else
      if (MU_label->in->unit.type == ext)
	U_delete_lemma(MU_label->in->unit.u.ext,1);
    if (MU_label->in->feat_struct)
      MU_graph_free_feat_struct(MU_label->in->feat_struct);
    free(MU_label->in);
  }
  //Free the label's output
  if (MU_label->out) {
    MU_graph_free_feat_struct(MU_label->out);
    //    free(MU_label->out);
  }
  if (liberate_main_structure)
    free(MU_label);
}

/////////////////////////////////////////////////
// Liberates the memory allocated for a MU_graph morpho.    
void MU_graph_free_feat_struct(MU_graph_feat_struct_T* MU_feat_struct) {
  int cv;  //number of the current category-value pair
  if (!MU_feat_struct)
    return;

  for (cv=0; cv<MU_feat_struct->no_catvals; cv++)
    if (MU_feat_struct->catvals[cv].type == unif_var) {
      if (MU_feat_struct->catvals[cv].val.unif_var) {
	if (MU_feat_struct->catvals[cv].val.unif_var->var)
	  free(MU_feat_struct->catvals[cv].val.unif_var->var);
	if (MU_feat_struct->catvals[cv].val.unif_var->values)
	  free(MU_feat_struct->catvals[cv].val.unif_var->values);
	free(MU_feat_struct->catvals[cv].val.unif_var);
      }
    }
    else
      if (MU_feat_struct->catvals[cv].type == inherit_var)
	free(MU_feat_struct->catvals[cv].val.inherit_var);
  free(MU_feat_struct);
}



