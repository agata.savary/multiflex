/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  * Testing:
  *   LD_LIBRARY_PATH=./morfeusz; export LD_LIBRARY_PATH; ./multiflex-morfeusz ../../MULTIFLEX_1.4beta/multiflex-morfeusz/test-letter-case.txt ../ling/morfeusz-ling/Polish/Alphabet.txt
  */

/********************************************************************************/
/* MAIN MODULE FOR RUNNING THE DELAC-INTO-DELACF INFLECTION                     */
/********************************************************************************/


#include <stdio.h>
#include <string.h>
#include "IOBuffer.h"  //Required by Unitex "golden rules"
#include "Alphabet.h"
#include "Error.h"
#include "File.h"
#include "MF_Convert.h"
#include "MF_Copyright.h"
#include "MF_DLC_inflect.h"
#include "MF_LangMorpho.h"
#include "MF_MorphoEquiv.h"
#include "MF_MU_graph.h"
#include "MF_LetterCase.h"
#include "MF_Global2.h"
#include "morfeusz/morfeusz2_c.h"

//Current language's alphabet
Alphabet* alph;

// Directory containing the inflection tranducers and the 'Morphology' file
char inflection_directory[FILENAME_MAX];

//DELAC files containing description of compounds being constituents of the compounds to be inflected
char** embedded_DLC;
//Number of these files
int no_embedded_DLC;

//Error code indicating loading status of configuration files 'Morphology' and 'Equivalences' if any.
//extern int config_files_status=CONFIG_FILES_OK;

void usage();
int init_morphology(MultiFlexParameters* params, char* dir);
int init_alphabet(MultiFlexParameters* params, char* alph_file);
int init_equivalences(MultiFlexParameters* params, char* dir);
int init_transducers(MultiFlexParameters* params, char* dir);
int init_embedded_dlc(char** files, int no_files);


/////////////////////////////////// 
// Inflection of a DELAC to a DELACF
// argv[1] = path and name of the example file
// argv[2] = path and name of the alphabet file
// If any problem occurs, returns 1. Otherwise returns 0.
int main(int argc, char* argv[]) {

  MultiFlexParameters* params=new_MultiFlexParameters();
  setBufferMode();  //Required by Unitex "golden rules"
  
  //  if (argc < 3) {
  //  usage();
    //   return 0;
  //}

  //Load the morphology description, the alphabet, the equivalence files, 
  //the structure for inflection transducers, a
  //and the list of DELAC files containing embedded compounds
  if (init_alphabet(params,argv[2]))
    return 1;
  //Initialize the conversion descriptors
  if (init_convert()) 
    return 1;

  /**************************************************/
  // Testing DLC_line2entry
  //
  /**************************************************/
  
  /**************************************************/
  /* 
 // Testing Morfeusz' segmentation

  unichar s_u[1000];
  char s_c[1000];
  FILE* f;
  InterpMorf* im;  //Morfeusz interpretation of a string

  f = u_fopen(argv[1],U_READ);
  if (!f) {
    error("Unable to open file: '%s' !\n", argv[1]);
    return 1;
  }
  
  while (!feof(f)) { 
    fscanf(f,"%s\n",s_c);
    im = morfeusz_analyse(s_c);
    int t; //Number of the current token
    for (t=0; im[t].p!=-1; t++) {
      u_printf("%s ",im[t].forma);
    }
    u_printf("\n");
  }
  
  //End of testing Testing Morfeusz' segmentation
  */
  /**************************************************/





  
  /**************************************************/
  // Testing Letter Case
  //Test of letter case functions on a file
  //  unichar s[1000];
  //  FILE* f;
  //  f = u_fopen(argv[1],U_READ);
  //  if (!f) {
  //   error("Unable to open file: '%s' !\n", argv[1]);
  //    return 1;
  //  }
  
  //  while (!feof(f)) { 
  //    u_fgets(s,f);

    //Output the letter case of the current string
  //    u_printf("Letter case of %S is ",s);
  //    switch (letter_case(params,s)) {
  //    case same:   u_printf("same"); break;
  //    case all_lower:  u_printf("all_lower"); break;
  //    case all_upper:  u_printf("all_upper"); break;
  //    case first_upper:  u_printf("first_upper"); break;
      //    case all_lower_each_word: u_printf("all_lower_each_word"); break;
      //    case all_upper_each_word: u_printf("all_upper_each_word"); break;
  //    case first_upper_each_word: u_printf("first_upper_each_word"); break;
  //    case no_letter_case: u_printf("no_letter_case"); break;
  //    case other: u_printf("other"); break;
  //    }
  //    u_printf(".\n");

    //Output the result of transforming the current string into another letter case
  //    unichar s_copy[1000];
  //    u_strcpy(s_copy,s);
  //    u_printf("%S transformed into all_lower gives %S\n",s,letter_case_apply(params,s_copy,all_lower));
  //    u_strcpy(s_copy,s);
  //    u_printf("%S transformed into all_upper gives %S\n",s,letter_case_apply(params,s_copy,all_upper));
  //    u_strcpy(s_copy,s);
  //    u_printf("%S transformed into first_upper gives %S\n",s,letter_case_apply(params,s_copy,first_upper));
  //    u_strcpy(s_copy,s);
  //    u_printf("%S transformed into first_upper_each_word gives %S\n",s,letter_case_apply(params,s_copy,first_upper_each_word));
  //    u_strcpy(s_copy,s);
  //    u_printf("%S transformed into no_letter_case gives %S\n",s,letter_case_apply(params,s_copy,no_letter_case));
  //    u_strcpy(s_copy,s);
  //    u_printf("%S transformed into other gives %S\n",s,letter_case_apply(params,s_copy,other));
  //    u_printf("\n");
  //  }
//End of testing Letter Case
/************************************/
    
  //Liberate the structures 
  free_alphabet(alph);
  return 0;
}

//////////////////////////////////
// Usage note shown if the number of command line arguments is incorrect (<5) 
void usage() {
  u_printf("%S",COPYRIGHT);
  u_printf("Usage: DlcInflect <delac> <delacf> <alpha> <dir>\n");
  u_printf("     <delac> : the unicode DELAC file to be inflected\n");
  u_printf("     <delacf> : the unicode resulting DELACF dictionary \n");
  u_printf("     <alpha> : the alphabet file \n");
  u_printf("     <dir> : the directory containing 'Morphology' file and \n");
  u_printf("             inflection graphs for single and compound words.\n");
  u_printf("\nInflects a DELAC into a DELACF.\n");
}

/////////////////////////////////// 
// Loads the Morphology.txt file
// containing the morphological model of the language
// (i.e. the list of inflectional classes, categories and values).
// params = "global" Multiflex parameters
// dir = directory containing this file
// Checks if the letter case category and values have correct labels
// If any problem occurs, returns 1. Otherwise returns 0.
int init_morphology(MultiFlexParameters* params, char* dir) {
  char morphology[FILENAME_MAX];
  new_file(dir,"Morphology.txt",morphology);
  if(read_language_morpho(params,morphology)) {
    free_language_morpho(params);
    params->config_files_status=CONFIG_FILES_ERROR;
    return 1;
  }
  //  print_language_morpho();
  return 0;
}

/////////////////////////////////// 
// Loads the alphabet file 'alph_file'
// params = "global" Multiflex parameters
// If any problem occurs, returns 1. Otherwise returns 0.
int init_alphabet(MultiFlexParameters* params, char* alph_file) {
  params->alph=load_alphabet(alph_file);  //To be done once at the beginning of the inflection
  if (params->alph==NULL) {
    error("Cannot open alphabet file %s\n",alph_file);
    free_language_morpho(params);
    free_alphabet(params->alph);    
    return 1;
  }
  return 0;
}

/////////////////////////////////// 
// Loads the Equivalences.txt file if any
// containing the equivalences between the morphological
// values for simple words and those for compounds.
// params = "global" Multiflex parameters
// dir = directory containing this file
// If any problem occurs, returns 1. Otherwise returns 0.
int init_equivalences(MultiFlexParameters* params, char* dir) {
  char equivalences[FILENAME_MAX];
  int err; //Error code
  new_file(dir,"Equivalences.txt",equivalences);
  err = init_morpho_equiv(params,equivalences);
  if (err) {
    free_language_morpho(params);
    free_alphabet(params->alph);    
    params->config_files_status=CONFIG_FILES_ERROR;
    return 1;
  }
  //  print_morpho_equiv();
  
  init_class_equiv(params);
  return 0;
}

/////////////////////////////////// 
// Initializes the structure of inflection transducers
// params = "global" Multiflex parameters
// dir = directory containing this file
// If any problem occurs, returns 1. Otherwise returns 0.
int init_transducers(MultiFlexParameters* params, char* dir) {
  //  int err; //Error code
  strcpy(params->inflection_directory,dir);
  //  err = MU_graph_init_graphs();
  //  if (err) {
  //    MU_graph_free_graphs();
  //    return 1;
  //  }
  return 0;
}

