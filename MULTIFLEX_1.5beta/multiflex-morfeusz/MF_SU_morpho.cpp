/*
  * Multiflex for Morfeusz - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */
/* This file contains parts of the Unitex software distributed under the terms of LGPL.   */
/* It is a modified and extended version of Inflect.cpp file of the the Unitex 1.2 beta   */
/* version downloaded from http://www-igm.univ-mlv.fr/~unitex/ on June 6, 2005.           */ 
/* The modifications has been done by Agata Savary (agata.savary@univ-tours.fr).          */
/* Latest modification on December 6, 2005.                                               */


/********************************************************************************/
/* MORPHOLOGY AND INFLECTION OF A SIMPLE WORD                                   */
/********************************************************************************/

#include "MF_LangMorpho.h"
#include "MF_FeatStruct.h"
#include "MF_FeatStructStr.h"
#include "MF_UnitMorpho.h"
#include "MF_SU_morpho.h"
#include "MF_Util.h"
#include "MF_Convert.h"
#include "MF_CurrentForms.h"
#include "Error.h"
#include "Alphabet.h"
#include "Unicode.h"
#include "morfeusz/morfeusz2_c.h"
#include <string.h>
#include <stdlib.h>

int SU_inflect(MultiFlexParameters* params, U_form_list_set_T* CURRENT_FORMS, U_lemma_T* lemma,f_feat_struct_T* desired_features, U_forms_T* forms);
int SU_morfeusz_get_all_forms(MultiFlexParameters* params, U_lemma_T* lemma, U_forms_T* forms);
int SU_morfeusz_convert_forms(MultiFlexParameters* params, InterpMorf* mrfsz_forms, U_forms_T* forms, l_class_T* desired_class);
//int SU_morfeusz_convert_forms(MultiFlexParameters* params, InterpMorf* mrfsz_forms, U_forms_T* forms);
void SU_print_f(U_form_id_T* f);

////////////////////////////////////////////
// For a given single unit, generates all the inflected forms corresponding to the given inflection features 'feat' of the given class (flexeme).
// The forms generated are put into 'forms' (which has its space allocated and initialized)
// params = "global" parameters
// CURRENT_FORMS = inflected forms of the current MWU generated up till now
// lemma:  the unit's lemma
// desired_features: feature structure of the desired forms, e.g. {Gen=fem, Case=Inst}, or {} (if separator)
// forms: return parameter; set of the inflected forms corresponding to the given inflection features of the given class (flexeme)
//        e.g. (3,{[reka,{Gen=fem,Nb=sing,Case=Instr}],[rekami,{Gen=fem,Nb=pl,Case=Instr}],[rekoma,{Gen=fem,Nb=pl,Case=Instr}]})
//        or   (1,{["-",{}]})
// If feat is NULL, all inflected forms are to be generated (note that for Morfeusz all flexemes of the same lexeme are generated)
// If feat is [0,NULL] no inflected form is to be generated
// Returns 0 on success, 1 otherwise.   
int SU_inflect(MultiFlexParameters* params, U_form_list_set_T* CURRENT_FORMS, U_lemma_T* lemma, f_feat_struct_T* desired_features, U_forms_T* forms) { 
  int err; //Error code
  int unfound; //Boolean saying if the lemma's forms failed to be found in the current forms

  //If no form required return
  if (desired_features && desired_features->no_catvals == 0)
    return 0;

  U_forms_T all_forms;  //All inflected forms of the current lemma
  U_init_forms(&all_forms);

  //Check if the forms have already been generated
  unfound = get_current_form_list(CURRENT_FORMS, lemma, &all_forms);

  //If not yet generated, generate them all
  if (unfound) {

    //Generate all inflected forms of the simple word via Morfeusz
    err = SU_morfeusz_get_all_forms(params, lemma, &all_forms);
    if (err) 
      return 1;
    
    //Add the new forms to the current ones (all_forms becomes empty)
    err = add_current_forms(CURRENT_FORMS,lemma,&all_forms);
    if (err) {
      U_delete_forms(&all_forms);
      return 1;
    }
  }

  //If all forms required return them all
  if (!desired_features) {
    err = U_copy_forms(forms,&all_forms);
    U_delete_forms(&all_forms);
    return err;
  }
  
 //Choose only the desired forms
  U_forms_T desired_forms;
  U_init_forms(&desired_forms);
  //Copy only the forms of the desired features
  err = U_duplicate_desired_forms(&all_forms, desired_features, &desired_forms);
  if (err) {
    U_delete_forms(&all_forms);
    U_delete_forms(&desired_forms);
    return 1;
  }

  //Apply the post-inflection graphical forms, e.g. Init, LetterCase
  err = U_apply_graphical_feat(params,&desired_forms,desired_features,forms);
  U_delete_forms(&all_forms);
  U_delete_forms(&desired_forms);

  return err;
}

////////////////////////////////////////////
// For a given single unit, generates all its inflected forms.
// The forms generated are put into 'forms' (which has its space allocated and is empty).
// params: "global" parameters
// lemma:  single unit's lemma and its annotation (inflection homomym number and class)
// Returns 0 on success, 1 otherwise.
int SU_morfeusz_get_all_forms(MultiFlexParameters* params, U_lemma_T* lemma, U_forms_T* forms) {

  //Convert the UTF-16 lemma into UTF-8 lemma
  char* lemma_str_utf8 = convert_utf16_to_utf8(lemma->simple_lemma);
  if (!lemma_str_utf8) 
    return 1;

  //Get the homonym (only if !="0) and concatenate it with the lemma
  if (strcmp(lemma->paradigm,"0")) {
    lemma_str_utf8 = (char*) realloc(lemma_str_utf8, (strlen(lemma_str_utf8)+strlen(lemma->paradigm)+2) * sizeof(char));
    if (!lemma_str_utf8)
      fatal_error("Memory allocation problem in function 'SU_morfeusz_get_all_forms'.\n");
    strcat(lemma_str_utf8,":");
    strcat(lemma_str_utf8,lemma->paradigm);
  }

  //Get all the inflected forms via Morfeusz morphological module for simple words
  InterpMorf* mrfsz_forms;
  mrfsz_forms = morfeusz_generate(lemma_str_utf8);

  //Liberate the conversion strings
  free(lemma_str_utf8);

  //For debugging
  // printf("Liczba form: %d\n",*nb_forms);
  //  for (int nf = 0; nf<*nb_forms; nf++) {
  //    printf("%2d: ", nf+1);
  //    u_fprints_utf8(mrfsz_forms[nf].word, stdout);
  //    printf(" > ");
  //    u_fprints_utf8(mrfsz_forms[nf].tag, stdout);
  //    printf("\n");
  //  }
  //End of debugging

  //Convert the Morfeusz forms (of the correct class/flexeme) into Multiflex forms
  return SU_morfeusz_convert_forms(params, mrfsz_forms, forms, lemma->cl);
}

////////////////////////////////////////////
//Convert the set of Morfeusz forms (of the correct class/flexeme) into Multiflex forms
// params = "global" parameters
// mrfsz_forms = the forms to be converted
// forms = output parameter receiving the converted forms (should have its space allocated and be empty)
// cl = the desired class
// Returns 0 on success, 1 otherwise.
//OLD: int SU_morfeusz_convert_forms(MultiFlexParameters* params, morfeusz_form* mrfsz_forms, U_forms_T* forms){
int SU_morfeusz_convert_forms(MultiFlexParameters* params, InterpMorf* mrfsz_forms, U_forms_T* forms, l_class_T* desired_class){
  if (!forms)
    return 1;
  if (!mrfsz_forms)
    return 0;

  int f; //Index of the current form
  f_feat_struct_set_T* feats = NULL; //Feature structure of the current form
  U_form_id_T new_form_id; //A Multiflex form equivalent to the current form
  for (f = 0; mrfsz_forms[f].p != -1; f++) {
    unichar* tag_utf16; //UTF-16 version of the Morfeusz tag
    tag_utf16 = convert_utf8_to_utf16((char*)mrfsz_forms[f].interp);
    if (!tag_utf16) 
      return 1;
    l_class_T* cl;
    //Convert the Morfeusz sequences of features into a set of Multiflex feature structures
    feats = get_feat_struct_set_class_str(params, tag_utf16, &cl);
    free(tag_utf16);
    if (!feats) 
      return 1;

    //If the form corresponds to the desired class, add it to the result
    if (cl == desired_class) {
      U_init_form_id(&new_form_id);  //Initialize a new form
      if (!mrfsz_forms[f].forma || !mrfsz_forms[f].interp)
	return 1;
       //Get the form string
      new_form_id.form = convert_utf8_to_utf16(mrfsz_forms[f].forma);
      if (! new_form_id.form)
	return 1;
      //Attach empty extras
      form_extras_T extras;  //Extra information attached to a form
      fe_init_extras(&extras);
      new_form_id.extras = &extras;
    
      //The current form may have several feature structures obtained from the Morfeusz tag
      //Add as many new forms as there are feature structures 
      int feat; //Index of the current feature structure in the feature structure set 'feats'
      for (feat=0; feat<feats->no_feat_structs; feat++) {
	//Put the feature structure obtained from the Morfeusz tag
	new_form_id.feat = &(feats->feat_structs[feat]);
	//Add the current form into the result (by duplication) unless it already appears in 'forms'
	if (U_add_form(&new_form_id, forms, 1))
	  return 1;
      }
    }
    //Delete the set of feature structures
    f_delete_feat_struct_set(&feats);
  }
  return 0;
}

////////////////////////////////////////////
// *COMMENT CONCERNING THE UNIQUE IDENTIFICATION OF SINGLE WORD FORMS
//
// In order to uniquely identify a word form four elements are necessary:
//	- the form  (e.g. "rekami")
//	- its lemma (e.g. "reka")
//	- its inflection paradigm (e.g. N56)
//	- its inflection features (e.g. {Gen=fem,Nb=pl,Case=Inst}
// Note that omitting one of these elements may introduce ambiguities from the morphological point of view.
// Examples :
//	1) If the form itself is missing, an ambiguity may exist between variants corresponding to the same inflection features
//	   e.g. given only the lemma "reka", the paradigm N56, and the features {Gen=fem,Nb=pl,Case=Inst}), we may not distinguish
// 	   between "rekami" and "rekoma"
//	2) If the lemma is missing we may not lemmatize the form (unless the paradigm allows to do that on the basis of the 3 elements)
//	3) If the inflection paradigm is missing we may not produce other inflected forms.
//	4) If the inflection deatures are missing the may be an ambiguity in case of homographs e.g. "rece" may be both
//	   {Gen=fem,Nb=pl,Case=Nom} and {Gen=fem,Nb=pl,Case=Acc}
