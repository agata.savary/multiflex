Graph Naming Conventions for Polish :

SIMPLE WORDS

 The graph name for nouns 
       N<gen>-<ending>(-<variant>)(-[(<cat>)inv|<val>]).grf

       where <gen> is one of the following numbers:	
       	     1 for masculine human
       	     2 for masculine animate
       	     3 for masculine inanimate
       	     4 for feminine
       	     5 for neuter

       <ending> may contain several letters
       
       <variant> is empty or a number 1, 2, etc.

       <cat> is a category :
       	     n for number
	     g for gender
	     c for case
	     etc.

       inv means that the preceding category is invariable

       <val> is a morphological value :
       	     pl for plural
	     s for singular
	     etc.

       Example : "N4-ka-2-s" describes a feminine noun teminated by '-ka' having only the singular form (e.g. Polska)

--------------------

COMPOUNDS

Graph name :
      <class>C-<cons>(-<variant>)(-[<cat>inv|val])(-rev)

where
	<class> is the class category : 
		N for nouns, 
		Ngen for genitive nouns,
		A for adjectives, 
		ADV for adverbs, etc.
	<cons> is the code of constitutent sequence, where
	       X stands for a non inflected constituent
	       C stands for a caracteristic constituent or another inflected constituent
	       A, N, V, etc. may stand for morphological categories of constituents (adjective, noun, ...)

       <variant> is empty or a number 1, 2, etc.

       <cat> is a category :
       	     n for number
	     g for gender
	     c for case
	     etc.

       <val> is a morphological value :
       	     pl for plural
	     s for singular
	     etc.

	inv means that the preceding category is invariable, or that the whole compound is invariable

	rev means that some constituents are reversed in some inflected forms


	Example : "NC-CXC-2-s" describes a compound noun containing three constituents, the first and the last constituent may inflect, the compound is always in singular (e.g. Bośnia-Hercegowina). 
