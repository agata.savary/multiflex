Unable to open equivalence file ../ling/morfeusz-ling/Polish/Inflection/Equivalences.txt
Equivalences between dictionary and Multiflex features are considered as straightforward.
Graph ../ling/morfeusz-ling/Polish/Inflection/NC-O_ONO-OSOBA.fst2 is empty
The following delac entry could not be inflected:
Felicjan(Felicjan:subst:sg:nom:m1) Sławoj(Sławoj:subst:sg:nom:m1)-(-:interp)Składkowski(Składkowski:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
The following delac entry could not be inflected:
Ferenc(Ferenc:subst:sg:nom:m1) Keresztes(Keresztes:subst:sg:nom:m1)-(-:interp)Fischer(Fischer:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
Graph ../ling/morfeusz-ling/Polish/Inflection/NC-O_ONO-OSOBA-a.fst2 is empty
The following delac entry could not be inflected:
Fryderyk(Fryderyk:subst:sg:nom:m1) Joliot(Joliot:subst:sg:nom:m1)-(-:interp)Curie(Curie:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA-a)
The following delac entry could not be inflected:
generał(generał:subst:sg:nom:m1) {Felicjan Sławoj-Składkowski}(Felicjan Sławoj-Składkowski:subst:sg:nom:m1),subst(NC-O_O-OSOBA-gen)
The following delac entry could not be inflected:
generał(generał:subst:sg:nom:m1) {Jan Jur-Gorzechowski}(Jan Jur-Gorzechowski:subst:sg:nom:m1),subst(NC-O_O-OSOBA-gen)
The following delac entry could not be inflected:
generał(generał:subst:sg:nom:m1) {Józef Hauke-Bosak}(Józef Hauke-Bosak:subst:sg:nom:m1),subst(NC-O_O-OSOBA-gen)
The following delac entry could not be inflected:
generał(generał:subst:sg:nom:m1) {Michał Tokarzewski-Karaszewicz}(Michał Tokarzewski-Karaszewicz:subst:sg:nom:m1),subst(NC-O_O-OSOBA-gen)
No annotation could be found for: Stefan Grot-Rowecki
in entry: generał(generał:subst:sg:nom:m1) {Stefan Grot-Rowecki}(Stefan Grot-Rowecki:subst:sg:nom:m1),subst(NC-O_O-OSOBA-gen).
The following delac entry could not be inflected:
generał(generał:subst:sg:nom:m1) {Tadeusz Bór-Komorowski}(Tadeusz Bór-Komorowski:subst:sg:nom:m1),subst(NC-O_O-OSOBA-gen)
The following delac entry could not be inflected:
Henryk(Henryk:subst:sg:nom:m1) Melcer(Melcer:subst:sg:nom:m1)-(-:interp)Szczawiński(Szczawiński:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
The following delac entry could not be inflected:
Ignacy(Ignacy:subst:sg:nom:m1) Wyssogota(Wyssogota:subst:sg:nom:m1)-(-:interp)Zakrzewski(Zakrzewski:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
Cannot open the file ../ling/morfeusz-ling/Polish/Inflection/NC-O_O_O-OSOBA-III.fst2
The following delac entry could not be inflected:
Jan(Jan:subst:sg:nom:m1) III(III:adj:sg:nom:m1:pos) Sobieski(Sobieski:subst:sg:nom:m1),subst(NC-O_O_O-OSOBA-III)
The following delac entry could not be inflected:
Jan(Jan:subst:sg:nom:m1) Jur(Jur:subst:sg:nom:m1)-(-:interp)Gorzechowski(Gorzechowski:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
The following delac entry could not be inflected:
Jan(Jan:subst:sg:nom:m1) Nowak(Nowak:subst:sg:nom:m1)-(-:interp)Jeziorański(Jeziorański:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
No inflected form could be generated for: Ostroróg:sg:nom:m1:same
No inflected form could be generated for: Ostroróg:sg:nom:m1:same
No inflected form could be generated for: Ostroróg:sg:nom:m1:same
No inflected form could be generated for: Ostroróg:sg:gen:m1:same
No inflected form could be generated for: Ostroróg:sg:gen:m1:same
No inflected form could be generated for: Ostroróg:sg:gen:m1:same
No inflected form could be generated for: Ostroróg:sg:dat:m1:same
No inflected form could be generated for: Ostroróg:sg:dat:m1:same
No inflected form could be generated for: Ostroróg:sg:dat:m1:same
No inflected form could be generated for: Ostroróg:sg:acc:m1:same
No inflected form could be generated for: Ostroróg:sg:acc:m1:same
No inflected form could be generated for: Ostroróg:sg:acc:m1:same
No inflected form could be generated for: Ostroróg:sg:inst:m1:same
No inflected form could be generated for: Ostroróg:sg:inst:m1:same
No inflected form could be generated for: Ostroróg:sg:inst:m1:same
No inflected form could be generated for: Ostroróg:sg:loc:m1:same
No inflected form could be generated for: Ostroróg:sg:loc:m1:same
No inflected form could be generated for: Ostroróg:sg:loc:m1:same
No inflected form could be generated for: Ostroróg:sg:voc:m1:same
No inflected form could be generated for: Ostroróg:sg:voc:m1:same
No inflected form could be generated for: Ostroróg:sg:voc:m1:same
No inflected form could be generated for:
Jan(Jan:subst:sg:nom:m1) Ostroróg(Ostroróg:subst:sg:nom:m1),subst(NC-O_O-OSOBA)
The following delac entry could not be inflected:
Jerzy(Jerzy:subst:sg:nom:m1) Iwanow(Iwanow:subst:sg:nom:m1)-(-:interp)Szajnowicz(Szajnowicz:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
The following delac entry could not be inflected:
Józef(Józef:subst:sg:nom:m1) Hauke(Hauke:subst:sg:nom:m1)-(-:interp)Bosak(Bosak:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
The following delac entry could not be inflected:
Józef(Józef:subst:sg:nom:m1) Nusbaum(Nusbaum:subst:sg:nom:m1)-(-:interp)Hilarowicz(Hilarowicz:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
The following delac entry could not be inflected:
Juliusz(Juliusz:subst:sg:nom:m1) Kaden(Kaden:subst:sg:nom:m1)-(-:interp)Bandrowski(Bandrowski:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
The following delac entry could not be inflected:
Justyna(Justyna:subst:sg:nom:f) Budzińska(Budzińska:subst:sg:nom:f)-(-:interp)Tylicka(Tylicka:subst:sg:nom:f),subst(NC-O_ONO-OSOBA)
The following delac entry could not be inflected:
król(król:subst:sg:nom:m1) {Jan III Sobieski}(Jan III Sobieski:subst:sg:nom:m1),subst(NC-O_O-OSOBA-hetman)
The following delac entry could not be inflected:
Leopold(Leopold:subst:sg:nom:m1) Lis(Lis:subst:sg:nom:m1)-(-:interp)Kula(Kula:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
Cannot open the file ../ling/morfeusz-ling/Polish/Inflection/NC-O_O_O-OSOBA-Beethoven.fst2
The following delac entry could not be inflected:
Ludwik(Ludwik:subst:sg:nom:m1) van(van:burk) Beethoven(Beethoven:subst:sg:nom:m1),subst(NC-O_O_O-OSOBA-Beethoven)
The following delac entry could not be inflected:
Marcin(Marcin:subst:sg:nom:m1) Poczobutt(Poczobutt:subst:sg:nom:m1)-(-:interp)Odlanicki(Odlanicki:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
The following delac entry could not be inflected:
Maria(Maria:subst:sg:nom:f) Pawlikowska(Pawlikowska:subst:sg:nom:f)-(-:interp)Jasnorzewska(Jasnorzewska:subst:sg:nom:f),subst(NC-O_ONO-OSOBA)
The following delac entry could not be inflected:
Maria(Maria:subst:sg:nom:f) Skłodowska(Skłodowska:subst:sg:nom:f)-(-:interp)Curie(Curie:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
The following delac entry could not be inflected:
Michał(Michał:subst:sg:nom:m1) Tokarzewski(Tokarzewski:subst:sg:nom:m1)-(-:interp)Karaszewicz(Karaszewicz:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
The following delac entry could not be inflected:
Mikołaj(Mikołaj:subst:sg:nom:m1) Sęp(Sęp:subst:sg:nom:m1)-(-:interp)Szarzyński(Szarzyński:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
The following delac entry could not be inflected:
Natalia(Natalia:subst:sg:nom:f) Gąsiorowska(Gąsiorowska:subst:sg:nom:f)-(-:interp)Grabowska(Grabowska:subst:sg:nom:f),subst(NC-O_ONO-OSOBA)
The following delac entry could not be inflected:
pułkownik(pułkownik:subst:sg:nom:m1) {Zdzisław Kuźmirski-Pacak}(Zdzisław Kuźmirski-Pacak:subst:sg:nom:m1),subst(NC-O_O-OSOBA-plk)
No inflected form could be generated for: Luksemburg:sg:nom:f:same
No inflected form could be generated for: Luksemburg:sg:nom:f:same
No inflected form could be generated for: Luksemburg:sg:nom:f:same
No inflected form could be generated for: Luksemburg:sg:gen:f:same
No inflected form could be generated for: Luksemburg:sg:gen:f:same
No inflected form could be generated for: Luksemburg:sg:gen:f:same
No inflected form could be generated for: Luksemburg:sg:dat:f:same
No inflected form could be generated for: Luksemburg:sg:dat:f:same
No inflected form could be generated for: Luksemburg:sg:dat:f:same
No inflected form could be generated for: Luksemburg:sg:acc:f:same
No inflected form could be generated for: Luksemburg:sg:acc:f:same
No inflected form could be generated for: Luksemburg:sg:acc:f:same
No inflected form could be generated for: Luksemburg:sg:inst:f:same
No inflected form could be generated for: Luksemburg:sg:inst:f:same
No inflected form could be generated for: Luksemburg:sg:inst:f:same
No inflected form could be generated for: Luksemburg:sg:loc:f:same
No inflected form could be generated for: Luksemburg:sg:loc:f:same
No inflected form could be generated for: Luksemburg:sg:loc:f:same
No inflected form could be generated for: Luksemburg:sg:voc:f:same
No inflected form could be generated for: Luksemburg:sg:voc:f:same
No inflected form could be generated for: Luksemburg:sg:voc:f:same
No inflected form could be generated for:
Róża(Róża:subst:sg:nom:f) Luksemburg(Luksemburg:subst:sg:nom:f),subst(NC-O_O-OSOBA)
The following delac entry could not be inflected:
Stefan(Stefan:subst:sg:nom:m1) Szolc(Szolc:subst:sg:nom:m1)-(-:interp)Rogoziński(Rogoziński:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA-a)
The following delac entry could not be inflected:
Tadeusz(Tadeusz:subst:sg:nom:m1) Boy(Boy:subst:sg:nom:m1)-(-:interp)Żeleński(Żeleński:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
The following delac entry could not be inflected:
Tadeusz(Tadeusz:subst:sg:nom:m1) Bór(Bór:subst:sg:nom:m1)-(-:interp)Komorowski(Komorowski:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
No inflected form could be generated for: Łomnicki:sg:nom:m1:same
No inflected form could be generated for: Łomnicki:sg:nom:m1:same
No inflected form could be generated for: Łomnicki:sg:nom:m1:same
No inflected form could be generated for: Łomnicki:sg:gen:m1:same
No inflected form could be generated for: Łomnicki:sg:gen:m1:same
No inflected form could be generated for: Łomnicki:sg:gen:m1:same
No inflected form could be generated for: Łomnicki:sg:dat:m1:same
No inflected form could be generated for: Łomnicki:sg:dat:m1:same
No inflected form could be generated for: Łomnicki:sg:dat:m1:same
No inflected form could be generated for: Łomnicki:sg:acc:m1:same
No inflected form could be generated for: Łomnicki:sg:acc:m1:same
No inflected form could be generated for: Łomnicki:sg:acc:m1:same
No inflected form could be generated for: Łomnicki:sg:inst:m1:same
No inflected form could be generated for: Łomnicki:sg:inst:m1:same
No inflected form could be generated for: Łomnicki:sg:inst:m1:same
No inflected form could be generated for: Łomnicki:sg:loc:m1:same
No inflected form could be generated for: Łomnicki:sg:loc:m1:same
No inflected form could be generated for: Łomnicki:sg:loc:m1:same
No inflected form could be generated for: Łomnicki:sg:voc:m1:same
No inflected form could be generated for: Łomnicki:sg:voc:m1:same
No inflected form could be generated for: Łomnicki:sg:voc:m1:same
No inflected form could be generated for:
Tadeusz(Tadeusz:subst:sg:nom:m1) Łomnicki(Łomnicki:subst:sg:nom:m1),subst(NC-O_O-OSOBA)
Cannot open the file ../ling/morfeusz-ling/Polish/Inflection/NC-O_O_O-OSOBA-van Gogh.fst2
The following delac entry could not be inflected:
Vincent(Vincent:subst:sg:nom:m1) van(van:burk) Gogh(Gogh:subst:sg:nom:m1),subst(NC-O_O_O-OSOBA-van Gogh)
The following delac entry could not be inflected:
Walenty(Walenty:subst:sg:nom:m1) Skorochód(Skorochód:subst:sg:nom:m1)-(-:interp)Majewski(Majewski:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
No inflected form could be generated for: Rutkiewicz:sg:nom:f:same
No inflected form could be generated for: Rutkiewicz:sg:nom:f:same
No inflected form could be generated for: Rutkiewicz:sg:nom:f:same
No inflected form could be generated for: Rutkiewicz:sg:gen:f:same
No inflected form could be generated for: Rutkiewicz:sg:gen:f:same
No inflected form could be generated for: Rutkiewicz:sg:gen:f:same
No inflected form could be generated for: Rutkiewicz:sg:dat:f:same
No inflected form could be generated for: Rutkiewicz:sg:dat:f:same
No inflected form could be generated for: Rutkiewicz:sg:dat:f:same
No inflected form could be generated for: Rutkiewicz:sg:acc:f:same
No inflected form could be generated for: Rutkiewicz:sg:acc:f:same
No inflected form could be generated for: Rutkiewicz:sg:acc:f:same
No inflected form could be generated for: Rutkiewicz:sg:inst:f:same
No inflected form could be generated for: Rutkiewicz:sg:inst:f:same
No inflected form could be generated for: Rutkiewicz:sg:inst:f:same
No inflected form could be generated for: Rutkiewicz:sg:loc:f:same
No inflected form could be generated for: Rutkiewicz:sg:loc:f:same
No inflected form could be generated for: Rutkiewicz:sg:loc:f:same
No inflected form could be generated for: Rutkiewicz:sg:voc:f:same
No inflected form could be generated for: Rutkiewicz:sg:voc:f:same
No inflected form could be generated for: Rutkiewicz:sg:voc:f:same
No inflected form could be generated for:
Wanda(Wanda:subst:sg:nom:f) Rutkiewicz(Rutkiewicz:subst:sg:nom:f),subst(NC-O_O-OSOBA)
The following delac entry could not be inflected:
Włodzimierz(Włodzimierz:subst:sg:nom:m1) Przerwa(Przerwa:subst:sg:nom:m1)-(-:interp)Tetmajer(Tetmajer:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA-a)
The following delac entry could not be inflected:
Zdzisław(Zdzisław:subst:sg:nom:m1) Kuźmirski(Kuźmirski:subst:sg:nom:m1)-(-:interp)Pacak(Pacak:subst:sg:nom:m1),subst(NC-O_ONO-OSOBA)
