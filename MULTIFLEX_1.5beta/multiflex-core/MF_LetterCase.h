/*
  * Unitex 
  *
  * Copyright (C) 2001-2008 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on December 1 2008
 */
//---------------------------------------------------------------------------

/********************************************************************************/
//// Treatment of letter case of a or compound unit                            ///
/********************************************************************************/

#ifndef LetterCase_H
#define LetterCase_H

#include "Unicode.h"
#include "MF_LangMorpho.h"
#include "MF_Global2.h"

/////////////////////////////////////////////////
// Possible letter case variants of a string
typedef enum {same,                  //The string has the same lettercase spelling as its lemma, e.g. "Domku" lemma "Domek"  
	      all_lower,             //The string contains only lowercase letters, e.g. "domek", "port lotniczy"
	      all_upper,             //The string contains only uppercase letters, e.g. "DOMEK", "PORT LOTNICZY"
	      first_upper,           //The initial character of the string is an uppercase letter, 
	                             //all others are lowercase letters, e.g. "Domek"
	      //	      all_lower_each_word,   //Each word in the compound string contains only  
	      //	                             //lowercase letters, e.g. "port lotniczy"
	      //	      all_upper_each_word,   //Each word in the compound string contains only
	      //	                             //uppercase letters, e.g. "PORT LOTNICZY"
	      first_upper_each_word, //Each word in the compound string begins with 
	                             //an uppercase letter, e.g. "Port Lotniczy"
	      no_letter_case,        //The letter case is irrelevant for the string, i.e. an empty string or string 
	                             //containing no letters: a space, "1920", "../.." etc.
	      other                  //Any other case spelling then above, e.g. "DomeK", "Do-mek", "PORT lotniczy", "B314", etc.
  } letter_case_T;  

////////////////////////////////////////////
// Checks the letter case spelling of a string
// 'params' = "global" parameters
letter_case_T letter_case(MultiFlexParameters* params,unichar* s);

///////////////////////////////////////////
// Transforms string 's' so as to obtain the letter case spelling 'lc'
// For lc=other no transformation is done
// E.g. for any s and lc=same, returns s unchanged
//      for s="domek" and lc=first_upper, produces "Domek"
//      for s="domek" and lc=all_upper, produces "DOMEK"
//      for s="Domek" and lc=all_lower, produces "domek"
//      for s="DoMek" and lc=other, produces "DoMek"
// 'params' = "global" parameters
// Returns the transformed string or NULL in case of errors.
unichar* letter_case_apply(MultiFlexParameters* params,unichar *s, letter_case_T lc);

////////////////////////////////////////////
// Deduces the constituent's target letter case with respect to the lemma
// If the form is identical to the lemma no particular letter is admitted, i.e. the letter case is 'other'
// Otherwise it is: all_lower, all_upper or first_upper 
// E.g. if 'form' = "Radka" and 'lemma'="Radek" then returns 'other' (means as lemma)
//      if 'form' = "PeKaO" and 'lemma'="PeKaO" then returns 'other' (as lemma)
//      if 'form' = "PeKaO" and 'lemma'="pko" then returns 'other' (as lemma - this example is undescribable)
//      if 'form' = "Rondo" and 'lemma'="rondo" then returns 'first_upper'
//      if 'form' = "RONDA" and 'lemma'="rondo" then returns 'all_upper'
//      if 'form' = "pampersy" and 'lemma'="Pampers" then returns 'all_lower'
// 'params' = "global" parameters
// Returns the index of this value in the letter case category
// or -1 if not found.
letter_case_T letter_case_deduce_target(MultiFlexParameters* params,unichar* form, unichar* lemma);

#endif
