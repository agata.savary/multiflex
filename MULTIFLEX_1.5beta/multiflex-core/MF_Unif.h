/*
  * Unitex 
  *
  * Copyright (C) 2001-2007 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (savary@univ-mlv.fr)
 * Last modification on June 23 2005
 */
//---------------------------------------------------------------------------

////////////////////////////////////////////////////////////
// Implementation of unification variables

#ifndef UnifH
#define UnifH

#include <stdio.h>
#include "MF_LangMorpho.h"
#include "MF_Global2.h"


////////////////////////////////////////////
// Initializes the set of instantiations. 
// 'params' = "global" parameters 
// Returns 1 on error, 0 otherwise
int unif_init_vars(MultiFlexParameters* params);

////////////////////////////////////////////
// Prints the set of instantiations. 
// 'params' = "global" parameters 
// Returns 1 on error, 0 otherwise
int unif_print_vars(MultiFlexParameters* params);

//////////////////////////////////////////////////////////////
// Liberates the space allocated for the set of instantiations. 
// 'params' = "global" parameters 
// Returns 1 in case of erros, 0 otherwise
int unif_free_vars(MultiFlexParameters* params);

//////////////////////////////////////////////////////////////////////////////////
// If variable "var" already instantiated, returns -1. Otherwise,
// instantiates the unification variable "var" to category "cat" and value "val". 
// 'params' = "global" parameters 
// Returns 1 or -1 in case of error, 0 otherwise.
int unif_instantiate(MultiFlexParameters* params,unichar* var, l_category_T* cat, unichar* val);

//////////////////////////////////////////////////////////////////////////////////
// If variable "var" already instantiated, returns -1. Otherwise,
// instantiates the unification variable "var" to category "cat" and value 
// whose index in the domain of "cat" is "val". 
// 'params' = "global" parameters 
// Returns 1 or -1 in case of error, 0 otherwise.
int unif_instantiate_index(MultiFlexParameters* params,unichar* var, l_category_T* cat, int val);

//////////////////////////////////////////////////////////////////////////////////
// Desinstantiates the unification variable "var". 
// 'params' = "global" parameters 
// Returns 1 in case of error, 0 otherwise.
int unif_desinstantiate(MultiFlexParameters* params,unichar* var);

//////////////////////////////////////////////////////////////////////////////////
// Returns 1 if the unification variable "var" is instantiated, 0 otherwise.
// 'params' = "global" parameters 
int unif_instantiated(MultiFlexParameters* params,unichar* var);

//////////////////////////////////////////////////////////////////////////////////
// If the unification variable "var" is instantiated returns its value, 
// otherwise returns NULL.
// 'params' = "global" parameters 
unichar* unif_get_val(MultiFlexParameters* params,unichar* var);

//////////////////////////////////////////////////////////////////////////////////
// If the unification variable "var" is instantiated returns its index 
// in the domain of its category otherwise returns -1.
// 'params' = "global" parameters 
int unif_get_val_index(MultiFlexParameters* params,unichar* var);

//////////////////////////////////////////////////////////////////////////////////
// If the unification variable "var" is instantiated returns its index 
// in the domain of its category otherwise returns -1.
// 'params' = "global" parameters 
int unif_get_val_index(MultiFlexParameters* params,unichar* var);

//////////////////////////////////////////////////////////////////////////////////
// If the unification variable "var" is instantiated returns its category, 
// otherwisz returns NULL..
// 'params' = "global" parameters 
l_category_T* unif_get_cat(MultiFlexParameters* params,unichar* var);

#endif
