/*
  * Unitex 
  *
  * Copyright (C) 2001-2008 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *22
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on June 19 2009
 */
//---------------------------------------------------------------------------

/********************************************************************************/
//// Treatment of initialisms of a  unit                                       ///
/********************************************************************************/

#include "Unicode.h"
#include "Error.h"
#include "MF_Initialism.h"

///////////////////////////////////////////
// Transforms string 's' so as to obtain the initialism of type 'init'
// E.g. for s="Jan" and init=no_init, produces "Jan"
//      for s="Jan" and init=dot, produces "J."
//      for s="Jan" and init=no_dot, produces "J"
// Returns the transformed string or NULL in case of errors.
unichar* init_apply(unichar *s, init_T init) {
  if (! s || !u_strlen(s) || init == no_init)
    return s;

  switch (init) {
  case dot :
    if (u_strlen(s)<2) {
      error("The initialism cannot be retrieved: the word is too short: %S\n",s);
      return NULL;
    }
    s[1] = (unichar) '.';
    s[2] = 0;
    break;
  case no_dot:
    if (!u_strlen(s)) {
      error("The initialism cannot be retrieved: the word is too short: %S\n",s);
      return NULL;
    }
    s[1] = 0;
    break;
  case dot2 :
    if (u_strlen(s) < 3) {
      error("The initialism cannot be retrieved: the word is too short: %S\n",s);
      return NULL;
    }
    s[2] = (unichar) '.';
    s[3] = 0;
    break;
  case no_dot2:
    if (u_strlen(s) < 2) {
      error("The initialism cannot be retrieved: the word is too short: %S\n",s);
      return NULL;
    }
    s[2] = 0;
    break;
  case dot3 :
    if (u_strlen(s) < 4) {
      error("The initialism cannot be retrieved: the word is too short: %S\n",s);
      return NULL;
    }
    s[3] = (unichar) '.';
    s[4] = 0;
    break;
  case no_dot3:
    if (u_strlen(s) < 3) {
      error("The initialism cannot be retrieved: the word is too short: %S\n",s);
      return NULL;
    }
    s[3] = 0;
    break;
  case dot4 :
    if (u_strlen(s) < 5) {
      error("The initialism cannot be retrieved: the word is too short: %S\n",s);
      return NULL;
    }
    s[4] = (unichar) '.';
    s[5] = 0;
    break;
  case no_dot4:
    if (u_strlen(s) < 4) {
      error("The initialism cannot be retrieved: the word is too short: %S\n",s);
      return NULL;
    }
    s[4] = 0;
    break;
  case dot5 :
    if (u_strlen(s) < 6) {
      error("The initialism cannot be retrieved: the word is too short: %S\n",s);
      return NULL;
    }
    s[5] = (unichar) '.';
    s[6] = 0;
    break;
  case no_dot5:
    if (u_strlen(s) < 5) {
      error("The initialism cannot be retrieved: the word is too short: %S\n",s);
      return NULL;
    }
    s[5] = 0;
    break;
  default:
    break;
  }
  return s;
}
