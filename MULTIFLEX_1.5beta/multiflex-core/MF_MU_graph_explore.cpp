/*
  * Unitex 
  *
  * Copyright (C) 2001-2007 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on September 18 2009
 */
//---------------------------------------------------------------------------

/********************************************************************************/
//// Exploring and inflection graph for multi-word units                //////////
/********************************************************************************/

#include "MF_MU_graph_explore.h"
#include "MF_MU_graph.h"
#include "MF_MU_graph_label.h"
#include "MF_Util.h"
#include "MF_Unif.h"
#include "MF_FeatStruct.h"
#include "MF_FeatStructStr.h"
#include "MF_LetterCase.h"
#include "MF_MU_embedded.h"
#include "Error.h"
#include "Fst2.h"
#include "Transitions.h"
#include "MF_UnitMorphoBase.h"

int MU_graph_explore_graph(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_lemma_T* MU_lemma, U_forms_T* forms);
int MU_graph_explore_state(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,int q_no, U_forms_T* forms,U_lemma_T* MU_lemma, int T);
int MU_graph_explore_label(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,MU_graph_label_T* l, int q_bis_no, U_forms_T* forms,U_lemma_T* MU_lemma, int T);
int MU_graph_explore_label_in(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_form_id_T* u,U_lemma_T* ext_lemma,MU_graph_label_T* l, int q_bis_no, U_forms_T* forms,U_lemma_T* MU_lemma, int T);
int MU_graph_explore_label_in_rec(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_form_id_T* u,U_lemma_T* ext_lemma,MU_graph_label_T* l, int i_catval, f_feat_struct_T* feat, int q_bis_no, U_forms_T* forms,U_lemma_T* MU_lemma, int T);
int MU_graph_explore_and_concat_label_out(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,MU_graph_label_T* l, int q_bis_no, U_forms_T* U_forms, U_forms_T* forms,U_lemma_T* MU_lemma, int T);
int MU_graph_explore_label_out(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,MU_graph_label_T* l, int q_bis_no, U_forms_T* forms,U_lemma_T* MU_lemma,int T);
int MU_graph_explore_label_out_rec(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,MU_graph_label_T* l,int i_catval,f_feat_struct_T* feat, int q_bis_no, U_forms_T* forms,U_lemma_T* MU_lemma, int T);
int MU_graph_explore_label_tail(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,int input, MU_graph_catval_T* cv, U_form_id_T* u, U_lemma_T* ext_lemma, MU_graph_label_T* MU_graph_label, int i_catval, f_feat_struct_T* feat, int q_bis_no, U_forms_T* forms, U_lemma_T* MU_lemma, int T);
int MU_graph_get_unit_forms(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_form_id_T* u,f_feat_struct_T* feat,U_forms_T* U_forms);
int MU_graph_get_ext_unit_forms(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_lemma_T* lemma,f_feat_struct_T* feat,U_forms_T* U_forms);
void MU_graph_get_unit_forms_check(MultiFlexParameters* params,U_lemma_T* lemma,f_feat_struct_T* feat,U_forms_T* forms);
int f_determine_full_features(U_form_id_T* u, f_feat_struct_T* desired_feat, f_feat_struct_T* full_feat);
int*  MU_graph_explore_label_get_admitted_values(MultiFlexParameters* params,MU_graph_catval_T* cv,U_lemma_T* MU_lemma, U_form_id_T* u);
int*  MU_graph_explore_label_get_admitted_values_cnst(MU_graph_catval_T* cv);
int*  MU_graph_explore_label_get_admitted_values_inherit_const(MU_graph_catval_T* cv,U_lemma_T* MU_lemma);
int* MU_graph_explore_label_get_admitted_values_unif_var(MultiFlexParameters* params,MU_graph_catval_T* cv);
int* MU_graph_explore_label_get_admitted_values_inherit_var(MultiFlexParameters* params,MU_graph_catval_T* cv, U_form_id_T* u);
int MU_graph_explore_label_instantiate(MultiFlexParameters* params,int val, MU_graph_catval_T* cv);
int  MU_graph_explore_label_desinstantiate(MultiFlexParameters* params,MU_graph_catval_T* cv);
 
/////////////////////////////////////////////////
// Explores the inflection transducer of the MU-lemma 'MU_lemma' 
// in order to generate all its inflected forms. The generated forms are put to 'forms'
// Initially, 'forms' has its space allocated but is empty.
// T = index of the inflection transducer of MU_lemma
// CURRENT_FORMS = all orm geerated up till now
// 'params' = "global" parameters 
// Returns 0 on success, 1 otherwise. 
int MU_graph_explore_graph(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_lemma_T* MU_lemma, U_forms_T* forms) {
  int res;
  int T; //index of the inflection transducer of MU_lemma

  //Initialize the structure for graph unification variables
  unif_init_vars(params);
  
  //Get the index of the initial state of the inflection tranducer
  int initial;
  initial = MU_graph_get_initial(params,MU_lemma->paradigm, &T);

  if (initial<0)
    return 1;

  //Explore the inflection transducer starting from its initial state
  res=MU_graph_explore_state(params,CURRENT_FORMS,initial,forms,MU_lemma,T);

  unif_free_vars(params);
  return res;
}

////////////////////////////////////////////
// Given the current instantiation of unification variables and of
// inheritance variables explore the state q_no number of an inflection transducer for MWUs.
// Generate suffixes of the inflected forms and their features, and put them to 'forms'.
// In case of error put an empty list into 'forms' (which is not identical to [1,((epsilon,empty_set))]). 
// Initially, 'forms' has its space allocated but is empty.
// CURRENT_FORMS = all orm geerated up till now
// 'params' = "global" parameters 
// 'MU_lemma' = muti-word unit lemma being inflected
// T = index of the inflection transducer of MU_lemma in the transducer table
// Return a number !=0 in case of errors, 0 otherwise. 
int MU_graph_explore_state(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,int q_no, U_forms_T* forms,U_lemma_T* MU_lemma, int T) {
  int err;
  MU_graph_label_T l; //Label of the current transition
  int q_bis_no; //Index of the arrival state
  int e_no; //Index of the transition label
  form_extras_T extras;  //Extra information attached to a form

  //If we are in a final state, then the empty word is recognized, we add it to 'forms' and we continue to explore
  Fst2State q; //Current state
  q = MU_graph_get_state(params,q_no, T);
  if (q->control & 1) {
    fe_init_extras(&extras,&(q_no));
    U_add_empty_form(forms,&extras);
  }

  //Explore each outgoing transitions
  Transition* t; //Current transition
  U_forms_T forms_bis; //Suffixes obtained by the exploration of one outgoing transition
  t = q->transitions;
  while (t) {
    //Scan the transition label
    Fst2Tag e = MU_graph_get_label(params,t, T);
    if (MU_graph_scan_label(params,e->input,e->output,&l,MU_lemma))
      return 1;     
    //MU_graph_print_label(&l);

    //Get the arrival state and the label index
    q_bis_no = MU_graph_get_arrival_state(t);
    e_no = MU_graph_get_label_number(t);

    //Initialize the set of inflected forms
    U_init_forms(&forms_bis);
    if (MU_graph_explore_label(params,CURRENT_FORMS,&l,q_bis_no,&forms_bis,MU_lemma,T)) {
      U_delete_forms(&forms_bis);
      return 1;
    }

    //Prefix the extra information attached to each generated form
    //by the transition number preceded by the starting state number
    err = U_concat_extras(&forms_bis, &e_no);
    err = err | U_concat_extras(&forms_bis, &q_no);

    //Add each new form to the ones generated previously, if it does not exist already
    err = err | U_merge_forms(forms, &forms_bis);
    if (err) {
      U_delete_forms(&forms_bis);
      return 1;
    }
    
    U_delete_forms(&forms_bis);

    //Go to the next transition
    t = t->next;
  }
  return 0;
}

////////////////////////////////////////////
// Given the current instantiation of unification variables and of
// inheritance variables, explore the current transition label 'l', 
// and the subautomaton starting from its arrival state 'q_bis_no'.
// Generate suffixes of the inflected
// forms and their features, and put them to 'forms'.
// In case of error put an empty list into 'forms'. 
// Initially, '*forms' has its space allocated but is empty.
// CURRENT_FORMS = all orm geerated up till now
// 'params' = "global" parameters 
// 'MU_lemma' = muti-word unit lemma being inflected
// T = index of the inflection transducer of MU_lemma in the transducer table
// Return a number !=0 in case of errors, 0 otherwise. 
int MU_graph_explore_label(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,MU_graph_label_T* l, int q_bis_no, U_forms_T* forms,U_lemma_T* MU_lemma, int T) {
  if (!l || q_bis_no<0 || !forms || !MU_lemma || T<0)
    return 1;

  int err;
  //If the current unit is a reference to a lemma's unit or and external lemma (e.g. $2 or trzeci.0:adj)
  if (l->in && l->in->unit.type!=cst) {
    U_form_id_T* u = NULL;  //Referenced lemma's unit
    U_lemma_T* ext_lemma = NULL; //External lemma
    if (l->in->unit.type==var) {  //Reference to lemma's unit e.g. $2
      int u_no;    //Number of the referenced unit
      u_no = l->in->unit.u.num;
      u = U_lemma_get_component(MU_lemma,u_no);//Get the referenced lemma's unit
      if (!u) {
	error("Unit $%i referenced in graph %s does not exist in the compound.\n",u_no,MU_lemma->paradigm);
	return 1;
      }
    }
    else   //External lemma e.g. trzeci.0:adj
      ext_lemma = l->in->unit.u.ext;
    //explore the current unit according to its morphology, then the label's output and the rest of the automaton
    err = MU_graph_explore_label_in(params,CURRENT_FORMS,u,ext_lemma,l,q_bis_no,forms,MU_lemma,T);
    if (err) 
      return err;
  }
  
  else { //If the current unit is fixed (empty or a fixed word, e.g. "of")
    
    //////////////////////////////////
    //Get the forms of the current unit
    U_forms_T U_forms;
    U_init_forms(&U_forms);
    
    ///If the current unit's input is empty (it is an epsilon input <E>)
    if (!l->in)
      //The current form is empty
      U_add_empty_form(&U_forms,NULL);
    
    //If the current unit is a constant (e.g. "of")
    else 
      //Get the form appearing in the node's input
      U_add_invariable_form(l->in->unit.u.seq,&U_forms,NULL);
     
    //Get the form suffixes from the output label and the rest of the automaton
    //Concatenate the forms of the current unit with those suffixes
    err = MU_graph_explore_and_concat_label_out(params,CURRENT_FORMS,l,q_bis_no,&U_forms,forms,MU_lemma,T);
    if (err)
      return err;
  } 
  return 0;
}

////////////////////////////////////////////
// Given the current instantiation of unification variables and of
// inheritance variables, recursively explore the input of current transition label, 
// knowing that it is a reference to the MU lemma's unit 'u' and its
// desired morphology is the input part of 'l'.
// Then explore the label's output.
// Then explore the subautomaton starting from its arrival state whose index is 'q_bis_no'.
// Generate suffixes of the inflected
// forms and their features, and put them to 'forms'.
// 'params' = "global" parameters 
// CURRENT_FORMS = all orm geerated up till now
// u = current unit (if any)
// ext_lemma = current external lemma (if any)
// l = graph label to be explored
// q_bis = arrival state of the current transition
// 'MU_lemma' = muti-word unit lemma being inflected
// T = index of the inflection transducer of MU_lemma in the transducer table
// forms = output parameter:  forms generated by this exploration
// Initially, '*forms' has its space allocated but is empty.
// In case of error put an empty list into 'forms'. 
// Return a number !=0 in case of errors, 0 otherwise. 
int MU_graph_explore_label_in(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_form_id_T* u, U_lemma_T* ext_lemma, MU_graph_label_T* l, int q_bis_no, U_forms_T* forms,U_lemma_T* MU_lemma, int T) {
  int err;
  f_feat_struct_T feat;   //A set of the already treated morphological features contained in the label's input
  f_init_feat_struct(&feat);
  err = MU_graph_explore_label_in_rec(params,CURRENT_FORMS, u, ext_lemma, l, 0, &feat, q_bis_no, forms, MU_lemma,T);
  return err;
}

////////////////////////////////////////////
// Recursive exploration of the current transition label's input
// then output, then of the rest of the automaton.
// 'params' = "global" parameters 
// CURRENT_FORMS = all orm geerated up till now
// u: lemma's unit concerned (if any)
// ext_lemma: external lemam concerned (if any)
// l = graph label to be explored
// i_catval: index of the first input label's category-value pair not yet treated 
// feat: accumulated desired features of the current unit (deduced from this unit's features
// in the MU lemma, and from the label's input).
// q_bis_no: arrival state of the label's transition
// forms: suffixes of the inflected forms and their features
// Initially, '*forms' has its space allocated but is empty.
// 'MU_lemma' = muti-word unit lemma being inflected
// T = index of the inflection transducer of MU_lemma in the transducer table
// In case of error put an empty list into 'forms'. 
// Return a number !=0 in case of errors, 0 otherwise. 
int MU_graph_explore_label_in_rec(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_form_id_T* u, U_lemma_T* ext_lemma, MU_graph_label_T* l, int i_catval, f_feat_struct_T* feat, int q_bis_no, U_forms_T* forms,U_lemma_T* MU_lemma, int T) {
  if (! l || ! l->in || !forms || !MU_lemma || (u && ext_lemma) || (!u && !ext_lemma))
    return 1;

  int err;
  MU_graph_feat_struct_T* l_in_feat_struct;  //Feature structure of the label's input
  l_in_feat_struct = l->in->feat_struct;

  /////////////////////////////////////////////////////////////////
  //If the whole label's input not yet treated continue treating it
  if (l_in_feat_struct && i_catval != l_in_feat_struct->no_catvals){
    MU_graph_catval_T* cv;  // a single graph category-value pair, e.g. Case==$n1
    cv = &(l_in_feat_struct->catvals[i_catval]);
    //Explore the tail of the input label starting from the next category-value pair
    err = MU_graph_explore_label_tail(params,CURRENT_FORMS,1,cv,u,ext_lemma,l,i_catval+1,feat,q_bis_no,forms,MU_lemma,T);
    if (err)
      return err;
  }														   
  
  ////////////////////////////////////////////////////////////////////////////////
  //If the whole label's input treated get the inflected forms of the current unit
  else {
    
    //Get the inflected forms of the current unit
    U_forms_T U_forms;
    U_init_forms(&U_forms);
    
    //If no morphology in the input label, take the unit as it appears in the MWU's lemma
    if (!l_in_feat_struct) {
      if (u) 
	//Get the inflected form from the current unit (this form is not to be modified since there is no morphology in the input label)
	U_add_invariable_form(u->form,&U_forms,NULL);
    }

    //If all morphological category-value equations in the input label treated
    else {
      //Inflect the unit concerned according to desired features
      if (u)
	err = MU_graph_get_unit_forms(params,CURRENT_FORMS,u,feat,&U_forms);
      else 
	err = MU_graph_get_ext_unit_forms(params,CURRENT_FORMS,ext_lemma,feat,&U_forms);
      if (err) {
	U_delete_forms(&U_forms);
	return err;
      }
    }
    //Get the form suffixes from the output label and the rest of the automaton
    //Concatenate the forms of the current unit with those suffixes
    err = MU_graph_explore_and_concat_label_out(params,CURRENT_FORMS,l,q_bis_no,&U_forms,forms,MU_lemma,T);
    if (err)
      return err;
  }

  return 0;
}


////////////////////////////////////////////
// Given the current instantiation of unification variables and of
// inheritance variables, explore the output of the current transition label 'l', 
// Generate suffixes of the inflected
// forms and their features, concatenate them with the forms of the current unit
// and put them to 'forms'.
// In case of error put an empty list into 'forms'. 
// 'params' = "global" parameters 
// CURRENT_FORMS = all orm geerated up till now
// l = label of the current node
// q_bis_no = arrival state of the current transition
// U_forms = inflected forms of the current unit
/// 'MU_lemma' = muti-word unit lemma being inflected
// T = index of the inflection transducer of MU_lemma in the transducer table
// forms = output parameter:  forms generated by this exploration
// Initially, '*forms' has its space allocated but is empty.
// Return a number !=0 in case of errors, 0 otherwise. 
int MU_graph_explore_and_concat_label_out(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,MU_graph_label_T* l, int q_bis_no, U_forms_T* U_forms, U_forms_T* forms,U_lemma_T* MU_lemma, int T) {
  int err; //Error code if any
  //Get the suffixes of the MWU forms
  U_forms_T suffix_forms;  
  U_init_forms(&suffix_forms);
  err = MU_graph_explore_label_out(params,CURRENT_FORMS,l,q_bis_no,&suffix_forms,MU_lemma,T);  //Explore the rest of the automaton
  if (err) {
    U_delete_forms(U_forms);
    U_delete_forms(&suffix_forms);      
    return err;
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////
  //Concatenante each inflected form of the current unit in front of each multi-unit form
  //resulting from the exploration of the rest of the automaton
  if (U_concat_forms(U_forms, &suffix_forms, forms)) {
    U_delete_forms(U_forms);
    U_delete_forms(&suffix_forms);      
    return 1;
  }
  
  //Delete the intermediate simple et compound forms
  U_delete_forms(U_forms);
  U_delete_forms(&suffix_forms);      

  return 0;
}

////////////////////////////////////////////
// Given the current instantiation of unification variables and of
// inheritance variables, explore the current transition label's output ' 
// and the subautomaton starting from its arrival state 'q_bis'.
// Generate suffixes of the inflected
// forms and their features, and put them to 'forms'.
// 'params' = "global" parameters 
// CURRENT_FORMS = all orm geerated up till now
// l = label of the current node
// q_bis_no = arrival state of the current transition
// 'MU_lemma' = muti-word unit lemma being inflected
// T = index of the inflection transducer of MU_lemma in the transducer table
// forms = output parameter:  forms generated by this exploration
// In case of error put an empty list into 'forms'. 
// Initially, '*forms' has its space allocated but is empty.
// Return a number !=0 in case of errors, 0 otherwise. 
int MU_graph_explore_label_out(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,MU_graph_label_T* l, int q_bis_no, U_forms_T* forms,U_lemma_T* MU_lemma,int T) {
  int err;
  f_feat_struct_T feat;   //A set of the already treated morphological features contained in the label's output
  f_init_feat_struct(&feat);
  err = MU_graph_explore_label_out_rec(params,CURRENT_FORMS,l,0,&feat,q_bis_no,forms,MU_lemma,T);
  return(err);
}

////////////////////////////////////////////
// Recursive exploration of the current transition label's output,
// then of the rest of the automaton.
// 'params' = "global" parameters 
// CURRENT_FORMS = all orm geerated up till now
// l = graph label to be explored
// i_catval: index of the first label's output category-value pair not yet treated 
// feat: accumulated output features of the MWU
// q_bis_no: arrival state of the label's transition
// 'MU_lemma' = muti-word unit lemma being inflected
// T = index of the inflection transducer of MU_lemma
// forms: output parameter: suffixes of the inflected forms and their features
// Initially, '*forms' has its space allocated but is empty.
// In case of error put an empty list into 'forms'. 
// Return a number !=0 in case of errors, 0 otherwise. 
int MU_graph_explore_label_out_rec(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,MU_graph_label_T* l,int i_catval,f_feat_struct_T* feat, int q_bis_no, U_forms_T* forms,U_lemma_T* MU_lemma, int T) {
  if (! l)
    return 1;

  int err; //Error code if any
  MU_graph_feat_struct_T* l_out_feat_struct;  //Feature structure of the label's input
  l_out_feat_struct = l->out;

 //If Label's input not empty and not yeat treated, go to the next category-value pair
  if (l_out_feat_struct && i_catval != l_out_feat_struct->no_catvals) {
    MU_graph_catval_T* cv;  // a single graph category-value pair, e.g. Case==$n1
    cv = &(l_out_feat_struct->catvals[i_catval]);
    //Explore the tail of the output label starting from the next category-value pair
    err = MU_graph_explore_label_tail(params,CURRENT_FORMS,0,cv,NULL,NULL,l,i_catval+1,feat,q_bis_no,forms,MU_lemma,T);
    if (err)
      return err;
  }
  else { //No morphology in the output label or whole output morphology has been treated
    //Explore the arrival state
    err = MU_graph_explore_state(params,CURRENT_FORMS,q_bis_no,forms,MU_lemma,T);
    if (err)
      return err;
    //Add the current features (if any) to the features of the suffixes
    if (U_enlarge_forms_morpho(params,forms,feat))
      return 1; 
  } 							   
  return 0;
}

////////////////////////////////////////////
// Recursive exploration of the current transition labels input or output,
// then of the rest of the automaton, starting from the current category-value pair
// 'params' = "global" parameters 
// CURRENT_FORMS = all orm geerated up till now
// input: boolean saying if the label is the input label (1) or the output label (0)
// cv: current category-value pair, e.g. Case=acc, Case=acc|inst, Case=$1.Case, Case=$c, Case==$c, Case=$c=acc|inst
// u: lemma's unit concerned if any, e.g. vive
// ext_lemma: external lemma concerned if any, e.g. trzeci.0:adj
// MU_graph_label: current graph label Nb=pl; Case=$c1; Gen==$g
// i_catval: index of the first category-value pair not yet treated in the input/output of  MU_graph_label:
// feat: accumulated desired features of 
//     - the current unit (if input=1)
//     - the whole MWU (if input=0)
// q_bis_no: arrival state of the label's transition
// forms: suffixes of the inflected forms and their features
// Initially, '*forms' has its space allocated but is empty.
// 'MU_lemma' = muti-word unit lemma being inflected
// T = index of the inflection transducer of MU_lemma in the transducer table
// In case of error put an empty list into 'forms'. 
// Return a number !=0 in case of errors, 0 otherwise. 
int MU_graph_explore_label_tail(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,int input, MU_graph_catval_T* cv, U_form_id_T* u, U_lemma_T* ext_lemma, MU_graph_label_T* MU_graph_label, int i_catval, f_feat_struct_T* feat, int q_bis_no, U_forms_T* forms, U_lemma_T* MU_lemma, int T) {
  int err; //Error code if any
  int inst; //Boolean saying if an instatiation took place

  int* values; //List of values admitted for the current category, terminated by -1
  values = MU_graph_explore_label_get_admitted_values(params,cv, MU_lemma, u);
  if (!values) {
    U_delete_forms(forms);
    return 1;
  }

  int v=0; //Current value
  //Try each admitted value
  while (values[v] != -1) {
    
    //Instantiated to unification variable if any
    inst = MU_graph_explore_label_instantiate(params,values[v], cv);
    if (inst == -1)
      return 1;

    //Add the current label's category-value pair to the features of the (single or compound) unit to be generated
    //The resulting feature structure should be sorted (with respect to the rang of categories on the language morphology file)
    err = f_enlarge_feat_struct(params,feat,cv->cat,values[v], 1);
    if (err) {
      error("Input/output morphology incorrect in graph %s.\n",MU_lemma->paradigm);
      U_delete_forms(forms);
      free(values);
      return 1;
  }

    //Explore recursively the rest of the label
    U_forms_T suffix_forms;  
    U_init_forms(&suffix_forms);
    if (input)
      err =  MU_graph_explore_label_in_rec(params,CURRENT_FORMS,u, ext_lemma, MU_graph_label, i_catval, feat, q_bis_no, &suffix_forms, MU_lemma, T);
    else
      err =  MU_graph_explore_label_out_rec(params,CURRENT_FORMS,MU_graph_label, i_catval, feat, q_bis_no, &suffix_forms, MU_lemma, T);
    if (err) {
      U_delete_forms(forms);
      U_delete_forms(&suffix_forms);
      free(values);
      return 1;
    }

    //Add each suffix obtained to the list of previously obtained suffixes
    if (U_merge_forms(forms, &suffix_forms)) {
      U_delete_forms(forms);
      U_delete_forms(&suffix_forms);
      free(values);
      return 1;
    }

    //Delete the current category-value pair
    err = f_del_one_catval(feat, cv->cat);
    if (err) {
      U_delete_forms(forms);
      U_delete_forms(&suffix_forms);
      free(values);
      return 1;
    }

    //Desinstantiate the current unification variable if any
    if (inst==1) {
      err = MU_graph_explore_label_desinstantiate(params,cv);
      if (err)
	return err;
    }
    
    //Delete the intermediate simple et compound forms
    U_delete_forms(&suffix_forms); 

    //Go to the next admitted value
    v++;
  }
  free(values);
  return 0;
}

/////////////////////////////////////////////////
// Generate the desired inflected forms (defined by 'feat')
// of the current (single or compound) unit 'u'.
// Return the generated forms in 'U_forms'.
// In case of error put an empty list into 'U_forms'. 
// Initially, 'U_forms' has its space allocated but is empty.
// 'params' = "global" parameters 
// CURRENT_FORMS = all orm geerated up till now
// Return a number !=0 in case of errors, 0 otherwise. 
int MU_graph_get_unit_forms(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_form_id_T* u,f_feat_struct_T* feat,U_forms_T* U_forms) {
  int err, err_bis;
  f_feat_struct_T* full_feat;  //Features that the current unit has in the lemma of the MWU
  full_feat = (f_feat_struct_T*) malloc(sizeof(f_feat_struct_T));
  if (!full_feat)
    fatal_error("Memory allocation problem in function 'MU_graph_get_unit_forms'!\n");
  f_init_feat_struct(full_feat);

  //Determine the full list of desired features for the unit's inflected form
  err = U_determine_full_features(params, u, feat, full_feat);
  if (err) {
    f_delete_feat_struct(&full_feat);
    return 1;
  }
  //Generate the desired inflected forms of the (single or compound) unit
  err_bis = incrementEmbeddingLevel(params);
  if (err_bis<0) {
    f_delete_feat_struct(&full_feat);
    return 1;
  }
  //Inflect the unit
  err = U_inflect(params,CURRENT_FORMS,u->lemma, full_feat, U_forms);
  err_bis = decrementEmbeddingLevel(params);
  if (err || err_bis<0) {
    f_delete_feat_struct(&full_feat);
    return 1;
  }
  MU_graph_get_unit_forms_check(params,u->lemma, full_feat, U_forms);
  f_delete_feat_struct(&full_feat);
  return err;
}

/////////////////////////////////////////////////
// Generate the desired inflected forms (defined by 'feat')
// of an external lemma 'lemma'
// Return the generated forms in 'U_forms'.
// In case of error put an empty list into 'U_forms'. 
// Initially, 'U_forms' has its space allocated but is empty.
// 'params' = "global" parameters 
// CURRENT_FORMS = all orm geerated up till now
// Return a number !=0 in case of errors, 0 otherwise. 
int MU_graph_get_ext_unit_forms(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_lemma_T* lemma,f_feat_struct_T* feat,U_forms_T* U_forms) {
  if (incrementEmbeddingLevel(params) < 0)
    return 1;
  
  //Inflect the unit
  if (U_inflect(params,CURRENT_FORMS,lemma, feat, U_forms))
    return 1;

  if (decrementEmbeddingLevel(params) < 0)
    return 1;
  
  //Check if any form could be generated
  MU_graph_get_unit_forms_check(params,lemma, feat, U_forms);
  return 0;
}

/////////////////////////////////////////////////
// Print an error message if no forms could be generated for a lemma
// 'params' = "global" parameters 
// CURRENT_FORMS = all orm geerated up till now
void MU_graph_get_unit_forms_check(MultiFlexParameters* params,U_lemma_T* lemma,f_feat_struct_T* feat,U_forms_T* forms) {
  if (!lemma || !feat || !forms)
    return;
  if (forms->no_forms == 0) {
    unichar* feat_str;  //Feature structure string of the form that could not be generated
    feat_str = get_str_feat_struct(params,feat);
    int lemma_len; //Length of the lemma
    lemma_len = U_get_lemma_str_len(lemma);
    unichar* lemma_str; //Lemma string
    lemma_str = (unichar*) malloc((lemma_len+1) * sizeof(unichar));
    if (!lemma_str)
      fatal_error("Memory allocation problem in function 'MU_graph_get_unit_forms_check'!\n");
    if (U_get_lemma_str(lemma_str, lemma_len+1, lemma)) {
      f_delete_feat_struct(&feat);
      return;
    }
    error("No inflected form could be generated for: %S:%S\n",lemma_str, feat_str);
    free(lemma_str);
    free(feat_str);
  }
}

////////////////////////////////////////////
// Get the list of the addmitted values for a category
// 'params' = "global" parameters 
// CURRENT_FORMS = all orm geerated up till now
// cv: current category-value pair, e.g. Case=acc, Case=acc|inst, Case=$1.Case, Case=$c, Case==$c, Case=$c=acc|inst
// 'MU_lemma' = muti-word unit lemma being inflected
// u: lemma's unit concerned if any, e.g. vive
// Return a list (terminated by -1) of indices of values admitted for the current category
// Return NULL in case of errors
int*  MU_graph_explore_label_get_admitted_values(MultiFlexParameters* params,MU_graph_catval_T* cv,U_lemma_T* MU_lemma, U_form_id_T* u) {
  if (!cv)
    return NULL;

  int* values; //A list of admitted values terminated by -1
  switch (cv->type) {

  case cnst: //e.g. m1|m3
    values = MU_graph_explore_label_get_admitted_values_cnst(cv);
    break;
  case inherit_const: //e.g. $1.Gen
    values = MU_graph_explore_label_get_admitted_values_inherit_const(cv, MU_lemma);
    break;
  case unif_var: //e.g. $g or $g=m1|m3
    values = MU_graph_explore_label_get_admitted_values_unif_var(params,cv);
    break;
  case inherit_var: //e.g. ==$g
    values = MU_graph_explore_label_get_admitted_values_inherit_var(params,cv, u);
    break;
  }
  return values;
}

////////////////////////////////////////////
// Get the list of the addmitted values for a category. THe values are of constant type (e.g. acc|inst)
// cv: current category-value pair, e.g. Case=acc|inst
// Return a list (terminated by -1) of indices of values admitted for the current category
// Return NULL in case of errors
int*  MU_graph_explore_label_get_admitted_values_cnst(MU_graph_catval_T* cv) {
  int* values;  //Admitted values to be returned
  int v; //Index of the current value
  if (!cv->val.values)
    return NULL;
  int size = 0;
  for (v=0; cv->val.values[v]!=-1;v++);
  size = v+1;
  values = (int*) malloc(size*sizeof(int));
  if (!values)
    fatal_error("Not enough memory in function  MU_graph_explore_label_get_admitted_values_cnst\n");
  for (v=0; cv->val.values[v]!=-1; v++)
    values[v] = cv->val.values[v];
  values[v] = -1;
  return values;
}

////////////////////////////////////////////
// Get the (one-element) list of the addmitted values for a category. The value is of inherited type (e.g. $1.Gen)
// cv: current category-value pair, e.g. Case=$1.Case
// 'MU_lemma' = muti-word unit lemma being inflected
// Return a list (terminated by -1) of indices of values admitted for the current category
// Return NULL in case of errors
int*  MU_graph_explore_label_get_admitted_values_inherit_const(MU_graph_catval_T* cv,U_lemma_T* MU_lemma) {
  int* values;  //Admitted values to be returned
  int const_nb;  //Number of the constituent from which the value is to be inherited

  //Get the value from the particular constituent
  const_nb = cv->val.inherit_const;
  if (const_nb<1 || MU_lemma->compound_lemma.no_forms<const_nb)
    return NULL;
  int val; //Value corresponding to the category in c within the constituent concerned
  //In a MU lemma the constituents are numbered from 0, while in a graph their are numbered from 1
  val =  f_get_value(MU_lemma->compound_lemma.forms[const_nb-1].feat,cv->cat); 
  if (val<0) {
    error("The morphological values to be inherited could not be deduced from constituent %d !\n", const_nb);
    return NULL;
  }
  values = (int*) malloc(2*sizeof(int));
  if (!values)
    fatal_error("Not enough memory in function  MU_graph_explore_label_get_admitted_values_inherit_const\n");
  values[0] = val;
  values[1] = -1;
  return values;
}

////////////////////////////////////////////
// Get the list of the addmitted values for a category. THe values are of unification variable type (e.g. $c, $c=acc|inst)
// 'params' = "global" parameters 
// CURRENT_FORMS = all orm geerated up till now
// cv: current category-value pair, e.g. Case=$c or Case=$c=acc|inst
// Return a list (terminated by -1) of indices of values admitted for the current category
// Return NULL in case of errors
int* MU_graph_explore_label_get_admitted_values_unif_var(MultiFlexParameters* params,MU_graph_catval_T* cv) {
  int* values;  //Admitted values to be returned
  int v; //Index of the current value;
  
  unichar* var;  //Unification variable's identifier
  var = cv->val.unif_var->var;
  
  //Variable already instatiated
  if (unif_instantiated(params,var)) {
    //If the same variable already instantiated for a DIFFERENT category then no value is admitted
    if (unif_get_cat(params,var)!=cv->cat)
      return NULL; 
    //If the same variable already instantiated for the SAME category then one value only is admitted
    values = (int*) malloc(2*sizeof(int));
    if (!values)
      fatal_error("Not enough memory in function  MU_graph_explore_label_get_admitted_values_unif_var\n");
    values[0] = unif_get_val_index(params,var);
    values[1] = -1;
  }

  //Variable not yet instantiated
  else {
    //Not all values from the category admitted
    if (cv->val.unif_var->values) {
      int size = 0; //Size of the value list
      for (v=0; cv->val.unif_var->values[v]!=-1;v++);
      size = v+1;
      values = (int*) malloc(size*sizeof(int));
      if (!values)
	fatal_error("Not enough memory in function  MU_graph_explore_label_get_admitted_values_unif_var\n");
      for (v=0; cv->val.unif_var->values[v]!=-1; v++)
	values[v] = cv->val.unif_var->values[v];
      values[v] = -1;
    }
    //All values from the category admitted
    else {
      values = (int*) malloc((cv->cat->no_values+1)*sizeof(int));
      if (!values)
	fatal_error("Not enough memory in function  MU_graph_explore_label_get_admitted_values_unif_var\n");
      for (v=0; v<cv->cat->no_values; v++)
	values[v] = v;
      values[v] = -1;
    }
  }
  return values;
}

////////////////////////////////////////////
// Get the (zero- or one-element) list of the addmitted values for a category. The value is of inheritence variable type (e.g. $c).
// 'params' = "global" parameters 
// CURRENT_FORMS = all orm geerated up till now
// cv: current category-value pair, e.g. Case==$c
// u: lemma's unit concerned, e.g. vive
// Return a list (terminated by -1) of indices of values admitted for the current category
// Return NULL in case of errors
int* MU_graph_explore_label_get_admitted_values_inherit_var(MultiFlexParameters* params,MU_graph_catval_T* cv, U_form_id_T* u) {
  int* values;  //Admitted values to be returned
  if (!u)
    return NULL;
  //Get the value that the unit has in the lemma of the MWU
  int val;  //Value of the current category in the unit
  val = f_get_value(u->feat,cv->cat);   //e.g. val: fem
  if (val == -1)
    return NULL;

  unichar* var; //Inheritence variable identifier
  var = cv->val.inherit_var;  //get the identifier of the variable, e.g. g1

  values = (int*) malloc(2*sizeof(int));
  if (!values)
    fatal_error("Not enough memory in function  MU_graph_explore_label_get_admitted_values_unif_var\n");
  //The only possible value for the inheritance variable is the one inherited from the unit
  values[0] = val;
  values[1] = -1;
  //If the same variable already instantiated to a DIFFERENT value then no value is admitted
  if (unif_instantiated(params,var) && ((unif_get_cat(params,var)!=cv->cat)
		  || (unif_get_val_index(params,var)!=val)))
    values[0] = -1;
  return values;
}

////////////////////////////////////////////
// Perform an instantiation (if any) of the unification variable contained in the current category-value pair
// It was previously ch
// 'params' = "global" parameters 
// CURRENT_FORMS = all orm geerated up till now
// val = current value to which the possible variable can be instantiated
// cv: current category-value pair, e.g. Case=acc, Case=acc|inst, Case=$1.Case, Case=$c, Case==$c, Case=$c=acc|inst
// Return -1 on error, 0 if no instantiation was performed, 1 if an instantiation was performed
int MU_graph_explore_label_instantiate(MultiFlexParameters* params,int val, MU_graph_catval_T* cv) {
  if (!cv)
    return 1;
  int err;//Error code if any
  unichar* var;  //Unification or inheritance variable's identifier

  switch (cv->type) {
  case unif_var:
    if (!cv->val.unif_var)
      return 1;
    var = cv->val.unif_var->var;
    break;
  case inherit_var:
    var = cv->val.inherit_var;  //get the identifier of the variable, e.g. g1
    break;
  default :
    return 0;
  } 
  if (unif_instantiated(params,var))
    //Error if variable already instantiated to a different category or a different value
    if ( (unif_get_cat(params,var)!=cv->cat) || (unif_get_val_index(params,var)!=val))
      return -1; 
    else  //Variable instantiated to the same category and same value
      return 0;
  else {  //Variable not yet instantiated
    err = unif_instantiate_index(params,var,cv->cat,val);
    if (err)
      return -1;
  }
  return 1;
}

////////////////////////////////////////////
// Perform a desinstantiation (if any) of the unification variable contained in the current category-value pair
// 'params' = "global" parameters 
// CURRENT_FORMS = all orm geerated up till now
// cv: current category-value pair, e.g. Case=acc, Case=acc|inst, Case=$1.Case, Case=$c, Case==$c, Case=$c=acc|inst
// Return 1 in case of errors, 0 otherwise
int  MU_graph_explore_label_desinstantiate(MultiFlexParameters* params,MU_graph_catval_T* cv) {
  if (!cv)
    return 1;

  unichar* var;  //Unification or inheritance variable's identifier
  switch (cv->type) {
  case unif_var:
    if (!cv->val.unif_var)
      return 1;
    var = cv->val.unif_var->var;
    break;
  case inherit_var:
    var = cv->val.inherit_var;  //get the identifier of the variable, e.g. g1
    break;
  default :
    return 0;
  } 
  //Delete the current instantiation
  unif_desinstantiate(params,var);
  return 0;
}

