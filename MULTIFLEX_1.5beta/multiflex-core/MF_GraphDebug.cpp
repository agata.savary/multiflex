/*
  * Unitex 
  *
  * Copyright (C) 2001-2008 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on September 15 2009
 */
//---------------------------------------------------------------------------

/*********************************************************************************/
//// Debugging functionalities for inflection graphs                           ///
/********************************************************************************/

#include <stdlib.h>
#include "MF_GraphDebug.h"
#include "Error.h"

void gp_init_graph_paths(graph_paths_T* paths);
void gp_add_empty_path(graph_paths_T* paths);
int gp_path_cmp(gr_path_T* p1, gr_path_T* p2);
int gp_add_path(graph_paths_T* paths, gr_path_T* new_path);
int gp_merge_paths(graph_paths_T* paths, graph_paths_T* new_paths);
int gp_add_transition_or_state_gr_path(gr_path_T* p, int ts);
int gp_add_transition_or_state(graph_paths_T* paths, int ts);
int gp_duplicate_gr_path(gr_path_T* p1,gr_path_T* p2);
int gp_duplicate_graph_paths(graph_paths_T* p1,graph_paths_T* p2);
unichar* gp_get_str_gr_path(gr_path_T* p);
void gp_print_gr_path(gr_path_T* p);
unichar* gp_get_str_graph_paths(graph_paths_T* paths);
void gp_print_graph_paths(graph_paths_T* paths);
void gp_delete_gr_path(gr_path_T* p);
void gp_delete_graph_paths(graph_paths_T* paths);


////////////////////////////////////////////
// Initialize the list 'paths' with null values
// We suppose that 'paths' has its space allocated
void gp_init_graph_paths(graph_paths_T* paths) {
  if (paths) {
    paths->no_paths = 0;
    paths->paths = NULL;
  }
}

////////////////////////////////////////////
// Add an empty path to the set of paths 
// We suppose that 'path' has its space allocated
void gp_add_empty_path(graph_paths_T* paths) {
  if (paths) {
    paths->paths = (gr_path_T*)realloc(paths->paths, (paths->no_paths+1)*sizeof(gr_path_T));
    if (!paths->paths) {
      fatal_error("Not enough memory in function gp_add_empty_path\n");
    }
    //Initialise the new empty path [0,<>]
    gr_path_T* p;  //New empty path to be added
    p = &(paths->paths[paths->no_paths]);
    p->len = 0;
    p->path = NULL;
    paths->no_paths++;
  }
}

////////////////////////////////////////////
// Compare two paths 'p1' and 'p2'
// Return 0 if they are identical, 1 if they are different
int gp_path_cmp(gr_path_T* p1, gr_path_T* p2) {
  if (!p1 && !p2)
    return 0;
  if (!p1 || !p2)
    return 1;

  //If the lengths are not equal the paths aren't either
  if (p1->len != p2->len)
    return 1;

  //Compare two paths of the same length
  int el; //Current element in each path
  for (el=0; el<p1->len; el++)
    if (p1->path[el] != p2->path[el])
      return 1;

  return 0;
}

////////////////////////////////////////////
// Add a new path 'new_path' to the set of paths 'paths' without repetitions
// We suppose that 'paths' has its space allocated
// Returns 0 on success, 1 otherwise.   
int gp_add_path(graph_paths_T* paths, gr_path_T* new_path) {
  if (!paths)
    return 1;
  if (!new_path)
    return 0;
  
  int p = 0; //Index of the current path in 'paths'
  int found; //Boolean saying if a path identical to 'new_path' was found in 'paths'
  found = 0;
  while (p<paths->no_paths && !found) {
    if (!gp_path_cmp(&(paths->paths[p]), new_path))
      found = 1;
    p++;
  }
  //If 'new_path' already exists in 'paths' do nothing
  if (found)
    return 0;

  //Add 'new_path' to 'paths'
  paths->paths = (gr_path_T*) realloc(paths->paths, (paths->no_paths+1) * sizeof(gr_path_T));
  if (!paths->paths)
    fatal_error("Not enough memory in function 'gp_add_path'!\n");
  if (gp_duplicate_gr_path(&(paths->paths[paths->no_paths]), new_path)) {
    gp_delete_graph_paths(paths);
    return 1;
  }
  paths->no_paths++;
  return 0;
}

////////////////////////////////////////////
// Add the set paths 'new_path' to the set of paths 'paths' without repetitions
// We suppose that 'paths' has its space allocated
// Return 0 on success, 1 otherwise.   
int gp_merge_paths(graph_paths_T* paths, graph_paths_T* new_paths) {
  if (!paths)
    return 1;
  if (!new_paths)  //Nothing to merge
    return 0;
    
  int np; //index of a sigle path in 'new_paths'
  for (np=0; np<new_paths->no_paths; np++) {
    if (gp_add_path(paths, &(new_paths->paths[np])))
      return 1;
  }
  return 0;
}

////////////////////////////////////////////
// Add the transition or state number 'ts' at the beginning of path 'p'
// Return 1 in case of errors, 0 otherwise
int gp_add_transition_or_state_gr_path(gr_path_T* p, int ts) {
  if (!p || ts<0)
    return 1;
  p->path = (int*) realloc(p->path,(p->len+1)*sizeof(int));
  if (!p->path)
    fatal_error("Not enough memory in function 'gp_add_transition_or_state'!\n");
  int el; //Index of the current element in the path
  for (el=p->len-1; 0<=el; el--)
    p->path[el+1] = p->path[el];
  p->path[0] = ts;
  p->len++;
  return 0;
}

////////////////////////////////////////////
// Add the transition or state number 'ts' at the beginning of each path in 'p'
// Return 1 in case of errors, 0 otherwise
int gp_add_transition_or_state(graph_paths_T* paths, int ts) {
  if (!paths || ts <0)
    return 1;
  int p; //Index of the current path
  for (p=0; p<paths->no_paths; p++)
    gp_add_transition_or_state_gr_path(&(paths->paths[p]), ts);
  return 0;
}

////////////////////////////////////////////
// Duplicate the path 'p2' into 'p1'
// 'p1' is allocated but empty
// Returns 0 on success, 1 on error
int gp_duplicate_gr_path(gr_path_T* p1,gr_path_T* p2) {
  if (!p1 || !p2)
    return 1;

  p1->len = p2->len;
  p1->path = (int*) malloc(p2->len * sizeof(int));
  if (!p1->path)
    fatal_error("Not enough memory in function 'gp_duplicate_gr_path'!\n");
  int el; //Current element in p2
  for (el=0; el<p2->len; el++)
    p1->path[el] = p2->path[el];
  return 0;
}

////////////////////////////////////////////
// Duplicate the paths 'p2' into 'p1'
// 'p1' is allocated but empty
// Returns 0 on success, 1 on error
int gp_duplicate_graph_paths(graph_paths_T* p1,graph_paths_T* p2) {
  if (!p1 || !p2)
    return 1;
  p1->paths = (gr_path_T*)malloc(p2->no_paths*sizeof(gr_path_T));
  if (!p1->paths)
    fatal_error("Not enough memory in function 'gp_duplicate_graph_paths'!\n");
  int p; //Current path in p2
  for (p=0; p<p2->no_paths; p++) {
    if (gp_duplicate_gr_path(&(p1->paths[p]),&(p2->paths[p]))) {
      gp_delete_graph_paths(p1);
      return 1;
    }
    p1->no_paths++;
}
  return 0;
}

////////////////////////////////////////////
// Gets a string from a graph path
// The return string is allocated in the function. The liberation has to take place  
// in the calling function.                                                         
unichar* gp_get_str_gr_path(gr_path_T* p) {
  if (!p)
    return NULL;

  unichar* p_str; //String representing the path
  int p_str_len; //Length of the string representing a path
  int el; //Index of the current element in the path
  //Allocate an empty string
  p_str = (unichar*) malloc(sizeof(unichar));
  if (!p_str)
    fatal_error("Not enough memory in function 'gp_get_str_gr_path'!\n");
  p_str[0] = (unichar) '\0';

  //Append the string representing the current element
  for (el=0; el<p->len; el++) {
    char el_str[10000]; //String representing the current element
    unichar el_str_u[10000]; //Unicode string representing the current element
    sprintf(el_str,"%d",p->path[el]);  //Convert the element number to string
    u_strcpy(el_str_u, el_str);
    p_str = (unichar*) realloc(p_str, (u_strlen(p_str)+u_strlen(el_str_u)+2)*sizeof(unichar));
    if (!p_str)
      fatal_error("Not enough memory in function 'gp_get_str_gr_path'!\n");
    u_strcat(p_str, el_str_u);
    //If not the last element, append a space
    if (el<p->len-1) {
      p_str_len = u_strlen(p_str);
      p_str[p_str_len] =(unichar)' ';
      p_str[p_str_len+1] = '\0';
    }
  }
  return p_str;
}

////////////////////////////////////////////
// Prints a path
void gp_print_gr_path(gr_path_T* p) {
  unichar* p_str =  gp_get_str_gr_path(p);
  u_printf("%S",p_str);
  free(p_str);
}

////////////////////////////////////////////
// Gets a string from a set of graph paths. The paths are separated by a ';'.
// The return string is allocated in the function. The liberation has to take place  
// in the calling function.                                                         
unichar* gp_get_str_graph_paths(graph_paths_T* paths){
  if (!paths)
    return NULL;
  
  unichar* paths_str; //String representing the set of paths
  int paths_str_len; //Length of the string representing the paths
  //Allocate an empty string
  paths_str = (unichar*) malloc(sizeof(unichar));
  if (!paths_str)
    fatal_error("Not enough memory in function 'gp_get_str_gr_path'!\n");
  paths_str[0] = (unichar) '\0';
  
  int p; //Index of the current path
  unichar* p_str; //String representing the current path
  for (p=0; p<paths->no_paths; p++) {
    p_str = gp_get_str_gr_path(&(paths->paths[p]));
    paths_str = (unichar*) realloc(paths_str, (u_strlen(paths_str)+u_strlen(p_str)+2)*sizeof(unichar));
    u_strcat(paths_str,p_str);
    free(p_str);
    //If not the last path, append a ';'
    if (p<paths->no_paths-1) {
      paths_str_len = u_strlen(paths_str);
      paths_str[paths_str_len] =(unichar)';';
      paths_str[paths_str_len+1] = '\0';
    }
  }
  return paths_str;
}

////////////////////////////////////////////
// Prints a set of paths
void gp_print_graph_paths(graph_paths_T* paths) {
  unichar* paths_str =  gp_get_str_graph_paths(paths);
  u_printf("%S", paths_str);
  free(paths_str);
}

////////////////////////////////////////////
// Deletes a path, the main structure is not liberated
void gp_delete_gr_path(gr_path_T* p) {
  if (p) {
    free(p->path);
    p->path = NULL;
    p->len = 0;
  }
}

////////////////////////////////////////////
// Deletes a set of paths
// The main structure is not liberated
void gp_delete_graph_paths(graph_paths_T* paths) {
  if (paths) {
    int p; //Index of the current path
    for (p=0; p<paths->no_paths; p++)
      gp_delete_gr_path(&(paths->paths[p]));
    free(paths->paths);
    paths->no_paths = 0;
  }
}
  
