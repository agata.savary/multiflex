/*
  * Unitex 
  *
  * Copyright (C) 2001-2008 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on September 9 2008
 */
//---------------------------------------------------------------------------


#ifndef MUEmbeddedH
#define MUEmbeddedH

#include "Unicode.h"
#include "MF_LangMorpho.h"
#include "MF_UnitMorpho.h"
#include "MF_Global2.h"

/**
 * 
 * This library is used to get the inflection annotation of embedded compounds
 * i.e. being components of compounds to be inflected.
 * 
 */


/////////////////////////////////////////////////
// Looks for the inflectional annotation of the (compound) unit 'lemma'
// in the list of embedded dlc files.
// 'params' = "global" parameters 
// 'lemma_str' = (compound) lemma string
// 'cl' = class of the lemma
// 'homonym' = homonym number of the lemma, starting from 0 (the 'embedded_DLC' files
//             are examined in their order of appearence, the n-th annotation of 'lemma'
//             is returned, where n=homonym)
// 'lemma' = return parameter containing the structure of the lemma given by the annotation
//           *lemma is allocated but empty, **lemma gets allocated by the function (if the right annotation found)
// Return 1 in case of problems, 0 otherwise
int get_MU_annotation(MultiFlexParameters* params,unichar* lemma_str, l_class_T* cl, int homonym, U_lemma_T** lemma);

/////////////////////////////////////////////////
// Initializes the level of embedding to 0
// 'params' = "global" parameters 
void initEmbeddingLevel(MultiFlexParameters* params);

/////////////////////////////////////////////////
// Returns the current level of embedding to 0
// Returns -1 on error
// 'params' = "global" parameters 
int getEmbeddingLevel(MultiFlexParameters* params);

/////////////////////////////////////////////////
// Increments the current level of embedding
// Returns the current level or -1 on error
// 'params' = "global" parameters 
int incrementEmbeddingLevel(MultiFlexParameters* params);

/////////////////////////////////////////////////
// Decrements the current level of embedding
// Returns the current level or -1 on error
// 'params' = "global" parameters 
int decrementEmbeddingLevel(MultiFlexParameters* params);

/////////////////////////////////// 
// Initializes the structure of DELAC files containing
// components of the compounds to be inflected
// 'params' = "global" parameters 
// list = the list of file paths
// no_files = number of these files
// If any problem occurs, returns 1. Otherwise returns 0.
int init_embedded_dlc(MultiFlexParameters* params, char** files, int no_files);

/////////////////////////////////// 
// Liberates the structure of DELAC files containing
// components of the compounds to be inflected
// 'params' = "global" parameters 
void free_embedded_dlc(MultiFlexParameters* params);

#endif
