/*
  * Unitex 
  *
  * Copyright (C) 2001-2008 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on September 3 2008
 */
//---------------------------------------------------------------------------

/********************************************************************************/
//// Register of the Inflected Forms of the Components of the Current Entry    ///
//// Used for optimisation purposes                                             ///
/********************************************************************************/

#ifndef CurrentFormsH
#define CurrentFormsH

#include "MF_UnitMorphoBase.h"


////////////////////////////////////////////
//Initialise the current forms
U_form_list_set_T* init_current_forms();

////////////////////////////////////////////
//Free the current forms
void free_current_forms(U_form_list_set_T* CURRENT_FORMS);

////////////////////////////////////////////
//If the inflected forms of the current lemma appear in the current forms
//put a copy of their list in "forms" and return 0. Otherwise return 1.
//"forms" has its space allocated but is empty
int get_current_form_list(U_form_list_set_T* CURRENT_FORMS,U_lemma_T* lemma, U_forms_T* forms);

////////////////////////////////////////////
//Add a lemma and the list of all its forms to the set of current forms
int add_current_forms(U_form_list_set_T* CURRENT_FORMS,U_lemma_T* lemma,U_forms_T* forms);

 
#endif
