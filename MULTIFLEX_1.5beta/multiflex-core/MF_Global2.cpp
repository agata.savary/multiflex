/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

#include "MF_Global2.h"
#include "MF_MU_embedded.h"
#include "MF_LangMorpho.h"
#include "MF_MU_embedded.h"
#include "Error.h"
#include "Fst2.h"
#include "File.h"

int init_transducer_tree(MultiFlexParameters* params);
void free_transducer_tree(MultiFlexParameters* params);
int get_transducer(MultiFlexParameters* params,char* flex);
struct node* new_node();
struct transition* new_transition(char c);
void free_transition(struct transition* t);
void free_node(struct node* n);
struct transition* get_transition(char c,struct transition* t,struct node** n);
int get_node(MultiFlexParameters* params,char* flex,int pos,struct node* n);


///////////////////////////////
//Initiate the tree for inflection transducers' names
// 'params' = "global" parameters
//On succes return 0, 1 otherwise
int init_transducer_tree(MultiFlexParameters* params) {
  params->n_fst2 = 0;
  params->root=new_node();
  if (!params->root) {
    error((char*)"Tranducer tree could not be initialized in function 'init_transducer_tree'\n");
    return 1;
  }
  return  0;
}

///////////////////////////////
// Free the transducer tree memory
// 'params' = "global" parameters
void free_transducer_tree(MultiFlexParameters* params) {
free_node(params->root);
}

///////////////////////////////
// Try to load the transducer flex and returns its position in the
// 'fst2' array. Returns -1 if the transducer cannot be loaded
// 'params' = "global" parameters
int get_transducer(MultiFlexParameters* params,char* flex) {
return get_node(params,flex,0,params->root);
}

///////////////////////////////
// Create a new node in the tree
struct node* new_node() {
struct node* n=(struct node*)malloc(sizeof(struct node));
n->final=-1;
n->t=NULL;
return n;
}

///////////////////////////////
// Create a new branch in the tree
struct transition* new_transition(char c) {
struct transition* t=(struct transition*)malloc(sizeof(struct transition));
t->c=c;
t->n=NULL;
t->suivant=NULL;
return t;
}

///////////////////////////////
// Free the branch
void free_transition(struct transition* t) {
struct transition* tmp;
while (t!=NULL) {
    free_node(t->n);
    tmp=t;
    t=t->suivant;
    free(tmp);
}
}

///////////////////////////////
// Free a node
void free_node(struct node* n) {
if (n==NULL) {
  error((char*)"NULL error in free_node\n");
  return;
}
free_transition(n->t);
free(n);
}

///////////////////////////////
// Looks for a transition by the char c
// Creates it if it does not exist
struct transition* get_transition(char c,struct transition* t,struct node** n) {
struct transition* tmp;
while (t!=NULL) {
    if (t->c==c) return t;
    t=t->suivant;
}
tmp=new_transition(c);
tmp->suivant=(*n)->t;
tmp->n=NULL;
(*n)->t=tmp;
return tmp;
}

///////////////////////////////
// Look for the path to 'flex', creating it if necessary
// The current node is n, and pos is the position in the flex string
// 'params' = "global" parameters
int get_node(MultiFlexParameters* params,char* flex,int pos,struct node* n) {
if (flex[pos]=='\0') {
    // we are at the final node for flex (a leaf)
    if (n->final!=-1) {
        // if the automaton already exists we returns its position in the transducer array (fst2)
        return n->final;
    }
    else {
        // else we load it
        if (params->n_fst2==N_FST2) {
	  fatal_error((char*)"Memory error: too much inflectional transducers\n");
        }
        char s[FILENAME_MAX];
        new_file(params->inflection_directory,flex,s);
        strcat(s,".fst2");
	//	u_printf("%s\n",s);
        params->fst2[params->n_fst2]=load_fst2(s,1);
        n->final=params->n_fst2;
        return (params->n_fst2)++;
        }
}
// if we are not at the end of the string flex
struct transition* trans=get_transition(flex[pos],n->t,&n);
if (trans->n==NULL) {
    trans->n=new_node();
}
return get_node(params,flex,pos+1,trans->n);
}

//////////////////////////////////////////////
// Initialize the "global" Multiflex parameters
MultiFlexParameters* new_MultiFlexParameters() {
MultiFlexParameters* p=(MultiFlexParameters*)malloc(sizeof(MultiFlexParameters));
init_transducer_tree(p);
p->config_files_status=CONFIG_FILES_OK;
return p;
}

//////////////////////////////////////////////
// Liberate the structure for the "global" Multiflex parameters
// 'params' = "global" parameters
void free_MultiFlexParameters(MultiFlexParameters* params) {
  if (params==NULL) return;
  free_alphabet(params->alph);
  free_language_morpho(params);
  free_embedded_dlc(params);
  if (params->embedded_DLC)
    free_embedded_dlc(params);
  free_transducer_tree(params);
  free(params);
}

