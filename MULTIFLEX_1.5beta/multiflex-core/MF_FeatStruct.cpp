/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

/************************************************************************************/
/* FEATURE-VALUE REPRESENTATION OF THE MORPHOLOGY OF A SIMPLE OR COMPOUND WORD FORM */
/************************************************************************************/

#include "MF_FeatStruct.h"
#include "Error.h"

////////////////////////////////////////////
// Creates the feature structure of a form.
void f_create_feat_struct(f_feat_struct_T **feat_struct) {
  *feat_struct = (f_feat_struct_T*) malloc(sizeof(f_feat_struct_T));
  if (!(*feat_struct))
    fatal_error("Not enough memory in function 'f_create_feat_struct' !\n");
}

////////////////////////////////////////////
// Creates the set of  feature structures.
// Returns 1 in case of error, 0 otherwise
void f_create_feat_struct_set(f_feat_struct_set_T **feat_struct_set) {
  *feat_struct_set = (f_feat_struct_set_T*) malloc(sizeof(f_feat_struct_set_T));
  if (!(*feat_struct_set)) 
    fatal_error("Memory allocation problem in function 'f_create_feat_struct' !\n");
}

////////////////////////////////////////////
// Initializes the feature structure of a form.
void f_init_feat_struct(f_feat_struct_T *feat_struct) {
  if (feat_struct) {
    feat_struct->no_catvals = 0;
    feat_struct->catvals = NULL;
  }
}

////////////////////////////////////////////
// Initializes the set of feature structures so that it contains no element.
void f_init_feat_struct_set(f_feat_struct_set_T *feat_struct_set) {
  if (feat_struct_set) {  
    feat_struct_set->no_feat_structs = 0;
    feat_struct_set->feat_structs = NULL;
  }
}

////////////////////////////////////////////
// Initializes the set of feature structures so that it contains one empty element.
// Returns 0 on success, 1 otherwise
void f_init_feat_struct_set_one_empty(f_feat_struct_set_T *feat_struct_set) {
  f_feat_struct_T* fs;  //A new empty feature structure
  if (feat_struct_set) {  
    f_create_feat_struct(&fs);
    f_init_feat_struct(fs);
    feat_struct_set->feat_structs = fs;
    feat_struct_set->no_feat_structs = 1;
  }
}

////////////////////////////////////////////
// Deletes a feature structure.
// 'feat_struct' is transferred as a double pointer so that
// it can remain liberated after the return from the function
void f_delete_feat_struct(f_feat_struct_T **feat_struct) {
  if (feat_struct && (*feat_struct)) {
    if ((*feat_struct)->catvals) {
      free((*feat_struct)->catvals);
      (*feat_struct)->catvals = NULL;
    }
    free(*feat_struct);
    *feat_struct = NULL;
  }
}

////////////////////////////////////////////
// Deletes a set of feature structures of a form.
// 'feat_struct_set' is transferred as a double pointer so that
// it can remain NULL after the function return
void f_delete_feat_struct_set(f_feat_struct_set_T **feat_struct_set) {
  if (feat_struct_set && (*feat_struct_set)) {
    int fs; //Index of the current feature structure
    for (fs=0; fs<(*feat_struct_set)->no_feat_structs; fs++)
      free((*feat_struct_set)->feat_structs[fs].catvals);  //Delete the cetagory-value pairs of the current feature structure
    free((*feat_struct_set)->feat_structs);
    free(*feat_struct_set);
    *feat_struct_set=NULL;
  }
}

////////////////////////////////////////////
// Checks if two feature structures agree
// fs1, fs2 = the feature structures to be compared
// empty_values (0 or 1) = says if expty values are admitted
// If empty_values=0: returns 1 if fs1 and fs2 contain exactly the same values
// (not necessarily in the same order).
// If empty_values=1: returns 1 if fs1 and fs2 contain exactly the same values
// (not necessarily in the same order) except the empty values and the 'graphical' values.
// In this case if an empty feature is present in one set then the 
// corresponding category in the other set must also be empty.
// e.g. if feat1=<Gen=m;Nb=pl> and feat2=<Gen=m;Nb=pl; Gr=<E>> then both agree
// But if feat1=<Gen=m;Nb=pl; Gr=D> and feat2=<Gen=m;Nb=pl> then they do not agree
// Otherwise returns 0.
int f_feat_struct_agreement(f_feat_struct_T* fs1, f_feat_struct_T* fs2, int empty_values) {
  if (!fs1 && !fs2)
    return 1;
  if (!fs1 || !fs2)
    return 0;
  //  if (fs1->no_catvals != fs2->no_catvals)
  //  return 0;

  int f1; //Index of the current feature in 'fs1'
  int f2; //Index of the current feature in 'fs2'
  int found; //Has the current feature's category been found in 'feat2'

  //Each category-value pair of the 'fs2' has to be present in 'fs1'
  //except (maybe) empty features (<E>)
  for (f2=0; f2<fs2->no_catvals; f2++) {
    found = 0;
    f1 = 0;
    while ((!found) && (f1<fs1->no_catvals)) {
      if (fs2->catvals[f2].cat == fs1->catvals[f1].cat) {
	found = 1;
	//If the same category then the value has to be the same
	if (fs2->catvals[f2].val != fs1->catvals[f1].val)
	  return 0;
      }
      f1++;
    }

    //If empty values are not admitted or if a desired category has a non empty value
    //and it does not appear in 'fs1', and it is not a 'graphical' value
    //then both feature structures do not agree
    if (!found && (!empty_values || !is_empty_val(fs2->catvals[f2].cat,fs2->catvals[f2].val))
	&& fs2->catvals[f2].cat->cat_type != graphical)
      return 0;
  }
  
  //Each category-value pair of the 'fs1' has to be present in 'fs2'
  //except empty features (<E>)
  for (f1=0; f1<fs1->no_catvals; f1++) {
    found = 0;
    f2 = 0;
    while ((!found) && (f2<fs2->no_catvals)) {
      if (fs1->catvals[f1].cat == fs2->catvals[f2].cat) {
	found = 1;
	//If the same category then the value has to be the same
	if (fs1->catvals[f1].val != fs2->catvals[f2].val)
	  return 0;
      }
      f2++;
    }
    
    //If empty values are not admitted or if a desired category has a non empty value
    //and it does not appear in 'fs2' then both feature structures do not agree
    if (!found && (!empty_values || !is_empty_val(fs1->catvals[f1].cat,fs1->catvals[f1].val))
	&& fs1->catvals[f1].cat->cat_type != graphical)
      //If empty values are not admitted then an unfound features means that the structures do not agree
      return 0;
  }
  return 1; //If no disagreement detected previously then fs1 and fs2 agree
}

////////////////////////////////////////////
// Modifies the feature structure of a form.
// Each category-value appearing in 'new_feat', if it is basic or extra, replaces the corresponding category in 'old_feat',
// If the category is graphical, it is added to old_feat
// e.g. if 'old_feat'={Gen=fem, Nb=sing} and 'new_feat'={Nb=pl} then 'old_feat' becomes {Gen=fem, Nb=pl}.
// If a category appearing in 'new_feat' does not appear in old feat, and if this category admits
// an empty value than the category-value pair from 'new_feat' is added to 'old_feat'
// e.g. if Usage admits an empty value, and 'old_feat'={Gen=fem, Nb=sing}, and 'new_feat'={Nb=pl; Usage=offic; Init=dot}
// then 'old_feat' becomes {Gen=fem, Nb=pl; Usage=offic; Init=dot}
// 'params' = "global" parameters
// 'sorted' - boolean indicating if the resulting feature structure is to be sorted (with respect to the order of categories in the language file)
// Returns 0 on success, returns 1 if a category from 'new_feat' does not appear in 'old_feat'.
int f_change_feat_struct(MultiFlexParameters* params,f_feat_struct_T *old_feat, f_feat_struct_T *new_feat, int sorted) {
  int c_old, c_new;  //category indices in old_feat and in new_feat
  int found;
  int err=0; //Error code

  for (c_new=0 ; c_new<new_feat->no_catvals; c_new++) {
    found = 0;
    for (c_old=0; c_old<old_feat->no_catvals && !found; c_old++)
      if (old_feat->catvals[c_old].cat == new_feat->catvals[c_new].cat) {
	old_feat->catvals[c_old].val = new_feat->catvals[c_new].val;
	found = 1;
      }
    if (!found) {
      //If a category from 'new_feat' does not appear in 'old_feat' 
      //maybe it takes implicitely the empty value or it is a graphical value applied after inflection
      if (admits_empty_val(new_feat->catvals[c_new].cat) || new_feat->catvals[c_new].cat->cat_type==graphical)
	err = f_enlarge_feat_struct(params,old_feat, new_feat->catvals[c_new].cat, new_feat->catvals[c_new].val, sorted);
      else 
	//Error if the category from 'new_feat' does not appear in 'old_feat' and does not admit an empty value
	return 1;
  }
  }
  return err;
} 

////////////////////////////////////////////
// Enlarges the feature structure of a form.
// The category-value pair created from 'cat' and 'val' is added to 'feat_struct',
// e.g. if 'feat_struct'={Gen=fem}, 'cat'=Nb, nb=(sing,pl) and 'val'=1 then 'feat_struct' becomes {Gen=fem, Nb=pl}.
// We assume that 'val' is a valid index in the domain of 'cat.
// If 'cat' with value 'val' already appears in 'feat_struct', no change is done to feat_struct
// but the function does not return an error. 
// If 'cat' already appears in 'feat_struct' with a value different from 'val', the function returns an error.
// 'params' = "global" parameters
// 'sorted' - boolean indicating if the resulting feature structure is to be sorted (with respect to the order of categories in the language file)
// Returns 0 on success, returns 1 in case of errors.
int f_enlarge_feat_struct(MultiFlexParameters* params,f_feat_struct_T *feat_struct, l_category_T *cat, int val, int sorted) {
  if (!feat_struct || !cat || val <0 )
    return 1;

  int c;  //category index in feat
  for (c=0; c<feat_struct->no_catvals; c++)
    if (feat_struct->catvals[c].cat == cat) {
      if (feat_struct->catvals[c].val == val) 
        return 0; //If 'cat with value 'val' already in 'feat_struct' do nothing
      else
    	  return 1; //Error if the category to be added already appears in old_cat with a value different from 'val'
    }
  //Reallocate space for the old set plus the new element
  feat_struct->catvals = (f_catval_T*) realloc(feat_struct->catvals, ((feat_struct->no_catvals)+1)*sizeof(f_catval_T));
  if (!(feat_struct->catvals))
    fatal_error("Memory allocation problem in function 'f_enlarge_feat_struct' !\n");

  int new_catval_i; //Index of the new category-value pair
  int new_cat_rang; //Rang of the new category in the language morphology file
  if (!sorted)
    new_catval_i = feat_struct->no_catvals;
  else {
    new_cat_rang = get_cat_rang(params,cat);
    //Shift right all categories whose rang is superior to the rang of the new category
    for (c = (feat_struct->no_catvals)-1; 0<=c && new_cat_rang<get_cat_rang(params,feat_struct->catvals[c].cat); c--)
      feat_struct->catvals[c+1] =  feat_struct->catvals[c];
    new_catval_i = c+1;
  }

  //Add the new category-value pair
  feat_struct->catvals[new_catval_i].cat = cat;  //Add new category-value pair
  feat_struct->catvals[new_catval_i].val = val;  //Add new category-value pair
  feat_struct->no_catvals++;
  return 0;
}

////////////////////////////////////////////
// Enlarges the feature structure of a form on a basis of char data.
// The category-value pair created from 'cat' and 'val' is added to 'feat_struct',
// e.g. if 'feat_struct'={Gen=fem}, 'cat'="Nb" and 'val'="pl" then 'feat_struct' becomes {Gen=fem, Nb=pl}.
// 'params' = "global" parameters
// 'sorted' - boolean indicating if the resulting feature structure is to be sorted (with respect to the order of categories in the language file)
// Returns 0 on success, returns 1 if 'cat' already appears in 'feat_struct'.
// Returns -1 if 'cat' or 'val' invalid category or value names in the current language.
int f_enlarge_feat_struct_unichar(MultiFlexParameters* params,f_feat_struct_T *feat_struct, unichar *cat, unichar* val, int sorted) {
  l_category_T *c;
  int v;  //category index in feat

  c = is_valid_cat(params,cat);    //Checks if 'cat' is a valid category name in the current language
  if (!c) {
    error("Invalid category: %S\n",cat);
    return -1;
  }
  v = is_valid_val(c,val);    //Checks if 'val' is a valid category name in the current language
  if (v == -1) {
    error("Invalid value: %S\n",val);
    return -1;
  }
  return (f_enlarge_feat_struct(params,feat_struct,c,v,sorted));
}

////////////////////////////////////////////
// Enlarges each feature structure in a set of structures.
// The category-value pair created from 'cat' and 'val' is added to each structure in 'feat_struct_set',
// e.g. if 'feat_struct_set'={{Gen=fem},{Gen=masc}}, 'cat'=Nb, nb=(sing,pl) and 'val'=1 then 'feat_struct_set' becomes {{Gen=fem, Nb=pl},{Gen=masc,Nb=pl}}.
// We assume that 'val' is a valid index in the domain of 'cat.
// 'params' = "global" parameters
// 'sorted' - boolean indicating if the resulting feature structure is to be sorted (with respect to the order of categories in the language file)
// Returns 0 on success, returns 1 if 'cat' already appears in 'feat_struct_set'.
int f_enlarge_feat_struct_set(MultiFlexParameters* params,f_feat_struct_set_T *feat_struct_set, l_category_T *cat, int val, int sorted) {
  int fs;  //feature structure index in feat
  int err; //error code

  for (fs=0; fs<feat_struct_set->no_feat_structs; fs++) {
    err = f_enlarge_feat_struct(params,&((feat_struct_set->feat_structs)[fs]), cat, val, sorted);
    if (err)
      return err;
  }
  return 0;
}
/////////////////////////////////////////////////
// Copy an existing feature structure to a new one.
// The new structure must be allocated by the calling function.
// The copies of the category-value pairs are allocated in the function.
// Returns 1 in case of error, 0 otherwise.
int f_copy_feat_struct(f_feat_struct_T *new_feat_struct,f_feat_struct_T* old_feat_struct) {

  if (!new_feat_struct)
    return 1;

  //If no category-values pairs in the old feature structure there is nothing to copy
  if (!old_feat_struct) {
    new_feat_struct->catvals = NULL;
    new_feat_struct->no_catvals = 0;
    return 0;
  }

  if (old_feat_struct->no_catvals == 0)
    new_feat_struct->catvals = NULL;
  else {
    //Allocate the category-value pairs
    new_feat_struct->catvals = (f_catval_T*) malloc(old_feat_struct->no_catvals*sizeof(f_catval_T));
    if (!(new_feat_struct->catvals))
      fatal_error("Memory allocation problem in function 'f_copy_feat_struct' !\n");
  }
  
  //Copy each category-value pair
  int cv; //Index of the current category-value pair
  for (cv=0; cv<old_feat_struct->no_catvals; cv++)
    new_feat_struct->catvals[cv] =  old_feat_struct->catvals[cv];
  
  //Copy the number of category-value pairs
  new_feat_struct->no_catvals = old_feat_struct->no_catvals;
  
  return 0;
}

/////////////////////////////////////////////////
// Duplicates and returns a set of feature structures (the new structure set is allocated
// in the function). 
// Returns NULL in case of error.
f_feat_struct_set_T* f_duplicate_feat_struct_set(f_feat_struct_set_T* old_set) {
  int err; //Error code

  f_feat_struct_set_T *new_set;
  f_create_feat_struct_set(&new_set);
  
  //If no feature structure in the old set there is nothing to copy
  if (old_set->no_feat_structs == 0)
    new_set->feat_structs = NULL;
  else {
    //Allocate space for all feature structures
    new_set->feat_structs = (f_feat_struct_T*) malloc(old_set->no_feat_structs * sizeof(f_feat_struct_T));
    if (! new_set->feat_structs)
      fatal_error("Memory allocation problem in function 'f_duplicate_feat_struct_set' !\n");
  }

  //Copy all feature structures
  for (int fs=0; fs<old_set->no_feat_structs; fs++) {
    err = f_copy_feat_struct(&(new_set->feat_structs[fs]),&(old_set->feat_structs[fs]));
    if (err)
      return NULL;
  }
  new_set->no_feat_structs = old_set->no_feat_structs;

  return new_set;
}

////////////////////////////////////////////
// Merges two sets of feature structures.
// The resulting set is kept in 'feat_struct_set'.
// The merged set 'new_feat' is not deleted.
// Returns 0 on success, 1 otherwise
int f_merge_feat_struct_sets(f_feat_struct_set_T *feat_struct_set, f_feat_struct_set_T *new_feat) {
  int nfs;  //feature structure index in new_feat
  
  //Allocate space for the old and the new feature structures alltogether
  feat_struct_set->feat_structs = (f_feat_struct_T*) realloc(feat_struct_set->feat_structs, (feat_struct_set->no_feat_structs+new_feat->no_feat_structs)*sizeof(f_feat_struct_T));
  if (!(feat_struct_set->feat_structs))
    fatal_error("Memory allocation problem in function 'f_merge_feat_struct_sets' !\n");

  //Copy the new structures behind the old ones
  int nb_old; //Number of the old structures
  nb_old = feat_struct_set->no_feat_structs;
  for (nfs=0; nfs<new_feat->no_feat_structs; nfs++)
    if (f_copy_feat_struct(&(feat_struct_set->feat_structs[nb_old+nfs]),&(new_feat->feat_structs[nfs])))
      return 1;
  feat_struct_set->no_feat_structs = nb_old + new_feat->no_feat_structs;

  return 0;
}

////////////////////////////////////////////
// Reduces the feature structure of a form.
// Category-value corresponding to 'cat' is deleted from 'feat_struct',
// e.g. if 'feat_struct'={Gen=fem,Nb=pl} and 'cat'=Gen then 'feat_struct' becomes {Nb=pl}
// Returns 0 on success, returns 1 if 'cat' not found in 'feat_struct'.
int f_del_one_catval(f_feat_struct_T *feat_struct, l_category_T* cat) {
  int c;  //category index in feat

  for (c=0; c<feat_struct->no_catvals; c++)
    if (feat_struct->catvals[c].cat == cat) {   //Category 'cat' found in 'feat'.
      while (c < feat_struct->no_catvals - 1) {  //Shift the remaining cat-val pairs to the left.
	feat_struct->catvals[c] = feat_struct->catvals[c+1];
	c++;
      }
      feat_struct->no_catvals--;
      return 0;
    }
  return 1;  //Error if category 'cat' not found in 'old_feat'.
}

////////////////////////////////////////////
// Reads the feature structure of a form.
// Returns the value of the category 'cat' in 'feat_struct' (i.e. the index of the value in the domain of 'cat'),
// e.g. if 'feat_struct'={Gen=neu,Nb=pl}, 'cat'=Gen, and Gen={masc,fem,neu} then return 2.
// Returns -1 if 'cat' not found in 'feat_struct'.
int f_get_value(f_feat_struct_T *feat_struct, l_category_T* cat) {
  int c; //category index in feat
  if (!feat_struct || !cat)
    return -1;
  for (c=0; c<feat_struct->no_catvals; c++)
    if (feat_struct->catvals[c].cat == cat)    //Category 'cat' found in 'feat'.
      return feat_struct->catvals[c].val;
  return -1;
}

/////////////////////////////////////////////////
// Prints the contents of a form's feature structure.
// Returns 0.
int f_print_feat_struct(f_feat_struct_T *feat_struct) {
  int c; //category index in feat
  int i; //index of the current value in the domain of the current category
  u_printf("{");
  for (c=0; c<feat_struct->no_catvals; c++) {
    u_printf("%S",feat_struct->catvals[c].cat->name);    //Print the category
    u_printf("=");
    i = feat_struct->catvals[c].val;
    u_printf("%S",feat_struct->catvals[c].cat->values[i]);    //Print the value
    if (c<feat_struct->no_catvals-1) 
      u_printf(";");
  }
  u_printf("}\n");
  return 0;

}

