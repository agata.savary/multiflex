/*
  * Unitex 
  *
  * Copyright (C) 2001-2007 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (savary@univ-tours.fr)
 * Last modification on June 2232005
 */
//---------------------------------------------------------------------------

/****************************************************************************************************/
/* TREATMENT OF EQUIVALENCES BETWEEN FEATURES FOR SIMPLE WORDS (E.G. "bier" or "B" or "acc")        */
/* AND THOSE DEEFINED IN MUTIFLEX (E.G. acc)                                                        */
/* IF BOTH TYPES OF FEATURES ARE IDENTICAL, THE EQUIVALENCE IS STRAIGHTFORWARD                      */
/* E.G. acc <=> Case=3                                                                              */
/****************************************************************************************************/

#include <stdio.h>
#include "MF_MorphoEquiv.h"
#include "MF_LangMorpho.h"
#include "MF_Util.h"
#include "Unicode.h"
#include "Error.h"

///////////////////////////////////////
//All functions defined in this file
int read_equiv_line(MultiFlexParameters* params,unichar* line,int line_no);
void init_morpho_equiv_straight(MultiFlexParameters* params);

/**************************************************************************************/
/* Initialises the set of equivalences between morphological features for simple words*/
/* and those in Multiflex.                                                            */
/* For instance in Unitex the features for simple words are represented by single     */
/* characters e.g. 's', with no precision of the relevant category. In the compound   */
/* inflection the features may be strings, e.g. "sing", and have to refer to a        */
/* category, e.g. Nb                                                                  */
/* 'equiv_file' is a file describing these equivalences for a given language          */
/* Each line of the file is of the form:                                              */
/*      <df>:<cat>=<val>                                                              */
/* meaning that in the morphological dictionaries of the given language the feature   */
/* 'df' corresponds to category 'cat' taking value 'val'. Each 'cat' and 'val' has to */
/* has to appear in the 'Morphology' file of the given language.                      */  
/* E.g. for Polish:                                                                   */
/*                    Polish                                                          */
/*                    s:Nb=sing                                                       */
/*                    p:Nb=pl                                                         */
/*                    N:Case=Nom                                                      */
/*                    G:Case=Gen                                                      */
/*                    D:Case=Dat                                                      */
/*                    A:Case=Acc                                                      */
/*                    I:Case=Inst                                                     */
/*                    L:Case=Loc                                                      */
/*                    V:Case=Voc                                                      */
/*                    o:Gen=masc_pers                                                 */
/*                    z:Gen=masc_anim                                                 */
/*                    r:Gen=masc_inanim                                               */
/*                    f:Gen=fem                                                       */
/*                    n:Gen=neu                                                       */
/* The function fills out L_MORPHO_EQUIV.                                             */
/* If no equivalence file or no directory indicated then the equivalences are         */
/* straightforward, i.e. resulting from the 'Morphology' file.                        */
/* 'params' = "global" parameters                                                     */
/* Returns 0 on success, 1 otherwise.                                                 */
int init_morpho_equiv(MultiFlexParameters* params,char* equiv_file) {

  //If no equivalence file or no directory indicated then the equivalences are
  //straightforward, i.e. resulting from the 'Morphology' file
  if (!equiv_file || !strlen(equiv_file) ) {
    init_morpho_equiv_straight(params);
    return 0;
  }

  FILE* ef; //equivalence file
  int line_no;  //number of the current line
  unichar line[MAX_EQUIV_LINE];  //current line of the Equivalence file

  //Opening the equivalence file
  if ( !(ef = u_fopen(equiv_file, "r")))  {
    error("Unable to open equivalence file %s\n",equiv_file);
    error("Equivalences between dictionary and Multiflex features are considered as straightforward.\n");
    init_morpho_equiv_straight(params);
    return 0;
  }
  
  //Initialize L_MORPHO_EQUIV
  params->L_MORPHO_EQUIV.no_equiv = 0;
  
  line_no = 0;

  //Omit the first line (language name)
  if (u_fgets(line,MAX_EQUIV_LINE-1,ef)!=EOF) {
    line_no++;
  } else {
   return 1;
  }

  while (u_fgets(line,MAX_EQUIV_LINE-1,ef)!=EOF) {
    line_no++;
    int l=u_strlen(line);
    if (l>0 && line[l-1]=='\n') {
       /* If necessary, we remove the final \n */
       line[l-1]='\0';
    }
    if (line[0]!='\0' && read_equiv_line(params,line,line_no)) return 1;
  }
  return 0;
}

/**************************************************************************************/
/* Read a line of the equivalence file, e.g. s:Nb=sing                                */
/* Fill out a line of L_MORPHO_EQUIV.                                                 */
/* 'params' = "global" parameters                                                     */
/* Return 0 on success, 1 otherwise.                                                  */
int read_equiv_line(MultiFlexParameters* params,unichar* line, int line_no) {
int l;   //Length of a scanned sequence
  unichar* line_pos;   //current position in the input line
  unichar tmp[MAX_MORPHO_NAME];  //buffer for line elements
  unichar tmp_void[MAX_MORPHO_NAME];  //buffer for void characters
  l_category_T* cat;
  
  line_pos = line;

  line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters

  //Read the dictionary value
  l = u_scan_until_char(tmp,line_pos,MAX_MORPHO_NAME-1,": \t",1);
  line_pos = line_pos + l;
  line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters

  if (line_pos[0] != (unichar) ':') {
    error("Bad format in \'Equivalence\' file:\n");
    error("Line %d: a \':\' missing.\n",line_no);
    return 1;
  }
  // if (u_strlen(tmp) != 1) {
  //  error("Bad format in \'Equivalence\' file:\n");
  //  error("Line %d: s morphological values in a dictionary must be of one caracter.\n", line_no);
  //  return 1;
  //}
  u_strcpy(params->L_MORPHO_EQUIV.equiv[params->L_MORPHO_EQUIV.no_equiv].SU_feat,tmp);
  line_pos++;  //Skip the ':'

  //Read the category
  line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
  l = u_scan_until_char(tmp,line_pos,MAX_MORPHO_NAME-1,"= \t",1);
  line_pos = line_pos + l;
  line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
  if (!u_strlen(tmp)) {
    error("Bad format in \'Equivalence\' file:\n");
    error("Line %d: category missing.\n",line_no);
    return 1;
  }
  if (line_pos[0] != (unichar) '=') {
    error("Bad format in \'Equivalence\' file:\n");
    error("Line %d: a \'=\' missing.\n",line_no);
    return 1;
  }
  if (!(cat = is_valid_cat(params,tmp))) {
    error("In \'Equivalence\' file:\n");
    error("%S is not a valid category in line %d.\n",tmp,line_no);
    return 1;    
  };
  params->L_MORPHO_EQUIV.equiv[params->L_MORPHO_EQUIV.no_equiv].MU_catval.cat = cat;
  line_pos++;  //Skip the '='

  //Read the value
  line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
  l = u_scan_until_char(tmp,line_pos,MAX_MORPHO_NAME-1," \t",1);
  line_pos = line_pos + l;
  line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
  if (!u_strlen(tmp)) {
    error("Bad format in \'Equivalence\' file\n");
    error("Line %d: value missing.\n",line_no);
    return 1;
  }
  if ((params->L_MORPHO_EQUIV.equiv[params->L_MORPHO_EQUIV.no_equiv].MU_catval.val = is_valid_val(cat,tmp)) == -1) {
    error("In \'Equivalence\' file\n");
    error("%S is not a valid value in line %d.\n",tmp,line_no);
    return 1;    
  }
  if (line_pos[0]) {
    error("Bad format in \'Equivalence\' file\n");
    error("Line %d: unnecessary string:%S.:\n",line_no,line_pos);
    return 1;
  }

  params->L_MORPHO_EQUIV.no_equiv++;
  return 0; 
}

/**************************************************************************************/
/* Initialises the set of equivalences between morphological and dictionary features  */
/* in a straightforward way,  i.e. such as resulting from the 'Morphology' file.      */
/* E.g. if the 'Morphology' file contains Case: nom, gen, dat, acc, inst, loc, voc    */
/* then 'acc' is equivalent to Case=3 (values are indexed from 0).                    */
/* 'params' = "global" parameters                                                     */
void init_morpho_equiv_straight(MultiFlexParameters* params) {
  int cat; //index of the current category in L_CATS
  int val; //index of the current value in category 'cat'
  int e; //index of the current equivalence

  for (cat=0; cat<params->L_CATS.no_cats; cat++)
    for (val=0; val<params->L_CATS.cats[cat].no_values; val++) {
   
      //Get the pointer to the current category
      l_category_T* cat_ptr;
      cat_ptr = &(params->L_CATS.cats[cat]);

      //Get the pointer to the string representing the current value
      unichar* val_ptr;
      val_ptr = params->L_CATS.cats[cat].values[val];
      
      //Get the index of the current equivalence
      e = params->L_MORPHO_EQUIV.no_equiv;

      //Add the current value of the current category to the equivalence structure L_MORPHO_EQUIV
      u_strcpy(params->L_MORPHO_EQUIV.equiv[e].SU_feat,val_ptr);
      params->L_MORPHO_EQUIV.equiv[e].MU_catval.cat = cat_ptr;
      params->L_MORPHO_EQUIV.equiv[e].MU_catval.val = val;
      params->L_MORPHO_EQUIV.no_equiv++;
    }
}

/**************************************************************************************/
/* Prints to the standard output the equivalences between dictionary and morphology   */
/* features.                                                                          */
/* 'params' = "global" parameters                                                     */
void print_morpho_equiv(MultiFlexParameters* params) {
int e;  //index of the current equivalence
unichar tmp[MAX_MORPHO_NAME+1];
for (e=0; e<params->L_MORPHO_EQUIV.no_equiv; e++) {
  u_fprintf(stderr,"%S:",params->L_MORPHO_EQUIV.equiv[e].SU_feat);
  copy_cat_str(tmp,params->L_MORPHO_EQUIV.equiv[e].MU_catval.cat);
  u_fprintf(stderr,"%S=",tmp);
  copy_val_str(tmp,params->L_MORPHO_EQUIV.equiv[e].MU_catval.cat,params->L_MORPHO_EQUIV.equiv[e].MU_catval.val);
  u_fprintf(stderr,"%S\n",tmp);
}
}


/**************************************************************************************/
/* Initialises the set of equivalences between class names in a dictionary (e.g. "N") */
/* and language classes (e.g. noun)                                                   */
/* This function is temporarily done for Polish. In future it has to be replaced by   */
/* a function scanning an external equivalence file for the given language.           */
/* 'params' = "global" parameters                                                     */
void init_class_equiv(MultiFlexParameters* params) {
  //Noun
  u_strcpy(params->L_CLASS_EQUIV.equiv[0].SU_class,"N");
  params->L_CLASS_EQUIV.equiv[0].MU_class = &(params->L_CLASSES.classes[0]);
  u_strcpy(params->L_CLASS_EQUIV.equiv[1].SU_class,"NC");
  params->L_CLASS_EQUIV.equiv[1].MU_class = &(params->L_CLASSES.classes[0]);

  //Adjectif
  u_strcpy(params->L_CLASS_EQUIV.equiv[2].SU_class,"A");
  params->L_CLASS_EQUIV.equiv[2].MU_class = &(params->L_CLASSES.classes[1]);
  u_strcpy(params->L_CLASS_EQUIV.equiv[3].SU_class,"AC");
  params->L_CLASS_EQUIV.equiv[3].MU_class = &(params->L_CLASSES.classes[1]);

  //Verb
  u_strcpy(params->L_CLASS_EQUIV.equiv[6].SU_class,"V");
  params->L_CLASS_EQUIV.equiv[6].MU_class = &(params->L_CLASSES.classes[2]);
  u_strcpy(params->L_CLASS_EQUIV.equiv[7].SU_class,"VC");
  params->L_CLASS_EQUIV.equiv[7].MU_class = &(params->L_CLASSES.classes[2]);

  //Adverb
  u_strcpy(params->L_CLASS_EQUIV.equiv[4].SU_class,"ADV");
  params->L_CLASS_EQUIV.equiv[4].MU_class = &(params->L_CLASSES.classes[3]);
  u_strcpy(params->L_CLASS_EQUIV.equiv[5].SU_class,"ADVC");
  params->L_CLASS_EQUIV.equiv[5].MU_class = &(params->L_CLASSES.classes[3]);

  params->L_CLASS_EQUIV.no_equiv = 8;
} 

/**************************************************************************************/
/* Gives the SU dictionary class equivalent for a MU class name, e.g. nouns ---> N    */ 
/* THe resulting string is not allocated in the fuction, it should not be deallocated */
/* before the final space liberation.                                                 */
/* If MU_class is NULL or does not exist in the equivalence list, returns '\0'        */
/* 'params' = "global" parameters                                                     */
unichar* get_SU_class(MultiFlexParameters* params,l_class_T* MU_class) {
  if (!MU_class)
    return NULL;

  int eq; //Index of the current equivalence
  for (eq=0; eq<params->L_CLASS_EQUIV.no_equiv; eq++)
    if (params->L_CLASS_EQUIV.equiv[eq].MU_class == MU_class)
      return params->L_CLASS_EQUIV.equiv[eq].SU_class;
  
  //If no equivalence found
  return NULL;
}
