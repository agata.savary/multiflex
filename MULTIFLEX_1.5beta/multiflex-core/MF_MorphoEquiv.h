/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

/****************************************************************************************************/
/* TREATMENT OF EQUIVALENCES BETWEEN FEATURES FOR SIMPLE WORDS (E.G. "bier" or "B" or "acc")        */
/* AND THOSE DEEFINED IN MUTIFLEX (E.G. acc)                                                        */
/* IF BOTH TYPES OF FEATURES ARE IDENTICAL, THE EQUIVALENCE IS STRAIGHTFORWARD                      */
/* E.G. acc <=> Case=3                                                                              */
/****************************************************************************************************/

#ifndef MorphoEquivH
#define MorphoEquivH

#include "Unicode.h"
#include "MF_LangMorpho.h"
#include "MF_UnitMorpho.h"
#include "MF_Global2.h"
#include "MF_MorphoEquivBase.h"


/**************************************************************************************/
/* Initialises the set of equivalences between morphological features for simple words*/
/* and those in Multiflex.                                                           */
/* For instance in Unitex the features for simple words are represented by single     */
/* characters e.g. 's', with no precision of the relevant category. In the compound   */
/* inflection the features may be strings, e.g. "sing", and have to refer to a        */
/* category, e.g. Nb                                                                  */
/* 'equiv_file' is a file describing these equivalences for a given language          */
/* Each line of the file is of the form:                                              */
/*      <df>:<cat>=<val>                                                              */
/* meaning that in the morphological dictionaries of the given language the feature   */
/* 'df' corresponds to category 'cat' taking value 'val'. Each 'cat' and 'val' has to */
/* has to appear in the 'Morphology' file of the given language.                      */  
/* E.g. for Polish:                                                                   */
/*                    Polish                                                          */
/*                    s:Nb=sing                                                       */
/*                    p:Nb=pl                                                         */
/*                    N:Case=Nom                                                      */
/*                    G:Case=Gen                                                      */
/*                    D:Case=Dat                                                      */
/*                    A:Case=Acc                                                      */
/*                    I:Case=Inst                                                     */
/*                    L:Case=Loc                                                      */
/*                    V:Case=Voc                                                      */
/*                    o:Gen=masc_pers                                                 */
/*                    z:Gen=masc_anim                                                 */
/*                    r:Gen=masc_inanim                                               */
/*                    f:Gen=fem                                                       */
/*                    n:Gen=neu                                                       */
/* The function fills out L_MORPHO_EQUIV.                                             */
/* If no equivalence file or no directory indicated then the equivalences are         */
/* straightforward, i.e. resulting from the 'Morphology' file.                        */
/* 'params' = "global" parameters                                                     */
/* Returns 0 on success, 1 otherwise.                                                 */
int init_morpho_equiv(MultiFlexParameters* params,char* equiv_file);

/**************************************************************************************/
/* Initialises the set of equivalences between class names for simple words (e.g. "N")*/
/* and language classes in Multiflex (e.g. noun)                                      */
/* This function is temporarily done for Polish. In future it has to be replaced by   */
/* a function scanning an external equivalence file for the given language.           */
/* 'params' = "global" parameters                                                     */
void init_class_equiv(MultiFlexParameters* params);

/**************************************************************************************/
/* Prints to the standard output the equivalences between dictionary and morphology   */
/* features.                                                                          */
/* 'params' = "global" parameters                                                     */
void print_morpho_equiv(MultiFlexParameters* params);

/**************************************************************************************/
/* Gives the SU dictionary class equivalent for a MU class name, e.g. nouns ---> N    */ 
/* The resulting string is not allocated in the fuction, it should not be deallocated */
/* before the final space liberation.                                                 */
/* If MU_class is NULL or does not exist in the equivalence list, returns '\0'        */
/* 'params' = "global" parameters                                                     */
unichar* get_SU_class(MultiFlexParameters* params,l_class_T* MU_class);

#endif
