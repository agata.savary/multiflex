/*
  * Unitex 
  *
  * Copyright (C) 2001-2008 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on September 15 2009
 */
//---------------------------------------------------------------------------

/********************************************************************************/
//// Handling extra information attached to an inflected form                  ///
/********************************************************************************/

#ifndef MU_FormExtrasH
#define MU_FormExtrasH

#include "MF_GraphDebug.h"
#include "Unicode.h"

///////////////////////////////////////
// Extra information attached to an inflected form
typedef graph_paths_T form_extras_T;

///////////////////////////////////////
// Elementary unit of the extra information
typedef int extras_elem_T;

////////////////////////////////////////////
// Allocate a space for an empty 'extras' structure
// And initialize it to an empty set
void fe_create_extras(form_extras_T** extras);

////////////////////////////////////////////
// Initialize the 'extras' with null values
// We suppose that 'extras' has its space allocated
void fe_init_extras(form_extras_T* extras);

////////////////////////////////////////////
// Initialize the 'extras' with one path containing the element 'e'
// We suppose that 'extras' has its space allocated
void fe_init_extras(form_extras_T* extras, extras_elem_T* e);

////////////////////////////////////////////
// Add a new path 'new_path' to the set of paths 'paths' without repetitions
// We suppose that 'paths' has its space allocated
// Returns 0 on success, 1 otherwise.   
int fe_merge_extras(form_extras_T* extras1, form_extras_T* extras2);

////////////////////////////////////////////
// Enlarges the extras by a new element
// Returns 0 on success, 1 on error
int fe_enlarge_extras(form_extras_T* extras, extras_elem_T* new_elem);

////////////////////////////////////////////
// Duplicate the extras 'e2' into 'e1'
// 'p1e is allocated but empty
// Returns 0 on success, 1 on error
int fe_duplicate_extras(form_extras_T* e1,form_extras_T* e2);

////////////////////////////////////////////
// Gets a string from the extras. 
// The return string is allocated in the function. The liberation has to take place  
// in the calling function.                                                         
unichar* fe_get_str_extras(form_extras_T* extras);

////////////////////////////////////////////
// Prints a set of paths
void fe_print_extras(form_extras_T* extras);

////////////////////////////////////////////
// Deletes the 'extras'
// The main structure is not liberated
void fe_delete_extras(form_extras_T* extras);

#endif
