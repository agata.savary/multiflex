/*
  * Unitex 
  *
  * Copyright (C) 2001-2007 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (savary@univ-tours.fr)
 * Last modification on June 23 2005
 */
//---------------------------------------------------------------------------

////////////////////////////////////////////////////////////
// Implementation of unification variables
////////////////////////////////////////////////////////////

#include <stdio.h>
#include "MF_Unif.h"
#include "MF_MU_embedded.h"
#include "Error.h"
#include "MF_UnifBase.h"



////////////////////////////////////////////
// Initializes the set of instantiations. 
// 'params' = "global" parameters 
// Returns 1 on error, 0 otherwise
int unif_init_vars(MultiFlexParameters* params) {
  int l; //Current level of embedding
  l = getEmbeddingLevel(params);
  if (l<0 || MAX_EMBED_LEVEL<l)
    return 1;
  params->UNIF_VARS[l].no_vars = 0;
  return 0;
}

////////////////////////////////////////////
// Prints the set of instantiations. 
// 'params' = "global" parameters 
// Returns 1 on error, 0 otherwise
int unif_print_vars(MultiFlexParameters* params) {
  int v;
  int i;
  int l; //Current level of embedding
  l = getEmbeddingLevel(params);
  if (l<0 || MAX_EMBED_LEVEL<l)
    return 1;
  u_printf("UNIFICATION VARIABLES OF THE CURRENT EMBEDDING LEVEL:\n");
  for (v=0;v<params->UNIF_VARS[l].no_vars;v++) {
    u_printf("%S:%S=",params->UNIF_VARS[l].vars[v].id,params->UNIF_VARS[l].vars[v].cat->name);
    i=params->UNIF_VARS[l].vars[v].val;
    u_printf("%S\n",params->UNIF_VARS[l].vars[v].cat->values[i]);
  }
  return 0;
}

//////////////////////////////////////////////////////////////////////////////////
// If variable "var" already instantiated, returns -1. Otherwise,
// instantiates the unification variable "var" to category "cat" and value "val". 
// 'params' = "global" parameters 
// Returns 1 or -1 in case of error, 0 otherwise.
int unif_instantiate(MultiFlexParameters* params,unichar* var, l_category_T* cat, unichar* val) {
  int i;
  int v;
  int l; //Current level of embedding  
  l = getEmbeddingLevel(params);
  if (l<0 || MAX_EMBED_LEVEL<l)
    return 1;
  //Check if not yet instantiated.
  if (unif_instantiated(params,var))
      return -1;
 
  i = params->UNIF_VARS[l].no_vars;

  //Category
  params->UNIF_VARS[l].vars[i].cat = cat;

  //value
  v = is_valid_val(cat,val);
  if (v == -1) {
    error("Instantiation impossible: %S is an invalid value in category %S.\n",val,cat->name);
    return 1;    
  }
  params->UNIF_VARS[l].vars[i].val = v;

  //id
  if (!(params->UNIF_VARS[l].vars[i].id = (unichar*) malloc((u_strlen(var)+1)*sizeof(unichar)))) {
     fatal_error("Not enough memory in function unif_instantiate\n");
  }
  u_strcpy(params->UNIF_VARS[l].vars[i].id,var);

  params->UNIF_VARS[l].no_vars++;
  return 0;
}

//////////////////////////////////////////////////////////////////////////////////
// If variable "var" already instantiated, returns -1. Otherwise,
// instantiates the unification variable "var" to category "cat" and value 
// whose index in the domain of "cat" is "val". 
// 'params' = "global" parameters 
// Returns 1 or -1 in case of error, 0 otherwise.
int unif_instantiate_index(MultiFlexParameters* params,unichar* var, l_category_T* cat, int val) {
  //Check if not yet instantiated.
  if (unif_instantiated(params,var))
      return -1;
  
  //Check if the value is in the domain of "cat"
  if (cat->no_values <= val)
    return -1;
 
  int l; //Current level of embedding  
  l = getEmbeddingLevel(params);
  if (l<0 || MAX_EMBED_LEVEL<l)
    return 1;

  int i = params->UNIF_VARS[l].no_vars;
  //Category
  params->UNIF_VARS[l].vars[i].cat = cat;
  //Value
  params->UNIF_VARS[l].vars[i].val = val;
  //Variable's id
  if (!(params->UNIF_VARS[l].vars[i].id = (unichar*) malloc((u_strlen(var)+1)*sizeof(unichar)))) {
     fatal_error("Not enough memory in function unif_instantiate_index\n");
  }
  u_strcpy(params->UNIF_VARS[l].vars[i].id,var);

  params->UNIF_VARS[l].no_vars++;
  return 0;

}

//////////////////////////////////////////////////////////////////////////////////
// Desinstantiates the unification variable "var". 
// 'params' = "global" parameters 
// Returns 1 in case of error, 0 otherwise.
int unif_desinstantiate(MultiFlexParameters* params,unichar* var) {
  int v, w, found;
  int l; //Current level of embedding  
  l = getEmbeddingLevel(params);
  if (l<0 || MAX_EMBED_LEVEL<l)
    return 1;

  found = 0;
  for (v=0; v<params->UNIF_VARS[l].no_vars && (!found); v++)
    if (!u_strcmp(var,params->UNIF_VARS[l].vars[v].id)) {
      found = 1;
      free(params->UNIF_VARS[l].vars[v].id);
      }
  if (found) { //v points to the variable following the one we want to eliminate
    for (w=v; w<params->UNIF_VARS[l].no_vars;w++)
    	params->UNIF_VARS[l].vars[w-1] = params->UNIF_VARS[l].vars[w];
    params->UNIF_VARS[l].no_vars--;
  }
  return 0;
}

//////////////////////////////////////////////////////////////////////////////////
// Returns 1 if the unification variable "var" is instantiated, -1 in case of errors, 0 otherwise.
// 'params' = "global" parameters 
int unif_instantiated(MultiFlexParameters* params,unichar* var) {
  int v;
  int l; //Current level of embedding  
  l = getEmbeddingLevel(params);
  if (l<0 || MAX_EMBED_LEVEL<l)
    return -1;
  for (v=0; v<params->UNIF_VARS[l].no_vars; v++)
    if (!u_strcmp(var,params->UNIF_VARS[l].vars[v].id))
      return 1;
  return 0;
  }

//////////////////////////////////////////////////////////////////////////////////
// If the unification variable "var" is instantiated returns its index 
// in the domain of its category otherwise returns -1.
// 'params' = "global" parameters 
int unif_get_val_index(MultiFlexParameters* params,unichar* var) {
  int v;
  int l; //Current level of embedding  
  l = getEmbeddingLevel(params);
  if (l<0 || MAX_EMBED_LEVEL<l)
    return -1;
  for (v=0; v<params->UNIF_VARS[l].no_vars; v++)
    if (!u_strcmp(var,params->UNIF_VARS[l].vars[v].id))
      return params->UNIF_VARS[l].vars[v].val;
  return -1;
  }

//////////////////////////////////////////////////////////////////////////////////
// If the unification variable "var" is instantiated returns its value, 
// otherwise returns NULL.
// 'params' = "global" parameters 
unichar* unif_get_val(MultiFlexParameters* params,unichar* var) {
  int v,i;
  int l; //Current level of embedding  
  l = getEmbeddingLevel(params);
  if (l<0 || MAX_EMBED_LEVEL<l)
    return NULL;
  for (v=0; v<params->UNIF_VARS[l].no_vars; v++)
    if (!u_strcmp(var,params->UNIF_VARS[l].vars[v].id)) {
      i = params->UNIF_VARS[l].vars[v].val;
      return params->UNIF_VARS[l].vars[v].cat->values[i];
  }
  return NULL;
  }

//////////////////////////////////////////////////////////////////////////////////
// If the unification variable "var" is instantiated returns its category, 
// otherwise returns NULL.
// 'params' = "global" parameters 
l_category_T* unif_get_cat(MultiFlexParameters* params,unichar* var) {
  int v;
  int l; //Current level of embedding  
  l = getEmbeddingLevel(params);
  if (l<0 || MAX_EMBED_LEVEL<l)
    return NULL;
  for (v=0; v<params->UNIF_VARS[l].no_vars; v++)
    if (!u_strcmp(var,params->UNIF_VARS[l].vars[v].id))
      return params->UNIF_VARS[l].vars[v].cat;
  return NULL;
}

//////////////////////////////////////////////////////////////
// Liberates the space allocated for the set of instantiations
// of the current embedding level
// 'params' = "global" parameters 
// Returns 1 in case of erros, 0 otherwise
int unif_free_vars(MultiFlexParameters* params) {
  int v;
  int l; //Current level of embedding  
  l = getEmbeddingLevel(params);
  if (l<0 || MAX_EMBED_LEVEL<l)
    return 1;
  for (v=0; v<params->UNIF_VARS[l].no_vars; v++)
    if (params->UNIF_VARS[l].vars[v].id)
      free(params->UNIF_VARS[l].vars[v].id);
  params->UNIF_VARS[l].no_vars = 0;
  return 0;
}
