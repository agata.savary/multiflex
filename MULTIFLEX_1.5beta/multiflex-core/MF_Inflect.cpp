/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

/********************************************************************************/
/* MAIN MODULE FOR RUNNING THE DELAC-INTO-DELACF INFLECTION                     */
/********************************************************************************/


#include <stdio.h>
#include <string.h>
#include "IOBuffer.h"  //Required by Unitex "golden rules"
#include "Alphabet.h"
#include "Error.h"
#include "File.h"
#include "MF_Copyright.h"
#include "MF_DLC_inflect.h"
#include "MF_LangMorpho.h"
#include "MF_MorphoEquiv.h"
#include "MF_MU_graph.h"
#include "MF_LetterCase.h"
#include "MF_MU_embedded.h"
#include "MF_Convert.h"

//Error code indicating loading status of configuration files 'Morphology' and 'Equivalences' if any.
//extern int config_files_status=CONFIG_FILES_OK;

void usage();
int init_morphology(char* dir);
int init_alphabet(char* alph_file);
int init_equivalences(char* dir);
int init_transducers(char* dir);
int init_embedded_dlc(char** files, int no_files);


/////////////////////////////////// 
// Inflection of a DELAC to a DELACF
// argv[1] = path and name of the DELAC file
// argv[2] = path and name of the DELACF file
// argv[3] = path and name of the alphabet file
// argv[4] = path and name of the inflection directory 
//           containing the 'Morphology' file and the inflection transducers
//           for simple and compound words
// argv[5], 
// argv[6], 
// etc.    = 0 or more DELAC files containing compounds that can be constituents
//           of those to be inflected (in argv[1]), these files are not
//           inflected themselves
// If any problem occurs, returns 1. Otherwise returns 0.
int main(int argc, char* argv[]) {
  int err;  //0 if a function completes with no error 
  
  setBufferMode();  //Required by Unitex "golden rules"
  
  if (argc < 5) {
    usage();
    return 0;
  }

  //Load the morphology description, the alphabet, the equivalence files, 
  //the structure for inflection transducers, a
  //and the list of DELAC files containing embedded compounds
  if (init_morphology(argv[4]))
      return 1;
  //  print_language_morpho();

  if (init_alphabet(argv[3]))
      return 1;
  if (init_equivalences(argv[4]))
      return 1;
  if (init_transducers(argv[4]))
    return 1;
  if (init_embedded_dlc(argv+5, argc-5))
    return 1;
  
  initEmbeddingLevel();


  //DELAC inflection
  err = DLC_inflect(argv[1],argv[2]);
  if (err)
    u_printf("Problems occured while inflecting a DELAC.\n");
  else 
    u_printf("Done.\n");
    
  //Liberate the structures needed for the DELAC inflection
  MU_graph_free_graphs();
  free_alphabet(alph);
  free_language_morpho();
  return 0;
}

//////////////////////////////////
// Usage note shown if the number of command line arguments is incorrect (<5) 
void usage() {
  u_printf("%S",COPYRIGHT);
  u_printf("Usage: DlcInflect <delac> <delacf> <alpha> <dir>\n");
  u_printf("     <delac> : the unicode DELAC file to be inflected\n");
  u_printf("     <delacf> : the unicode resulting DELACF dictionary \n");
  u_printf("     <alpha> : the alphabet file \n");
  u_printf("     <dir> : the directory containing 'Morphology' file and \n");
  u_printf("             inflection graphs for single and compound words.\n");
  u_printf("\nInflects a DELAC into a DELACF.\n");
}

/////////////////////////////////// 
// Loads the Morphology.txt file
// containing the morphological model of the language
// (i.e. the list of inflectional classes, categories and values).
// dir = directory containing this file
// Checks if the letter case category and values have correct labels
// If any problem occurs, returns 1. Otherwise returns 0.
int init_morphology(char* dir) {
  char morphology[FILENAME_MAX];
  new_file(dir,"Morphology.txt",morphology);
  if(read_language_morpho(morphology)) {
    free_language_morpho();
    config_files_status=CONFIG_FILES_ERROR;
    return 1;
  }
  //  print_language_morpho();
  return 0;
}

/////////////////////////////////// 
// Loads the alphabet file 'alph_file'
// If any problem occurs, returns 1. Otherwise returns 0.
int init_alphabet(char* alph_file) {
  alph=load_alphabet(alph_file);  //To be done once at the beginning of the inflection
  if (alph==NULL) {
    error("Cannot open alphabet file %s\n",alph_file);
    free_language_morpho();
    free_alphabet(alph);    
    return 1;
  }
  return 0;
}

/////////////////////////////////// 
// Loads the Equivalences.txt file if any
// containing the equivalences between the morphological
// values for simple words and those for compounds.
// dir = directory containing this file
// If any problem occurs, returns 1. Otherwise returns 0.
int init_equivalences(char* dir) {
  char equivalences[FILENAME_MAX];
  int err; //Error code
  new_file(dir,"Equivalences.txt",equivalences);
  err = init_morpho_equiv(equivalences);
  if (err) {
    free_language_morpho();
    free_alphabet(alph);    
    config_files_status=CONFIG_FILES_ERROR;
    return 1;
  }
  //  print_morpho_equiv();
  
  init_class_equiv();
  return 0;
}

/////////////////////////////////// 
// Initializes the structure of inflection transducers
// dir = directory containing this file
// If any problem occurs, returns 1. Otherwise returns 0.
int init_transducers(char* dir) {
  int err; //Error code
  strcpy(inflection_directory,dir);
  err = MU_graph_init_graphs();
  if (err) {
    MU_graph_free_graphs();
    return 1;
  }
  return 0;
}

/////////////////////////////////// 
// Initializes the structure of DELAC files containing
// components of the compounds to be inflected
// list = the list of file paths
// no_files = number of these files
// If any problem occurs, returns 1. Otherwise returns 0.
int init_embedded_dlc(char** files, int no_files) {
  no_embedded_DLC = no_files;
  embedded_DLC = (char**) malloc(no_embedded_DLC * sizeof(char*));
  if (!embedded_DLC) 
    fatal_error("Not enough memory in function main\n");
  int ed; //Index of the current file of embedded compounds
  for (ed=0; ed<no_embedded_DLC; ed++) {
    embedded_DLC[ed] = (char*) malloc((strlen(files[ed])+1)* sizeof(char));
    if (!embedded_DLC[ed]) 
      fatal_error("Not enough memory in function main\n");
    strcpy(embedded_DLC[ed], files[ed]);
  }
  return 0;
}
