/*
  * Unitex 
  *
  * Copyright (C) 2001-2007 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on July 11 2005
 */
//---------------------------------------------------------------------------

/********************************************************************************/
//// Exploring and inflection graph for multi-word units                //////////
/********************************************************************************/

#ifndef MU_GraphExploreH
#define MU_GraphExploreH

#include "MF_UnitMorpho.h"

/////////////////////////////////////////////////
// Explores the inflection transducer of the MU-lemma 'MU_lemma' 
// in order to generate all its inflected forms. The generated forms are put to 'forms'
// (initially, 'forms' does not have its space allocated).
// 'params' = "global" parameters 
// Returns 0 on success, 1 otherwise. 
int MU_graph_explore_graph(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_lemma_T* MU_lemma, U_forms_T* forms);

#endif
