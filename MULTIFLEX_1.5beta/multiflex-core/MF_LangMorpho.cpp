/*
  * Unitex 
  *
  * Copyright (C) 2001-2007 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (savary@univ-tours.fr)
 * Last modification on June 2232005
 */
//---------------------------------------------------------------------------

#include <stdio.h>
#include "MF_LangMorpho.h"
#include "MF_MorphoEquiv.h"
#include "MF_Util.h"
#include "Unicode.h"
#include "MF_Unif.h"   //debug
#include "Error.h"

///////////////////////////////////////////////////////////////////////////////////////
// TREATMENT OF THE MORPHOLOGICAL CLASSES, CATEGORIES AND VALUES OF THE LANGUAGE (Morphology.txt file)


///////////////////////////////////////
//All functions defined in this file
int read_language_morpho(MultiFlexParameters* params,char *lan_file, char *dir);
int read_cats(MultiFlexParameters* params,FILE* lf,unichar* line,int *line_no,
		char* line_ch,char* word_ch,l_cat_type_T *CATS_BLOCK);
int read_cat_line(MultiFlexParameters* params,int cat_no,unichar* line,int *line_no,l_cat_type_T CATS_BLOCK);
int read_classes(MultiFlexParameters* params,FILE* lf,unichar* line,int *line_no,char* line_ch,char* word_ch);
int read_class_line(MultiFlexParameters* params,int class_no,unichar* line,int *line_no);
int print_language_morpho(MultiFlexParameters* params);
int free_language_morpho(MultiFlexParameters* params);
l_category_T* is_valid_cat(unichar* cat);
int is_valid_val(l_category_T* cat, unichar* val);
l_category_T* get_cat(unichar* val);

//////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////
// TREATMENT OF THE MORPHOLOGICAL EQUIVALENCE BETWEEN CLASSES, CATEGORIES AND VALUES 
// FOR SIMPLE WORDS AND THOSE FOR COMPOUNDS (Equivalences.txt file)
// Categories and values defined for simple words determine those attributed to compounds
// inflected forms.

l_class_T* is_valid_class(unichar* cl);
l_category_T* is_valid_cat(unichar* cat);
int get_cat_rang(l_category_T* cat);
int is_valid_val(l_category_T* cat, unichar* val);
int is_empty_val(l_category_T* cat, int val);
int admits_empty_val(l_category_T* cat);
int get_empty_val(l_category_T* cat);
l_category_T* get_cat(unichar* val);
int copy_cat_str(unichar* cat_str,l_category_T* cat);
int copy_val_str(unichar* val_str, l_category_T* cat, int val);

/**************************************************************************************/
/* Read the language file "file".                                                     */
/* This file contains lists of all classes (nous, verb, etc.) of the language,        */
/* with their inflection categories (number, case, gender, etc.) and values           */
/* (e.g. sing, pl, masc, etc.).                                                       */
/* <E> is a special character meaning that the feature may have an empty value, e.g.  */
/* the base form in gradation                                                         */
/* E.g. for Polish:								      */
/* 			Polish							      */
/*                      <CATEGORIES>                                                  */
/* 			Nb:sing,pl		                 		      */
/* 			Case:Nom,Gen,Dat,Acc,Inst,Loc,Voc			      */
/* 			Gen:masc_pers,masc_anim,masc_inanim,fem,neu                   */
/*                      Gr:<E>,aug,sup                                                */
/*                      <CLASSES>                                                     */
/*                      noun: (Nb,<var>),(Case,<var>),(Gen,<fixed>)                   */
/*                      adj: (Nb,<var>),(Case,<var>),(Gen,<var>),(Gr,<var>)           */
/*                      adv: (Gr,<var>)                                               */
/* Fills out L_CLASSES and L_CATS.						      */
/* 'params' = "global" parameters                                                     */
/* Returns 0 if success, 1 otherwise                                                  */
int read_language_morpho(MultiFlexParameters* params,char *file) {

  //Current line of the morphology file
  unichar line[MAX_LANG_MORPHO_LINE];
  //Current character (not unichar) line, and current character word of the morphology file
  char line_ch[MAX_LANG_MORPHO_LINE];
  char word_ch[MAX_LANG_MORPHO_LINE];
  //Says if we are in a category block (basic), in the extra category block (extra),
  //or in the graphical category block (graphical) in the language morphology file
  l_cat_type_T CATS_BLOCK;

  //Curent line number of the morphology file
  int line_no = 0;

  FILE* lf;
  //Open the Morphology file
  if ( !(lf = u_fopen(file, "r")))  {
    error("Unable to open language morphology file %s\n",file);
    return 1;
  }
  
  //Omit the first line (language name)
  if (! feof(lf)) {
    u_fgets(line,MAX_LANG_MORPHO_LINE-1,lf);
    line_no++;
  }

  //scan the following line
  u_fgets(line,MAX_LANG_MORPHO_LINE-1,lf);
  u_to_char(line_ch,line);
  sscanf(line_ch,"%s",word_ch);
  line_no++;

  if (! feof(lf))
    if (read_cats(params,lf,line,&line_no,line_ch,word_ch,&CATS_BLOCK))
      return 1;
  if (! feof(lf))
    if (read_classes(params,lf,line,&line_no,line_ch,word_ch))
      return 1;
  return 0;
}

//////////////////////////////////////////////////////////////////////////////////////
// Read the category block of the language morphology file.                                     
// It should begin with <CATEGORIES>
// Before the beginning and after the end of the function "line" (global) contains 
// the current line of the morpho file.
// 'params' = "global" parameters
int read_cats(MultiFlexParameters* params,FILE* lf,unichar* line,int *line_no,
		char* line_ch,char* word_ch,l_cat_type_T *CATS_BLOCK) {

  int cat_no; //category's number
  int l;   //lenght of the scanned line
  
  //Current line should contain <CATEGORIES>
  
  if (feof(lf) || strcmp(word_ch,"<CATEGORIES>")) {
    error("Language morphology file format incorrect in line %d!\n",*line_no);
    return 1;
  }
  *CATS_BLOCK = basic; //We are in the block of categories
  
  //Scan categories
  l = u_fgets(line,MAX_LANG_MORPHO_LINE-1,lf);
  u_to_char(line_ch,line);
  sscanf(line_ch,"%s",word_ch);
  (*line_no)++;
  cat_no = 0;
  while (l && strcmp(word_ch,"<CLASSES>")) {
    if (! strcmp(word_ch,"<EXTRA_CATEGORIES>"))
      *CATS_BLOCK = extra; //We are in the block of extra categories (not shared with the module for simple words)
    else 
      if (! strcmp(word_ch,"<GRAPHICAL_CATEGORIES>"))
	*CATS_BLOCK = graphical; //We are in the block of extra categories (not shared with the module for simple words)
      else {
	if (read_cat_line(params,cat_no,line,line_no,*CATS_BLOCK))
	  return 1;
	cat_no++;
      }
    l = u_fgets(line,MAX_LANG_MORPHO_LINE-1,lf);
    u_to_char(line_ch,line);
    sscanf(line_ch,"%s",word_ch);
    (*line_no)++;
  }
  params->L_CATS.no_cats = cat_no;
  return 0;
}

//////////////////////////////////////////////////////////////////////////////////////
// Read a category line (having number cat_no) of the language morphology file.                                     
// Before the beginning of the function "line" (global) contains 
// the current line of the morpho file.
// 'params' = "global" parameters
int read_cat_line(MultiFlexParameters* params,int cat_no,unichar* line,int *line_no,l_cat_type_T CATS_BLOCK) {
  
  int v_cnt;  //counter of values for the present category
  unichar* cat_name;   //category's name
  unichar* cat_val;    //category's value
  unichar tmp[MAX_MORPHO_NAME];  //buffer for line elements
  unichar tmp_void[MAX_MORPHO_NAME];  //buffer for void characters
  unichar* line_pos; //current position in the input line
  int l;  //length of a scanned sequence
  int done;
  
  line_pos = line;
  
  //Read category name
  line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
  l = u_scan_until_char(tmp,line_pos,MAX_MORPHO_NAME-1,": \t",1);
  line_pos = line_pos + l;
  line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
  if (*line_pos != (char) ':') {
    error("Language morphology file format incorrect: ':' missing in line %d!\n", *line_no);
    return 1;
  }
  cat_name=u_strdup(tmp);
  params->L_CATS.cats[cat_no].name = cat_name;
  line_pos++;   //Omit the ':' 
  
  //Read category values
  v_cnt = 0;
  done = 0;
  do {
    line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
    l = u_scan_until_char(tmp,line_pos,MAX_MORPHO_NAME-1,", \t\n",1);
    line_pos = line_pos + l;
    line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
    cat_val=u_strdup(tmp);
    params->L_CATS.cats[cat_no].values[v_cnt] = cat_val;
    v_cnt++;
    if (*line_pos == (char) '\n')
      done = 1;
    else      
      line_pos++;  //Omit the ',' or the newline
  } while (!done);
  params->L_CATS.cats[cat_no].no_values = v_cnt;

  //Set the type of the current category
  params->L_CATS.cats[cat_no].cat_type = CATS_BLOCK;
  return 0;
}

//////////////////////////////////////////////////////////////////////////////////////
// Read that class block of the language morphology file.                                     
// It should begin with <CLASSES>.
// Before the beginning and after the end of the function "line" (global) contains 
// the current line of the morpho file.
// 'params' = "global" parameters
int read_classes(MultiFlexParameters* params,FILE* lf,unichar* line,int *line_no,char* line_ch,char* word_ch) {
  
  int class_no; //class number
  int l;   //lenght of the scanned line
  
  //Current line should contain <CLASSES>
  if (feof(lf) || strcmp(word_ch,"<CLASSES>")) {
    error("Language morphology file format incorrect: <CLASSES> missing in line %d!\n", *line_no);
    return 1;
  }

 //Scan classes
  l = u_fgets(line,MAX_LANG_MORPHO_LINE-1,lf);
  u_to_char(line_ch,line);
  sscanf(line_ch,"%s",word_ch);
  (*line_no)++;

  class_no = 0;
  while (l>1) {
    if (read_class_line(params,class_no,line,line_no))
      return 1;
    l = u_fgets(line,MAX_LANG_MORPHO_LINE-1,lf);
    u_to_char(line_ch,line);
    sscanf(line_ch,"%s",word_ch);
    (*line_no)++;
    class_no++;
  }
  params->L_CLASSES.no_classes = class_no;
  return 0;
}

//////////////////////////////////////////////////////////////////////////////////////
// Read a class line (having number class_no) of the language morphology file.                                     
// Before the beginning and after the end of the function "line" (global) contains 
// the current line of the morpho file.
// 'params' = "global" parameters
int read_class_line(MultiFlexParameters* params,int class_no,unichar* line,int *line_no) {

  int c_cnt;  //counter of categories for the present class
  unichar* class_name;   //class' name
  unichar tmp[MAX_MORPHO_NAME];  //buffer for line elements
  unichar tmp_void[MAX_MORPHO_NAME];  //buffer for void characters
  unichar* line_pos; //current position in the input line
  int l;  //length of a scanned sequence
  int done;
  int c;
  int found;

  line_pos = line;

  //Read class name
  line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
  l = u_scan_until_char(tmp,line_pos,MAX_MORPHO_NAME-1,": \t",1);
  line_pos = line_pos + l;
  line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
  class_name=u_strdup(tmp);
  params->L_CLASSES.classes[class_no].name = class_name;

  //Read class' categories
  c_cnt = 0;
  line_pos++;   //Omit the ':' 
  line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
  if (*line_pos != (char) '\n') {
    done = 0;
    do {

      //Read category
      line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
      line_pos++; //Omit the '('
      line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
      l = u_scan_until_char(tmp,line_pos,MAX_MORPHO_NAME-1,", \t",1);
      line_pos = line_pos + l;
      line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
      //Check if tmp contains an existing category
      for (c=0, found=0; c<params->L_CATS.no_cats && !found; c++)
	if (! u_strcmp(params->L_CATS.cats[c].name,tmp))
	  found = 1;
      if (!found) {
	      error("Undefined category in language morphology file: line %d!\n", *line_no);
	      return 1;
      }
      else
    params->L_CLASSES.classes[class_no].cats[c_cnt].cat = &(params->L_CATS.cats[c-1]);
      
      //Read the fixedness
      line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
      line_pos ++; //Omit the ','
      line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
      line_pos ++; //Omit the '<'
      line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
      l = u_scan_until_char(tmp,line_pos,MAX_MORPHO_NAME-1,"> \t",1);
      line_pos = line_pos + l;
      line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
      if (!u_strcmp(tmp,"fixed"))
	params->L_CLASSES.classes[class_no].cats[c_cnt].fixed = 1;
      else
	if (!u_strcmp(tmp,"var"))
	  params->L_CLASSES.classes[class_no].cats[c_cnt].fixed = 0;
	else {
	  error("Undefined fixedness symbol in language morphology file: line %d!\n", *line_no);
	  return 1;
      }
      c_cnt++;
      line_pos++; //Omit the '>'
      line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
      line_pos++; //Omit the ')'
      line_pos = line_pos + u_scan_while_char(tmp_void, line_pos, MAX_MORPHO_NAME-1," \t");  //Omit void characters
      if (*line_pos == (char) '\n')
	done = 1;
      else
	line_pos++;  //Omit the ','
    } while (!done);
  }
  params->L_CLASSES.classes[class_no].no_cats = c_cnt;
  return 0;
}

/**************************************************************************************/
/* Prints to the standard output the morphological system of the language             */
/* as defined by L_CLASSES.       		    			              */
// 'params' = "global" parameters
/* Returns 0 on success, 1 otherwise.                                                 */
int print_language_morpho(MultiFlexParameters* params) {
  int c,v, cl;
  //Print categories
  u_printf("<CATEGORIES>\n");
  for (c=0; c<params->L_CATS.no_cats; c++)
    if (params->L_CATS.cats[c].cat_type == basic) {
      //Print category
      u_printf("%S:",params->L_CATS.cats[c].name);
      //Print values
      for (v=0; v<params->L_CATS.cats[c].no_values; v++) {
	u_printf("%S",params->L_CATS.cats[c].values[v]);
	if (v != params->L_CATS.cats[c].no_values-1) {
	  u_printf(",");
	}
      }
      u_printf("\n");
    }
  
  //Print extra categories
  u_printf("<EXTRA CATEGORIES>\n");
  for (c=0; c<params->L_CATS.no_cats; c++)
    if (params->L_CATS.cats[c].cat_type == extra) {
      //Print category
      u_printf("%S:",params->L_CATS.cats[c].name);
      //Print values
      for (v=0; v<params->L_CATS.cats[c].no_values; v++) {
	u_printf("%S",params->L_CATS.cats[c].values[v]);
	if (v != params->L_CATS.cats[c].no_values-1) {
	  u_printf(",");
	}
      }
      u_printf("\n");
    }
    
  //Print extra categories
  u_printf("<EXTRA CATEGORIES>\n");
  for (c=0; c<params->L_CATS.no_cats; c++)
    if (params->L_CATS.cats[c].cat_type == graphical) {
      //Print category
      u_printf("%S:",params->L_CATS.cats[c].name);
      //Print values
      for (v=0; v<params->L_CATS.cats[c].no_values; v++) {
	u_printf("%S",params->L_CATS.cats[c].values[v]);
	if (v != params->L_CATS.cats[c].no_values-1) {
	  u_printf(",");
	}
      }
      u_printf("\n");
    }

  //Print classes
  u_printf("<CLASSES>\n");
  for (cl=0; cl<params->L_CLASSES.no_classes; cl++) {
    //print class
    u_printf("%S:",params->L_CLASSES.classes[cl].name);
    //print relevant categories
    for (c=0; c<params->L_CLASSES.classes[cl].no_cats; c++) {
      //print category
      u_printf("(%S,<",params->L_CLASSES.classes[cl].cats[c].cat->name);
      //print fixedness
      if (params->L_CLASSES.classes[cl].cats[c].fixed) u_printf("fixed");
      else u_printf("var");
      u_printf(">)");
      if (c != params->L_CLASSES.classes[cl].no_cats-1) {
	u_printf(",");
      }	
    }
    u_printf("\n");
  }
  return 0;
}


/**************************************************************************************/
/* Liberates the space allocated for the language morphology description.             */
/* 'params' = "global" parameters                                                     */
int free_language_morpho(MultiFlexParameters* params) {

  int c, v;

  //Liberate L_CATS
  for (c=0; c<params->L_CATS.no_cats; c++) {
    free(params->L_CATS.cats[c].name);
    for (v=0; v<params->L_CATS.cats[c].no_values; v++)
      free(params->L_CATS.cats[c].values[v]);
  }
	
  //Liberate L_CLASSES
  for (c=0; c<params->L_CLASSES.no_classes; c++)
    free(params->L_CLASSES.classes[c].name);

  return 0;
}

/*******************************************************************************/
/* If cl is a valid class name, returns a pointer to this class.               */
/* Otherwise returns NULL.                                                     */
/* 'params' = "global" parameters                                              */
l_class_T* is_valid_class(MultiFlexParameters* params,unichar* cl) {
  int c;
  for (c=0; c<params->L_CLASSES.no_classes; c++)
    if (!u_strcmp(cl,params->L_CLASSES.classes[c].name))
      return &(params->L_CLASSES.classes[c]);
  return NULL;
}

/**************************************************************************************/
/* If cat is a valid category name, returns a pointer to this category.               */
/* Otherwise returns NULL.                                                            */
/* 'params' = "global" parameters                                                     */
l_category_T* is_valid_cat(MultiFlexParameters* params,unichar* cat) {
  int c;
  for (c=0; c<params->L_CATS.no_cats; c++)
    if (!u_strcmp(cat,params->L_CATS.cats[c].name))
      return &(params->L_CATS.cats[c]);
  return NULL;
}

/**************************************************************************************/
/* Returns the rang (>=1) of category 'cat' on the list of all categories.            */
/* On error returns -1.                 c                                             */
/* 'params' = "global" parameters                                                     */
/* 'params' = "global" parameters                                                     */
int get_cat_rang(MultiFlexParameters* params,l_category_T* cat) {
  int c;  //Current Category's index
  for (c=0; c<params->L_CATS.no_cats; c++)
    if (&(params->L_CATS.cats[c]) == cat)
      return c+1;  //return a rang starting from 1 and not 0
  return -1;
}

/**************************************************************************************/
/* If val is a valid value in the domain of category cat, returns the index of val    */
/* in cat. Otherwise returns -1.                                                      */
int is_valid_val(l_category_T* cat, unichar* val) {
  int v;
  for (v=0; v<cat->no_values; v++)
    if (!u_strcmp(val,cat->values[v]))
      return v;
  return -1;
}

/**************************************************************************************/
/* If val is an empty value in the domain of category cat, returns 1,                 */
/* otherwise returns 0.                                                               */
/* val is the ordinal number of the value in 'cat'                                    */
int is_empty_val(l_category_T* cat, int val) {
  if (! u_strcmp(cat->values[val],"<E>"))
    return 1;
  else
    return 0;
}

/**************************************************************************************/
/* If category 'cat' admits an empty value returns 1, otherwise returns 0.                                                               */
/* val is the ordinal number of the value in 'cat'                                    */
int admits_empty_val(l_category_T* cat) {
  if (get_empty_val(cat) >= 0)
    return 1;
  else
    return 0;
}

/**************************************************************************************/
/* If category 'cat' admits an empty value returns the ordinal number of this value   */
/* in 'cat'. Otherwise returns -1.                                                    */
int get_empty_val(l_category_T* cat) {
  int v;  //Current value
  for (v=0; v<cat->no_values; v++)
    if (! u_strcmp(cat->values[v],"<E>"))
      return v;
  return 0;
}  

/**************************************************************************************/
/* If val is a valid value, returns the pointer to its (first) category.              */
/* Otherwise returns NULL.                                                            */
/* 'params' = "global" parameters                                                     */
l_category_T* get_cat(MultiFlexParameters* params,unichar* val) {
  int c;
  int v;
  for (c=0; c<params->L_CATS.no_cats; c++)
    for (v=0; v<params->L_CATS.cats[c].no_values; v++)
      if (!u_strcmp(val,params->L_CATS.cats[c].values[v]))
	return &(params->L_CATS.cats[c]);
  return NULL;
}

/**************************************************************************************/
/* If cat is a valid category, returns the number of its values.                      */
/* Otherwise returns 0.                                                               */
int get_cat_nb_values(l_category_T* cat) {
  if (!cat)
    return 0;
  return cat->no_values;
}

/**************************************************************************************/
/* If 'cat' is a valid category, copies its name to 'cat_str' which should have its   */
/* space allocated, and returns 0. Otherwise returns 1.                               */
int copy_cat_str(unichar* cat_str,l_category_T* cat) {
  if (!cat)
    return 1;
  if (!cat->name)
    return 1;
  u_strcpy(cat_str,cat->name);
  return 0;
}

/**************************************************************************************/
/* If 'cat' is a valid category, copies its name of its vale number 'val' to 'val_str'*/
/* which should have its space allocated, and returns 0. Otherwise returns 1.         */
int copy_val_str(unichar* val_str, l_category_T* cat, int val) {
  if (!cat)
    return 1;
  if (!cat->values)
    return 1;
  if (!cat->values[val])
    return 1;
  u_strcpy(val_str,cat->values[val]);
  return 0;
}

/**************************************************************************************/
/* If 'cl' is a valid class, copies its name to 'cl_str' which should have its   */
/* space allocated, and returns 0. Otherwise returns 1.                               */
int copy_class_str(unichar* cl_str,l_class_T* cl) {
  if (!cl)
    return 1;
  if (!cl->name)
    return 1;
  u_strcpy(cl_str,cl->name);
  return 0;
}
