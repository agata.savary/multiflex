/*
  * Unitex 
  *
  * Copyright (C) 2001-2008 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on September 3 2008
 */
//---------------------------------------------------------------------------

/********************************************************************************/
//// Morphology and inflection of a simple unit or a multi-word unit ///
/********************************************************************************/

#ifndef MF_UnitMorphoBaseH
#define MF_UnitMorphoBaseH

#include "Unicode.h"
#include "MF_FeatStructBase.h"
#include "MF_FormExtras.h"

/////////////////////////////////////////////////
// Possible types for a unit's lemma:
// e.g. "vif" if simple, 
//      "Maria Sklodowska-Curie" if compound
typedef enum {simple, compound} U_lemma_type_T;

/////////////////////////////////////////////////
// Unique identification of an inflected form of a unit
struct U_lemma_;

typedef struct {
  unichar* form;        //inflected form, e.g. "vive"
  U_lemma_* lemma;     //pointer to the (simple or compound) lemma
  f_feat_struct_T* feat; //features, e.g. {Gen=fem, Nb=pl}
  form_extras_T* extras;  //Extra information attached to a form, e.g. set of paths used to generate the form
} U_form_id_T;

/////////////////////////////////////////////////
// List of inflected forms
typedef struct  {
  int no_forms;         //number of inflected forms
  U_form_id_T *forms;   //table of inflected forms
} U_forms_T;

/////////////////////////////////////////////////
//Structure for the (simple or compound) lemma of a unit
//Embedded compounding is allowed, i.e. a unit can be either a simple word or a compound
typedef struct U_lemma_ {
  U_lemma_type_T type;          //Type of the lemma (simple or compound)
  union {
    unichar* simple_lemma;      //Simple lemma, e.g. "vif"
    U_forms_T compound_lemma;   //List of forms forming the lemma (it used to be a static table)
  };
  l_class_T *cl;	         //e.g. adj, ger (Morfeusz)
  char *paradigm;		 //e.g. A41 (Unitex), s3, v (Morfeusz)
} U_lemma_T;

/////////////////////////////////////////////////
// Lemma and its inflected forms
typedef struct  {
  U_lemma_T* lemma;   //lemma
  U_forms_T* forms;   //table of its inflected forms
} U_lemma_forms_T;

/////////////////////////////////////////////////
// Set of lemmas and their inflected forms
typedef struct  {
  int no_lemmas_forms;         //number of lemmas
  U_lemma_forms_T *lemma_forms;   //table of inflected forms
} U_lemmas_forms_set_T;



/********************************************************************************/
//// Register of the Inflected Forms of the Components of the Current Entry    ///
//// Used for optimisation purposes                                             ///
/********************************************************************************/
/////////////////////////////////////////////////
// A lemma and the list of all its inflected forms
typedef struct  {
  U_lemma_T* lemma;    //lemma
  U_forms_T* forms;   //table of its inflected forms
} U_form_list_T;

/////////////////////////////////////////////////
// Set of lists of inflected forms, each list concerns one lemma
typedef struct  {
  int no_form_lists;         //number of lemmas
  U_form_list_T* form_lists;   //table of inflected forms
} U_form_list_set_T;
  
#endif
