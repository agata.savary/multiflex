/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

/************************************************************************************/
/* FEATURE-VALUE REPRESENTATION OF THE MORPHOLOGY OF A SIMPLE OR COMPOUND WORD FORM */
/************************************************************************************/

#ifndef MF_FeatStructBaseH
#define MF_FeatStructBaseH

#include "MF_LangMorphoBase.h"

//////////////////////////////////////////////////////
// Structure for a category-value pair, e.g. Gen=fem
typedef struct {
  l_category_T* cat;   //category, e.g. Gen
  int val;             //category's value (e.g. fem): index of val in the domain of cat
} f_catval_T;

/////////////////////////////////////////////////////////
// A feature structure describing the morphology of a wordform.
// i.e. a set of category-value couples, e.g. {Gen=fem, Nb=sing}
typedef struct {
  int no_catvals;     //number of category-value couples
  //  f_catval_T catvals[MAX_CATS];  //collection of category-value couples
  f_catval_T* catvals;  //collection of category-value couples
} f_feat_struct_T;

/////////////////////////////////////////////////////////
// Set of several morphological interpretations of a wordform.
// e.g. {{Gen=fem,Nb=sing},{Gen=fem,Nb=pl}}
typedef struct {
  int no_feat_structs;     //number of morphological interpretations
  f_feat_struct_T* feat_structs;  //collection of morphological interpretations
} f_feat_struct_set_T;


#endif
