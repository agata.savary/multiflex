/*
  * Unitex 
  *
  * Copyright (C) 2001-2007 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on June 22 2005
 */
//---------------------------------------------------------------------------


#ifndef LangMorphoH
#define LangMorphoH

#include "Unicode.h"
#include "MF_Global2.h"
#include "MF_LangMorphoBase.h"

/**
 * 
 * This library is used to parse the "Morphology.txt" and "Equivalences.txt" files that are 
 * supposed to be in the same directory as the inflection graphs.
 * 
 */


/**************************************************************************************/
/* Read the language file "file".                                                     */
/* This file contains lists of all classes (nous, verb, etc.) of the language,        */
/* with their inflection categories (number, case, gender, etc.) and values           */
/* (e.g. sing, pl, masc, etc.).                                                       */
/* <E> is a special character meaning that the feature may have an empty value, e.g.  */
/* the base form in gradation                                                         */
/* E.g. for Polish:								      */
/* 			Polish							      */
/*                      <CATEGORIES>                                                  */
/* 			Nb:sing,pl		                 		      */
/* 			Case:Nom,Gen,Dat,Acc,Inst,Loc,Voc			      */
/* 			Gen:masc_pers,masc_anim,masc_inanim,fem,neu                   */
/*                      Gr:<E>,aug,sup                                                */
/*                      <CLASSES>                                                     */
/*                      noun: (Nb,<var>),(Case,<var>),(Gen,<fixed>)                   */
/*                      adj: (Nb,<var>),(Case,<var>),(Gen,<var>),(Gr,<var>)           */
/*                      adv: (Gr,<var>)                                               */
/* Fills out L_CLASSES and L_CATS.						      */
/* 'params' = "global" parameters                                                     */
/* Returns 0 if success, 1 otherwise                                                  */
int read_language_morpho(MultiFlexParameters* params,char *file);

/**************************************************************************************/
/* Prints to the standard output the morphological system of the language             */
/* as defined by L_CLASSES.       		    			              */
/* 'params' = "global" parameters                                                     */
/* Returns 0 on success, 1 otherwise.                                                 */
int print_language_morpho(MultiFlexParameters* params);

/**************************************************************************************/
/* Liberates the space allocated for the language morphology description.             */
/* 'params' = "global" parameters                                                     */
int free_language_morpho(MultiFlexParameters* params);

/*******************************************************************************/
/* If cl is a valid class name, returns a pointer to this class.               */
/* Otherwise returns NULL.                                                     */
/* 'params' = "global" parameters                                                     */
l_class_T* is_valid_class(MultiFlexParameters* params,unichar* cl);

/**************************************************************************************/
/* If cat is a valid category name, returns a pointer to this category.               */
/* Otherwise returns NULL.                                                            */
/* 'params' = "global" parameters                                                     */
l_category_T* is_valid_cat(MultiFlexParameters* params,unichar* cat);

/**************************************************************************************/
/* Returns the rang (>=1) of category 'cat' on the list of all categories.                  */
/* On error returns -1.                                                               */
/* 'params' = "global" parameters                                                     */
int get_cat_rang(MultiFlexParameters* params,l_category_T* cat);

/**************************************************************************************/
/* If val is a valid value in the domain of category cat, returns the index of val    */
/* in cat. Otherwise returns -1.                                                      */
int is_valid_val(l_category_T* cat, unichar* val);

/**************************************************************************************/
/* If val is an empty value in the domain of category cat, returns 1,                 */
/* otherwise returns 0.                                                               */
/* val is the ordinal number of the value in 'cat'                                    */
int is_empty_val(l_category_T* cat, int val);

/**************************************************************************************/
/* If category 'cat' admits an empty value returns 1, otherwise returns 0.                                                               */
/* val is the ordinal number of the value in 'cat'                                    */
int admits_empty_val(l_category_T* cat);

/**************************************************************************************/
/* If category 'cat' admits an empty value returns the ordinal number of this value   */
/* in 'cat'. Otherwise returns -1.                                                    */
int get_empty_val(l_category_T* cat);

/**************************************************************************************/
/* If val is a  valid value, returns the pointer to its (first) category.             */
/* Otherwise returns NULL.                                                            */
l_category_T* get_cat(unichar* val);

/**************************************************************************************/
/* If cat is a valid category, returns the number of its values.                      */
/* Otherwise returns 0.                                                               */
int get_cat_nb_values(l_category_T* cat);

/**************************************************************************************/
/* If 'cat' is a valid category, copies its name to 'cat_str' which should have its   */
/* space allocated, and returns 0. Otherwise returns 1.                               */
int copy_cat_str(unichar* cat_str,l_category_T* cat);

/**************************************************************************************/
/* If 'cat' is a valid category, copies its name of its value number 'val' to 'val_str'*/
/* which should have its space allocated, and returns 0. Otherwise returns 1.         */
int copy_val_str(unichar* val_str, l_category_T* cat, int val);

/**************************************************************************************/
/* If 'cl' is a valid class, copies its name to 'cl_str' which should have its        */
/* space allocated, and returns 0. Otherwise returns 1.                               */
int copy_class_str(unichar* cl_str,l_class_T* cl);

#endif
