/*
  * Unitex 
  *
  * Copyright (C) 2001-2008 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on September 3 2008
 */
//---------------------------------------------------------------------------

/********************************************************************************/
//// Morphology and inflection of a simple unit or a multi-word unit ///
/********************************************************************************/

#ifndef MF_UnitMorphoH
#define MF_UnitMorphoH

#include "Unicode.h"
#include "MF_FeatStruct.h"
#include "MF_LetterCase.h"
#include "MF_FormExtras.h"
#include "MF_UnitMorphoBase.h"
#include "MF_Global2.h"


////////////////////////////////////////////
// For a given (simple or multi-word) unit, generates all its inflected forms,
// corresponding to the given inflection features 'feat' 
// The forms generated are put into 'forms'.
// Forms has its space allocated and initialized
// E.g. if feat = {Gen=fem}, and lemma = ["cousin germain",subst,NC-CXC]
// then the generated forms are  {["cousine germaine",{Gen=fem,Nb=sing}],["cousines germaines",{Gen=fem,Nb=pl}]}
// E.g. if feat = {Gen=fem;LatterCase=first_upper}, lemma = ["cousin",subst,N32],
// then the generated forms are  {["Cousine",{Gen=fem,Nb=sing}],["Cousines",{Gen=fem,Nb=pl}]}
// If feat is NULL, all inflected forms are to be generated
// If feat is [0,NULL] no inflected form is to be generated
// 'params' = "global" parameters 
// 'CURRENT_FORMS' = forms generated up till now
// Returns 0 on success, 1 otherwise.   
int U_inflect(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_lemma_T* lemma, f_feat_struct_T* feat, U_forms_T* forms);

///////////////////////////////////////////////
//Determine the full list of desired features for the unit's inflected form
// 'params' = "global" parameters 
//u = current unit to be inflected
//desired_feat = the features desired for the unit
//full_feat = output parameter: the full list of desired features for the unit
//Each desired feature must:
//   - be 'basic' or 'extra' and its category must appear in the features of the current unit
//   - be graphical and its category may or may not appear in the current unit
//E.g. if u->form="Dom", u->lemma->simple_lemma="dom",  desired_feat=<Case=acc;Usage=neut;Init=dot> and u->feat=<Nb=sing;Gen=m3;Case=nom;Usage=offic>
//     then full_feat = <Nb=sing;Gen=m3;Case=acc;Usage=dot;Init=dot;LetterCase=first_upper>
//Return 1 on serror, 0 otherwise.
int U_determine_full_features(MultiFlexParameters* params,U_form_id_T* u, f_feat_struct_T* desired_feat, f_feat_struct_T* full_feat);

////////////////////////////////////////////
// Initialize the list 'forms' with null values
// We suppose that 'forms' has its space allocated
void U_init_forms(U_forms_T* forms);

////////////////////////////////////////////
// Initialize a lemma
void U_init_lemma(U_lemma_T* lemma, U_lemma_type_T type);

////////////////////////////////////////////
// Initialize a form
void U_init_form_id(U_form_id_T* form);

////////////////////////////////////////////
// Add an empty form with empty features 'feat' and extra information 'extras' 
// to the initially empty set of forms 'forms'
void U_add_empty_form(U_forms_T* forms, form_extras_T* extras);

////////////////////////////////////////////
// Input:
//    forms: a set of inflected forms (allocated)
//    new_form: a form with no annotation to be added
//    extras: extra information to be attached to the form
// Output:
//    forms - set of the inflected forms possibly enlarged with the new form
// Adds the non annotated form 'new_form' to 'forms' in any case (with repetitions)
// The added value is duplicated
// Returns 0 on success, 1 otherwise.   
int U_add_invariable_form(unichar* new_form,U_forms_T* forms, form_extras_T* extras);

////////////////////////////////////////////
// Input:
//    forms: a list of inflected forms (allocated)
//    new_form: a form to be added 
//    unique: 0 if the new_form is to be added in any case,
//            1 if new_forms is to be added only if it does not yet appear in 'forms'
// Output:
//    forms - set of the inflected forms possibly enlarge
// Adds a 'new_form' to 'forms' with or without repetitions.
// The form added is duplicated
// Returns 0 on success, 1 otherwise.   
int U_add_form(U_form_id_T* new_form,U_forms_T* forms, int unique);

////////////////////////////////////////////
// Duplicate lemma 'l2' into 'l1'
// 'l1' is allocated but empty
// The lemma units and paradigm for 'l1' are duplicated in the function
// Returns 0 on success, 1 on error
int U_duplicate_lemma(U_lemma_T* l1,U_lemma_T* l2);

////////////////////////////////////////////
// Duplicate form 'f2' into 'f1'
// 'f1' is allocated but empty
// The form, lemma and feature structures for 'f1' are duplicated in the function
// Returns 0 on success, 1 on error
int U_duplicate_form_id(U_form_id_T* f1,U_form_id_T* f2);

////////////////////////////////////////////
// Input:
//    all_forms: a set of inflected forms
//    desired_features: feature structure of the desired forms, e.g. {Gen=fem, Case=Inst}, or {} (if separator)
// Output:
//    forms - set of the inflected forms corresponding to the desired features
//        e.g. (3,{[reka,{Gen=fem,Nb=sing,Case=Instr}],[rekami,{Gen=fem,Nb=pl,Case=Instr}],[rekoma,{Gen=fem,Nb=pl,Case=Instr}]})
//        or   (1,{["-",{}]})
// Returns 0 on success, 1 otherwise.   
int U_duplicate_desired_forms(U_forms_T* all_forms,f_feat_struct_T* desired_features,U_forms_T* forms);

////////////////////////////////////////////
// Concatenantes each form in 'front_forms' in front of each form in 'back_forms'. 
// Adds the resulting forms into 'forms'.
// Each new concatenated forms takes its morphological features from the back form.
// The lemmas are not copied
// E.g. while generating the instrumental of "rece pelne roboty", if we have :
// front_forms = {("rekami",{Case=Inst, Nb=pl, Gen=fem}), ("rekoma",{Case=Inst, Nb=pl, Gen=fem})}
// back_forms = {("pelnymi roboty",{Case=Inst, Nb=pl, Gen=fem})}
// forms = {("rak pelnych roboty",{Case=Acc, Nb=pl, Gen=fem})}
// then we obtain {("rak pelnych roboty",{Case=Acc, Nb=pl, Gen=fem}),
//                 ("rekami pelnymi roboty",{Case=Inst, Nb=pl, Gen=fem}),
//                 ("rekoma pelnymi roboty",{Case=Inst, Nb=pl, Gen=fem})}
// Initially, 'forms' has its space allocated, it may be empty or non empty.
// If it is non empty, the existing forms must not be lost
// Returns 1 in case of errors, 0 otherwise
int U_concat_forms(U_forms_T* front_forms, U_forms_T* back_forms, U_forms_T* forms);

////////////////////////////////////////////
// Add forms appearing in 'new_forms' to 'forms' so that
// no form appears twice in the result. 
// (In the previous version of this function the forms allocated in
// 'new_forms' were copied by pointer assignment. They were not to be 
// liberated when 'new_forms' were liberated.)
// The forms added are duplicated.
// Returns 1 in case of errors, 0 otherwise
int U_merge_forms(U_forms_T* forms, U_forms_T* new_forms);

////////////////////////////////////////////
// Copy the set of forms in 'old_forms' to 'new_form'
// The forms are not duplicated, 'old_forms' becomes empty
// 'new_forms' and 'old_forms' should be allocated before function call
// Returns 0 on success, 1 on error
int U_copy_forms(U_forms_T* new_forms,U_forms_T* old_forms);

////////////////////////////////////////////
// Copy the set of forms in 'old_forms' to 'new_form'
// The forms are duplicated, 'old_forms' is not deleted before function call
// 'new_forms' and 'old_forms' should be allocated 
// Returns 0 on success, 1 on error
int U_duplicate_forms(U_forms_T* new_forms,U_forms_T* old_forms);

////////////////////////////////////////////
// Enlarge the extra information attached to each form in 'forms'
// by the 'extra'
// Return 1 in case of errors, 0 otherwise
int U_concat_extras(U_forms_T* forms, extras_elem_T* extra);

////////////////////////////////////////////
// Prints a set of forms, their lemmas and inflection features if any.
void U_print_forms(U_forms_T* F);

////////////////////////////////////////////
// Prints a lemma and its info.
void U_print_lemma(U_lemma_T* l);

////////////////////////////////////////////
// Deletes a form, its lemma and inflection features if any.
// The form structure itself is not liberated
void U_delete_f(U_form_id_T* f);

////////////////////////////////////////////
// Deletes a set of forms, their lemmas and inflection features if any.
void U_delete_forms(U_forms_T* F);

////////////////////////////////////////////
// Deletes a lemma stucture.
// 'l' = lemma to be liberated
// 'liberate_main_structure' = Boolean saying if the main structure should be liberated (1) or not (0)
void U_delete_lemma(U_lemma_T* l, int liberate_main_structure);

////////////////////////////////////////////
// Checks if 'form' is a simple or a compound form
// 'params' = "global" parameters 
// Returns 1 if 'forms' is a compound form, 0 if it is a simple form
// -1 in case of errors.
int U_is_compound(MultiFlexParameters* params,unichar* form);

////////////////////////////////////////////
// Produces a lemma string from a lemma structure
// 'lemma' = lemma structure
// 'max' = maximum length of the resulting lemma string
// 'lemma_str' = return parameter, it must be allocated before function call but empty
// Returns 1 if 'forms' is a compound form, 0 if it is a simple form
// -1 in case of errors, 0 otherwise.
int U_get_lemma_str(unichar* lemma_str, int max, U_lemma_T* lemma);

////////////////////////////////////////////
// Returns the length of a lemma string, or -1 in case of errors.
// 'lemma' = lemma structure
int U_get_lemma_str_len(U_lemma_T* lemma);

////////////////////////////////////////////
// Finds the component number 'num' in the compound lemma 'MU_lemma' 
// 'num' >=1 (not >=0)
// Returns the pointer to this component
// Returns NULL if not found, or if the lemma is not a compound
U_form_id_T* U_lemma_get_component(U_lemma_T* MU_lemma, int num);

////////////////////////////////////////////
// Enlarges the morphology of all forms appearing in 'forms'
// with the category-value pairs appearing in 'feat'
// 'params' = "global" parameters 
// 'forms': input/output parameter conating the forms to be enlarged
// 'feat': features to be added to each form in 'forms'
// If a feature in 'feat' is not compatible with the morphology
// of a forms (it has the same category but not the same value)
// this form is eliminated.
// E.g. if forms={(ulica Bitwy,<Gen=f,Nb=sg,Case=nom>),...} and 'feat'=<Usage=marked>
//      then the forms becomes {(ulica Bitwy,<Gen=f,Nb=sg,Case=nom,Usage=marked>,...)}
// E.g. if forms={(ulica Bitwy,<Gen=f,Nb=sg,Case=nom,Usage=marked>),...} and 'feat'=<Usage=neut>
//      then the forms gets eliminated
// Returns 1 on error, 0 otherwise
int U_enlarge_forms_morpho(MultiFlexParameters* params,U_forms_T* forms, f_feat_struct_T* feat);

////////////////////////////////////////////
// Input:
//    params: "global" parameters 
//    all_forms: a set of inflected forms
//    desired_features: feature structure of the desired forms, e.g. {Gen=fem, Case=Inst, LetterCase=first_upper, Init=dot}, or {} (if separator), the features of type 'graphical' may be neglected if they do not appear in all_forms
// Output:
//    forms - set of the inflected forms obtained from all_forms by the application of the 'graphical' (post-inflection) features
// E.g. if all_forms is {<rekami,<Gen=fem;Nb=p;Case=Inst>>,<rekomam,<Gen=fem;Nb=p;Case=Inst>>} and desired features contains <LetterCase=first_upper;Init=dot}, then forms becomes {<R.,<Gen=fem;Nb=p;Case=Inst;LetterCase=first_upper;Init=dot>>}
// Returns 0 on success, 1 otherwise.   
int U_apply_graphical_feat(MultiFlexParameters* params,U_forms_T* all_forms,f_feat_struct_T* desired_features,U_forms_T* forms);
  
#endif
