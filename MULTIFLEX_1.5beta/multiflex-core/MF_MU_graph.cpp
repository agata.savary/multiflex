/*
  * Unitex 
  *
  * Copyright (C) 2001-2007 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on July 11 2005
 */
//---------------------------------------------------------------------------

/********************************************************************************/
/********************************************************************************/

#include "Fst2.h"
#include "Error.h"
#include "Transitions.h"
#include "MF_Global2.h"


/////////////////////////////////////////////////
// In the graph not yet loaded loads it otherwise searches for it in the structure.
// 'params' = "global" parameters 
// T = return parameter - index of the inflection transducer corresponding to 'graph_name' in the transducer table
// On success returns the index of the graph's initial state, otherwise returns -1.
int MU_graph_get_initial(MultiFlexParameters* params,char* graph_name, int* T) {
  //Get the index of the transducer in the transducer table
  *T=get_transducer(params,graph_name);
  if (*T<0 || params->fst2[*T]==NULL) {
    // if the automaton has not been loaded
    return -1;
  }
  //Get the initial state whose index is always 0
  Fst2* a = params->fst2[*T];
  Fst2State q=a->states[0];
  if (q)
    return 0;
  else
    return -1;
}

/////////////////////////////////////////////////
// Get the state whose index is q_no in the transducer whose index is T
// 'params' = "global" parameters 
// Return the pointer to this state or NULL in case of errors
Fst2State MU_graph_get_state(MultiFlexParameters* params,int q_no, int T) {
  if (T < 0)
    return NULL;

  Fst2* a = params->fst2[T];
  //Check if the state number is not too big 
  if (q_no<0 || a->number_of_states<=q_no)
    return NULL;

  return a->states[q_no];
}

/////////////////////////////////////////////////
// Get the label of the transition t in the transducer whose index is T
// 'params' = "global" parameters 
// Return the pointer to this label or NULL in case of errors
Fst2Tag MU_graph_get_label(MultiFlexParameters* params,Transition* t, int T) {
  if (T<0 || !t)
    return NULL;

  Fst2* a; //Transducer
  a = params->fst2[T];

  return a->tags[t->tag_number];
}

/////////////////////////////////////////////////
// Get the index of label of the transition t
// Return this index or -1 in case of errors
int MU_graph_get_label_number(Transition* t) {
  if (!t)
    return -1;
  return t->tag_number;
}

/////////////////////////////////////////////////
// Get the arrival state of the transition t 
// Return the index of this state or -> in case of errors
int MU_graph_get_arrival_state(Transition* t) {
  if (!t)
    return -1;

  return t->state_number;
}

