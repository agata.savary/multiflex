/*
  * Unitex 
  *
  * Copyright (C) 2001-2007 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on July 11 2005
 */
//---------------------------------------------------------------------------

/*********************************************************************************************/
/* Scanning and printing the graph node labels in an inflection graphs for multi-word units  */
/*********************************************************************************************/

#ifndef MU_GrapLabelH
#define MU_GraphScanH

#include "Unicode.h"
#include "MF_UnitMorpho.h"

#define MAX_GRAPH_NODE 2000  //Maximum length of the contents of a graph node

/////////////////////////////////////////////////
// Possible types for a value description
// e.g. Gen=fem is a constant description, 
//      Gen=$g1 is a description containing a unification variable
//      Gen==$g1 is a description containing an inheritance variable
//      Gen=$1.Gen is a description containing an value inherited from a particular constituent
typedef enum {cnst, inherit_const, unif_var, inherit_var} MU_graph_value_T;

/////////////////////////////////////////////////
//Structure for a unification variable and its admitted values
//Used in particular for unification variables with restriction, e.g.:
//<"c",<0,3,-1>> if the equation in graph was Case=$c in {Nom, Acc}
//<"c",<0,1,2,3,6,-1>> if the equation in graph was Case=$c in ^{Inst, Loc}
//<"c",<0,1,2,3,4,5,6,-1>> if the equation in graph was Case=$c 

typedef struct {
  unichar* var;  //The variable identifier, e.g. "c"
  int* values;   //The list of admitted values (terminated by a -1), e.g. <0,3,-1>
                 //If NULL all values of the category are allowed
} MU_graph_unif_var_T;

/////////////////////////////////////////////////
//Structure for a single category-value pair in a graph's node
typedef struct {
  l_category_T* cat;       //e.g. Gen
  MU_graph_value_T type;   //type of the description: constant, unification variable, inheritance variable, or inheritence constant
  union {
    int* values;            //category's value (e.g. fem): indices of val in the domain of 'cat', 
                            //e.g. <1,2,-1> if the equation was Gen=m2|m3
    MU_graph_unif_var_T* unif_var;     //e.g. <"g1",<0,2,4,5,6,-1>> if the equation in graph was Gen=$g1!=m2|f
    unichar* inherit_var;  //e.g. g1 if the node was Gen==$g1
    int inherit_const;     //e.g. 2 if the node was Gen=$2.Gen
  } val;  
} MU_graph_catval_T;

/////////////////////////////////////////////////
// Set of category-value pairs in a node, e.g. <Gen==$g1, Nb=$n1, Case=gen>
typedef struct {
  int no_catvals;                       //number of category-value pairs
  MU_graph_catval_T catvals[MAX_CATS];  //collection of category-value pairs
} MU_graph_feat_struct_T;

/////////////////////////////////////////////////
// Possible types for a graphical unit reference in a node
// e.g. "of" is a reference to a constant unit, 
//      $2 is a reference to a variable (here representing the 2nd constituent of the MWU's lemma)
//      trzeci.0:adj is a reference to a external lemma (inexistent in the MWU)
typedef enum {cst,var,ext} MU_graph_u_T;

/////////////////////////////////////////////////
// Reference to a graphical unit of a MWU. It may be either a constant (e.g. "of")
// or a variable (e.g. $1) referring to a unit in the lemma of the MWU.
typedef struct {
  MU_graph_u_T type; //cst or var
  union {
    unichar* seq;   //e.g. "of"
    int num;        //number of the constituent referred to, e.g. 1 if $1
    U_lemma_T* ext;  //e.g. trzeci.0:adj
  } u;
} MU_graph_unit_T;

/////////////////////////////////////////////////
// Input of a node, e.g. <$1:Gen==$g1, Nb=$n1, Case=gen>
typedef struct {
  MU_graph_unit_T unit;       //e.g. $1, of 
  MU_graph_feat_struct_T* feat_struct;   //e.g. <Gen==$g1, Nb=$n1, Case=gen>
} MU_graph_in_T;

/////////////////////////////////////////////////
// Output of a node, e.g. <Gen=$g1, Nb=$n1, Case=gen>
typedef MU_graph_feat_struct_T MU_graph_out_T;

/////////////////////////////////////////////////
// Label of a node, e.g. $1<Gen==$g1, Nb=$n1, Case=gen>/<Gen=$g1, Nb=sing, Case=$c2>
typedef struct {
  MU_graph_in_T* in;     //e.g. <$1:Gen==$g1, Nb=$n1, Case=gen>; equal to NULL in case of "<E>"
  MU_graph_out_T* out;   //e.g. <Gen=$g1, Nb=sing, Case=$c2>; equal to NULL in case of "<E>"
} MU_graph_label_T;

/////////////////////////////////////////////////

/////////////////////////////////////////////////
// Frees the memory allocated for a MU_graph label.    
// 'liberate_main_structure' = Boolean saying if the main structure should be liberated (1) or not (0)
void MU_graph_free_label(MU_graph_label_T* MU_label, int liberate_main_structure);

/////////////////////////////////////////////////
// Creates a MU_graph label from two strings. 
// 'params' = "global" parameters
// label_in = inout label of the node
// label_out = output label of a node
// 'MU_lemma' = muti-word unit lemma being inflected
// MU_label = output parameter: the resulting graph node
// We suppose that MU_label already has its memory allocated.
// Returns 0 on success, 1 otherwise.
int MU_graph_scan_label(MultiFlexParameters* params,unichar* label_in, unichar* label_out, MU_graph_label_T* MU_label, U_lemma_T* MU_lemma);

/////////////////////////////////////////////////
// Prints a MU_graph label.    
void MU_graph_print_label(MU_graph_label_T* MU_label);

#endif

