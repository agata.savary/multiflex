/*
  * Unitex 
  *
  * Copyright (C) 2001-2008 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *22
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on December 1 2008
 */
//---------------------------------------------------------------------------

/********************************************************************************/
//// Treatment of letter case of a simple or a compound unit                   ///
/********************************************************************************/

#include "Unicode.h"
#include "Alphabet.h"
#include "Error.h"
#include "MF_LetterCase.h"
#include "MF_LangMorpho.h"
#include "MF_Tokenize.h"
#include "MF_Global2.h"

letter_case_T letter_case(Alphabet* alph,unichar* s);
int letter_case_is_all_lower(Alphabet* alph,unichar* s);
int letter_case_is_all_upper(Alphabet* alph,unichar* s);
int letter_case_is_first_upper(Alphabet* alph,unichar* s);
int letter_case_is_no_letter_case(Alphabet* alph,unichar* s);
int letter_case_is_first_upper_each_word(MultiFlexParameters* params,unichar* s);
unichar* letter_case_apply(MultiFlexParameters* params,unichar *s, letter_case_T lc);
unichar* letter_case_to_all_lower(unichar* s);
unichar* letter_case_to_all_upper(unichar* s);
unichar* letter_case_to_first_upper(Alphabet* alph,unichar* s);
unichar* letter_case_apply_compound(MultiFlexParameters* params,unichar* s, letter_case_T lc);
letter_case_T letter_case_deduce_target(Alphabet* alph,unichar* form, unichar* lemma);


////////////////////////////////////////////
// Checks the letter case spelling of a string
// 'params' = "global" parameters
// Return the letter case, or no_letter_case in case of errors
letter_case_T letter_case(MultiFlexParameters* params,unichar* s) {
  
  if (letter_case_is_all_lower(params->alph,s))
    return all_lower;

  if (letter_case_is_all_upper(params->alph,s))
    return all_upper;
  
  if (letter_case_is_first_upper(params->alph,s))
    return first_upper;

  if (letter_case_is_no_letter_case(params->alph,s))
    return no_letter_case;

  if (letter_case_is_first_upper_each_word(params,s))
    return first_upper_each_word;


  return other;
}

////////////////////////////////////////////
// Checks if string s contains only lowercase letters
// Returns 1 if it does, 0 otherwise.
// If s is empty returns 0
int letter_case_is_all_lower(Alphabet* alph,unichar* s) {
  if (!s || !u_strlen(s))
    return 0;
  
  int i; //Index of the current character in s
  for (i=0; i<u_strlen(s); i++)
    if (!is_lower(s[i],alph))
      return 0;

  return 1;
}


////////////////////////////////////////////
// Checks if string s contains only lowercase letters
// Returns 1 if it does, 0 otherwise.
// If s is empty returns 0
int letter_case_is_all_upper(Alphabet* alph,unichar* s) {
  if (!s || !u_strlen(s))
    return 0;
  
  int i; //Index of the current character in s
  for (i=0; i<u_strlen(s); i++)
    if (!is_upper(s[i],alph))
      return 0;

  return 1;
}

////////////////////////////////////////////
// Checks if string s contains only lowercase letters
// Returns 1 if it does, 0 otherwise.
// If s is empty returns 0
int letter_case_is_first_upper(Alphabet* alph,unichar* s) {
  if (!s || !u_strlen(s))
    return 0;
  
  //Check if the first letter is an uppercase
  if (!is_upper(s[0],alph))
    return 0;

  int i; //Index of the current character in s
  for (i=1; i<u_strlen(s); i++)
    if (!is_lower(s[i],alph))
      return 0;

  return 1;
}

////////////////////////////////////////////
// Checks if the letter case is irrelevant for the string 
// I.e. the string is empty or contains no letters
// Returns 1 if it is irrelevant, 0 otherwise.
int letter_case_is_no_letter_case(Alphabet* alph,unichar* s) {
  if (!s || !u_strlen(s))
    return 1;
  
  int i; //Index of the current character in s
  for (i=0; i<u_strlen(s); i++)
    if (is_letter(s[i],alph))
      return 0;

  return 1;
}

////////////////////////////////////////////
// Checks if all words in the compound string s 
// begin with an uppercase and continue with non uppercase letters
// 'params' = "global" parameters
// Returns 1 if it is the case, 0 otherwise.
// If t is empty or in case of errors returns 0
int letter_case_is_first_upper_each_word(MultiFlexParameters* params,unichar* s) {
  if (!s)
    return 0;
  
  int is_fuew; //Boolean saying if the string is of type first_upper_each_word
  is_fuew = 1;
  
  //Tokenize the string, do not eliminate backslashes
  tokenization* t = tokenize(params,s,0);
  if (t && t->nb_tokens != 0) {
    
    int i; //Index of the current token in t
    for (i=0; (i<t->nb_tokens) && (is_fuew); i++) {
      if (! letter_case_is_first_upper(params->alph,t->tokens[i]) && !letter_case_is_no_letter_case(params->alph,t->tokens[i]))
	is_fuew = 0;
    }
    
    //Delete the tokenization
    delete_tokenization(t);
  }
  return is_fuew;
}

///////////////////////////////////////////
// Transforms string 's' so as to obtain the letter case spelling 'lc'
// For lc=other no transformation is done
// E.g. for s="domek" and lc=first_upper, produces "Domek"
//      for s="domek" and lc=all_upper, produces "DOMEK"
//      for s="Domek" and lc=all_lower, produces "domek"
//      for s="DoMek" and lc=other, produces "DoMek"
// 'params' = "global" parameters
// The original string 's' is modified in the function
// The return string is not allocated in the function
// Returns the transformed string or NULL in case of errors.
unichar* letter_case_apply(MultiFlexParameters* params,unichar *s, letter_case_T lc) {
  if (!s || !u_strlen(s) || lc == other || lc == same)
    return s;

  switch (lc) {
  case all_lower: 
    letter_case_to_all_lower(s);
    break;
  case all_upper:   //The string contains only uppercase letters, e.g. "DOMEK"
    letter_case_to_all_upper(s);
    break;
  case first_upper:
    letter_case_to_first_upper(params->alph,s);
    break;
    //  case all_lower_each_word:
    //  case all_upper_each_word:
  case first_upper_each_word:
    letter_case_apply_compound(params,s,lc);
    break;
  default:  //No modification if letter case of type 'no_letter_case' and 'other'
    return NULL; 
    break;
  }
  return s;
}

////////////////////////////////////////////
// Transforms string s to only lowercase letters
// E.g. for s="Domek" produces "domek"
// The original string 's' is modified in the function
// The return string is not allocated in the function
// Returns the transformed string or NULL in case of errors.
unichar* letter_case_to_all_lower(unichar* s) {
  if (!s)
    return NULL;
  
  int i; //Index of the current character in s
  for (i=0; i<u_strlen(s); i++)
    s[i] = u_tolower(s[i]);

  return s;
}

////////////////////////////////////////////
// Transforms string s to only uppercase letters
// E.g. for s="domek" produces "DOMEK"
// The original string 's' is modified in the function
// The return string is not allocated in the function
// Returns the transformed string or NULL in case of errors.
unichar* letter_case_to_all_upper(unichar* s) {
  if (!s)
    return NULL;
  
  int i; //Index of the current character in s
  for (i=0; i<u_strlen(s); i++)
    s[i] = u_toupper(s[i]);

  return s;
}

////////////////////////////////////////////
// Transforms string s to a string with an initial uppercase letter
// and all other letters being lowercase letters
// E.g. for s="domek" produces "Domek"
// The original string 's' is modified in the function
// The return string is not allocated in the function
// Returns the transformed string or NULL in case of errors.
unichar* letter_case_to_first_upper(Alphabet* alph,unichar* s) {
  if (!s)
    return NULL;
  
  //The first character must be a letter
  if (!u_strlen(s) || !is_letter(s[0],alph))
      return NULL;

  //Put the first character into lower case
  s[0] = u_toupper(s[0]);

  //Put the remaining characters into lowercase
  int i; //Index of the current character in s
  for (i=1; i<u_strlen(s); i++)
    s[i] = u_tolower(s[i]);

  return s;
}

////////////////////////////////////////////
// Transforms compound string s to a a given lettercase spelling
// The allowed lettercase spellings are: 
// all_lower_each_word, all_upper_each_word, first_upper_each_word
// 'params' = "global" parameters
// The original string 's' is modified in the function
// The return string is not allocated in the function
// Returns the transformed string or NULL in case of errors.
unichar* letter_case_apply_compound(MultiFlexParameters* params,unichar* s, letter_case_T lc) {
  if (!s)
    return NULL;
  
  //Tokenize the string, do not eliminate backslashes
  tokenization* tt = tokenize(params,s,0);
  if (!tt)
    return NULL;

  //Leave each token whose letter case is irrelevant intact
  //Transform each other token into lowercase
  int t; //Index of the current token in t
  unichar* ss;  //Result of the tranformation of the current token
  for (t=0; t<tt->nb_tokens; t++) {
    if (!letter_case_is_no_letter_case(params->alph,tt->tokens[t]))
      switch (lc) {
	//      case all_lower_each_word :
	//	ss = letter_case_to_all_lower(tt->tokens[t]); 
	//	break;
	//      case all_upper_each_word :
	//	ss = letter_case_to_all_upper(tt->tokens[t]); 
	//	break;
      case first_upper_each_word :
	ss = letter_case_to_first_upper(params->alph,tt->tokens[t]);
	break;
      default :
	return NULL;
      }
    if (!ss) { //If the transformation could not be done for the current token
      delete_tokenization(tt);
      return NULL;
    }
  }

  //Tranform the original string
  int si; //Index of the current character in s
  int ti; //Index of the current character in the current token
  t = 0; //First token
  si = 0;
  ti = 0;
  while (si<u_strlen(s) && t<tt->nb_tokens) {
    //Transform the current token in s
    ti = 0;
    while (si<u_strlen(s) && ti<u_strlen(tt->tokens[t]))
      s[si++] = tt->tokens[t][ti++];
    //If the string is finished but not the token
    if (si>=u_strlen(s) || ti<u_strlen(tt->tokens[t])) {
      delete_tokenization(tt);
      return NULL;
    }
    t++; //Go to the next token
  }
  //If no more tokens but the string is not finished or the opposite
  if ( (si<u_strlen(s) && t>=tt->nb_tokens) || (si>=u_strlen(s) && t<tt->nb_tokens)) {
    delete_tokenization(tt);
    return NULL;
  }

  delete_tokenization(tt);
  return s;
}

////////////////////////////////////////////
// Deduces the constituent's target letter case with respect to the lemma
// If the form is identical to the lemma no particular letter is admitted, i.e. the letter case is 'other'
// Otherwise it is: all_lower, all_upper or first_upper 
// E.g. if 'form' = "Radka" and 'lemma'="Radek" then returns 'same' (means as lemma)
//      if 'form' = "PeKaO" and 'lemma'="PeKaO" then returns 'same' (as lemma)
//      if 'form' = "PeKaO" and 'lemma'="pko" then returns 'other' (this example is undescribable)
//      if 'form' = "Rondo" and 'lemma'="rondo" then returns 'first_upper'
//      if 'form' = "RONDA" and 'lemma'="rondo" then returns 'all_upper'
//      if 'form' = "pampersy" and 'lemma'="Pampers" then returns 'all_lower'
//      if 'form' = "Port Lotniczy" and 'lemma'="port lotniczy" then returns 'all_upper_each_word'
// 'params' = "global" parameters
// Returns the index of this value in the letter case category
// or -1 if not found.
letter_case_T letter_case_deduce_target(MultiFlexParameters* params,unichar* form, unichar* lemma) {
  //Get the letter case of the form and the lemma
  letter_case_T lcf, lcl; //Letter case of the constituent's form and of its lemma
  lcf = letter_case(params,form);
  lcl = letter_case(params,lemma);

  //If the form and the lemma are identical or their letter case is identical, the taget letter case is 'other'
  if (!u_strcmp(form,lemma) || lcf==lcl)
    return same;
  //Otherwise the taget letter case is the same as in the form
  return lcf;
}

