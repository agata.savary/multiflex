/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

/**********************************************************************************/
/* CONVERTING FEATURE STRUCTURES INTO SURFACE FEATURE STRINGS AND VICE VERSA      */
/* (ACCORDING TO MORPHOLOGICAL EQUIVALENCES BETWEEN SIMPLES WORDS AND MULTIFLEX), */
/* e.g <Gen=masc_hum;Nb=plural;Case=mian> <=> :m1:pl:nomin                        */
/**********************************************************************************/

#ifndef FeatStructStrH
#define FeatStructStrH

#include "Unicode.h"
#include "MF_LangMorpho.h"
#include "MF_UnitMorpho.h"
#include "MF_MorphoEquiv.h"
#include "MF_Global2.h"

/****************************************************************************************/
/* "get a feature structure (i.e. a set of category-value pairs) from a string"         */
/* Produces a feature structure, e.g. {<Gen=m1;Case=inst;Nb=pl;Deg=pos> }               */
/* from a feature string (e.g. ":pl:inst:m1:pos").                                      */
/* A valid features string here is a sequence of unique values separated by a FEAT_SEP. */
/* The feature string must correspond to one feature structure only,                    */
/* e.g.":pl:inst:m1.m2.f:pos" is not a correct string as it correponds to 3 feature     */
/* structures.                                                                          */
/* If the feature string has a bad format,or if it is ambiguous (contains multiple      */
/* for one category), or if a string component is not equivalent to a morphological     */
/* feature, or in case of any other problem the function returns NULL                   */
/* The return structure is allocated in the function. The liberation has to take place  */
/* in the calling function (by f_delete_feat_struct).                                   */
/* 'params' = "global" parameters                                                       */
f_feat_struct_T* get_feat_struct_str(MultiFlexParameters* params,unichar* feat_str);

/****************************************************************************************/
/* "get a feature structure set (i.e. a set of category-value sets) from a string"      */
/* Produces a set of feature structures,                                                */
/* e.g. {<Gen=m1;Case=inst;Nb=pl;Deg=pos>,<Gen=m2;Case=inst;Nb=pl;De=pos>,              */
/* <Gen=m3;Case=inst;Nb=pl;Deg=pos>,<Gen=f;Case=inst;Nb=pl;Deg=pos>}                    */
/* from a feature string (e.g. "subst:pl:inst:m1.m2.m3.f|subst:pl:nom.acc:m1")          */
/* A features string is a sequence of feature substrings separated by a colon bar ('|').*/
/* See get_feat_struct_set_substr for the format of a feature substring.                */
/* If the feature string has a bad format or if a string component is not equivalent    */
/* to a morphological feature or in case of any other problem the function returns NULL */
/* The return structure is allocated in the function. The liberation has to take place  */
/* in the calling function (by f_delete_feat_struct_set).                               */
/* 'params' = "global" parameters                                                       */
f_feat_struct_set_T* get_feat_struct_set_str(MultiFlexParameters* params,unichar* feat_str);

/************************************************************************************************/
/* "get a feature structure set and a class (i.e. a set of category-value sets) from a string"  */
/* Produces a set of feature structures,                                                        */
/* e.g. {<Gen=m1;Case=inst;Nb=pl;Deg=pos>,<Gen=m2;Case=inst;Nb=pl;De=pos>,                      */
/* <Gen=m3;Case=inst;Nb=pl;Deg=pos>,<Gen=f;Case=inst;Nb=pl;Deg=pos>}                            */
/* as well an inflection class, e.g. subst                                                      */
/* from a feature string (e.g. "subst:pl:inst:m1.m2.m3.f|subst:pl:nom.acc:m1")                  */
/* A features string is a sequence of feature substrings separated by a colon bar ('|').        */
/* See get_feat_struct_set_substr for the format of a feature substring.                        */
/* If the feature string has a bad format or if a string component is not equivalent            */
/* to a morphological feature or in case of any other problem the function returns NULL         */
/* The return structure is allocated in the function. The liberation has to take place          */
/* in the calling function (by f_delete_feat_struct_set).                                       */
/* 'params' = "global" parameters                                                               */
f_feat_struct_set_T* get_feat_struct_set_class_str(MultiFlexParameters* params,unichar* feat_str,l_class_T** cl);

/**************************************************************************************/
/* Produces a feature string (e.g. ":zen:narz:poj")) from a feature structure         */
/* (e.g.  <Gen=fem;Case=Inst;Nb=sing>).                                               */
/* 'params' = "global" parameters                                                     */
/* The return string is allocated in the function. The liberation has to take place   */
/* in the calling function.                                                           */
/* If 'feat' is empty or a morphological feature has no corresponding string value,   */
/* returns NULL.                                                                      */
unichar* get_str_feat_struct(MultiFlexParameters* params,f_feat_struct_T* feat);

/**************************************************************************************/
/* Returns the class (e.g. noun) corresponding to a class string as it appears in a   */
/* dictionary (e.g. "N"). If no class corresponds to the string, returns NULL.        */
/* The return structure is NOT allocated in the function.                             */
/* 'params' = "global" parameters                                                       */
l_class_T* get_class_str(MultiFlexParameters* params,unichar* cl_str);

/*****************************************************************************/
/* Returns the pointer to the class (e.g. <noun,...>) directly corresponding */
/* to the class string cl_str (e.g. "noun").                                 */
/* If no class corresponds to the string returns NULL.                       */
/* The return structure is NOT allocated in the function.                    */
/* 'params' = "global" parameters                                                       */
l_class_T* get_class_str_direct(MultiFlexParameters* params,unichar* cl_str);

#endif
