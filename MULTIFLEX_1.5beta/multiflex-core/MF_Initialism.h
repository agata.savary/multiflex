/*
  * Unitex 
  *
  * Copyright (C) 2001-2008 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on June 19 2009
 */
//---------------------------------------------------------------------------

/********************************************************************************/
//// Treatment of initialisms of a  unit                                       ///
/********************************************************************************/

#ifndef Initialism_H
#define Initialism_H

#include "Unicode.h"
#include "MF_LangMorpho.h"


/////////////////////////////////////////////////
// Possible initialism variants of a string
typedef enum {
  no_init,         //The string is not modified
  dot,             //The string contains the initial letter followed by a dot, e.g. "J."
  no_dot,           //The string contains the initial letter, e.g. "J"
  dot2,             //The string contains two initial letterw followed by a dot, e.g. "ul."
  no_dot2,           //The string contains two initial letters, e.g. "ul"
  dot3,             //The string contains three initial letters followed by a dot, e.g. "gen."
  no_dot3,           //The string contains three initial letters, e.g. "gen"
  dot4,             //The string contains four initial letters followed by a dot, e.g. "prof."
  no_dot4,           //The string contains four initial letters, e.g. "prof"
  dot5,             //The string contains five initial letters followed by a dot, e.g. "marsz."
  no_dot5,           //The string contains five initial letters, e.g. "marsz"
  } init_T;  

///////////////////////////////////////////
// Transforms string 's' so as to obtain the initialism of type 'init'
// E.g. for s="Jan" and init=no_init, produces "Jan"
//      for s="Jan" and init=dot, produces "J."
//      for s="Jan" and init=no_dot, produces "J"
// Returns the transformed string or NULL in case of errors.
unichar* init_apply(unichar *s, init_T init);

#endif
