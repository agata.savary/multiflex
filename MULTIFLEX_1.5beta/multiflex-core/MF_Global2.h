/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

/************************************************************************************/
/* FEATURE-VALUE REPRESENTATION OF THE MORPHOLOGY OF A SIMPLE OR COMPOUND WORD FORM */
/************************************************************************************/

#ifndef MF_Global2H
#define MF_Global2H

#include "Alphabet.h"
#include "MF_UnifBase.h"
#include "Fst2.h"
#include "MF_LangMorphoBase.h"
#include "MF_MorphoEquivBase.h"


//Maximum number of inflectional transducers
#define N_FST2 3000


///////////////////////////////
// A node of the tree for inflectional transducers' names
struct node {
  int final;  //-1 if the node is non final; otherwise gives the index
              //of the inflection transducer (whose name leads to this node)
              //in the tranducer table
  struct transition* t;
};

///////////////////////////////
// A branch of the tree for inflection transducers' names
struct transition {
    char c;
    struct node* n;
    struct transition* suivant;
};

typedef struct {
	//////////////////////////////////////////////////////////////////////////////////////
	// Constant indication if Multiflex is being run in debug mode
	int debug_status;

	Alphabet* alph;

	char inflection_directory[FILENAME_MAX];

	//DELAC files containing description of compounds being constituents of the compounds to be inflected
	char** embedded_DLC;
	//Number of these files
	int no_embedded_DLC;
	/////////////////////////////////////////////////
	// Current level of embedding (contained between 0 and MAX_EMBED_LEVEL)
	// E.g. for ulica {generala {Jozefa Bema}}
	// the embedding level for {Jozefa Bema} is 2
	int embeddingLevel;

	////////////////////////////////////////////
	// Set of all existing instantiations
	unif_vars_T UNIF_VARS;

	///////////////////////////////
	// Table of inflection tranducers
	Fst2* fst2[N_FST2];
	struct node* root;
	int n_fst2;
	int config_files_status;

	///////////////////////////////////////////////////////////////////////////////////////
	// structure describing the morphological categories of a language
	l_cats_T L_CATS;

	///////////////////////////////////////////////////////////////////////////////////////
	// structure describing the morphological system of a language
	l_classes_T L_CLASSES;

	//////////////////////////////////////////////////////////////////////////////////////
	// structure describing the morphological equivalences between values for simple words and those in Multiflex
	l_morpho_equiv_T L_MORPHO_EQUIV;

	//////////////////////////////////////////////////////////////////////////////////////
	// structure describing the equivalences between class names for simple words (e.g. "N")
	// and language classes in Multiflex(e.g. noun)
	l_class_equiv_T L_CLASS_EQUIV;

} MultiFlexParameters;

///////////////////////////////
// Initialize the "global" Multiflex parameters
MultiFlexParameters* new_MultiFlexParameters();

//////////////////////////////////////////////
// Liberate the structure for the "global" Multiflex parameters
void free_MultiFlexParameters(MultiFlexParameters* params);

///////////////////////////////
// Try to load the transducer flex and returns its position in the
// 'fst2' array. Return -1 if the transducer cannot be loaded.
// 'params' = "global" parameters
int get_transducer(MultiFlexParameters* params,char* flex);

#endif
