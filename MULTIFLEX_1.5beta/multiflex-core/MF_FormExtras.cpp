/*
  * Unitex 
  *
  * Copyright (C) 2001-2008 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on September 15 2009
 */
//---------------------------------------------------------------------------

/********************************************************************************/
//// Handling extra information attached to an inflected form                  ///
/********************************************************************************/

#include "MF_FormExtras.h"
#include "MF_GraphDebug.h"
#include "Error.h"

int fe_enlarge_extras(form_extras_T* extras, extras_elem_T* new_elem);

////////////////////////////////////////////
// Initialize the 'extras' with null values
// We suppose that 'extras' has its space allocated
void fe_init_extras(form_extras_T* extras) {
  gp_init_graph_paths(extras);
}

////////////////////////////////////////////
// Initialize the 'extras' with one path containing the element 'e'
// We suppose that 'extras' has its space allocated
void fe_init_extras(form_extras_T* extras, extras_elem_T* e) {
  fe_init_extras(extras);
  gp_add_empty_path(extras);
  fe_enlarge_extras(extras,e);
}

////////////////////////////////////////////
// Allocate a space for an empty 'extras' structure
// And initialize it to an empty set
void fe_create_extras(form_extras_T** extras) {
  *extras = (form_extras_T*)malloc(sizeof(form_extras_T));
  if (!*extras)
    fatal_error("Not enough memory in function 'create_extras'\n");
  fe_init_extras(*extras);
}


////////////////////////////////////////////
// Add a new path 'new_path' to the set of paths 'paths' without repetitions
// We suppose that 'paths' has its space allocated
// Returns 0 on success, 1 otherwise.   
int fe_merge_extras(form_extras_T* extras1, form_extras_T* extras2) {
  return gp_merge_paths(extras1, extras2);
}

////////////////////////////////////////////
// Enlarges the extras by a new element
// Returns 0 on success, 1 on error
int fe_enlarge_extras(form_extras_T* extras, extras_elem_T* new_elem) {
  return gp_add_transition_or_state(extras, *new_elem);
}

////////////////////////////////////////////
// Duplicate the extras 'e2' into 'e1'
// 'p1e is allocated but empty
// Returns 0 on success, 1 on error
int fe_duplicate_extras(form_extras_T* e1,form_extras_T* e2) {
  return gp_duplicate_graph_paths(e1,e2);
}

////////////////////////////////////////////
// Gets a string from the extras. 
// The return string is allocated in the function. The liberation has to take place  
// in the calling function.                                                         
unichar* fe_get_str_extras(form_extras_T* extras) {
  return gp_get_str_graph_paths(extras);
}

////////////////////////////////////////////
// Prints a set of paths
void fe_print_extras(form_extras_T* extras) {
  gp_print_graph_paths(extras);
}

////////////////////////////////////////////
// Deletes the 'extras'
// The main structure is not liberated
void fe_delete_extras(form_extras_T* extras) {
  gp_delete_graph_paths(extras);
}
  
