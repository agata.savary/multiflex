/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

/************************************************************************************/
/* FEATURE-VALUE REPRESENTATION OF THE MORPHOLOGY OF A SIMPLE OR COMPOUND WORD FORM */
/************************************************************************************/

#ifndef FormMorphoH
#define FormMorphoH

#include "Unicode.h"
#include "MF_LangMorpho.h"
#include "MF_Global2.h"
#include "MF_FeatStructBase.h" 

////////////////////////////////////////////
// Creates the feature structure of a form.
// Returns 1 in case of error, 0 otherwise
void f_create_feat_struct(f_feat_struct_T **feat);

////////////////////////////////////////////
// Creates the set of feature structures.
// Returns 1 in case of error, 0 otherwise
void f_create_feat_struct_set(f_feat_struct_set_T **feat_struct_set);

////////////////////////////////////////////
// Initializes the feature structure of a form.
void f_init_feat_struct(f_feat_struct_T *feat);

////////////////////////////////////////////
// Initializes the set of feature structures so that it contains no element.
void f_init_feat_struct_set(f_feat_struct_set_T *feat_struct_set);

////////////////////////////////////////////
// Initializes the set of feature structures so that it contains one empty element.
// Returns 0 on success, 1 otherwise
void f_init_feat_struct_set_one_empty(f_feat_struct_set_T *feat_struct_set);

////////////////////////////////////////////
// Deletes the feature structure of a form.
void f_delete_feat_struct(f_feat_struct_T **feat_struct);

////////////////////////////////////////////
// Deletes a set of feature structures of a form.
// 'feat_struct_set' is transferred as a double pointer so that
// it can remain NULL after the function return
void f_delete_feat_struct_set(f_feat_struct_set_T **feat_struct_set);

////////////////////////////////////////////
// Checks if two feature structures agree
// fs1, fs2 = the feature structures to be compared
// empty_values (0 or 1) = says if expty values are admitted
// If empty_values=0: returns 1 if fs1 and fs2 contain exactly the same values
// (note necessarily in the same order).
// If empty_values=0: returns 1 if fs1 and fs2 contain exactly the same values
// (note necessarily in the same order) except the empty values.
// In this case if an empty feature is present in one set then the 
// corresponding category in the other set must also be empty.
// e.g. if feat1=<Gen=m;Nb=pl> and feat2=<Gen=m;Nb=pl; Gr=<E>> then both agree
// But if feat1=<Gen=m;Nb=pl; Gr=D> and feat2=<Gen=m;Nb=pl> then they do not agree
// Otherwise returns 0.
int f_feat_struct_agreement(f_feat_struct_T* fs1, f_feat_struct_T* fs2, int empty_values);

////////////////////////////////////////////
// Modifies the feature structure of a form.
// Each category-value appearing in 'new_feat' replaces the corresponding category in 'old_feat',
// e.g. if 'old_feat'={Gen=fem, Nb=sing} and 'new_feat'={Nb=pl} then 'old_feat' becomes {Gen=fem, Nb=pl}
// If a category appearing in 'new_feat' does not appear in old feat, and if this category admits
// an empty value than the category-value pair from 'new_feat' is added to 'old_feat'
// e.g. if Usage admits an empty value, and 'old_feat'={Gen=fem, Nb=sing}, and 'new_feat'={Nb=pl; Usage=offic}
// then 'old_feat' becomes {Gen=fem, Nb=pl; Usage=offic}
// 'sorted' - boolean indicating if the resulting feature structure is to be sorted (with respect to the order of categories in the language file)
// 'params' = "global" parameters
// Returns 0 on success, 1 otherwise.
int f_change_feat_struct(MultiFlexParameters* params,f_feat_struct_T *old_feat, f_feat_struct_T *new_feat, int sorted);

////////////////////////////////////////////
// Enlarges the feature structure of a form.
// The category-value pair created from 'cat' and 'val' is added to 'feat_struct',
// e.g. if 'feat_struct'={Gen=fem}, 'cat'=Nb, nb=(sing,pl) and 'val'=1 then 'feat_struct' becomes {Gen=fem, Nb=pl}.
// We assume that 'val' is a valid index in the domain of 'cat.
// If 'cat' with value 'val' already appears in 'feat_struct', no change is done to feat_struct
// but the function does not return an error. 
// If 'cat' already appears in 'feat_struct' with a value different from 'val', the function returns an error.
// 'sorted' - boolean indicating if the resulting feature structure is to be sorted (with respect to the order of categories in the language file)
// 'params' = "global" parameters
// Returns 0 on success, returns 1 in case of errors.
int f_enlarge_feat_struct(MultiFlexParameters* params,f_feat_struct_T *feat_struct, l_category_T *cat, int val, int sorted);

////////////////////////////////////////////
// Enlarges the feature structure of a form on a basis of char data.
// The category-value pair created from 'cat' and 'val' is added to 'feat_struct',
// e.g. if 'feat_struct'={Gen=fem}, 'cat'="Nb" and 'val'="pl" then 'feat_struct' becomes {Gen=fem, Nb=pl}.
// 'sorted' - boolean indicating if the resulting feature structure is to be sorted (with respect to the order of categories in the language file)
// Returns 0 on success, returns 1 if 'cat' already appears in 'feat_struct'.
// Returns -1 if 'cat' or 'val' invalid category or value names in the current language.
int f_enlarge_feat_struct_unichar(f_feat_struct_T *feat_struct, unichar *cat, unichar* val, int sorted);

////////////////////////////////////////////
// Enlarges each feature structure in a set of structures.
// The category-value pair created from 'cat' and 'val' is added to each structure in 'feat_struct_set',
// e.g. if 'feat_struct_set'={{Gen=fem},{Gen=masc}}, 'cat'=Nb, nb=(sing,pl) and 'val'=1 then 'feat_struct_set' becomes {{Gen=fem, Nb=pl},{Gen=masc,Nb=pl}}.
// We assume that 'val' is a valid index in the domain of 'cat.
// 'sorted' - boolean indicating if the resulting feature structure is to be sorted (with respect to the order of categories in the language file)
// 'params' = "global" parameters
// Returns 0 on success, returns 1 if 'cat' already appears in 'feat_struct_set'.
int f_enlarge_feat_struct_set(MultiFlexParameters* params,f_feat_struct_set_T *feat_struct_set, l_category_T *cat, int val, int sorted);

/////////////////////////////////////////////////
// Copy an existing feature structure to a new one.
// The new structure must be allocated by the calling function.
// The copies of the category-value pairs are allocated in the function.
// Returns 1 in case of error, 0 otherwise.
int f_copy_feat_struct(f_feat_struct_T *new_feat_struct,f_feat_struct_T* old_feat_struct);

/////////////////////////////////////////////////
// Duplicates and returns a feature structure (the new structure is allocated
// in the function). 
// Returns NULL in case of error.
f_feat_struct_set_T* f_duplicate_feat_struct_set(f_feat_struct_set_T* old_set);

////////////////////////////////////////////
// Merges two sets of feature structures.
// The resulting set is kept in 'feat_struct_set'.
// The merged set 'new_feat_struct_set' is not deleted.
// Returns 0 on success, 1 otherwise
int f_merge_feat_struct_sets(f_feat_struct_set_T *feat_struct_set, f_feat_struct_set_T *new_feat_struct_set);

////////////////////////////////////////////
// Reduces the feature structure of a form.
// Category-value corresponding to 'cat' is deleted from 'feat_struct',
// e.g. if 'feat_struct'={Gen=fem,Nb=pl} and 'cat'=Gen then 'feat_struct' becomes {Nb=pl}
// Returns 0 on success, returns 1 if 'cat' not found in 'feat_struct'.
int f_del_one_catval(f_feat_struct_T *feat_struct, l_category_T* cat);

////////////////////////////////////////////
// Reads the feature structure of a form.
// Returns the value of the category 'cat' in 'feat_struct' (i.e. the index of the value in the domain of 'cat'),
// e.g. if 'feat'={Gen=neu,Nb=pl}, 'cat'=Gen, and Gen={masc,fem,neu} then return 2.
// Returns -1 if 'act' not found in 'feat_struct'.
int f_get_value(f_feat_struct_T *feat_struct, l_category_T* cat);

/////////////////////////////////////////////////
// Prints the contents of a form's feature structure.
// Returns 0.
int f_print_feat_struct(f_feat_struct_T *feat_struct);

#endif
