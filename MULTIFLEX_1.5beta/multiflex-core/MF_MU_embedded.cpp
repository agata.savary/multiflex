/*
  * Unitex 
  *
  * Copyright (C) 2001-2008 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on September 9 2008
 */
//---------------------------------------------------------------------------

#include "Unicode.h"
#include "Error.h"
#include "MF_MU_embedded.h"
#include "MF_DLC_inflect.h"
#include "MF_Util.h"
#include "MF_Global2.h"

/**
 * 
 * This library is used to get the inflection annotation of embedded compounds
 * i.e. being components of compounds to be inflected.
 * 
 */


int get_MU_annotation_file(MultiFlexParameters* params,FILE* dlc, unichar* lemma_str, l_class_T* cl, int homonym, U_lemma_T** lemma);
int MU_line_cmp(MultiFlexParameters* params,unichar* line, unichar* lemma_str, l_class_T* cl);
int get_MU_annotation_line(MultiFlexParameters* params,unichar*line, U_lemma_T** lemma);


/////////////////////////////////////////////////
// Looks for the inflectional annotation of the (compound) unit 'lemma'
// in the list of embedded dlc files.
// 'params' = "global" parameters 
// 'lemma_str' = (compound) lemma string
// 'cl' = class of the lemma
// 'homonym' = homonym number of the lemma, starting from 0 (the 'embedded_DLC' files
//             are examined in their order of appearence, the n-th annotation of 'lemma'
//             is returned, where n=homonym)
// 'lemma' = return parameter containing the structure of the lemma given by the annotation
//           *lemma is allocated but empty, **lemma gets allocated by the function (if the right annotation found)
// Return 1 in case of problems, 0 otherwise
int get_MU_annotation(MultiFlexParameters* params,unichar* lemma_str, l_class_T* cl, int homonym, U_lemma_T** lemma) {
  if (!lemma || !cl || homonym<0)
    return 1;
  if (u_strlen(lemma_str) == 0)
    return 0;

  *lemma = NULL;

  int ed = 0; //Index of the current embedded dlc file
  FILE* dlc; //Current embedded dlc file
  int nb_ann; //Number of relevant lemma annotations found in a file
  int found = 0;  //Boolean saying if the right annotation has already been found
  while (ed<params->no_embedded_DLC && !found) {
    
    //Open current dlc file
    dlc = u_fopen(params->embedded_DLC[ed],U_READ);
    if (!dlc) {
     error("Unable to open dlc file: '%s' !\n", params->embedded_DLC[ed]);
     return 1;
    }
    
    //Get the annotations of the desired lemma
    nb_ann = get_MU_annotation_file(params,dlc, lemma_str, cl, homonym, lemma);
    if (nb_ann < 0)
      return 1;
    //If the homym number is lower and the number of relevant annotations found in the file
    //then the right annotation has been found
    if (homonym < nb_ann)
      found = 1;
    else {
      //If some relevant annotations found, the next homonym number must be deremented
      homonym = homonym - nb_ann; 
      ed++;  //Go to the next file
    }
    //Close the current file
    u_fclose(dlc);
  }
  if (!found)
    return 1;

  return 0;
}

/////////////////////////////////////////////////
// Looks for the inflectional annotations of the (compound) 'lemma'
// in the delac file of embedded compounds. 
// 'params' = "global" parameters 
// 'lemma_str' = (compound) lemma string
// 'cl' = class of the lemma
// 'homonym' = homonym number of the lemma, starting from 0
//  (the n-th annotation of 'lemma' is returned, where n=homonym)
// 'lemma' = return parameter containing the desired lemma annotation 
//           *lemma is allocated but empty, **lemma gets allocated by the function (if the right annotation found)
//           NULL if the desired lemma not found in the file
// Return the number of relevant lemma annotations found, -1 on case of errors.
int get_MU_annotation_file(MultiFlexParameters* params,FILE* dlc, unichar* lemma_str, l_class_T* cl, int homonym, U_lemma_T** lemma) {
  unichar dlc_line[MAX_DLC_LINE];  //current DELAC line 
  int l;  //length of the line scanned
  int ann_found; //Number of relevant lemma annotations foundor
  int found; //Boolean indicating if the right annotation has been found
  int err; //Error code
  int cmp; //Result of the comparison of two compounds

  found = 0;
  ann_found = 0;
  //Read a line
  l = u_fgets(dlc_line,MAX_DLC_LINE-1,dlc);
  //Omit the final newline
  u_delete_newline(dlc_line);

  //If a line is empty the file is not necessarily finished. 
  //If the last entry has no newline, we should not skip this entry
  while (!found && (l>0 || !feof(dlc))) {  
    //If the current line contains a relevant annotation
    cmp = MU_line_cmp(params,dlc_line, lemma_str, cl);
    //    if (cmp < 0)
    //      return -1;
    if (cmp == 0) {
      ann_found++;  //A new relevant annotation found
      if (homonym == 0) {
	err = get_MU_annotation_line(params,dlc_line, lemma);
	if (err)
	  return -1;
	else
	  found = 1;
      }
      else
	homonym--;
    }
   
    //Read the next line
    l = u_fgets(dlc_line,MAX_DLC_LINE-1,dlc);
    //Omit the final newline
    u_delete_newline(dlc_line);
  }

  return ann_found;
}

/////////////////////////////////////////////////
// Compares a delac line with the (compound) lemma, and category
// 'params' = "global" parameters 
// 'line' = a dlc line
// 'lemma_str' = (compound) lemma string
// 'cl' = class of the lemma
// Returns 0 if the line contains an annotation of the lemma.
// Returns 1 if the line contains an annotation of a different lemma
// Retuns -1 in case of any problems
int MU_line_cmp(MultiFlexParameters* params,unichar* line, unichar* lemma_str, l_class_T* cl) {
  if (!line || !lemma_str || !cl)
    return -1;

  //Scan the DLC line without embedding
  int err; //Error code
  DLC_entry_T dlc_entry;
  init_DLC_entry(&dlc_entry);
  err = DLC_line2entry(params,line, &dlc_entry, 0);
  if (err)
    return err;
  
  //Get the lemma string from line
  unichar* l_str; //Lemma string extracted from the line
  int ll;  //Length of the lemma string 
  ll = U_get_lemma_str_len(dlc_entry.lemma);
  if (ll <0) 
    return -1;
  l_str = (unichar*) malloc((ll+1)*sizeof(unichar));
  if (!l_str)
    fatal_error("Memory allocation problem in function 'MU_line_cmp'!\n");
  err = U_get_lemma_str(l_str, ll+1 , dlc_entry.lemma);
  if (err)
    return -1;

  //Compare the lemma strings and the categories
  int different; //Boolean saying if the lemmas or the classes are different
  if (!u_strcmp(l_str,lemma_str) && dlc_entry.lemma->cl==cl)
    different = 0;
  else
    different = 1;
  
  free(l_str);
  DLC_delete_entry(&dlc_entry);
  return different;
}

/////////////////////////////////////////////////
// Scans a delac line and creates the lemma structure
// 'params' = "global" parameters 
// 'line' = a dlc line to be scanned
// 'lemma' = return structure
// Returns 1 in case of errors, 0 otherwise
int get_MU_annotation_line(MultiFlexParameters* params,unichar*line, U_lemma_T** lemma) {
  if (!line || !lemma)
    return 1;
  
  int err; //Error code
  DLC_entry_T DLC_entry; //Delac entry obtained from the current line
  init_DLC_entry(&DLC_entry);
  err = DLC_line2entry(params,line, &DLC_entry,1);
  if (err) {
    DLC_delete_entry(&DLC_entry);
    return 1;
  }
  else {
    *lemma = (U_lemma_T*)malloc(sizeof(U_lemma_T));
    if (! *lemma)
      fatal_error("Memory allocation problem in function 'get_MU_annotation'!\n");
    err = U_duplicate_lemma(*lemma, DLC_entry.lemma);
    DLC_delete_entry(&DLC_entry);
    if (err) {
      U_delete_lemma(*lemma,1);
      return 1;
    }
  }
  return 0;
}

/////////////////////////////////////////////////
// Initializes the level of embedding to 0
// 'params' = "global" parameters 
void initEmbeddingLevel(MultiFlexParameters* params) {
	params->embeddingLevel = 0;
}

/////////////////////////////////////////////////
// Returns the current level of embedding to 0
// Returns -1 on error
// 'params' = "global" parameters 
int getEmbeddingLevel(MultiFlexParameters* params) {
  if (0 <= params->embeddingLevel && params->embeddingLevel <= MAX_EMBED_LEVEL)
    return params->embeddingLevel;
  else
    return -1;
}

/////////////////////////////////////////////////
// Increments the current level of embedding
// Returns the current level or -1 on error
// 'params' = "global" parameters 
int incrementEmbeddingLevel(MultiFlexParameters* params) {
  if (0 <= params->embeddingLevel && params->embeddingLevel < MAX_EMBED_LEVEL) {
	  (params->embeddingLevel)++;
    return params->embeddingLevel;
  }
  else
    return -1;
}

/////////////////////////////////////////////////
// Decrements the current level of embedding
// Returns the current level or -1 on error
// 'params' = "global" parameters 
int decrementEmbeddingLevel(MultiFlexParameters* params) {
  if (params->embeddingLevel > 0) {
    (params->embeddingLevel)--;
    return params->embeddingLevel;
  }
  else
    return -1;
}

/////////////////////////////////// 
// Initializes the structure of DELAC files containing
// components of the compounds to be inflected
// list = the list of file paths
// no_files = number of these files
// 'params' = "global" parameters 
// If any problem occurs, returns 1. Otherwise returns 0.
int init_embedded_dlc(MultiFlexParameters* params, char** files, int no_files) {
	params->no_embedded_DLC = no_files;
	params->embedded_DLC = (char**) malloc(params->no_embedded_DLC * sizeof(char*));
  if (!params->embedded_DLC)
    fatal_error("Not enough memory in function main\n");
  int ed; //Index of the current file of embedded compounds
  for (ed=0; ed<params->no_embedded_DLC; ed++) {
	  params->embedded_DLC[ed] = (char*) malloc((strlen(files[ed])+1)* sizeof(char));
    if (!params->embedded_DLC[ed])
      fatal_error("Not enough memory in function main\n");
    strcpy(params->embedded_DLC[ed], files[ed]);
  }
  return 0;
}

/////////////////////////////////// 
// Liberates the structure of DELAC files containing
// components of the compounds to be inflected
// 'params' = "global" parameters 
void free_embedded_dlc(MultiFlexParameters* params) {
  if (params->embedded_DLC) {
    int f; //Index of the embedded delac file
    for (f=0; f<params->no_embedded_DLC; f++)
      free(params->embedded_DLC[f]);
    free(params->embedded_DLC);
    params->embedded_DLC = NULL;
  }
}
