/*
  * Unitex 
  *
  * Copyright (C) 2001-2007 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on July 11 2005
 */
//---------------------------------------------------------------------------

/********************************************************************************/
//// Inflection graphs for multi-word units //////////
/********************************************************************************/

#ifndef MU_GraphH
#define MU_GraphH

#include "MF_Global2.h"
#include "Transitions.h"

/////////////////////////////////////////////////
// In the graph not yet loaded loads it otherwise searches for it in the structure.
// 'params' = "global" parameters 
// T = return parameter - index of the inflection transducer corresponding to 'graph_name' in the transducer table
// On success returns the index of the graph's initial state, otherwise returns -1.
int MU_graph_get_initial(MultiFlexParameters* params,char* graph_name, int* T);

/////////////////////////////////////////////////
// Get the state whose index is q_no in the transducer whose index is T
// 'params' = "global" parameters 
// Return the pointer to this state or NULL in case of errors
Fst2State MU_graph_get_state(MultiFlexParameters* params,int q_no, int T);

/////////////////////////////////////////////////
// Get the label of the transition t in the transducer whose index is T
// 'params' = "global" parameters 
// Return the pointer to this label or NULL in case of errors
Fst2Tag MU_graph_get_label(MultiFlexParameters* params,Transition* t, int T);

/////////////////////////////////////////////////
// Get the index of label of the transition t
// Return this index or -1 in case of errors
int MU_graph_get_label_number(Transition* t);

/////////////////////////////////////////////////
// Get the arrival state of the transition t 
// Return the index of this state or -1 in case of errors
int MU_graph_get_arrival_state(Transition* t);

#endif
