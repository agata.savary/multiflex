/*
  * Unitex 
  *
  * Copyright (C) 2001-2008 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on September 3 2008
 */
//---------------------------------------------------------------------------

/********************************************************************************/
//// Morphology and inflection of a simple unit or a multi-word unit ///
/********************************************************************************/

#include "MF_UnitMorpho.h"
#include "MF_SU_morpho.h"
#include "MF_Unif.h"
#include "MF_MU_graph_explore.h"
#include "MF_Tokenize.h"
#include "MF_Initialism.h"
#include "MF_UnitMorphoBase.h"
#include "Error.h"
#include "Alphabet.h"
#include "MF_CurrentForms.h"
#include "MF_Global2.h"

int U_inflect(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_lemma_T* lemma, f_feat_struct_T* feat, U_forms_T* forms);
int MU_inflect(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_lemma_T* lemma, f_feat_struct_T* feat, U_forms_T* forms);
int MU_get_all_forms(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_lemma_T* lemma,U_forms_T* forms);
int U_duplicate_desired_forms(U_forms_T* all_forms,f_feat_struct_T* desired_features,U_forms_T* forms);
int U_apply_graphical_feat(MultiFlexParameters* params,U_forms_T* all_forms,f_feat_struct_T* desired_features,U_forms_T* forms);
int U_apply_one_graphical_feat(MultiFlexParameters* params,U_form_id_T* form, f_catval_T* cv);
int U_add_form(U_form_id_T* new_form,U_forms_T* forms, int unique);
int U_duplicate_form_id(U_form_id_T* f1,U_form_id_T* f2);
int U_duplicate_lemma(U_lemma_T* l1,U_lemma_T* l2);
int U_copy_forms(U_forms_T* new_forms,U_forms_T* old_forms);
int U_duplicate_forms(U_forms_T* new_forms,U_forms_T* old_forms);
int U_form_cmp(U_form_id_T* f1, U_form_id_T* f2);
int U_lemma_cmp(U_lemma_T* l1, U_lemma_T* l2);
int U_forms_cmp_ordered(U_forms_T* fs1, U_forms_T* fs2);
void U_init_forms(U_forms_T* forms);
void U_add_empty_form(U_forms_T* forms, form_extras_T* extras);
int U_add_invariable_form(unichar* new_form,U_forms_T* forms, form_extras_T* extras);
int U_concat_forms(U_forms_T* front_forms, U_forms_T* back_forms, U_forms_T* forms);
int U_merge_forms(U_forms_T* forms, U_forms_T* new_forms);
int U_concat_extras(U_forms_T* forms, extras_elem_T* extra);
void U_print_f(U_form_id_T* f);
void U_print_forms(U_forms_T* F);
void U_print_lemma(U_lemma_T* l);
void U_delete_f(U_form_id_T* f);
void U_delete_forms(U_forms_T* F);
void U_delete_lemma(U_lemma_T* l, int liberate_main_structure);
int U_is_compound(MultiFlexParameters* params,unichar* form);
int U_get_lemma_str(unichar* lemma_str, int max, U_lemma_T* lemma);
int U_get_lemma_str_len(U_lemma_T* lemma);

////////////////////////////////////////////
// For a given (simple or multi-word) unit, generates all its inflected forms,
// corresponding to the given inflection features 'feat'
// The forms generated are put into 'forms'.
// Forms has its space allocated and initialized
// E.g. if feat = {Gen=fem}, and lemma = ["cousin germain",subst,NC-CXC]
// then the generated forms are  {["cousine germaine",{Gen=fem,Nb=sing}],["cousines germaines",{Gen=fem,Nb=pl}]}
// E.g. if feat = {Gen=fem}, lemma = ["cousin",subst,N32], and lc = first_upper
// then the generated forms are  {["Cousine",{Gen=fem,Nb=sing}],["Cousines",{Gen=fem,Nb=pl}]}
// If feat is NULL, all inflected forms are to be generated
// If feat is [0,NULL] no inflected form is to be generated
// 'params' = "global" parameters 
// 'CURRENT_FORMS' = forms generated up till now
// Returns 0 on success, 1 otherwise.   
int U_inflect(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_lemma_T* lemma, f_feat_struct_T* feat, U_forms_T* forms){
  int err; //Error code if any

  //If a simple unit, call the underlying module for simple words
  if (lemma->type == simple)
    //    err = SU_inflect(lemma, basic_feat, forms);
    err = SU_inflect(params,CURRENT_FORMS,lemma, feat, forms);

  //If a compound then inflect it
  else
    err = MU_inflect(params,CURRENT_FORMS,lemma, feat, forms);

  if (err)
    return 1;
  return 0;
}

////////////////////////////////////////////
// For a given multi-word unit, generates all its inflected forms,
// corresponding to the given inflection features 'feat'
// The forms generated are put into 'forms'.
// Forms has its space allocated and initialized
// E.g. if feat = {Gen=fem}, and lemma = ["cousin germain",subst,NC-CXC]
// then the generated forms are  {["cousine germaine",{Gen=fem,Nb=sing}],["cousines germaines",{Gen=fem,Nb=pl}]}
// If feat is NULL, all inflected forms are to be generated
// If feat is [0,NULL] no inflected form is to be generated
// 'params' = "global" parameters 
// 'CURRENT_FORMS' = forms generated up till now
/// Returns 0 on success, 1 otherwise.
int MU_inflect(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_lemma_T* lemma, f_feat_struct_T* feat, U_forms_T* forms){
  int err;  //Error code if any
  int unfound; //Boolean saying if the lemma's forms failed to be found in the current forms

  //If no form required return
  if (feat && feat->no_catvals == 0)
    return 0;

  U_forms_T all_forms;  //All inflected forms of the current lemma
  U_init_forms(&all_forms);
  //Check if the forms have already been generated
  unfound = get_current_form_list(CURRENT_FORMS,lemma, &all_forms);
  
  //If not yet generated, generate them all
  if (unfound) {

    //Generate all inflected forms of the new MU lemma
    err = MU_get_all_forms(params,CURRENT_FORMS,lemma, &all_forms);
    if (err) 
      return 1;
 
    //Add the new forms to the current ones (all_forms becomes empty)
    err = add_current_forms(CURRENT_FORMS,lemma,&all_forms);
    if (err) {
      U_delete_forms(&all_forms);
      return 1;
    }
  }

  //If all forms required return them all
  if (!feat) {
    err = U_copy_forms(forms,&all_forms);
    U_delete_forms(&all_forms);
    return err;
  }

  //Otherwise choose only the desired forms
  U_forms_T desired_forms;
  U_init_forms(&desired_forms);  
  err = U_duplicate_desired_forms(&all_forms, feat, &desired_forms);
  if (err) {
     U_delete_forms(&all_forms);
     U_delete_forms(&desired_forms);
     return 1;
  }

  //Apply the post-inflection graphical forms, e.g. Init, LetterCase
  err = U_apply_graphical_feat(params,&desired_forms,feat,forms);
  U_delete_forms(&all_forms);
  U_delete_forms(&desired_forms);
     
  return err;
}

////////////////////////////////////////////
// For a given multi-word unit, generates all its inflected forms,
// The forms generated are put into 'forms'.
// Forms has its space allocated and initialized
// 'params' = "global" parameters 
// 'CURRENT_FORMS' = forms generated up till now
/// Returns 0 on success, 1 otherwise.
int MU_get_all_forms(MultiFlexParameters* params,U_form_list_set_T* CURRENT_FORMS,U_lemma_T* lemma,U_forms_T* forms) {
  //Explore the inflection tranducer and produce the inflected forms
  if (MU_graph_explore_graph(params,CURRENT_FORMS,lemma,forms))
    return 1;
  else
    return 0;
}

///////////////////////////////////////////////
//Determine the full list of desired features for the unit's inflected form
// 'params' = "global" parameters 
//u = current unit to be inflected
//desired_feat = the features desired for the unit
//full_feat = output parameter: the full list of desired features for the unit
//Each desired feature must:
//   - be 'basic' or 'extra' and its category must appear in the features of the current unit
//   - be graphical and its category may or may not appear in the current unit
//E.g. if u->form="Dom", u->lemma->simple_lemma="dom",  desired_feat=<Case=acc;Usage=neut;Init=dot> and u->feat=<Nb=sing;Gen=m3;Case=nom;Usage=offic>
//     then full_feat = <Nb=sing;Gen=m3;Case=acc;Usage=dot;Init=dot;LetterCase=first_upper>
//Return 1 on serror, 0 otherwise.
int U_determine_full_features(MultiFlexParameters* params,U_form_id_T* u, f_feat_struct_T* desired_feat, f_feat_struct_T* full_feat) {
  int err; //Error code if any
  err = f_copy_feat_struct(full_feat, u->feat);
  if (err) {
    error("The morphological features of constituent %S cannot be determined\n", u->form);
    return err;
  }

  //Change the features to adapt them to the desired features, e.g. if 'feat'=<Nb=pl> then old_feat<-<Nb=pl; Gen=fem>
  //The resulting feature structure should be sorted (w.r.t. the caterories' rangs in the language morphology file)
  err = f_change_feat_struct(params,full_feat,desired_feat,1);
  if (err) {
    error("The morphological features of constituent %S cannot agree with:\n", u->form);
    f_print_feat_struct(desired_feat);
    return err;
  }

  //Treat the letter case if it is not implicitly indicated
  unichar lc_cat_str[MAX_MORPHO_NAME]; //LetterCase category name
  l_category_T* lc_cat; //LetterCase category
  u_strcpy(lc_cat_str,"LetterCase");
  //LetterCase category must be defined and not specified explicitly in full_feat
  if ((lc_cat = is_valid_cat(params,lc_cat_str)) && (f_get_value(full_feat,lc_cat)==-1)) {
    
    //Deduce the letter case of the resulting forms
    letter_case_T lc; //Target letter case of the current constituent
    int lemma_len; //Length of the lemma
    lemma_len = U_get_lemma_str_len(u->lemma);
    unichar* lemma_str; //Lemma string
    lemma_str = (unichar*) malloc((lemma_len+1) * sizeof(unichar));
    if (!lemma_str)
      fatal_error("Memory allocation problem in function 'MU_graph_get_unit_forms'!\n");
    if (U_get_lemma_str(lemma_str, lemma_len+1, u->lemma)) 
      return 1;
    lc = letter_case_deduce_target(params,u->form, lemma_str);
    f_enlarge_feat_struct(params,full_feat,lc_cat,int(lc),1); //The resulting feature structure should be sorted
    free(lemma_str);
  }

  return 0;
}

////////////////////////////////////////////
// Input:
//    all_forms: a set of inflected forms
//    desired_features: feature structure of the desired forms, e.g. {Gen=fem, Case=Inst, Usage=neut, LetterCase=first_upper}, or {} (if separator), the features of type 'graphical' may be neglected if they do not appear in all_forms
// Output:
//    forms - set of the inflected forms corresponding to the desired features
//        e.g. (3,{[reka,{Gen=fem,Nb=sing,Case=Instr}],[rekami,{Gen=fem,Nb=pl,Case=Instr}],[rekoma,{Gen=fem,Nb=pl,Case=Instr}]})
//        or   (1,{["-",{}]})
// 
// Returns 0 on success, 1 otherwise.   
int U_duplicate_desired_forms(U_forms_T* all_forms,f_feat_struct_T* desired_features,U_forms_T* forms){
  int f; //Index of the current form

  if (!all_forms)
    return 1;

  for (f=0; f<all_forms->no_forms; f++)
    //Check if features of the current form agree with the desired features (empty values are admitted)
    if  (f_feat_struct_agreement(all_forms->forms[f].feat,desired_features,1))
      //    if (f_feat_struct_agreement(all_forms->forms[f].feat,desired_features,1))
      if (U_add_form(&(all_forms->forms[f]),forms,1))
	return 1;

  return 0; 
}

////////////////////////////////////////////
// Input:
//    params: "global" parameters 
//    all_forms: a set of inflected forms
//    desired_features: feature structure of the desired forms, e.g. {Gen=fem, Case=Inst, LetterCase=first_upper, Init=dot}, or {} (if separator), the features of type 'graphical' may be neglected if they do not appear in all_forms
// Output:
//    forms - set of the inflected forms obtained from all_forms by the application of the 'graphical' (post-inflection) features
// E.g. if all_forms is {<rekami,<Gen=fem;Nb=p;Case=Inst>>,<rekomam,<Gen=fem;Nb=p;Case=Inst>>} and desired features contains <LetterCase=first_upper;Init=dot}, then forms becomes {<R.,<Gen=fem;Nb=p;Case=Inst;LetterCase=first_upper;Init=dot>>}
// Returns 0 on success, 1 otherwise.   
int U_apply_graphical_feat(MultiFlexParameters* params,U_forms_T* all_forms,f_feat_struct_T* desired_features,U_forms_T* forms) {
  if (!all_forms || !forms || !desired_features)
    return 1;

  int cv; //Index of the current category-value pair in desired_features
  int f; //Index of the current form
  for (f=0; f<all_forms->no_forms; f++) {
    for (cv=0; cv<desired_features->no_catvals; cv++) {
      //Apply only the graphical features
      if (desired_features->catvals[cv].cat->cat_type == graphical) 
	//Apply the current feature to the current form
	if (U_apply_one_graphical_feat(params,&all_forms->forms[f],&(desired_features->catvals[cv])))
	  return 1;
    }
    if (U_add_form(&(all_forms->forms[f]),forms,1))
      return 1;
  }
  return 0;
}

////////////////////////////////////////////
// Input:
//   params = "global" parameters 
//   cv = category-value pair to be applied to form
// Input/Output:
//   form = inflected form to be modified
// Apply a graphical (post-inflection) feature (e.g. LetterCase=all_upper, Init=dot) to form 
// Return 1 on error, 0 otherwise
int U_apply_one_graphical_feat(MultiFlexParameters* params,U_form_id_T* form, f_catval_T* cv) {
  if (!u_strcmp(cv->cat->name, "LetterCase")) {
    if (! letter_case_apply(params,form->form,letter_case_T(cv->val)))
      return 1;
  }
  else
    if (!u_strcmp(cv->cat->name, "Init")) {
      if (! init_apply(form->form,init_T(cv->val)))
	return 1;
    }
  return 0;
}

////////////////////////////////////////////
// Input:
//    forms: a list of inflected forms (allocated)
//    new_form: a form to be added
//    unique: 0 if the new_form is to be added in any case,
//            1 if new_form is to be added only if it does not yet appear in 'forms'
//              if new_form does appear in 'forms' then its extras are added to 
//              its previous appearence in 'forms'
// Output:
//    forms - set of the inflected forms possibly enlarge
// Adds a 'new_form' to 'forms' with or without repetitions.
// The form added is duplicated
// Returns 0 on success, 1 otherwise.   
int U_add_form(U_form_id_T* new_form,U_forms_T* forms, int unique) {
    if (!forms)
      return 1;
    if (!new_form)
      return 0;
    
    if (unique) {
      int f = 0; //Index of the current form in 'forms'
      int found;  //Is a form identical to 'new_form' found in 'forms' ?
      found = 0;
      while (f<forms->no_forms && !found) {
	if (!U_form_cmp(&(forms->forms[f]),new_form))
	  found = 1;
	f++; //Go to the next form
      }
      if (found)  //If 'new_form' already exists in 'forms' do nothing except adding the extras
	return fe_merge_extras(forms->forms[f-1].extras,new_form->extras);
    }

    //Add 'new_form' to 'forms'
    forms->forms = (U_form_id_T*) realloc(forms->forms, ((forms->no_forms)+1) * sizeof(U_form_id_T));
    if (!forms->forms)
      fatal_error("Memory allocation problem in function 'U_add_form'!\n");
    if (U_duplicate_form_id(&(forms->forms[forms->no_forms]), new_form)) 
      return 1;
    forms->no_forms++;
    
    return 0;
}
 
////////////////////////////////////////////
// Duplicate form 'f2' into 'f1'
// 'f1' is allocated but empty
// The form, lemma and feature structures for 'f1' are duplicated in the function
// Returns 0 on success, 1 on error
int U_duplicate_form_id(U_form_id_T* f1,U_form_id_T* f2) {
  if (!f1 || !f2)
    return 1;
  
  //Duplicate the form string
  f1->form = (unichar*)malloc((u_strlen(f2->form)+1) * sizeof(unichar));
  if (!f1->form)
    fatal_error("Memory allocation problem in function 'U_duplicate_form_id'!\n");
  u_strcpy(f1->form,f2->form);

  //Duplicate the lemma, if any
  if (!f2->lemma) 
    f1->lemma = NULL;
  else {
    f1->lemma = (U_lemma_T*)malloc(sizeof(U_lemma_T));
    if (!f1->lemma)
      fatal_error("Memory allocation problem in function 'U_duplicate_form_id'!\n");
    if (U_duplicate_lemma(f1->lemma, f2->lemma)) {
      free(f1->form);
      free(f1->lemma);
      return 1;
    }
  }

  //Duplicate the feature structure if any
  if (!f2->feat)
    f1->feat = NULL;
  else {
    f1->feat = (f_feat_struct_T*)malloc(sizeof(f_feat_struct_T));
    if (!f1->feat)
      fatal_error("Memory allocation problem in function 'U_duplicate_form_id'!\n");
    if (f_copy_feat_struct(f1->feat, f2->feat)) {
      free(f1->form);
      free(f1->lemma);
      free(f1->feat);
     return 1;
    }
  }

  //Duplicate the extra infos if any
  if (!f2->extras)
    f1->extras = NULL;
  else {
    fe_create_extras(&(f1->extras));
    if (fe_duplicate_extras(f1->extras,f2->extras)) {
      free(f1->form);
      free(f1->lemma);
      free(f1->feat);
      free(f1->extras);
      return 1;
    }
  }
  return 0;
}

////////////////////////////////////////////
// Duplicate lemma 'l2' into 'l1'
// 'l1' is allocated but empty
// The lemma units and paradigm for 'l1' are duplicated in the function
// Returns 0 on success, 1 on error
int U_duplicate_lemma(U_lemma_T* l1,U_lemma_T* l2) {
  if (!l1 || !l2)
    return 1;

  //Copy the lemma type
  l1->type = l2->type;

  //Duplicate the lemma units
  if (l2->type == simple) { //The lemma is a simple word
    l1->simple_lemma = (unichar*) malloc((u_strlen(l2->simple_lemma)+1) * sizeof(unichar));
    if (!l1->simple_lemma)
      fatal_error("Memory allocation problem in function 'U_duplicate_lemma'!\n");
    u_strcpy(l1->simple_lemma,l2->simple_lemma);
  }
  else  //The lemma is a MWU
    if (U_duplicate_forms(&(l1->compound_lemma),&(l2->compound_lemma)))
      return 1;

  //Copy the class pointer
  l1->cl = l2->cl;

  //Duplicate the paradigm
  l1->paradigm = (char*) malloc((strlen(l2->paradigm)+1) * sizeof(char));
  if (!l1->paradigm)
    fatal_error("Memory allocation problem in function 'U_duplicate_lemma'!\n");
  strcpy(l1->paradigm,l2->paradigm);

  return 0;
}

////////////////////////////////////////////
// Copy the set of forms in 'old_forms' to 'new_forms
// The forms are not duplicated, 'old_forms' becomes empty
// 'new_forms' and 'old_forms' should be allocated before function call
// Returns 0 on success, 1 on error
int U_copy_forms(U_forms_T* new_forms,U_forms_T* old_forms) {
  //If forms not allocated return error
  if (!old_forms || !new_forms)
    return 1;
  
  new_forms->no_forms = old_forms->no_forms;
  new_forms->forms = old_forms->forms; 
  old_forms->no_forms = 0;
  old_forms->forms = NULL;
  return 0;
}

////////////////////////////////////////////
// Copy the set of forms in 'old_forms' to 'new_form'
// The forms are duplicated, 'old_forms' is not deleted before function call
// 'new_forms' and 'old_forms' should be allocated 
// Returns 0 on success, 1 on error
int U_duplicate_forms(U_forms_T* new_forms,U_forms_T* old_forms) {
  //If forms not allocated return error
  if (!old_forms || !new_forms)
    return 1;

  new_forms->no_forms = old_forms->no_forms;
  new_forms->forms = (U_form_id_T*) malloc(old_forms->no_forms * sizeof(U_form_id_T));
  if (!new_forms->forms)
    fatal_error("Memory allocation problem in function 'U_duplicate_forms'!\n");
  int f; //Index of the current form
  int err; //Error code
  for (f=0; f<old_forms->no_forms; f++) {
    err = U_duplicate_form_id(&(new_forms->forms[f]),&(old_forms->forms[f]));
    if (err) {
      free(new_forms->forms);
      new_forms->forms = NULL;
      new_forms->no_forms = 0;
      return 1;
    }
  }
  return 0;
}

////////////////////////////////////////////
// Compare two forms
// Return 0 if they are identical, 1 if they are different
int U_form_cmp(U_form_id_T* f1, U_form_id_T* f2) {
  if (!f1 && !f2)
    return 0;
  if (!f1 || !f2)
    return 1;

  //If the strings do not agree, the forms are differents
  if (u_strcmp(f1->form,f2->form))
    return 1;

  //If the lemmas do not agree, the forms are different
  if (U_lemma_cmp(f1->lemma,f2->lemma))
      return 1;

  //If the feature structures do not agree,the forms are different
  if (! f_feat_struct_agreement(f1->feat, f2->feat,0))
    return 1;

  //The forms are identical
  return 0;

}

////////////////////////////////////////////
// Compare two lemmas
// Return 0 if they are identical, 1 if they are diffrent
int U_lemma_cmp(U_lemma_T* l1, U_lemma_T* l2) {
   if (!l1 && !l2)
    return 0;
  if (!l1 || !l2)
    return 1;

  //If different lemma types, lemmas are different
  if (l1->type != l2->type)
    return 1;

  if (l1->type == simple) {
    //If different simple lemma strings, the lemmas are different
    if (u_strcmp(l1->simple_lemma,l1->simple_lemma))
      return 1;
  }
  else 
    //If different lemma components, the lemmas are different
    if (U_forms_cmp_ordered(&(l1->compound_lemma),&(l2->compound_lemma)))
	return 1;

  //If different classes, the lemmas are different
  if (l1->cl != l2->cl)
    return 1;

  //If different paradigm, the lemmas are different
  if (strcmp(l1->paradigm, l2->paradigm))
      return 1;

  //The lemmas are identical
  return 0;
}

////////////////////////////////////////////
// Compare two lists of forms
// Return 0 if they are identical (and in the same order), 1 otherwise
int U_forms_cmp_ordered(U_forms_T* fs1, U_forms_T* fs2) {
    if (!fs1 && !fs2)
    return 0;
  if (!fs1 || !fs2)
    return 1;

  //If different number of forms, the lists are different
  if (fs1->no_forms != fs2->no_forms)
    return 1;

  //If each pair of corresponding forms is not identical, the lists are different
  int f; //Index of the current form in fs1 and in fs2
  for (f=0; f<fs1->no_forms; f++)
    if (U_form_cmp(&(fs1->forms[f]), &(fs2->forms[f])))
      return 1;

  //Lists are identical
  return 0;
}

////////////////////////////////////////////
// Initialize the list 'forms' with null values
// We suppose that 'forms' has its space allocated
void U_init_forms(U_forms_T* forms) {
  if (forms) {
    forms->no_forms = 0;
    forms->forms = NULL;
  }
}

////////////////////////////////////////////
// Initialize a lemma as a simple or a compound lemma
//
void U_init_lemma(U_lemma_T* lemma,U_lemma_type_T type) {
  if (lemma) {
    lemma->type = type;
    if (type == simple)
      lemma->simple_lemma = NULL;
    else 
      U_init_forms(&(lemma->compound_lemma));
    lemma->cl = NULL;
    lemma->paradigm = NULL;
  }
}

////////////////////////////////////////////
// Initialize a form
void U_init_form_id(U_form_id_T* form) {
  if (form) {
    form->form = NULL;
    form->lemma = NULL;
    form->feat = NULL;
    form->extras = NULL;
  }
}

////////////////////////////////////////////
// Add an empty form with empty features 'feat' and extas information 'extras' 
// to the initially empty set of forms 'forms'
void U_add_empty_form(U_forms_T* forms, form_extras_T* extras) {
  //Allocate space for a couple (epsilon,empty_set)
   if (forms || !forms->forms) {
    forms->forms = (U_form_id_T*)malloc(sizeof(U_form_id_T));
    if (!forms->forms) {
      fatal_error("Not enough memory in function U_add_empty_form\n");
    }
    U_form_id_T* f;
    f = &(forms->forms[0]);
    f->form = (unichar*) malloc(sizeof(unichar));
    if (f->form==NULL)
      fatal_error("Not enough memory in function U_add_empty_form\n");
    f->form[0] = (unichar) '\0';
    f->lemma = NULL;
    f_create_feat_struct(&(f->feat));
    f_init_feat_struct(f->feat);
    fe_create_extras(&(f->extras));
    if (extras)
      fe_duplicate_extras(f->extras,extras);
    forms->no_forms = 1;
   }
 }

////////////////////////////////////////////
// Input:
//    forms: a set of inflected forms (allocated)
//    new_form: a form with no annotation to be added
//    extras: extra information to be attached to the form
// Output:
//    forms - set of the inflected forms possibly enlarged with the new form
// Adds the non annotated form 'new_form' to 'forms' in any case (with repetitions)
// The added value is duplicated
// Returns 0 on success, 1 otherwise.   
int U_add_invariable_form(unichar* new_form,U_forms_T* forms, form_extras_T* extras) {
  U_form_id_T f;  //Structure for the new form
  f.form = new_form;
  f.lemma = NULL;
  f.feat = NULL;
  f.extras = extras;
  return U_add_form(&f,forms,0);
}

////////////////////////////////////////////
// Concatenantes each form in 'front_forms' in front of each form in 'back_forms'. 
// Adds the resulting forms into 'forms'.
// Each new concatenated form takes its morphological features and its extras from the back form.
// The lemmas are not copied
// E.g. while generating the instrumental of "rece pelne roboty", if we have :
// front_forms = {("rekami",{Case=Inst, Nb=pl, Gen=fem}), ("rekoma",{Case=Inst, Nb=pl, Gen=fem})}
// back_forms = {("pelnymi roboty",{Case=Inst, Nb=pl, Gen=fem})}
// forms = {("rak pelnych roboty",{Case=Acc, Nb=pl, Gen=fem})}
// then we obtain {("rak pelnych roboty",{Case=Acc, Nb=pl, Gen=fem}),
//                 ("rekami pelnymi roboty",{Case=Inst, Nb=pl, Gen=fem}),
//                 ("rekoma pelnymi roboty",{Case=Inst, Nb=pl, Gen=fem})}
// Initially, 'forms' has its space allocated, it may be empty or non empty.
// If it is non empty, the existing forms must not be lost
// Returns 1 in case of errors, 0 otherwise
int U_concat_forms(U_forms_T* front_forms, U_forms_T* back_forms, U_forms_T* forms) {
  int ff; //Index of a form in front_forms 
  int bf; //Index of a form in back_form
  int f;  //Index of a concatenated form

  if (!front_forms || !back_forms || !forms)
    return 1;

  if (back_forms->no_forms && front_forms->no_forms) {  //Check if there is anything to concatenante
    forms->forms = (U_form_id_T*) realloc(forms->forms,(forms->no_forms+back_forms->no_forms*front_forms->no_forms) * sizeof(U_form_id_T));
    if (!forms->forms)
      fatal_error("Not enough memory in function U_concat_forms\n");
    f = forms->no_forms;
    for (ff=0; ff<front_forms->no_forms; ff++)
      for (bf=0; bf<back_forms->no_forms;bf++) {
	forms->forms[f].form = 
	  (unichar*) malloc((u_strlen(back_forms->forms[bf].form)+u_strlen(front_forms->forms[ff].form)+1) * sizeof(unichar));
	if (!forms->forms[f].form) {
	  fatal_error("Not enough memory in function U_concat_forms\n");
	}
	//Concatenate the forms
	u_strcpy(forms->forms[f].form, front_forms->forms[ff].form);
	u_strcat(forms->forms[f].form, back_forms->forms[bf].form);
	//Copy the features
	f_create_feat_struct(&(forms->forms[f].feat));
	if (f_copy_feat_struct(forms->forms[f].feat,back_forms->forms[bf].feat)) {
	  U_delete_forms(forms);
	  return 1;
	}
	//Copy the extras
	fe_create_extras(&(forms->forms[f].extras));
	if (fe_duplicate_extras(forms->forms[f].extras,back_forms->forms[bf].extras)) {
	  U_delete_forms(forms);
	  return 1;
	}
	//The lemma (if any) is not copied
	forms->forms[f].lemma = NULL;
	f++;
      }
    forms->no_forms = f; 
  }
  return 0;
}

////////////////////////////////////////////
// Add forms appearing in 'new_forms' to 'forms' so that
// no form appears twice in the result. 
// (In the previous version of this function the forms allocated in
// 'new_forms' were copied by pointer assignment. They were not to be 
// liberated when 'new_forms' were liberated.)
// The forms added are duplicated.
// Returns 1 in case of errors, 0 otherwise
int U_merge_forms(U_forms_T* forms, U_forms_T* new_forms) {
  if (!forms)
    return 1;
  if (!new_forms)
    return 0;
    
  int nf; //index of a sigle form in 'new_forms (nmf)
  int err; //Error code
  for (nf=0; nf<new_forms->no_forms; nf++) {
    err = U_add_form(&(new_forms->forms[nf]), forms, 1);
    if (err)
      return 1;
  }
  return 0;
}
 
////////////////////////////////////////////
// Transforms each form appearing in 'forms'
// according to the letter case spelling required by 'lc'
// (all_lower, all_upper, first_upper, as_lemma)
// E.g. if forms = {"cousine", "cousine"} and lc = first_upper
// forms becomes {"Cousine", "Cousines"}
// 'forms' is an input/output parameter
// Returns 1 in case of errors, 0 otherwise
//int U_adapt_letter_case(U_forms_T* forms, letter_case_T lc) {
//  if (!forms)
//    return 1;
//  int f; //Index of the current form
//
//  for (f=0; f<forms->no_forms; f++)
//    if (!letter_case_apply(forms->forms[f].form,lc))
//	return 1;
//
//  return 0;
//}

////////////////////////////////////////////
// Enlarge the extra information attached to each form in 'forms'
// by the 'extra'
// Return 1 in case of errors, 0 otherwise
int U_concat_extras(U_forms_T* forms, extras_elem_T* extra) {
  if (!forms)
    return 1;
  if (!extra)
    return 0;
    
  int f; //index of a sigle form in 'forms'
  for (f=0; f<forms->no_forms; f++)
    if (fe_enlarge_extras(forms->forms[f].extras, extra))
      return 1;
  return 0;
 }

////////////////////////////////////////////
// Prints a form, its lemma, inflection features and extras if any.
void U_print_f(U_form_id_T* f) {
  if (!f)
    return;
  //Print the form string if any
  if (f->form)
    u_printf("%S : ",f->form);
  //Print the lemma if any
  if (f->lemma)
    U_print_lemma(f->lemma);
  //Print the features if any
  if (f->feat)
    f_print_feat_struct(f->feat);
  //Print the extras if any
  if (f->extras)
    fe_print_extras(f->extras);
}

////////////////////////////////////////////
// Prints a set of forms, their lemmas and inflection features if any.
void U_print_forms(U_forms_T* F) {
  if (!F)
    return;
  int f;
  if (F->forms)
    for (f=0; f<F->no_forms; f++)
      U_print_f(&(F->forms[f]));
}

////////////////////////////////////////////
// Prints a lemma and its info.
void U_print_lemma(U_lemma_T* l) {
  if (!l)
    return;

  //If a simple lemma, print one string
  if (l->type == simple) {
    if (l->simple_lemma) {
      u_printf("%S",l->simple_lemma);  
  //if a compound lemma, print all components
    } else {
      int f;  //Index of the current constituent form in the lemma
      for (f=0; f<l->compound_lemma.no_forms; f++) {
    	  U_print_f(&(l->compound_lemma.forms[f]));      //Print the for of a component
      }
    }
}
  
  //Print the class and the paradigm
  u_printf(":%S",l->cl->name);
  u_printf("(%s)\n",l->paradigm);
}

////////////////////////////////////////////
// Deletes a form, its lemma, inflection features and extras if any.
// The form structure itself is not liberated
void U_delete_f(U_form_id_T* f) {
   if (!f)
     return;
   //Dlete the form string if any
   if (f->form) {
    free(f->form);
    f->form=NULL;
   }
  //Delete the lemma if any
   if (f->lemma) {
    U_delete_lemma(f->lemma,1);
    f->lemma = NULL;
   }
  //Delete the features if any
   if (f->feat) {
     f_delete_feat_struct(&(f->feat));
     f->feat = NULL;
   }
   //Deletes the extras if any
   if (f->extras) {
     fe_delete_extras(f->extras);
     f->extras = NULL;
   }
}

////////////////////////////////////////////
// Deletes a set of forms, their lemmas and inflection features if any.
void U_delete_forms(U_forms_T* F) {
  if (!F)
    return;
  int f;
  if (F->forms) {
    for (f=0; f<F->no_forms; f++)
      U_delete_f(&(F->forms[f]));
    free(F->forms);
    F->forms = NULL;
  }
  F->no_forms = 0;
}

////////////////////////////////////////////
// Deletes a lemma stucture.
// 'l' = lemma to be liberated
// 'liberate_main_structure' = Boolean saying if the main structure should be liberated (1) or not (0)
void U_delete_lemma(U_lemma_T* l, int liberate_main_structure){
  if (!l)
    return;
  
  //Delete the lemma string
  if (l->type == simple) {
    if (l->simple_lemma) {
      free(l->simple_lemma);
      l->simple_lemma = NULL;
    }
  }
  else  //compound lemma
    U_delete_forms(&(l->compound_lemma));
  
  //Don't delete the class because it is a global variable usefull all through the treatment 

  //Delete the paradigm
  if (l->paradigm) {
    free(l->paradigm);
    l->paradigm = NULL;
  }
  
  //Delete the lemma
  if (liberate_main_structure) {
    free(l);
    l = NULL;
  }
}
 
////////////////////////////////////////////
// Checks if 'form' is a simple or a compound form
// Returns 1 if 'forms' is a compound form, 0 if it is a simple form
// 'params' = "global" parameters 
// -1 in case of errors.
int U_is_compound(MultiFlexParameters* params,unichar* form) {
   if (!form)
     return -1;

   int l; //Length of the scanned sequence
   unichar* tmp;
   tmp = (unichar*)malloc((u_strlen(form)+1) * sizeof(unichar));
   if (!tmp)
     fatal_error("Memory allocation problem in function 'U_is_compound'!\n");     
   l = get_unit(params,tmp, form, u_strlen(form), 0);
   free(tmp);
   if (l < u_strlen(form))
     return 1;
   else
     return 0;
 }

////////////////////////////////////////////
// Produces a lemma string from a lemma structure
// 'lemma' = lemma structure
// 'max' = maximum length of the resulting lemma string (including the final '\0')
// 'lemma_str' = return parameter, it must be allocated before function call but empty
// Returns 1 if 'forms' is a compound form, 0 if it is a simple form
// -1 in case of errors, 0 otherwise.
int U_get_lemma_str(unichar* lemma_str, int max, U_lemma_T* lemma) {
   if (!lemma_str || !lemma)
     return 1;

   //Simple lemma
   if (lemma->type == simple) 
     if (u_strlen(lemma->simple_lemma)< max)
       u_strcpy(lemma_str, lemma->simple_lemma);
     else
       return -1;

   else {//Compound lemma
     int c; //Index of the current constituent of the compound lemma
     unichar* constit_str; //String containing the current constituent
     lemma_str[0] = (unichar)'\0';
     for (c=0; c<lemma->compound_lemma.no_forms; c++) {
       constit_str = lemma->compound_lemma.forms[c].form;
       if (u_strlen(lemma_str) + u_strlen(constit_str) >= max)
	 return -1;
       u_strcat(lemma_str, constit_str);
     }
   }
   return 0;
 }

////////////////////////////////////////////
// Returns the length of a lemma string, or -1 in case of errors.
// 'lemma' = lemma structure
int U_get_lemma_str_len(U_lemma_T* lemma) {
  if (!lemma)
    return 1;
  
  //Simple lemma
  if (lemma->type == simple) 
    return u_strlen(lemma->simple_lemma);
  
  else {//Compound lemma
    int c; //Current constituent
    int tot_l; //Total length of the compound lemma
    tot_l = 0;
    for (c=0; c<lemma->compound_lemma.no_forms; c++)
      tot_l = tot_l + u_strlen(lemma->compound_lemma.forms[c].form);
    return tot_l;
  }
}

////////////////////////////////////////////
// Finds the component number 'num' in the compound lemma 'MU_lemma' 
// 'num' >=1 (not >=0)
// Returns the pointer to this component or NULL if not found
U_form_id_T* U_lemma_get_component(U_lemma_T* MU_lemma, int num) {
  //The lemma must not be a simple word
  if (MU_lemma->type == simple)
      return NULL;

  if (num < 1 || MU_lemma->compound_lemma.no_forms < num)
    return NULL;

  return &(MU_lemma->compound_lemma.forms[num-1]);
}

////////////////////////////////////////////
// Enlarges the morphology of all forms appearing in 'forms'
// with the category-value pairs appearing in 'feat'
// 'params': "global" parameters 
// 'forms': input/output parameter containing the forms to be enlarged
// 'feat': features to be added to each form in 'forms'
// If a feature in 'feat' is not compatible with the morphology
// of a forms (it has the same category but not the same value)
// this form is eliminated.
// E.g. if forms={(ulica Bitwy,<Gen=f,Nb=sg,Case=nom>),...} and 'feat'=<Usage=marked>
//      then the forms becomes {(ulica Bitwy,<Gen=f,Nb=sg,Case=nom,Usage=marked>,...)}
// E.g. if forms={(ulica Bitwy,<Gen=f,Nb=sg,Case=nom,Usage=marked>),...} and 'feat'=<Usage=neut>
//      then the forms gets eliminated
// Returns 1 on error, 0 otherwise
int U_enlarge_forms_morpho(MultiFlexParameters* params,U_forms_T* forms, f_feat_struct_T* feat) {
  int f;  //Index of the current suffix
  int c;  //Index of the current category-value pair in 'feat' 
  int to_add; //Boolean saying if the current form with enlarged morphology should be added to the result
  U_forms_T* new_forms; //Temporary set of forms to be returned
  int err; //Error code

  if (!forms || !feat)
    return 1;
  if (feat->no_catvals == 0)  //No enlargement needed
    return 0;

  new_forms = (U_forms_T*)malloc(sizeof(U_forms_T));
  if (!new_forms)
    fatal_error("Memory allocation problem in function 'U_enlarge_forms_morpho'!\n");
  U_init_forms(new_forms);

  for (f=0; f<forms->no_forms; f++) {
    to_add = 1;
    for (c=0; c<feat->no_catvals; c++) { 
      //If the ouput morphology in a graph is ambiguous, the final MWU's morphology is undefined
      err = f_enlarge_feat_struct(params,forms->forms[f].feat, feat->catvals[c].cat,feat->catvals[c].val, 1);  //The final feature structure should be sorted
      //If the current feature to be added is not compatible with the features of the current form
      //this form is invalid, otherwise the 
      if (err)
	to_add = 0;
    }
    if (to_add) {
      err = U_add_form(&(forms->forms[f]),new_forms,0);
      if (err)
	return 1;
    }
  }
  U_delete_forms(forms);
  if (U_copy_forms(forms,new_forms))
    return 1;
  free(new_forms);
  return 0;
}

