/*
  * Unitex 
  *
  * Copyright (C) 2001-2008 Universit� de Marne-la-Vall�e <unitex@univ-mlv.fr>
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
  *
  */

/* Created by Agata Savary (agata.savary@univ-tours.fr)
 * Last modification on September 3 2008
 */
//---------------------------------------------------------------------------

/********************************************************************************/
//// Register of the Inflected Forms of the Components of the Current Entry    ///
//// Used for optimisation puposes                                             ///
/********************************************************************************/

#include "MF_CurrentForms.h"
#include "MF_UnitMorpho.h"
#include "Error.h"
#include "Unicode.h"

//////////////////////////////
//Lemmas and all inflected forms of the components of the current entry


////////////////////////////////////////////
//Initialise the current lemma and its inflected forms
U_form_list_set_T* init_current_forms() {
  U_form_list_set_T* CURRENT_FORMS = (U_form_list_set_T*) malloc(sizeof(U_form_list_set_T));
  if (!CURRENT_FORMS)
    fatal_error("Not enough memory in function 'init_current_forms'!\n");
  CURRENT_FORMS->no_form_lists = 0;
  CURRENT_FORMS->form_lists = NULL;
  return CURRENT_FORMS;
}

////////////////////////////////////////////
//Free the current lemma and its inflected forms
void free_current_forms(U_form_list_set_T* CURRENT_FORMS) {
  if (!CURRENT_FORMS)
    return;
  int fl; //Index of the current form list
  for (fl=0; fl<CURRENT_FORMS->no_form_lists; fl++) {
    U_delete_lemma(CURRENT_FORMS->form_lists[fl].lemma,1);
    U_delete_forms(CURRENT_FORMS->form_lists[fl].forms);
  }
  if (CURRENT_FORMS->form_lists)
    free(CURRENT_FORMS->form_lists);
  free(CURRENT_FORMS);
}

////////////////////////////////////////////
//If the inflected forms of the current lemma appear in the current forms
//put a copy of their list in "forms" and return 0. Otherwise return 1.
//"forms" has its space allocated but is empty
int get_current_form_list(U_form_list_set_T* CURRENT_FORMS,U_lemma_T* lemma, U_forms_T* forms) {
  if (! CURRENT_FORMS || !forms)
    return 1;

  int fl; //Index of the current form list
  int err1, err2; //Error codes if any
  unichar lemma_str[MAX_LANG_MORPHO_LINE]; //Lemma string
  unichar current_lemma_str[MAX_LANG_MORPHO_LINE]; //Lemma string in the current forms
  for (fl=0; fl<CURRENT_FORMS->no_form_lists; fl++) {
    if (CURRENT_FORMS->form_lists[fl].forms->no_forms) {
      //Get the lemma of the list
      err1 = U_get_lemma_str(lemma_str,MAX_LANG_MORPHO_LINE-1,lemma);
      err2 = U_get_lemma_str(current_lemma_str,MAX_LANG_MORPHO_LINE-1,CURRENT_FORMS->form_lists[fl].lemma);
      if (err1 || err2)
	return 1;
      if (!u_strcmp(lemma_str,current_lemma_str)) {
	err1 = U_duplicate_forms(forms, CURRENT_FORMS->form_lists[fl].forms);
	if (err1) {
	  U_delete_forms(forms);
	  return 1;
	} 
	return 0;
      }
    }
  }      
  return 1;
}

////////////////////////////////////////////
//Add the list of forms to the set of current forms
//Return 1 on error, 0 otherwise
int add_current_forms(U_form_list_set_T* CURRENT_FORMS,U_lemma_T* lemma, U_forms_T* forms) {
  int err; //Error code if any
  int nb_fl; //Current number of the form lists
  if (!forms || !CURRENT_FORMS)
    return 1;
  nb_fl = CURRENT_FORMS->no_form_lists;
  CURRENT_FORMS->form_lists = (U_form_list_T*) realloc(CURRENT_FORMS->form_lists,(nb_fl+1) * sizeof(U_form_list_T));
  if (!CURRENT_FORMS->form_lists)
    fatal_error("Not enough memory in function 'add_current_forms'!\n");
  CURRENT_FORMS->form_lists[nb_fl].lemma = (U_lemma_T*) malloc(sizeof(U_lemma_T));
  if (!CURRENT_FORMS->form_lists[nb_fl].lemma)
    fatal_error("Not enough memory in function 'add_current_forms'!\n");
  err = U_duplicate_lemma(CURRENT_FORMS->form_lists[nb_fl].lemma,lemma);
  if (err) {
    free(CURRENT_FORMS->form_lists[nb_fl].lemma);
    return 1;
  }

  CURRENT_FORMS->form_lists[nb_fl].forms = (U_forms_T*) malloc(sizeof(U_forms_T));
  if (!CURRENT_FORMS->form_lists[nb_fl].forms)
    fatal_error("Not enough memory in function 'add_current_forms'!\n");  
  U_init_forms(CURRENT_FORMS->form_lists[nb_fl].forms);
  err = U_duplicate_forms(CURRENT_FORMS->form_lists[nb_fl].forms,forms);
  if (err) {
    free(CURRENT_FORMS->form_lists[nb_fl].lemma);
    free(CURRENT_FORMS->form_lists[nb_fl].forms);
    return 1;
  }

  CURRENT_FORMS->no_form_lists = nb_fl + 1;
  return 0;
}

