/*
  * Multiflex - automatic inflection of multi-word units 
  *
  * Copyright (C) 2005 Agata Savary <agata.savary@univ-tours.fr>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  * 
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
  *
  */

/****************************************************************************************************/
/* TREATMENT OF EQUIVALENCES BETWEEN FEATURES FOR SIMPLE WORDS (E.G. "bier" or "B" or "acc")        */
/* AND THOSE DEEFINED IN MUTIFLEX (E.G. acc)                                                        */
/* IF BOTH TYPES OF FEATURES ARE IDENTICAL, THE EQUIVALENCE IS STRAIGHTFORWARD                      */
/* E.G. acc <=> Case=3                                                                              */
/****************************************************************************************************/

#ifndef MorphoEquivBaseH
#define MorphoEquivBaseH

#include "Unicode.h"
#include "MF_LangMorphoBase.h"
#include "MF_UnitMorphoBase.h"
#include "MF_Global2.h"
#include "MF_FeatStructBase.h"

//maximum length of a line in the file containing equivalences between values for simple words and those in Multiflex
#define MAX_EQUIV_LINE 100
//maximum number of equivalences between values for simple words and those in Multiflex
#define MAX_MORPHO_EQUIV 500
//maximum number of equivalences between classes for simple words and those in Multiflex
#define MAX_CLASS_EQUIV 500

////////////////////////////////////////////
//A list of equivalences between morphological features in the language file for Multiflex
//and those defined for simple words.
typedef struct{
  unichar SU_feat[MAX_MORPHO_NAME];   //a morphological feature for simple words, e.g. 'poj' or 'sing' (previous name:dico_feat)
  f_catval_T MU_catval;               //category-value pair corresponding to SU_feat in Mutitflex, e.g. Nb=sing <Nb,0>
} l_morpho_eq_T;

typedef struct {
  int no_equiv;       //number of equivalences
  l_morpho_eq_T equiv[MAX_MORPHO_EQUIV];  //set of equivalences
} l_morpho_equiv_T;


////////////////////////////////////////////
//A list of equivalences between class names for simple words (e.g. "N") and a language class in Multiflex (e.g. noun).
//Later this table should be replaced by an external file scanned at each initialisation.
typedef struct {
  unichar SU_class[MAX_CLASS_NAME];  //morphological class for simple words (e.g. "N") (previous name: dico_class)
  l_class_T* MU_class;               //morphological class for compounds (e.g. "Noun")  (previous name: cl)
} l_class_eq_T;

typedef struct {
  int no_equiv;       //number of class equivalences
  l_class_eq_T equiv[MAX_CLASS_EQUIV];  //set of class equivalences
} l_class_equiv_T;


#endif
